; All segment registers contain 0x0000
[ORG 0x7C00]
[BITS 16]
section .text
	jmp 0x0000:bootloader_stage1

; include file goes here

%include "disk_int13.s"

bootloader_stage1:
; magic breakpoint
	xchg bx, bx

; init segment
	xor ax, ax
	mov ds, ax
	mov es, ax
; set stack
	mov sp, data2.stack_end

; save boot disk
	mov byte [data.boot_disk], dl

; test int13h LBA48 extention
	mov byte [data.error_msg + 0x11], '1'
	mov ah, 0x41
	mov bx, 0x55aa
	mov dl, byte [data.boot_disk]
	int 0x13
	jc error            ; carry set when error
	cmp bx, 0xaa55      ; bx must be 0xaa55, otherwise error
	jne error
; disk ok

; ============================================================================

; load bootloader_stage2
load_bootloader_stage2_start:

load_bootloader_stage2.loop.init:
; data.tmp1 will hold total sector loaded
; 4 because first 2048 bytes of bootloader stage2 is skipped
	mov dword [data.tmp1], 4

; data.tmp2 will hold sector LBA to read next
	mov dword [data.tmp2], 5

; data.tmp3 will hold next address to write loaded sector to
	mov dword [data.tmp3], bootloader_stage2_start

load_bootloader_stage2.loop.check:
; check if total is equa or larger than bootloader stage 2 size in sector
	mov eax, dword [data.tmp1]
	cmp eax, dword [data.bootloader_stage2_size_in_sector]
	jae load_bootloader_stage2.loop.end

load_bootloader_stage2.loop.body:
; read 1 sector of bootloader stage2 to data2.buffer
	mov eax, dword [data.tmp2]
	xor ebx, ebx
	mov cx, 1
	mov dl, byte [data.boot_disk]
	mov si, data2.buffer
	call disk_int13_read

; move data2.buffer to correct position
	cld
	mov esi, data2.buffer
	mov edi, dword [data.tmp3]
	mov ecx, 128
	rep movsd

load_bootloader_stage2.loop.continue:
; update counter
	add dword [data.tmp1], 1
	add dword [data.tmp2], 1
	mov dword [data.tmp3], edi

; continue loop
	jmp load_bootloader_stage2.loop.check
load_bootloader_stage2.loop.end:

load_bootloader_stage2_end:

; ============================================================================

; jump to bootloader_stage2_start
	jmp bootloader_stage2_start

; ============================================================================

; halt in case of error
error:
halt_start:
	cli
.loop:
	hlt
	jmp .loop
halt_end:

; ============================================================================

TIMES 510 - ($ - $$) - (data_end - data) db 0x00

; data goes here
data:
.magic: db 'data:   '
.error_msg: db "Bootloader error  , halt!", 0
.boot_disk: db 1
.tmp1: dq 0
.tmp2: dq 0
.tmp3: dq 0
.tmp4: dq 0

; size of bootloader stage 2 in sector
; 503th byte, index 502
.bootloader_stage2_size_in_sector: dd 0

; size of initrd in sector
; 507th byte, index 506
.initrd_size_in_sector: dd 0
data_end:

bootloader_stage1_end:

; bootloader signatual
dw 0xAA55

; ============================================================================

data2:
.buffer: TIMES 512 db 0
.stack_start: TIMES 512*3 db 0
.stack_end:
data2_end:

; ============================================================================

; bootloader stage 2 start here
bootloader_stage2_start:

; enable a20 to have access to ram above 1MB
enable_a20:
	mov byte [data.error_msg + 0x11], '2'

; Method 1: int 0x15
	mov     ax,0x2403                ;--- A20-Gate Support ---
	int     0x15
	jb      .a20_2                  ;INT 15h is not supported
	cmp     ah,0x00
	jnz     .a20_2                  ;INT 15h is not supported

	mov     ax,2402h                ;--- A20-Gate Status ---
	int     15h
	jb      .a20_2              ;couldn't get status
	cmp     ah,0
	jnz     .a20_2              ;couldn't get status

	cmp     al,1
	jz      a20_end           ;A20 is already activated

	mov     ax,2401h                ;--- A20-Gate Activate ---
	int     15h
	jb      .a20_2              ;couldn't activate the gate
	cmp     ah,0
	jnz     .a20_2              ;couldn't activate the gate

; Method 2: Keyboard Controller
.a20_2:
	cli
	call    .a20wait
	mov     al,0xAD
	out     0x64,al

	call    .a20wait
	mov     al,0xD0
	out     0x64,al

	call    .a20wait2
	in      al,0x60
	push    eax

	call    .a20wait
	mov     al,0xD1
	out     0x64,al

	call    .a20wait
	pop     eax
	or      al,2
	out     0x60,al

	call    .a20wait
	mov     al,0xAE
	out     0x64,al

	call    .a20wait
	sti
	jmp .check_a20
.a20wait:
	in      al,0x64
	test    al,2
	jnz     .a20wait
	ret
.a20wait2:
	in      al,0x64
	test    al,1
	jz      .a20wait2
	ret
.check_a20:
	mov ax, 0x0000
	mov es, ax
	mov di, 0x7e00
	mov [es:di], byte 0x00
	mov ax, 0xffff
	mov es, ax
	mov di, 0x7e10
	mov [es:di], byte 0xff
	mov ax, 0x0000
	mov es, ax
	mov di, 0x7e00
	cmp [es:di], byte 0x00
	je a20_end

; A20 not support
	jmp error
a20_end:

; ==============================Enter unreal mode=============================

; go to unreal mode to have access to 4GB RAM
enable_unreal_mode_start:
	cli
	lgdt [data3.gdt16info]
	mov  eax, cr0
	or al, 1
	mov  cr0, eax
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov eax, cr0
	and eax, 0xFFFFFFFE
	mov cr0, eax
	mov ax, 0
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
enable_unreal_mode_end:

; ===========================Get memory map===================================

; get memory map to know with RAM address can use
get_memory_map:
	mov byte [data.error_msg + 0x11], '3'
; get memory map with INT 0x15, EAX = 0xE820
; first call
	mov ax, data3.memory_map_start
	mov di, ax
	xor ebx, ebx
	mov edx, 0x534D4150
	mov eax, 0xE820
	mov ecx, 24
	int 0x15
	jc error
	cmp eax, 0x534D4150
	jne error
	add dword [data3.memory_map_entry_num], 1

; save entry size
	mov byte [data3.memory_map_entry_size], cl

	cmp cl, 20
	je .fix_entry1
	cmp cl, 24
	je .continue1
	jmp error

.fix_entry1:
; fix that entry
	xor eax, eax
	mov ax, di
	mov dword [eax + 20], 1

.continue1:
	cmp ebx, 0
	je get_memory_map_end

; all other call
.loop:
	mov cx, 24			; adjust di
	add di, cx			;
	mov eax, 0xE820
	mov ecx, 24
	int 0x15
	jc error
	cmp eax, 0x534D4150
	jne error
	add dword [data3.memory_map_entry_num], 1
	cmp dword [data3.memory_map_entry_num], 0xff
	jae error

	mov cl, byte [data3.memory_map_entry_size]
	cmp cl, 24
; if entry size == 24, dont do anything
	je .continue2

.fix_entry2:
	xor eax, eax
	mov ax, di
	mov dword [eax+ 20], 1

.continue2:
	cmp ebx, 0
	jne .loop
get_memory_map_end:


; ============================================================================

; calc initrd size
	mov ecx, dword [data.initrd_size_in_sector]
	shl ecx, 9

; save initrd size
	mov dword [data3.initrd_size], ecx

; check that initrd size%4096 == 0
	and ecx, 0xfff
	cmp ecx, 0
	jne error

; save tmp paging struct size
	mov dword [data3.tmp_paging_struct_size], (1+1+4+512*4)*4096

; ==============Find a hole to load initrd+tmp paging struct==================

find_initrd_start_address:

	mov byte [data.error_msg + 0x11], '4'

find_initrd_start_address.loop.init:
; eax hold next entry address
	mov eax, data3.memory_map_start

; ebx hold current index
	mov ebx, 0

; ecx hold initrd size + tmp paging struct
	mov ecx, dword [data3.initrd_size]
	add ecx, dword [data3.tmp_paging_struct_size]

; data.tmp4 hold isFound flag
	mov dword [data.tmp4], 0

find_initrd_start_address.loop.check:
; exit if current index >= memory_map_entry num
	cmp ebx, dword [data3.memory_map_entry_num]
	jae find_initrd_start_address.loop.end

; continue if current entry is not usable
	cmp dword [eax + 16], 1
	jne find_initrd_start_address.loop.continue

; continue if current entry should be ignored
	mov edx, dword [eax + 20]
	and edx, 1
	cmp edx, 1
	jne find_initrd_start_address.loop.continue

; continue if current entry start is above 4GB
	cmp dword [eax + 4], 0
	jne find_initrd_start_address.loop.continue

; continue if current entry start < 1MB
	cmp dword [eax], 1*1024*1024
	jb find_initrd_start_address.loop.continue

; continue if current entry size <= initrd + tmp paging struct
; initrd+tmp paging struct < 4GB
	cmp dword [eax + 12], 0
	jne find_initrd_start_address.loop.found

	cmp dword [eax + 8], ecx
	ja find_initrd_start_address.loop.found

	jmp find_initrd_start_address.loop.continue

find_initrd_start_address.loop.found:
; save initrd start address
	mov eax, dword [eax]
	mov dword [data3.initrd_start], eax
	mov dword [data.tmp4], 1
	jmp find_initrd_start_address.loop.end

find_initrd_start_address.loop.continue:
	add eax, 24
	add ebx, 1
	jmp find_initrd_start_address.loop.check

find_initrd_start_address.loop.end:
; check if a hole is found
	cmp dword [data.tmp4], 1
; jump to error if not found
	jne error

find_initrd_start_address_end:

; ==============================Load initrd===================================

; load initrd
load_initrd_start:

load_initrd.loop.init:
; data.tmp1 will hold total sector loaded
	mov dword [data.tmp1], 0

; data.tmp2 will hold sector LBA to read next
	mov eax, 1
	add eax, dword [data.bootloader_stage2_size_in_sector]
	mov dword [data.tmp2], eax

; data.tmp3 will hold next address to write loaded sector to
	mov eax, dword [data3.initrd_start]
	mov dword [data.tmp3], eax

load_initrd.loop.check:
; check if total is equa or larger than initrd size in sector
	mov eax, dword [data.tmp1]
	cmp eax, dword [data.initrd_size_in_sector]
	jae load_initrd.loop.end

load_initrd.loop.body:
; read 1 sector of initrd to data2.buffer
	mov eax, dword [data.tmp2]
	xor ebx, ebx
	mov cx, 1
	mov dl, byte [data.boot_disk]
	mov si, data2.buffer
	call disk_int13_read

; move data2.buffer to correct position
	cld
	mov esi, data2.buffer
	mov edi, dword [data.tmp3]
	mov ecx, 128
	a32 rep movsd

load_initrd.loop.continue:
; update counter
	add dword [data.tmp1], 1
	add dword [data.tmp2], 1
	mov dword [data.tmp3], edi

; continue loop
	jmp load_initrd.loop.check
load_initrd.loop.end:

load_initrd_end:

; =============================Disable interrupt==============================

	cli

; ==========================Build tmp paging struct===========================

; Identify mapping first 1GB

build_tmp_paging_struct:

.init:
; calc tmp paging struct start address
	mov eax, dword [data3.initrd_start]
	add eax, dword [data3.initrd_size]
	add eax, 4096 - 1
	shr eax, 12
	shl eax, 12
; save tmp paging struct start address
	mov dword [data3.tmp_paging_struct_start], eax

; zero out tmp paging struct
	xor eax, eax
	mov edi, dword [data3.tmp_paging_struct_start]
	mov ecx, (1+1+1+512)*4096
	shr ecx, 2
	a32 rep stosd

	mov eax, dword [data3.tmp_paging_struct_start]

.L4:
	mov ebx, eax
	add ebx, 4096
	or ebx, 3
	mov dword [eax], ebx

.L3:
	add eax, 4096
	mov ebx, eax
	add ebx, 4096
	or ebx, 3
	mov dword [eax], ebx

.L2:

.L2.init:
	mov eax, dword [data3.tmp_paging_struct_start]
	add eax, 4096*2
	mov ebx, eax
	add ebx, 4096
	or ebx, 3
	mov ecx, 512

.L2.loop:
	mov dword [eax], ebx
	add eax, 8
	add ebx, 4096
	a32 loop .L2.loop

.L1:

.L1.init:
	mov eax, dword [data3.tmp_paging_struct_start]
	add eax, 4096*3
	mov ebx, 3
	mov ecx, 512*512

.L1.loop:
	mov dword [eax], ebx
	add eax, 8
	add ebx, 4096
	a32 loop .L1.loop

build_tmp_paging_struct_end:

; ============================Probe CPU features==============================

probe_cpu_features:

; mandatory features
.cpuid:
	mov byte [data.error_msg + 0x11], '5'

	pushfd                               ;Save EFLAGS
	pushfd                               ;Store EFLAGS
	xor dword [esp],0x00200000           ;Invert the ID bit in stored EFLAGS
	popfd                                ;Load stored EFLAGS (with ID bit inverted)
	pushfd                               ;Store EFLAGS again (ID bit may or may not be inverted)
	pop eax                              ;eax = modified EFLAGS (ID bit may or may not be inverted)
	xor eax,[esp]                        ;eax = whichever bits were changed
	popfd                                ;Restore original EFLAGS
	and eax,0x00200000                   ;eax = zero if ID bit cant be changed, else non-zero

	cmp eax, 0
	je error

.extended_topology_enumeration_leaf:
	mov eax, 0
	cpuid
	cmp eax, 0xb
	jl error

	mov eax, 0xb
	mov ecx, 0
	cpuid
	cmp ebx, 0
	je error

.extended_cpuid:
	mov eax, 0x80000000
	cpuid
	cmp eax, 0x80000007
	jb error

.long_mode:
	mov byte [data.error_msg + 0x11], '7'
	mov eax, 0x80000001
	cpuid
	and edx, 1<<29
	je error

.nx_bit:
	mov eax, 0x80000001
	cpuid
	and edx, 1<<20
	je error

.global_page:
	mov eax, 1
	cpuid
	and edx, 1<<13
	je error

.page_1g:
	mov eax, 0x80000001
	cpuid
	and edx, 1<<26
	je error
	mov dword [data3.page_1g], edx

.msr:
	mov byte [data.error_msg + 0x11], '8'
	mov eax, 1
	cpuid
	and edx, 1<<5
	je error

.local_acpi:
	mov eax, 1
	cpuid
	and edx, 1<<9
	je error

.apic2:
	mov eax, 1
	cpuid
	and ecx, 1<<21
	je error

.rdtsc_instruction:
	mov eax, 1
	cpuid
	and edx, 1<<4
	je error

.always_run_lapic:
	mov eax, 6
	cpuid
	and eax, 1<<2
	je error

; optional features
.l5_paging:
	mov eax, 7
	xor ecx, ecx
	cpuid
	shr ecx, 16
	and ecx, 1
	mov dword [data3.l5_paging], ecx

probe_cpu_features_end:


; ==========================Enable protected mode=============================


enable_proteced_mode:
	mov eax, cr0
	or eax, 1
	mov cr0, eax

.load_gdt32:
	lgdt [data3.gdt32info]
	xor eax, eax
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	jmp 0x08:protected_mode_start

enable_proteced_mode_end:


; ===========================Protected mode code==============================


[BITS 32]
protected_mode_start:


; ============================Enable Long mode================================


enable_long_mode:
.CR4_PAE:
; set CR4.PAE bit (bit 5)
	mov eax, cr4
	or eax, 0b00100000
	mov cr4, eax

.EFER_LME:
; set EFER.LME bit (bit 2)
	mov ecx, 0xC0000080               ; Read from the EFER MSR.
	rdmsr

	or eax, 0x00000100                ; Set the LME bit.
	wrmsr

.CR3:
; point CR3 to page table level 4
	mov eax, [data3.tmp_paging_struct_start]
	mov cr3, eax

.enable_paging:
; enable paging and long mode
	mov eax, cr0
	or eax, 0x80000000
	mov cr0, eax

.load_gdt64:
; reload gdt to 64 bit
	lgdt [data3.gdt64info]
	xor eax, eax
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	jmp 0x08:long_mode_start

enable_long_mode_end:

; ==============================Long mode code================================

[BITS 64]
long_mode_start:

; ============================Get kernel loaded size==========================

get_kernel_loaded_size:
	mov byte [data.error_msg + 0x11], '9'

.kernel_file_start_address:
; get kernel file start address
	mov eax, dword [data3.initrd_start]
	add eax, 4096
	mov qword [data3.kernel_file_start], rax

.file_signalture:
; check file signalture
	mov rbx, 0x00010102464c457f
	cmp qword [rax], rbx
	jne error64

	mov rbx, 0x00000001003e0002
	cmp qword [rax + 0x10], rbx
	jne error64

.kernel_entry_point:
; save kernel entry point
	mov rbx, qword [rax + 0x18]
	mov qword [data3.kernel_entry], rbx

; check number of program headers
	cmp word [rax + 0x38], 0
	je error64

; get program headers table start address
	mov rbx, qword [rax + 0x20]
	add rax, rbx

; check that first program segment type is load
	cmp dword [rax], 1
	jne error64

.get_kernel_size_in_file:
; get kernel size in file
	mov rbx, qword [rax + 0x20]
	mov qword [data3.kernel_load_size], rbx

.get_kernel_size_in_memory:
; get kernel size in memory
	mov rbx, qword [rax + 0x28]

; check if loaded kernel size > 1GB
	cmp rax, 1*1024*1024*1024
	ja error64

	mov qword [data3.kernel_loaded_size], rbx

; save kernel segment start address
	mov rax, qword [rax + 0x08]
	add rax, qword [data3.kernel_file_start]
	mov qword [data3.kernel_load_start], rax

get_kernel_loaded_size_end:

; ===========================Calc avoid area size=============================

	xor rax, rax

; BootInfo
	add rax, 4096

; initrd size
	add rax, [data3.initrd_size]

; kernel
	add rax, qword [data3.kernel_loaded_size]

	add rax, 4096 - 1
	shr rax, 12
	shl rax, 12

; stack
	add rax, 2*1024*1024

; memory map
	push rax
	mov rax, qword [data3.memory_map_entry_num]
	mov rbx, 24
	mul rbx
	mov rcx, rax
	pop rax
	add rax, rcx

	add rax, 4096 - 1
	shr rax, 12
	shl rax, 12

; paging struct
; 1 l4
; 1 l3 kernel
; 1 l2 kernel
; 512 l1 kernel
; 1 l3 stack
; 1 l2 stack
; 2 l1 stack
; 8 l3 physical mapping
	add rax, 527*4096

; save avoid area size
	mov qword [data3.avoid_area_size], rax

; ======================Find a hole to save avoid area========================

find_avoid_area_start_address:

	mov byte [data.error_msg + 0x11], 'A'

find_avoid_area_start_address.loop.init:
; rax hold next entry address
	mov rax, data3.memory_map_start

; rbx hold current index
	mov rbx, 0

; rcx hold avoid area size
	mov rcx, qword [data3.avoid_area_size]

; data.tmp4 hold isFound flag
	mov qword [data.tmp4], 0

find_avoid_area_start_address.loop.check:
; exit if current index >= memory_map_entry num
	cmp rbx, qword [data3.memory_map_entry_num]
	jae find_avoid_area_start_address.loop.end

; continue if current entry is not usable
	cmp dword [rax + 16], 1
	jne find_avoid_area_start_address.loop.continue

; continue if current entry should be ignored
	mov edx, dword [eax + 20]
	and edx, 1
	cmp edx, 1
	jne find_avoid_area_start_address.loop.continue

; continue if current entry start < 1MB
	cmp qword [rax], 1*1024*1024
	jb find_avoid_area_start_address.loop.continue

; continue if current entry size <= avoid area
	cmp qword [rax + 8], rcx
	jb find_avoid_area_start_address.loop.continue

; check if initrd+tmp paging struct is loaded to this hole
	mov rdx, qword [rax]
	cmp rdx, qword [data3.initrd_start]
	jne find_avoid_area_start_address.loop.not_same_as_initrd

find_avoid_area_start_address.loop.same_as_initrd:
	mov r8, qword [rax]
	mov r9, qword [rax + 8]
	mov r10, r8
	add r10, qword [data3.initrd_size]
	add r10, qword [data3.tmp_paging_struct_size]
	add r10, 4096 - 1
	shr r10, 12
	shl r10, 12
	mov r11, r10
	sub r11, r8
	sub r9, r11

; check if size after adjust is ok
	cmp r9, rcx
	jb find_avoid_area_start_address.loop.continue
	jmp find_avoid_area_start_address.loop.found

; adjust
find_avoid_area_start_address.loop.not_same_as_initrd:
	mov r8, qword [rax]
	mov r9, qword [rax + 8]
	mov r10, r8
	add r10, 4096 - 1
	shr r10, 12
	shl r10, 12
	mov r11, r10
	sub r11, r8
	sub r9, r11

; check if size after adjust is ok
	cmp r9, rcx
	jb find_avoid_area_start_address.loop.continue

find_avoid_area_start_address.loop.found:
	mov qword [data3.avoid_area_start], r10
	mov qword [data.tmp4], 1
	jmp find_avoid_area_start_address.loop.end

find_avoid_area_start_address.loop.continue:
	add rax, 24
	add rbx, 1
	jmp find_avoid_area_start_address.loop.check

find_avoid_area_start_address.loop.end:
; check if a hole is found
	cmp qword [data.tmp4], 1
; jump to error if not found
	jne error64

find_avoid_area_start_address_end:

; =====================Calc some address for avoid area=======================

calc_some_avoid_area_address:

	mov byte [data.error_msg + 0x11], 'B'

	mov rax, qword [data3.avoid_area_start]
	add rax, 4096
	add rax, qword [data3.initrd_size]
	mov qword [data3.kernel_start], rax
	add rax, qword [data3.kernel_loaded_size]
	add rax, 4096 - 1
	shr rax, 12
	shl rax, 12
	mov qword [data3.kernel_stack_start], rax
	add rax, 2*1024*1024
	mov qword [data3.memory_map_start_address], rax
	push rax
	mov rax, qword [data3.memory_map_entry_num]
	mov rbx, 24
	mul rbx
	mov rbx, rax
	pop rax
	mov qword [data3.memory_map_size], rbx
	add rax, rbx
	add rax, 4096 - 1
	shr rax, 12
	shl rax, 12
	mov qword [data3.paging_struct_start], rax
	mov qword [data3.paging_struct_size], 527*4096


; search 0xE0000->0xFFFFF
.search_rsdp1.loop.init:
	cld
	mov rsi, 0x000E0000

.search_rsdp1.loop.check:
	cmp rsi, 0x000FFFFF
	ja .search_rsdp1.loop.end

.search_rsdp1.loop.body:
	mov rax, 0x2052545020445352
	cmp qword [rsi], rax
	je .found

.search_rsdp1.loop.continue:
	add rsi, 0x10
	jmp .search_rsdp1.loop.check

.search_rsdp1.loop.end:

; search 1KB from EBDA
	xor rax, rax
	mov ax, word [0x040E]
	shl rax, 4

.search_rsdp2.loop.init:
	cld
	mov rsi, rax
	mov rdi, rsi
	add rdi, 1024

.search_rsdp2.loop.check:
	cmp rsi, rdi
	jae .search_rsdp2.loop.end

.search_rsdp2.loop.body:
	mov rax, 0x2052545020445352
	cmp qword [rsi], rax
	je .found

.search_rsdp2.loop.continue:
	add rsi, 0x10
	jmp .search_rsdp2.loop.check

.search_rsdp2.loop.end:

	jmp error64

.found:
	mov qword [data3.rsdp_address], rsi

calc_some_avoid_area_address_end:

; =============================Build avoid area===============================

build_avoid_area:

	mov rax, [data3.avoid_area_start]

build_avoid_area.bootinfo:
; avoidAreaStartAddress
	mov rbx, qword [data3.avoid_area_start]
	mov qword [rax], rbx

; avoidAreaSize
	mov rbx, qword [data3.avoid_area_size]
	mov qword [rax + 8], rbx

; initrdStartAddress
	mov rbx, qword [data3.avoid_area_start]
	add rbx, 4096
	mov qword [rax + 0x10], rbx

; initrdSize
	mov rbx, qword [data3.initrd_size]
	mov qword [rax + 0x18], rbx

; kernelStartAddress
	mov rbx, qword [data3.kernel_start]
	mov qword [rax + 0x20], rbx

; kernelSizeInMemory
	mov rbx, qword [data3.kernel_loaded_size]
	mov qword [rax + 0x28], rbx

; kernelStackStartAddress
	mov rbx, qword [data3.kernel_stack_start]
	mov qword [rax + 0x30], rbx

; kernelStackSize
	mov rbx, 2*1024*1024
	mov qword [rax + 0x38], rbx

; memoryMapStartAddress
	mov rbx, qword [data3.memory_map_start_address]
	mov qword [rax + 0x40], rbx

; memoryMapEntryNum
	mov rbx, qword [data3.memory_map_entry_num]
	mov qword [rax + 0x48], rbx

; memoryMapSize
	mov rbx, qword [data3.memory_map_size]
	mov qword [rax + 0x50], rbx

; pagingStructStartAddress
	mov rbx, qword [data3.paging_struct_start]
	mov qword [rax + 0x58], rbx

; pagingStructSize
	mov rbx, qword [data3.paging_struct_size]
	mov qword [rax + 0x60], rbx

; rsdpAddress
	mov rbx, qword [data3.rsdp_address]
	mov qword [rax + 0x68], rbx


build_avoid_area.initrd:
	mov rdi, qword [data3.avoid_area_start]
	add rdi, 4096
	mov rsi, qword [data3.initrd_start]
	mov rcx, qword [data3.initrd_size]
	shr rcx, 3
	rep movsq


build_avoid_area.kernel:
; zero out kernel bss
	xor rax, rax
	mov rdi, qword [data3.kernel_start]
	mov rcx, qword [data3.kernel_loaded_size]
	rep stosb

; load kernel
	mov rsi, qword [data3.kernel_load_start]
	mov rdi, qword [data3.kernel_start]
	mov rcx, qword [data3.kernel_load_size]
	rep movsb

build_avoid_area.memory_map:
	mov rsi, data3.memory_map_start
	mov rdi, qword [data3.memory_map_start_address]
	mov rcx, qword [data3.memory_map_size]
	rep movsb


build_avoid_area.paging_struct:
; zeroing the whole paging area
	xor rax, rax
	mov rdi, qword [data3.paging_struct_start]
	mov rcx, qword [data3.paging_struct_size]
	shr rcx, 3
	rep stosq


; current bootloader code is running inside identify mapped area
; so next paging struct also need to have identify mapped area for the switch to work
build_avoid_area.paging_struct.identify:

build_avoid_area.paging_struct.identify.l4:
	mov rax, qword [data3.paging_struct_start]
	mov rbx, rax
	add rbx, 4096
	or rbx, 3
	mov qword [rax], rbx

build_avoid_area.paging_struct.identify.l3:
	add rax, 4096
	mov rbx, 0b10000011
	mov rcx, 512

build_avoid_area.paging_struct.identify.l3.loop:
	mov qword [rax], rbx
	add rax, 8
	add rbx, 1*1024*1024*1024
	loop build_avoid_area.paging_struct.identify.l3.loop


build_avoid_area.paging_struct.kernel.l4:
	mov rax, qword [data3.paging_struct_start]
	mov rbx, rax
	add rbx, (1+1)*4096
	or rbx, 3
	mov qword [rax + 256*8], rbx

build_avoid_area.paging_struct.kernel.l3:
	mov rax, qword [data3.paging_struct_start]
	add rax, (1+1)*4096
	mov rbx, rax
	add rbx, 4096
	or rbx, 3
	mov qword [rax], rbx

build_avoid_area.paging_struct.kernel.l2:
	mov rax, qword [data3.paging_struct_start]
	add rax, (1+1+1)*4096
	add rbx, 4096
	mov rcx, 512

build_avoid_area.paging_struct.kernel.l2.loop:
	mov qword [rax], rbx
	add rax, 8
	add rbx, 4096
	loop build_avoid_area.paging_struct.kernel.l2.loop

build_avoid_area.paging_struct.kernel.l1:
	mov rax, qword [data3.paging_struct_start]
	add rax, (1+1+1+1)*4096
	mov rbx, qword [data3.kernel_start]
	or rbx, 3
	mov rcx, 512*512

build_avoid_area.paging_struct.kernel.l1.loop:
	mov qword [rax], rbx
	add rax, 8
	add rbx, 4096
	loop build_avoid_area.paging_struct.kernel.l1.loop


build_avoid_area.paging_struct.stack:

build_avoid_area.paging_struct.stack.l4:
	mov rax, qword [data3.paging_struct_start]
	mov rbx, rax
	add rbx, (1+1+1+1+512)*4096
	or rbx, 3
	mov qword [rax + 494*8], rbx

build_avoid_area.paging_struct.stack.l3:
	mov rax, qword [data3.paging_struct_start]
	add rax, (1+1+1+1+512)*4096
	mov rbx, rax
	add rbx, 4096
	or rbx, 3
	mov qword [rax + 511*8], rbx

build_avoid_area.paging_struct.stack.l2:
	add rax, 4096
	add rbx, 4096
	mov qword [rax + 511*8], rbx

build_avoid_area.paging_struct.stack.l1:
	add rax, 4096
	mov rbx, qword [data3.kernel_stack_start]
	or rbx, 3
	mov rcx, 512

build_avoid_area.paging_struct.stack.l1.loop:
	mov qword [rax], rbx
	add rax, 8
	add rbx, 4096
	loop build_avoid_area.paging_struct.stack.l1.loop


build_avoid_area.paging_struct.physical:

build_avoid_area.paging_struct.physical.l4:
	mov rax, qword [data3.paging_struct_start]
	mov rbx, rax
	add rbx, (1+1+1+1+512+1+1+1)*4096
	or rbx, 3

	mov qword [rax + 503*8], rbx

	add rbx, 4096
	mov qword [rax + 504*8], rbx

	add rbx, 4096
	mov qword [rax + 505*8], rbx

	add rbx, 4096
	mov qword [rax + 506*8], rbx

	add rbx, 4096
	mov qword [rax + 507*8], rbx

	add rbx, 4096
	mov qword [rax + 508*8], rbx

	add rbx, 4096
	mov qword [rax + 509*8], rbx

	add rbx, 4096
	mov qword [rax + 510*8], rbx


build_avoid_area.paging_struct.physical.l3:
	mov rax, qword [data3.paging_struct_start]
	add rax, (1+1+1+1+512+1+1+1)*4096
	mov rbx, 0b10000011
	mov rcx, 8*512

build_avoid_area.paging_struct.physical.l3.loop:
	mov qword [rax], rbx
	add rax, 8
	add rbx, 1*1024*1024*1024
	loop build_avoid_area.paging_struct.physical.l3.loop


build_avoid_area.paging_struct.recursice_mapping:
	mov rax, qword [data3.paging_struct_start]
	mov rbx, rax
	or rbx, 3
	mov qword [rax + 511*8], rbx


build_avoid_area_end:

; ===========================Jump to kernel===================================

jump_to_kernel:
; set paging struct
	mov rax, qword [data3.paging_struct_start]
	mov cr3, rax

; set stack
; stack is at entry 494
; right before 495 entry
	mov rsp, 0xfffff78000000000

; pass pointer to avoid area
	mov rdi, qword [data3.avoid_area_start]

; jump to kernel
	mov rax, qword [data3.kernel_entry]
	jmp rax

jump_to_kernel_end:


error64:
	xchg bx, bx
	jmp $

; ============================================================================

data3:
.magic: db 'data3:  '

; ================================GDT16=======================================

.gdt16info:
   dw data3.gdt16_end - data3.gdt16_start - 1   ;size of gdt-1
   dd data3.gdt16_start                 ;start of table

.gdt16_start:
.gdt16_null_description:    dd 0,0        ; entry 0 is always unused
.gdt16_code_description:    db 0xff, 0xff, 0, 0, 0, 10011010b, 10001111b, 0
.gdt16_data_description:    db 0xff, 0xff, 0, 0, 0, 10010010b, 10001111b, 0
.gdt16_end:

; ================================GDT32=======================================

.gdt32info:
   dw data3.gdt32_end - data3.gdt32_start - 1   ;size of gdt-1
   dd data3.gdt32_start                 ;start of table

.gdt32_start:
.gdt32_null_description:    dq 0
.gdt32_code_description:    db 0xff, 0xff, 0x00, 0x00, 0x00, 10011010b, 11001111b, 0x00
.gdt32_data_description:    db 0xff, 0xff, 0x00, 0x00, 0x00, 10010010b, 11001111b, 0x00
.gdt32_end:


; ================================GDT64=======================================

.gdt64info:
   dw data3.gdt64_end - data3.gdt64_start - 1   ;size of gdt-1
   dd data3.gdt64_start                 ;start of table

.gdt64_start:
.gdt64_null_description:    db 0x00, 0x00, 0x00, 0x00, 0x00, 00000000b, 00000000b, 0x00
.gdt64_code_description:    db 0xff, 0xff, 0x00, 0x00, 0x00, 10011010b, 10101111b, 0x00
.gdt64_data_description:    db 0xff, 0xff, 0x00, 0x00, 0x00, 10010010b, 10101111b, 0x00
.gdt64_end:

; =============================CPU Features===================================

.l5_paging:					dq 0
.page_1g:					dq 0

; ============================================================================

.initrd_start:				dq 0
.initrd_end:				dq 0
.initrd_size:				dq 0

.avoid_area_start:			dq 0
.avoid_area_size:			dq 0
.kernel_start:				dq 0
.kernel_load_start:			dq 0
.kernel_load_size:			dq 0
.kernel_stack_start:		dq 0
.memory_map_start_address:	dq 0
.memory_map_size:			dq 0
.paging_struct_start:		dq 0
.paging_struct_size:		dq 0

.rsdp_address:				dq 0

.tmp_paging_struct_start:	dq 0
.tmp_paging_struct_size:	dq 0

.kernel_file_start:			dq 0
.kernel_entry:				dq 0
.kernel_loaded_size:		dq 0

; ============================================================================

.memory_map_entry_num:		dq 0
.memory_map_entry_size:		dq 0
.memory_map_start:
data3_end:
