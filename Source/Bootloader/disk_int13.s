;====================================
; eax, lower 32 bits of LBA
; bx, upper 16 bits of LBA
; ebx's upper 16 bits must be zero
; cx, number of sector to read
; dl, disk to read
; ds:si, buffer
; the push intruction is used to build DAP on stack
;====================================
;Disk Address Packet
;disk_int13_read_DAP:
;    db 0x10						; size of this DAP
;    db 0x00						; must be zero
;    dw 0x0003						; number sector to read
;    dw 0x9000						; offset
;    dw 0x0000						; segment
;    dq 0x0000000000000000			; LBA
disk_int13_read:
    push ebx
    push eax
    push ds
    push si
    push cx
    push 0x0010
    mov ah, 0x42
    mov si, sp
    int 0x13
    pop cx
    pop cx
    pop si
    pop ds
    pop ebx
    pop eax
    ret
disk_int13_read_end:

;====================================
; eax, lower 32 bits of LBA
; bx, upper 16 bits of LBA
; ebx's upper 16 bits must be zero
; cx, number of sector to write
; dl, disk to write
; ds:si, buffer
; the push intruction is used to build DAP on stack
;====================================
;Disk Address Packet
;disk_int13_write_DAP:
;    db 0x10                     ;size of this DAP
;    db 0x00                     ;must be zero
;    dw 0x0003                   ;number sector to write
;    dw 0x9000                   ;offset
;    dw 0x0000                   ;segment
;    dq 0x0000000000000000       ;LBA
;disk_int13_write:
;    push eax
;    push ebx
;    push ds
;    push si
;    push cx
;    push 0x0010
;    mov ah, 0x43
;    xor al, al
;    mov si, sp
;    int 0x13
;    pop cx
;    pop cx
;    pop si
;    pop ds
;    pop ebx
;    pop eax
;    ret
;disk_int13_write_end:

