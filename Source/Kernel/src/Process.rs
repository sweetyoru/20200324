use core::fmt;
use core::cell::SyncUnsafeCell;

use alloc::vec::Vec;
use alloc::sync::Arc;

use crate::CPU;
use crate::SMP;
use crate::println;
use crate::DataStruct::DefaultHashMap;
use crate::ISC::{self, SystemID};
use crate::Lock::{OneTimeFlag, Mutex, RwMutex};
use crate::Memory::Virtual::{self, KernelAddress, VirtualAddressSpace};
use crate::Thread::{Thread, ThreadID, ThreadPrivateID, ThreadState};


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct ProcessPrivateID(u128);

impl ProcessPrivateID
{
	pub const fn New (processPrivateID: u128) -> Self
	{
		Self(processPrivateID)
	}
}


impl fmt::Display for ProcessPrivateID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:032x}", self.0)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct ProcessID
{
	systemID: SystemID,
	privateID: ProcessPrivateID,
}

impl ProcessID
{
	pub const fn New (systemID: SystemID, privateID: ProcessPrivateID) -> Self
	{
		Self { systemID, privateID, }
	}
}


impl fmt::Display for ProcessID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "({}, {})", self.systemID, self.privateID)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum ProcessType
{
	NATIVE,
}


#[derive(Debug)]
struct ProcessMutableData1
{
	// once this field turn true, process is killed, no more schedule will be done
	// this field cannot be set to false after it turned true
	isKilled: OneTimeFlag,

	// once this field turn true, process creation is completed
	// this field cannot be set to false after it turned true
	isHatched: OneTimeFlag,

	// process threads
	threads: DefaultHashMap<ThreadID, Arc<Thread>>,
}

impl ProcessMutableData1
{
	pub fn New () -> Self
	{
		Self
		{
			isKilled: OneTimeFlag::New (),
			isHatched: OneTimeFlag::New (),
			threads: DefaultHashMap::New (),
		}
	}
}


#[derive(Debug)]
pub struct Process
{
	processID: ProcessID,
	parentID: ProcessID,
	processType: ProcessType,
	virtualAddressSpace: Arc<VirtualAddressSpace>,

	processMutableData1: Mutex<ProcessMutableData1>,
}

impl Process
{
	/// Process creation consist of 3 stages
	/// This is stage 1
	/// When in stage 1, process not completely initialized yet and cannot be scheduled
	pub fn New (processType: ProcessType, processID: ProcessID, parentID: ProcessID) -> Self
	{
		Self
		{
			processID,
			parentID,
			processType,
			virtualAddressSpace: Arc::new (VirtualAddressSpace::New ()),

			processMutableData1: Mutex::New (ProcessMutableData1::New ()),
		}
	}

	pub fn ProcessID (&self) -> ProcessID { self.processID }
	pub fn IsKilled (&self) -> bool { self.processMutableData1.Lock ().isKilled.IsSet () }


	// Mark this porcess as killed
	// Also kill all of its threads
	pub fn Kill (&self)
	{
		let mut processMutableData1 = self.processMutableData1.Lock ();

		processMutableData1.isKilled.Set ();

		for (_threadID, thread) in &mut processMutableData1.threads
		{
			thread.Kill ();
		}
	}


	pub fn IsHatched (&self) -> bool { self.processMutableData1.Lock ().isHatched.IsSet () }
	pub fn Hatch (&self) { self.processMutableData1.Lock ().isHatched.Set () }

	pub fn GetVirtualAddressSpace (&self) -> Arc<VirtualAddressSpace>
	{
		Arc::clone (&self.virtualAddressSpace)
	}

	pub fn GetThreadList (&self) -> Vec<Arc<Thread>>
	{
		let processMutableData1 = self.processMutableData1.Lock ();

		assert! (processMutableData1.isHatched.IsSet ());

		processMutableData1.threads.Iter ().map (|(_threadID, thread)| Arc::clone (thread)).collect ()
	}

	pub fn GetThread (&self, threadID: &ThreadID) -> Option<Arc<Thread>>
	{
		assert! (self.processMutableData1.Lock ().isHatched.IsSet ());
		self.processMutableData1.Lock ().threads.GetRef (threadID).and_then (|id| Some(Arc::clone (id)))
	}

	pub fn DestroyThread (&self)
	{
		unimplemented! ();
	}

	/// Add a created thread to this process
	pub fn AddThread (&self, thread: Arc<Thread>)
	{
		let mut processMutableData1 = self.processMutableData1.Lock ();
		processMutableData1.threads.Insert (thread.ThreadID (), thread);
	}

	pub fn RemoveThread (&self)
	{
		unimplemented! ();
	}
}


static processList: RwMutex<DefaultHashMap<ProcessID, Arc<Process>>> = RwMutex::New (DefaultHashMap::New ());


pub fn InitProcess ()
{
	println! ("[Boot CPU][+] Init Process...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }


	// create boot process
	let bootProcessID = ProcessID::New (SystemID::New (0), ProcessPrivateID::New (0));

	let bootProcess = Arc::new (Process
		{
			processID: bootProcessID,
			parentID: bootProcessID,
			processType: ProcessType::NATIVE,
			virtualAddressSpace: Arc::new (Virtual::GetBootVirtualAddressSpace ()),

			processMutableData1: Mutex::New (ProcessMutableData1::New ()),
		});


	// create boot thread
	let bootThreadID = ThreadID::New (SystemID::New (0), ThreadPrivateID::New (SMP::GetCurrentCPU ().ID ().ToU128 ()));
	let bootThread = Arc::new (Thread::NewRoot (Arc::downgrade (&bootProcess), KernelAddress::New (0), Some(bootThreadID)));


	// set boot thread to current cpu
	bootThread.SetState (ThreadState::Running);
	SMP::GetCurrentCPU ().SetCurrentThread (Arc::clone (&bootThread));
	bootThread.FinishRoot ();

	bootProcess.AddThread (Arc::clone (&bootThread));
	bootProcess.Hatch ();


	// create root process
	let rootProcess = Arc::new (Process::New (ProcessType::NATIVE, GetRootProcessID (), GetRootProcessID ()));

	let startAddress = KernelAddress::New (RootCleanUp as *const () as usize);


	// create 1 thread for each cpu
	for cpu in SMP::GetCPUList ()
	{
		let newThreadID = ThreadID::New (ISC::GetCurrentSystemID (), ThreadPrivateID::New (cpu.ID ().ToU128 ()));
		let newThread = Arc::new (Thread::New (Arc::downgrade (&rootProcess), startAddress, Some(newThreadID)));
		rootProcess.AddThread (newThread);
	}

	rootProcess.Hatch ();

	processList.WriterLock ().Insert (GetRootProcessID () , rootProcess);
	processList.WriterLock ().Insert (bootProcessID , bootProcess);
}


fn RootCleanUp ()
{
	let currentThreadID = SMP::GetCurrentCPU ().GetCurrentThread ().GetThreadID ();

	if currentThreadID == ThreadID::New (ISC::GetCurrentSystemID (), ThreadPrivateID::New (0))
	{
		println! ("Thread {} doing root clean up...", currentThreadID);

		// kill boot process
		let bootProcessID = ProcessID::New (SystemID::New (0), ProcessPrivateID::New (0));
		GetProcess (bootProcessID).unwrap ().Kill ();
	}

	IDleThread ();
}


fn IDleThread ()
{
	loop
	{
		println! ("[CPU {}][+] Current ThreadID: {}", CPU::CPU::GetCurrentCPUID (), SMP::GetCurrentCPU ().GetCurrentThread ().GetThreadID ());
		CPU::CPU::Halt ();
	}
}


pub fn GetProcess (processID: ProcessID) -> Option<Arc<Process>>
{
	let lock = processList.ReaderLock ();
	let process = lock.GetRef (&processID)?;
	Some(Arc::clone (process))
}


pub fn RemoveProcess (processID: ProcessID)
{
	if let Some(process) = processList.WriterLock ().Remove (&processID)
	{
		// mark all thread as killed
		process.Kill ();
	}
}


pub fn GetRootProcessID () -> ProcessID
{
	ProcessID::New (ISC::GetCurrentSystemID (), ProcessPrivateID::New (0))
}