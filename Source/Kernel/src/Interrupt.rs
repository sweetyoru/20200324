//! This module export the interrupt interface, mostly a wrapper to hide platform-dependent details


#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::Interrupt::*;


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct InterruptID(usize);

impl InterruptID
{
	pub const fn New (id: usize) -> Self { Self(id) }
	pub const fn ToUsize (&self) -> usize { self.0 }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum InterruptPriority
{
	High,
	Low,
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum InterruptAllocationStrategy
{
	Fixed(InterruptID),
	PriorityBased(InterruptPriority),
	Any,
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum InterruptTriggerMode
{
	FollowBus,
	Level,
	Edge,
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum InterruptPinPolarity
{
	FollowBus,
	HighActive,
	LowActive,
}


#[derive(Copy, Clone, Eq, PartialEq)]
pub enum InterruptDestinationShortHand
{
	NoShortHand,
	SelfShortHand,
	AllIncludingSelf,
	AllExcludingSelf,
}

impl InterruptDestinationShortHand
{
	pub fn New (value: usize) -> Self
	{
		match value
		{
			0b00 => Self::NoShortHand,
			0b01 => Self::SelfShortHand,
			0b10 => Self::AllIncludingSelf,
			0b11 => Self::AllExcludingSelf,
			_ => panic! ("Unknown value!"),
		}
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum InterruptDeliveryStatus
{
	Idle,
	Pending,
}

impl InterruptDeliveryStatus
{
	pub fn New (value: usize) -> Self
	{
		match value
		{
			0 => Self::Idle,
			1 => Self::Pending,
			_ => panic! ("Unknown value!"),
		}
	}
}


#[derive(Debug)]
pub enum InterruptError
{
	UnavailableInterruptID(InterruptID),
	NoInterruptIDAvailable,
}
