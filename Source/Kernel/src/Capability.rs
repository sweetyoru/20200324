use core::fmt;
use core::cell::SyncUnsafeCell;

use alloc::vec::Vec;

use crate::println;
use crate::CPU::CPU;
use crate::Lock::OneTimeFlag;
use crate::Process::ProcessID;
use crate::ISC::{self, SystemID};


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct CapabilityPrivateID(u128);

impl CapabilityPrivateID
{
	pub const fn New (capabilityPrivateID: u128) -> Self
	{
		Self(capabilityPrivateID)
	}
}


impl fmt::Display for CapabilityPrivateID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:032x}", self.0)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct CapabilityID
{
	systemID: SystemID,
	privateID: CapabilityPrivateID,
}

impl CapabilityID
{
	pub const fn New (systemID: SystemID, privateID: CapabilityPrivateID) -> Self
	{
		Self { systemID, privateID, }
	}
}


impl fmt::Display for CapabilityID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "({}, {})", self.systemID, self.privateID)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum CapabilityPermission
{
	Clone,
	Move,
	Use
}


#[derive(Debug)]
pub struct Capability
{
	capabilityID: CapabilityID,
	creatorID: ProcessID,
	ownerID: ProcessID,
	permission: CapabilityPermission,
	data: Vec<u8>,
}

impl Capability
{
	pub fn New (creatorID: ProcessID, ownerID: ProcessID, permission: CapabilityPermission, data: Vec<u8>) -> Self
	{
		let capabilityID = CapabilityID::New (ISC::GetCurrentSystemID (), CapabilityPrivateID::New (CPU::GetRandomNumberU128 ()));
		Self
		{
			capabilityID,
			creatorID,
			ownerID,
			permission,
			data,
		}
	}

	pub fn Clone (&self, newOwnerID: ProcessID, newPermission: CapabilityPermission) -> Result<Self, ()>
	{
		if self.permission == CapabilityPermission::Clone
		{
			let capabilityID = CapabilityID::New (ISC::GetCurrentSystemID (), CapabilityPrivateID::New (CPU::GetRandomNumberU128 ()));

			let newCapability = Self
			{
				capabilityID,
				creatorID: self.creatorID,
				ownerID: newOwnerID,
				permission: newPermission,
				data: self.data.clone (),
			};

			Ok(newCapability)
		}
		else
		{
			Err(())
		}
	}

	pub fn Move (&mut self, newOwnerID: ProcessID, newPermission: CapabilityPermission) -> Result<(), ()>
	{
		if self.permission == CapabilityPermission::Clone || self.permission == CapabilityPermission::Move // can move
		{
			if self.permission == CapabilityPermission::Move && newPermission == CapabilityPermission::Clone // new permission can't be clone if old permission only move
			{
				Err(())
			}
			else // passed all checks
			{
				self.ownerID = newOwnerID;
				self.permission = newPermission;
				Ok(())
			}
		}
		else
		{
			Err(())
		}
	}
}


static capabilityList: SyncUnsafeCell<Vec<Capability>> = SyncUnsafeCell::new (Vec::new ());


/// This function must be called only once by boot CPU at boot time
pub fn InitCapability ()
{
	println! ("[Boot CPU][+] Init Capability System...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }
}