use core::cell::SyncUnsafeCell;

use crate::println;
use crate::DataStruct::DefaultHashSet;
use crate::Interrupt::InterruptID;
use crate::Arch::Amd64::CPU::{self, CPUID};

#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::SystemCall::*;


pub const systemCallInterruptID: InterruptID = InterruptID::New (66);


/// This function must be called once at boot by each CPU
pub fn InitSystemCall ()
{
	println! ("[CPU {}][+] Init System Call System...", CPU::CPU::GetCurrentCPUID ());

	static calledCpu: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCpu.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	InsertSystemCallInterruptHandler ();
}


/*
#[derive(Debug)]
enum SystemRequestData
{

}


#[derive(Debug)]
struct SystemRequest
{
	processID: ProcessID,
	systemRequestID: usize,
	systemRequestData: SystemRequestData,
}
*/