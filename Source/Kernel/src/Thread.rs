use core::fmt;
use alloc::sync::{Arc, Weak};
use alloc::collections::vec_deque::VecDeque;
use core::cell::{SyncUnsafeCell, UnsafeCell};

use crate::CPU;
use crate::Process;
use crate::ISC::{self, SystemID};
use crate::Lock::{OneTimeFlag, Mutex};
use crate::Memory::Virtual::{KernelAddress, UserAddress};

#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::Thread::*;


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct ThreadPrivateID(u128);

impl ThreadPrivateID
{
	pub const fn New (threadPrivateID: u128) -> Self
	{
		Self(threadPrivateID)
	}
}

impl fmt::Display for ThreadPrivateID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:032x}", self.0)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct ThreadID
{
	systemID: SystemID,
	privateID: ThreadPrivateID,
}

impl ThreadID
{
	pub const fn New (systemID: SystemID, privateID: ThreadPrivateID) -> Self
	{
		Self { systemID, privateID, }
	}
}

impl fmt::Display for ThreadID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "({}, {})", self.systemID, self.privateID)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum ThreadState
{
	Running,
	Ready,
	Blocking,
}


#[derive(Debug)]
struct ThreadMutableData1
{
	isKilled: OneTimeFlag,
	threadState: ThreadState,
	asyncThreadExecutionStates: VecDeque<Arc<UnsafeCell<ThreadExecutionState>>>,
	mainThreadExecutionState: Arc<UnsafeCell<ThreadExecutionState>>,
	currentLoadState: Arc<UnsafeCell<ThreadExecutionState>>,
	nextLoadState: Arc<UnsafeCell<ThreadExecutionState>>,
	currentSaveState: Arc<UnsafeCell<ThreadExecutionState>>,
	nextSaveState: Arc<UnsafeCell<ThreadExecutionState>>,
}


#[derive(Debug)]
pub struct Thread
{
	threadID: ThreadID,
	ownerProcess: Weak<Process::Process>,

	threadMutableData1: Mutex<ThreadMutableData1>,
}

impl Thread
{
	pub fn New (ownerProcess: Weak<Process::Process>, startingAddress: KernelAddress, suggestedThreadID: Option<ThreadID>) -> Self
	{
		let threadID = match suggestedThreadID
		{
			Some(id) => id,
			None =>
			{
				ThreadID::New (ISC::GetCurrentSystemID (), ThreadPrivateID::New (CPU::CPU::GetRandomNumberU128 ()))
			}
		};

		let mainThreadExecutionState = Arc::new (UnsafeCell::new (ThreadExecutionState::NewAtKernel (startingAddress)));

		Self
		{
			threadID,
			ownerProcess,

			threadMutableData1: Mutex::New (ThreadMutableData1
				{
					isKilled: OneTimeFlag::New (),
					threadState: ThreadState::Ready,
					asyncThreadExecutionStates: VecDeque::new (),
					mainThreadExecutionState: Arc::clone (&mainThreadExecutionState),
					currentLoadState: Arc::clone (&mainThreadExecutionState),
					nextLoadState: Arc::clone (&mainThreadExecutionState),
					currentSaveState: Arc::clone (&mainThreadExecutionState),
					nextSaveState: Arc::clone (&mainThreadExecutionState),
				}),
		}
	}

	pub fn ThreadID (&self) -> ThreadID { self.threadID }

	/// this function is to break the dependency cycle betweeb thread -> heap -> thread
	/// this function must be called only once at boot
	/// must be couple with FinishRoot
	pub fn NewRoot (ownerProcess: Weak<Process::Process>, startingAddress: KernelAddress, suggestedThreadID: Option<ThreadID>) -> Self
	{
		static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
		unsafe { (*called.get ()).Set (); }

		let threadID = match suggestedThreadID
		{
			Some(id) => id,
			None =>
			{
				ThreadID::New (ISC::GetCurrentSystemID (), ThreadPrivateID::New (CPU::CPU::GetRandomNumberU128 ()))
			}
		};

		let mainThreadExecutionState = Arc::new (UnsafeCell::new (ThreadExecutionState::NewRoot (startingAddress)));

		Self
		{
			threadID,
			ownerProcess,

			threadMutableData1: Mutex::New (ThreadMutableData1
				{
					isKilled: OneTimeFlag::New (),
					threadState: ThreadState::Ready,
					asyncThreadExecutionStates: VecDeque::new (),
					mainThreadExecutionState: Arc::clone (&mainThreadExecutionState),
					currentLoadState: Arc::clone (&mainThreadExecutionState),
					nextLoadState: Arc::clone (&mainThreadExecutionState),
					currentSaveState: Arc::clone (&mainThreadExecutionState),
					nextSaveState: Arc::clone (&mainThreadExecutionState),
				}),
		}
	}

	/// this function is to break the dependency cycle betweeb thread -> heap -> thread
	/// this function must be called only once at boot
	/// must be called after NewRoot
	pub fn FinishRoot (&self)
	{
		static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
		unsafe { (*called.get ()).Set (); }

		unsafe
		{
			(*self.threadMutableData1.Lock ().mainThreadExecutionState.get ()).FinishRoot ();
		}
	}

	pub fn GetThreadID (&self) -> ThreadID { self.threadID }
	pub fn IsKilled (&self) -> bool { self.threadMutableData1.Lock ().isKilled.IsSet () }
	pub fn Kill (&self) { self.threadMutableData1.Lock ().isKilled.Set (); }

	pub fn GetOwnerProcess (&self) -> Arc<Process::Process>
	{
		Weak::upgrade (&self.ownerProcess).unwrap ()
	}

	pub fn GetState (&self) -> ThreadState
	{
		self.threadMutableData1.Lock ().threadState
	}

	pub fn SetState (&self, newState: ThreadState)
	{
		self.threadMutableData1.Lock ().threadState = newState;
	}

	pub fn GetExecutionStateToLoad (&self) -> Arc<UnsafeCell<ThreadExecutionState>>
	{
		let result = Arc::clone (&self.threadMutableData1.Lock ().currentLoadState);
		let nextLoadState = Arc::clone (&self.threadMutableData1.Lock ().nextLoadState);
		self.threadMutableData1.Lock ().currentLoadState = nextLoadState;

		result
	}

	pub fn GetExecutionStateToSave (&self) -> Arc<UnsafeCell<ThreadExecutionState>>
	{
		let result = Arc::clone (&self.threadMutableData1.Lock ().currentSaveState);
		let nextSaveState = Arc::clone (&self.threadMutableData1.Lock ().nextSaveState);
		self.threadMutableData1.Lock ().currentSaveState = nextSaveState;

		result
	}

	pub fn CreateAsyncState (&self, startAddress: UserAddress)
	{
		let mut threadMutableData1 = self.threadMutableData1.Lock ();

		let newAsyncState = Arc::new (UnsafeCell::new (ThreadExecutionState::NewAtUser (startAddress)));

		threadMutableData1.asyncThreadExecutionStates.push_back (Arc::clone (&newAsyncState));

		threadMutableData1.currentLoadState = Arc::clone (&newAsyncState);
		threadMutableData1.nextLoadState = Arc::clone (&newAsyncState);
		threadMutableData1.nextSaveState = Arc::clone (&newAsyncState);

		if threadMutableData1.threadState != ThreadState::Running
		{
			threadMutableData1.currentSaveState = Arc::clone (&newAsyncState);
		}
	}


	/// assume that only 1 cpu access to this thread
	/// and scheduler never call this function when the thread is running
	/// and scheduler always schedule this thread when call this function
	pub fn Load (&self, oldThread: Arc<Thread>)
	{
		// no need to worry about race condition, since the only thing can drop a thread's execution state is that thread itself

		// load virtual address space
		self.GetOwnerProcess ().GetVirtualAddressSpace ().Load ();

		// get current execution state
		unsafe
		{
			let currentExecutionState = { &mut *self.GetExecutionStateToLoad ().get () };
			let oldExecutionState = &mut *oldThread.GetExecutionStateToSave ().get ();

			// load execution state
			currentExecutionState.Load (oldExecutionState);
		}
	}

	/// Return kernel stack end address, aka, bottom of kernel stack
	/// Assume downward kernel stack
	pub fn GetKernelStackEndAddress (&self) -> KernelAddress
	{
		unsafe { (*self.GetExecutionStateToLoad ().get ()).GetKernelStackEndAddress () }
	}
}
