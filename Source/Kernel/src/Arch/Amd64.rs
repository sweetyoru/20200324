//! This module contains code for amd64 platform

pub mod Lock;
pub mod Serial;
pub mod BootInfo;
pub mod Memory;
pub mod SMP;
pub mod Interrupt;
pub mod CPU;
mod PortIO;
mod ACPI;
pub mod Timer;
pub mod Thread;
pub mod Init;
pub mod SystemCall;
pub mod ICC;