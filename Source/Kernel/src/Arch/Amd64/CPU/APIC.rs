use core::cell::SyncUnsafeCell;

use crate::SMP;
use crate::DataStruct::DefaultHashSet;
use crate::Arch::Amd64::ACPI::APICID;
use crate::Memory::Virtual::KernelAddress;
use crate::Arch::Amd64::Interrupt::InterruptStackFrame;
use crate::Arch::Amd64::CPU::{self, CPUID, MSRs, InterruptVector};
use crate::Interrupt::{InterruptDestinationShortHand, InterruptTriggerMode};
use crate::Arch::Amd64::Interrupt::IOAPIC::{InterruptDestinationMode, InterruptDeliveryMode, InterruptLevel};


pub (in crate::Arch::Amd64) enum X2ApicMSR
{
	LocalApicID							 = 0x802,
	LocalApicVersion					 = 0x803,
	TaskPriority						 = 0x808,
	ProcessorPriority					 = 0x80a,
	EOI									 = 0x80b,
	LogicalDestination					 = 0x80d,
	SpuriousInterruptVector				 = 0x80f,
	InService1							 = 0x810,
	InService2							 = 0x811,
	InService3							 = 0x812,
	InService4							 = 0x813,
	InService5							 = 0x814,
	InService6							 = 0x815,
	InService7							 = 0x816,
	InService8							 = 0x817,
	TriggerMode1						 = 0x818,
	TriggerMode2						 = 0x819,
	TriggerMode3						 = 0x81a,
	TriggerMode4						 = 0x81b,
	TriggerMode5						 = 0x81c,
	TriggerMode6						 = 0x81d,
	TriggerMode7						 = 0x81e,
	TriggerMode8						 = 0x81f,
	InterruptRequest1					 = 0x820,
	InterruptRequest2					 = 0x821,
	InterruptRequest3					 = 0x822,
	InterruptRequest4					 = 0x823,
	InterruptRequest5					 = 0x824,
	InterruptRequest6					 = 0x825,
	InterruptRequest7					 = 0x826,
	InterruptRequest8					 = 0x827,
	ErrorStatus							 = 0x828,
	LvtCorrectedMachineCheckInterrupt	 = 0x82f,
	InterruptCommand					 = 0x830,
	LvtTimer							 = 0x832,
	LvtThermalSensor					 = 0x833,
	LvtPerformanceMonitoringCounters	 = 0x834,
	LvtLint0							 = 0x835,
	LvtLint1							 = 0x836,
	LvtError							 = 0x837,
	InitialCount						 = 0x838,
	CurrentCount						 = 0x839,
	DivideConfiguration					 = 0x83e,
	SelfIpi								 = 0x83f,
}


const X2APIC_ENABLE: u64 = 1<<10;
const APIC_ENABLE: u64 = 1<<11;
const APIC_SPURIOUS_VECTOR: InterruptVector = InterruptVector::New (0xff);


/// This function must be called only once per CPU at boot
pub (in crate::Arch::Amd64) fn InitAPIC ()
{
	println! ("[CPU {}][+] Init APIC...", CPU::CPU::GetCurrentCPUID ());

	static calledCPUID: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCPUID.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	println! ("[CPU {}][+] Enable x2APIC...", CPU::CPU::GetCurrentCPUID ());
	let oldValue = CPU::CPU::ReadMSR(MSRs::IA32_APIC_BASE);
	let newValue = oldValue|APIC_ENABLE|X2APIC_ENABLE;
	CPU::CPU::WriteMSR(MSRs::IA32_APIC_BASE, newValue);

	println! ("[CPU {}][+] Enable Local APIC...", CPU::CPU::GetCurrentCPUID ());

	// set spurious apic interrupt vector and enable apic
	println! ("[CPU {}][+] Insert APIC Spurious Interrupt Handler...", CPU::CPU::GetCurrentCPUID ());

	SMP::GetCurrentCPU ().InsertInterruptHandler (APIC_SPURIOUS_VECTOR,
		KernelAddress::New (ApicSpuriousInterruptHandler as *const () as usize), false, false).unwrap ();

	WriteReg (X2ApicMSR::SpuriousInterruptVector, 0x1ff);
}


extern "x86-interrupt" fn ApicSpuriousInterruptHandler (interruptStackFrame: InterruptStackFrame)
{
	println! ("APIC Spurious Interrupt Handler called!");
	println! ("Stack Frame:\n{}", interruptStackFrame);
}


pub (in crate::Arch::Amd64) fn ReadReg (reg: X2ApicMSR) -> u64
{
	CPU::CPU::ReadMSR (reg as u32)
}

pub (in crate::Arch::Amd64) fn WriteReg (reg: X2ApicMSR, value: u64)
{
	CPU::CPU::WriteMSR (reg as u32, value)
}

pub (in crate::Arch::Amd64) fn SendEOI ()
{
	WriteReg (X2ApicMSR::EOI, 0);
}

/// This function is used to send Inter Processor Interrupt
pub (in crate::Arch::Amd64) fn SendIPI (apicID: APICID, destinationShortHand: InterruptDestinationShortHand, triggerMode: InterruptTriggerMode, level: InterruptLevel, destinationMode: InterruptDestinationMode, deliveryMode: InterruptDeliveryMode, vector: InterruptVector)
{
	const DESTINATION_SHIFT: usize = 32;
	const DESTINATION_SHORT_HAND_SHIFT: usize = 18;
	const TRIGGER_MODE_SHIFT: usize = 15;
	const LEVEL_SHIFT: usize = 14;
	const DESTINATION_MODE_SHIFT: usize = 11;
	const DELIVERY_MODE_SHIFT: usize = 8;

	assert! (vector.ToUsize () <= CPU::CPU::MAX_INTERRUPT_NUMBER);

	// following checks is based on intel manual, volume 3, section 10.6.1
	let mut levelCheckPassed = false;
	if deliveryMode == InterruptDeliveryMode::SMI
	{
		assert! (vector.ToUsize () == 0);
	}
	else if deliveryMode == InterruptDeliveryMode::Reserved
	{
		panic! ("Attemp to use Reserved IPI delivery mode!");
	}
	else if deliveryMode == InterruptDeliveryMode::INIT
	{
		assert! (vector.ToUsize () == 0);

		if level as usize == 0
		{
			assert! (triggerMode as usize == 1);
			levelCheckPassed = true;
		}
	}

	if !levelCheckPassed
	{
		assert! (level as usize == 1);
	}

	// following checks is based on intel manual, volume 3, section 10.6.1, table 10-4
	if destinationShortHand == InterruptDestinationShortHand::SelfShortHand
		|| destinationShortHand == InterruptDestinationShortHand::AllIncludingSelf
	{
		if deliveryMode == InterruptDeliveryMode::LowestPriority
			|| deliveryMode == InterruptDeliveryMode::NMI
			|| deliveryMode == InterruptDeliveryMode::INIT
			|| deliveryMode == InterruptDeliveryMode::SMI
			|| deliveryMode == InterruptDeliveryMode::SIPI
		{
			panic! ("Invalid combination ICR!");
		}
	}
	else if destinationShortHand == InterruptDestinationShortHand::AllExcludingSelf
	{
		if deliveryMode == InterruptDeliveryMode::SMI
			|| deliveryMode == InterruptDeliveryMode::SIPI
		{
			panic! ("Invalid combination ICR!");
		}
	}

	if deliveryMode == InterruptDeliveryMode::SMI
		|| deliveryMode == InterruptDeliveryMode::SIPI
	{
		if triggerMode == InterruptTriggerMode::Level
		{
			panic! ("Invalid combination ICR!");
		}
	}

	// all checks passed
	// make command
	let value: u64 = (apicID.ToUsize ()<<DESTINATION_SHIFT) as u64
						| ((destinationShortHand as usize)<<DESTINATION_SHORT_HAND_SHIFT) as u64
						| ((triggerMode as usize)<<TRIGGER_MODE_SHIFT) as u64
						| ((level as usize)<<LEVEL_SHIFT) as u64
						| ((destinationMode as usize)<<DESTINATION_MODE_SHIFT) as u64
						| ((deliveryMode as usize)<<DELIVERY_MODE_SHIFT) as u64
						| vector.ToUsize () as u64;

	// send command
	WriteReg (X2ApicMSR::InterruptCommand, value);
}


/// This will send Init IPI to target cpu
pub (in crate::Arch::Amd64) fn SendINIT (targetApicID: APICID)
{
	let destinationShortHand = InterruptDestinationShortHand::NoShortHand;
	let triggerMode = InterruptTriggerMode::Level;

	// always Assert, except for INIT level de-assert
	let level = InterruptLevel::Assert;

	let destinationMode = InterruptDestinationMode::Physical;
	let deliveryMode = InterruptDeliveryMode::INIT;

	// must be 0
	let vector = InterruptVector::New (0);

	// send the interrupt
	SendIPI (targetApicID, destinationShortHand, triggerMode, level, destinationMode, deliveryMode, vector);
}


/// This will send Starup IPI to target cpu
pub (in crate::Arch::Amd64) fn SendSIPI (targetApicID: APICID, vector: InterruptVector)
{
	let destinationShortHand = InterruptDestinationShortHand::NoShortHand;
	let triggerMode = InterruptTriggerMode::Edge;

	// always Assert, except for INIT level de-assert
	let level = InterruptLevel::Assert;

	let destinationMode = InterruptDestinationMode::Physical;
	let deliveryMode = InterruptDeliveryMode::SIPI;

	// send the interrupt
	SendIPI (targetApicID, destinationShortHand, triggerMode, level, destinationMode, deliveryMode, vector);
}
