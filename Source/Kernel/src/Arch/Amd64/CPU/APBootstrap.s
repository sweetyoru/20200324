[ORG 0x0000]
[BITS 16]


; WARNING: do not edit this code without rewrite the patching code in CPU::GetApBoostrapCode()


; setup all segments
xchg bx, bx
cli
xor ax, ax
mov ds, ax
mov es, ax
mov fs, ax
mov gs, ax
mov ss, ax
jmp dword 0x0000:codeStart


codeStart:
; load stack
mov esp, stackEnd


; Load a zero length IDT so that any NMI causes a triple fault
mov eax, IDT
lidt [eax]


; ============================Enable Long mode================================


enableLongMode:
.CR4_PAE:
; set CR4.PAE bit (bit 5)
	mov eax, cr4
	or eax, 0b00100000
	mov cr4, eax

.EFER_LME:
; set EFER.LME bit (bit 2)
	mov ecx, 0xC0000080               ; Read from the EFER MSR.
	rdmsr

	or eax, 0x00000100                ; Set the LME bit.
	wrmsr


.CR3:
; point CR3 to kernel top level paging table
	mov eax, data.bootVirtualAddressSpace
	mov eax, dword [eax]
	mov cr3, eax


.enablePaging:
; enable long mode by setting paging and protected bit at the same time
	mov eax, cr0
	or eax, 0x80000001
	mov cr0, eax


.loadGDT64:
; reload gdt to 64 bit
	mov eax, GDT64Pointer
	lgdt [eax]
	xor eax, eax
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	jmp dword 0x08:longModeStart

enableLongModeEnd:


[BITS 64]
longModeStart:

; enable various paging flags

; PGE bit
mov rax, cr4
mov rbx, 1
shl rbx, 7
or rax, rbx
mov cr4, rax

;NXE bit
mov ecx, 0xC0000080
rdmsr
mov ebx, 1
shl ebx, 11
or eax, ebx
wrmsr

;WP bit
mov rax, cr0
mov rbx, 1
shl rbx, 16
or rax, rbx
mov cr0, rax

; reload cr3
mov rax, cr3
mov cr3, rax


; load kernel stack
mov rax, data.kernelStackEndAddress
mov rsp, qword [rax]


; jump to kernel AP entry
mov rax, data.kernelEntryAddress
mov rax, qword [rax]
jmp rax


haltStart:
	xor bx, bx
	cli
.loop:
	hlt
	jmp .loop
haltEnd:


; fill up remaining space with zero
TIMES 4096 - ($ - $$) - (stackStart - data) - (stackEnd - stackStart) db 0x00


data:
	.bootVirtualAddressSpace: dq 0x1122334455667788
	.kernelEntryAddress:  dq 0x8877665544332211
	.kernelStackEndAddress: dq 0x1122334455667788


ALIGN 4
IDT:
	.length       dw 0
	.base         dd 0


ALIGN 4
GDT64:
	.null:       db 0x00, 0x00, 0x00, 0x00, 0x00, 00000000b, 00000000b, 0x00
	.kernelCode: db 0xff, 0xff, 0x00, 0x00, 0x00, 10011010b, 10101111b, 0x00
	.kernelData: db 0xff, 0xff, 0x00, 0x00, 0x00, 10010010b, 10101111b, 0x00


ALIGN 4

GDT64Pointer:
	.size: dw $ - GDT64 - 1
	.base: dd GDT64


dataEnd:

TIMES 512 - (dataEnd - data) db 0x00

stackStart: TIMES 512 db 0x00
stackEnd:
