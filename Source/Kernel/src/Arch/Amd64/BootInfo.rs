use core::cell::SyncUnsafeCell;

use crate::Lock::OneTimeFlag;
use crate::Memory::Physical::PhysicalAddress;


#[derive(Debug)]
pub struct BootInfo
{
	pub avoidAreaStartAddress:			PhysicalAddress,
	pub avoidAreaEndAddress:			PhysicalAddress,
	pub avoidAreaSize:					usize,

	pub initrdStartAddress:				PhysicalAddress,
	pub initrdEndAddress:				PhysicalAddress,
	pub initrdSize:						usize,

	pub kernelStartAddress:				PhysicalAddress,
	pub kernelEndAddress:				PhysicalAddress,
	pub kernelSize:						usize,

	pub kernelStackStartAddress:		PhysicalAddress,
	pub kernelStackEndAddress:			PhysicalAddress,
	pub kernelStacksize:				usize,

	pub memoryMapStartAddress:			PhysicalAddress,
	pub memoryMapEndAddress:			PhysicalAddress,
	pub memoryMapSize:					usize,
	pub memoryMapEntryNum:				usize,

	pub pagingStructStartAddress:		PhysicalAddress,
	pub pagingStructEndAddress:			PhysicalAddress,
	pub pagingStructSize:				usize,

	pub rsdpAddress:					PhysicalAddress,

	pub memoryMap:						&'static [MMEntry],
}


#[repr (C, packed)]
#[derive(Debug, Clone, Copy)]
pub struct MMEntry
{
	address: u64,
	size: u64,
	entryType: u32,
	flags: u32,
}

impl MMEntry
{
	pub fn Address (&self) -> PhysicalAddress
	{
		PhysicalAddress::from (self.address)
	}

	pub fn Size (&self) -> usize
	{
		self.size as usize
	}

	pub fn Type (&self) -> MMEntryType
	{
		if self.flags&1 == 0
		{
			MMEntryType::Used
		}
		else
		{
			match self.entryType
			{
				1 => MMEntryType::Free,
				2 => MMEntryType::Used,
				3 => MMEntryType::ACPI,
				4 => MMEntryType::ACPI,
				5 => MMEntryType::Used,
				_ => MMEntryType::Unknown,
			}
		}
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MMEntryType
{
	Free,
	Used,
	ACPI,
	MMIO,
	Unknown,
}


static bootInfo: SyncUnsafeCell<BootInfo> = SyncUnsafeCell::new (BootInfo
{
	avoidAreaStartAddress:			PhysicalAddress::New (0),
	avoidAreaEndAddress:			PhysicalAddress::New (0),
	avoidAreaSize:					0,

	initrdStartAddress:				PhysicalAddress::New (0),
	initrdEndAddress:				PhysicalAddress::New (0),
	initrdSize:						0,

	kernelStartAddress:				PhysicalAddress::New (0),
	kernelEndAddress:				PhysicalAddress::New (0),
	kernelSize:						0,

	kernelStackStartAddress:		PhysicalAddress::New (0),
	kernelStackEndAddress:			PhysicalAddress::New (0),
	kernelStacksize:				0,

	memoryMapStartAddress:			PhysicalAddress::New (0),
	memoryMapEndAddress:			PhysicalAddress::New (0),
	memoryMapSize:					0,
	memoryMapEntryNum:				0,

	pagingStructStartAddress:		PhysicalAddress::New (0),
	pagingStructEndAddress:			PhysicalAddress::New (0),
	pagingStructSize:				0,

	rsdpAddress:					PhysicalAddress::New (0),

	memoryMap:						&[],
});


/// This function must be called only once at boot by boot CPU
pub fn InitBootInfo (bootInfoAddress: PhysicalAddress)
{
	println! ("[Boot CPU][+] Init BootInfo...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	let myBootInfo = unsafe { &mut *bootInfo.get () };

	myBootInfo.avoidAreaStartAddress = PhysicalAddress::from ((bootInfoAddress + 0x00).Read::<u64> ());
	myBootInfo.avoidAreaSize = (bootInfoAddress + 0x08).Read::<u64> () as usize;
	myBootInfo.avoidAreaEndAddress = myBootInfo.avoidAreaStartAddress + myBootInfo.avoidAreaSize - 1;

	myBootInfo.initrdStartAddress = PhysicalAddress::from ((bootInfoAddress + 0x10).Read::<u64> ());
	myBootInfo.initrdSize = (bootInfoAddress + 0x18).Read::<u64> () as usize;
	myBootInfo.initrdEndAddress = myBootInfo.initrdStartAddress + myBootInfo.initrdSize - 1;

	myBootInfo.kernelStartAddress = PhysicalAddress::from ((bootInfoAddress + 0x20).Read::<u64> ());
	myBootInfo.kernelSize = (bootInfoAddress + 0x28).Read::<u64> () as usize;
	myBootInfo.kernelEndAddress = myBootInfo.kernelStartAddress + myBootInfo.kernelSize - 1;

	myBootInfo.kernelStackStartAddress = PhysicalAddress::from ((bootInfoAddress + 0x30).Read::<u64> ());
	myBootInfo.kernelStacksize = (bootInfoAddress + 0x38).Read::<u64> () as usize;
	myBootInfo.kernelStackEndAddress = myBootInfo.kernelStackStartAddress + myBootInfo.kernelStacksize - 1;
	myBootInfo.memoryMapStartAddress = PhysicalAddress::from ((bootInfoAddress + 0x40).Read::<u64> ());
	myBootInfo.memoryMapEntryNum = (bootInfoAddress + 0x48).Read::<u64> () as usize;
	myBootInfo.memoryMapSize = (bootInfoAddress + 0x50).Read::<u64> () as usize;
	myBootInfo.memoryMapEndAddress = myBootInfo.memoryMapStartAddress + myBootInfo.memoryMapSize - 1;

	myBootInfo.pagingStructStartAddress = PhysicalAddress::from ((bootInfoAddress + 0x58).Read::<u64> ());
	myBootInfo.pagingStructSize = (bootInfoAddress + 0x60).Read::<u64> () as usize;
	myBootInfo.pagingStructEndAddress = myBootInfo.pagingStructStartAddress + myBootInfo.pagingStructSize - 1;

	myBootInfo.rsdpAddress = PhysicalAddress::from ((bootInfoAddress + 0x68).Read::<u64> ());

	unsafe
	{
		myBootInfo.memoryMap = core::slice::from_raw_parts (
				(bootInfoAddress + 0x40).Read::<u64> () as *const MMEntry,
				myBootInfo.memoryMapEntryNum);
	}
}

pub fn GetBootInfo () -> &'static BootInfo
{
	unsafe { &*bootInfo.get () }
}
