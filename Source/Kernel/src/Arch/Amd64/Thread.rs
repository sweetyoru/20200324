use core::arch::asm;
use core::cell::SyncUnsafeCell;

use alloc::boxed::Box;

use crate::SMP;
use crate::Memory::Heap;
use crate::Lock::OneTimeFlag;
use crate::Memory::Virtual::{UserAddress, KernelAddress, PAGE_SIZE};


#[repr(C, packed)]
#[derive(Debug, Copy, Clone)]
pub struct CPUExecutionState
{
	// do not reorder this!
	// this struct layout is hardcoded and used in assembly code!
	rax: u64,
	rbx: u64,
	rcx: u64,
	rdx: u64,
	rdi: u64,
	rsi: u64,
	r8: u64,
	r9: u64,
	r10: u64,
	r11: u64,
	r12: u64,
	r13: u64,
	r14: u64,
	r15: u64,
	rbp: u64,

	fs: u64,
	gs: u64,

	rip: u64,
	cs: u64,
	rflags: u64,
	rsp: u64,
	ss: u64,
}

impl CPUExecutionState
{
	pub fn New () -> Self
	{
		Self
		{
			rax: 0,
			rbx: 0,
			rcx: 0,
			rdx: 0,
			rdi: 0,
			rsi: 0,
			r8: 0,
			r9: 0,
			r10: 0,
			r11: 0,
			r12: 0,
			r13: 0,
			r14: 0,
			r15: 0,
			rbp: 0,

			fs: 0,
			gs: 0,

			rip: 0,
			cs: 0,
			rflags: 0,
			rsp: 0,
			ss: 0,
		}
	}
}


#[derive(Debug)]
pub struct ThreadExecutionState
{
	cpuExecutionState: CPUExecutionState,
	topKernelStackAddress: KernelAddress,
	kernelStack: Box<[u8]>,
}

impl ThreadExecutionState
{
	const KERNEL_STACK_USABLE_SIZE: usize = 64*1024;
	const KERNEL_STACK_SIZE: usize = Self::KERNEL_STACK_USABLE_SIZE + PAGE_SIZE*2;

	pub fn NewAtKernel (startExecuteAddress: KernelAddress) -> Self
	{
		let mut result = Self
		{
			cpuExecutionState: CPUExecutionState::New (),
			topKernelStackAddress: KernelAddress::New (0),
			kernelStack: unsafe { Box::from_raw (Heap::Alloc (Self::KERNEL_STACK_USABLE_SIZE, PAGE_SIZE).ToMutRaw::<[u8; Self::KERNEL_STACK_SIZE]> ()) },
		};

		result.cpuExecutionState.rip = startExecuteAddress.ToU64 ();
		result.topKernelStackAddress = KernelAddress::New (&*result.kernelStack as *const [u8] as *const () as usize + Self::KERNEL_STACK_SIZE + PAGE_SIZE - 16);
		result.cpuExecutionState.rsp = result.topKernelStackAddress.ToU64 ();

		result.cpuExecutionState.rflags = 0x200;

		// kernel segments
		result.cpuExecutionState.cs = 0x08;
		result.cpuExecutionState.fs = 0x18;
		result.cpuExecutionState.gs = 0x18;

		result
	}

	pub fn NewAtUser (startExecuteAddress: UserAddress) -> Self
	{
		let mut result = Self
		{
			cpuExecutionState: CPUExecutionState::New (),
			topKernelStackAddress: KernelAddress::New (0),
			kernelStack: unsafe { Box::from_raw (Heap::Alloc (Self::KERNEL_STACK_USABLE_SIZE, PAGE_SIZE).ToMutRaw::<[u8; Self::KERNEL_STACK_SIZE]> ()) },
		};

		result.cpuExecutionState.rip = startExecuteAddress.ToU64 ();
		result.topKernelStackAddress = KernelAddress::New (&*result.kernelStack as *const [u8] as *const () as usize + Self::KERNEL_STACK_SIZE + PAGE_SIZE - 16);
		result.cpuExecutionState.rsp = 0;

		result.cpuExecutionState.rflags = 0x200;

		// user segments
		result.cpuExecutionState.cs = 0x28;
		result.cpuExecutionState.fs = 0x38;
		result.cpuExecutionState.gs = 0x38;

		result
	}

	/// this function is to break the dependency cycle between thread -> heap -> thread
	/// this function must be called only once at boot
	/// the different is that kernel stack don't have guard pages
	pub fn NewRoot (startExecuteAddress: KernelAddress) -> Self
	{
		static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
		unsafe { (*called.get ()).Set (); }

		let mut result = Self
		{
			cpuExecutionState: CPUExecutionState::New (),
			topKernelStackAddress: KernelAddress::New (0),
			kernelStack: unsafe { Box::from_raw (Heap::Alloc (Self::KERNEL_STACK_USABLE_SIZE, PAGE_SIZE).ToMutRaw::<[u8; Self::KERNEL_STACK_SIZE]> ()) },
		};

		result.cpuExecutionState.rip = startExecuteAddress.ToU64 ();
		result.topKernelStackAddress = KernelAddress::New (&*result.kernelStack as *const [u8] as *const () as usize + Self::KERNEL_STACK_SIZE + PAGE_SIZE - 16);
		result.cpuExecutionState.rsp = result.topKernelStackAddress.ToU64 ();

		// mostly for sure
		result.cpuExecutionState.cs = 0x08;
		result.cpuExecutionState.fs = 0x18;
		result.cpuExecutionState.gs = 0x18;

		result
	}

	/// this function is to break the dependency cycle betweeb thread -> heap -> thread
	/// this function must be called only once at boot
	/// must be called after NewRoot
	/// this add guard pages to kernel stack
	pub fn FinishRoot (&mut self)
	{
		static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
		unsafe { (*called.get ()).Set (); }

		self.kernelStack = unsafe { Box::from_raw (Heap::Alloc (Self::KERNEL_STACK_USABLE_SIZE, PAGE_SIZE).ToMutRaw::<[u8; Self::KERNEL_STACK_SIZE]> ()) };

		self.topKernelStackAddress = KernelAddress::New (&*self.kernelStack as *const [u8] as *const () as usize + Self::KERNEL_STACK_SIZE + PAGE_SIZE - 16);
		self.cpuExecutionState.rsp = self.topKernelStackAddress.ToU64 ();
	}

	pub fn GetKernelStackEndAddress (&self) -> KernelAddress
	{
		KernelAddress::New (self.kernelStack.as_ptr () as usize) + self.kernelStack.len ()
	}

	fn LoadCPUExcutionState (&self, stateToSave: &mut CPUExecutionState)
	{
		unsafe
		{
			asm! (
				"push {}",
				"push {}",
				"call {}",
				"add rsp, 0x10",
				in(reg) &self.cpuExecutionState,
				in(reg) stateToSave,
				in(reg) Self::InternalLoadCPUExcutionState,
				);
		}
	}

	/// low level task switching code
	/// expected stack state:
	///-------------------------------- RSP
	/// saved rip
	/// excution state to save address
	/// excution state to load address
	#[naked]
	extern "C" fn InternalLoadCPUExcutionState ()
	{
		// switching task always between kernels code so cs and ss is known
		//
		// expected stack before saving state to struct
		//
		// ---------------------<-- RSP
		// rax
		// rbx
		// rcx
		// rdx
		// rdi
		// rsi
		// r8
		// r9
		// r10
		// r11
		// r12
		// r13
		// r14
		// r15
		// rbp
		// fs
		// gs
		// rip
		// cs
		// rflags
		// rsp
		// ss
		// old rax
		// saved rip
		// excution state to save address
		// excution state to load address

		unsafe
		{
			asm! (
				// save the state to stack
				"push rax",
				"mov ax, ss",
				"push rax",						// ss
				"push rsp",						// rsp
				"pushfq",						// rflags
				"mov ax, cs",
				"push rax",						// cs
				"mov rax, qword ptr [rsp+0x28]",
				"push rax",						// rip
				"add qword ptr [rsp+0x18], 0x18",	// adjust rsp
				"push gs",
				"push fs",
				"push rbp",
				"push r15",
				"push r14",
				"push r13",
				"push r12",
				"push r11",
				"push r10",
				"push r9",
				"push r8",
				"push rsi",
				"push rdi",
				"push rdx",
				"push rcx",
				"push rbx",
				"mov rax, qword ptr [rsp+0xa8]",
				"push rax",

				// save the state to struct
				"mov rdi, qword ptr [rsp+0xc0]",
				"mov rsi, rsp",
				"mov rcx, 22",
				"rep movsq",

				// begin to load new state
				"mov rdi, rsp",
				"mov rsi, qword ptr [rsp+0xc8]",
				"mov rcx, 22",
				"rep movsq",

				// load new state
				"pop rax",
				"pop rbx",
				"pop rcx",
				"pop rdx",
				"pop rdi",
				"pop rsi",
				"pop r8",
				"pop r9",
				"pop r10",
				"pop r11",
				"pop r12",
				"pop r13",
				"pop r14",
				"pop r15",
				"pop rbp",
				"pop fs",
				"pop gs",
				"iretq",
				options(noreturn),
				);
		}
	}

	/// load currrent executation state, will save all regs and load new regs value
	/// also change rsp0 in TSS
	pub fn Load (&mut self, stateToSave: &mut ThreadExecutionState)
	{
		// change kernel stack address in tss
		SMP::GetCurrentCPU ().SetKernelStackAddress (self.topKernelStackAddress);

		// load new state
		self.LoadCPUExcutionState (&mut stateToSave.cpuExecutionState);
	}
}
