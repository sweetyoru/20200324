use core::arch::asm;
use core::{fmt, mem};
use core::ops::{Index, IndexMut};
use core::cell::SyncUnsafeCell;

use alloc::vec::Vec;
use alloc::sync::Arc;

use crate::Lock::Mutex;
use crate::Thread::Thread;
use crate::Process::Process;
use crate::DataStruct::DefaultHashSet;
use crate::Interrupt::InterruptID;
use crate::Interrupt::InterruptError;
use crate::Arch::Amd64::ACPI::APICID;
use crate::Memory::Physical::PhysicalAddress;
use crate::Timer::{self, TimeDuration, TimeUnit};
use crate::Memory::Virtual::{VirtualAddress, KernelAddress, VirtualAddressSpace};

use crate::CPU::*;


pub (in crate::Arch::Amd64) mod APIC;


#[derive(Debug)]
pub (in crate::Arch::Amd64) struct MSRs();

impl MSRs
{
	pub const IA32_APIC_BASE: u32 = 0x01B;
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub (in crate::Arch::Amd64) enum GDTEntryType
{
	NullEntry,
	KernelCode,
	KernelData,
	UserCode,
	UserData,
	TSS,
}


// Normal entry is 8 bytes, but TSS entry is 16 bytes so this struct size is 16 bytes
// Normal entry will be packing with a following null entry
#[derive(Debug, Clone, Copy)]
pub (in crate::Arch::Amd64) struct GDTEntry(u128);

impl GDTEntry
{
	/// Every entry created by this function have a null entry before to align with TSS
	pub fn NewNormal (entryType: GDTEntryType) -> Self
	{
		// read both osdev wiki and intel manual for this
		// base31:24. .type base23:16 base15:0 limit15:0

		match entryType
		{
			GDTEntryType::NullEntry => Self(0x00_0_0_00_00_0000_0000_00000000_00000000),
			GDTEntryType::KernelCode => Self(0x00_a_f_9a_00_0000_ffff_00000000_00000000),
			GDTEntryType::KernelData => Self(0x00_a_f_92_00_0000_ffff_00000000_00000000),
			GDTEntryType::UserCode => Self(0x00_a_f_fa_00_0000_ffff_00000000_00000000),
			GDTEntryType::UserData => Self(0x00_a_f_f2_00_0000_ffff_00000000_00000000),
			GDTEntryType::TSS => panic! ("GDTEntry::NewNormal called with entryType == GDTEntryType::TSS"),
		}
	}

	pub fn NewTSS (entryType: GDTEntryType, TSSAddress: KernelAddress) -> Self
	{
		match entryType
		{
			GDTEntryType::TSS => (),
			_ => panic! ("GDTEntry::newTSS called with entryType != GDTEntryType::TSS"),
		}

		let mut newGDTEntry = 0u128;

		// limit[0..15]
		newGDTEntry |= (mem::size_of::<TSS> ()&0xffff) as u128;

		// base[0..15]
		newGDTEntry |= (TSSAddress.ToUsize () as u128 & 0xffffu128)<<16;

		// base[16..23]
		newGDTEntry |= (TSSAddress.ToUsize () as u128 & 0xff_0000u128)<<(32-16);

		// type
		newGDTEntry |= 0b1001u128<<(32+8);

		// present + DPL
		newGDTEntry |= 0b1000u128<<(32+12);

		// limit[16..19]
		newGDTEntry |= ((mem::size_of::<TSS> ()&0xf_0000)<<(32+16-16)) as u128;

		// more flags
		newGDTEntry |= 0b0000u128<<(32+20);

		// base[24..31]
		newGDTEntry |= (TSSAddress.ToUsize () as u128 & 0xff00_0000u128)<<(32+24-16-8);

		// base[32..63]
		newGDTEntry |= (TSSAddress.ToUsize () as u128 & 0xffffffff_00000000u128)<<(64-32);

		Self(newGDTEntry)
	}
}


#[derive(Debug, Clone, Copy)]
pub (in crate::Arch::Amd64) struct GDT([GDTEntry; Self::ENTRY_COUNT]);

impl GDT
{
	pub const ENTRY_COUNT: usize = 5;
	pub const MAX_ENTRY_INDEX: usize = Self::ENTRY_COUNT - 1;

	pub const TSS_ENTRY_INDEX: usize = 4;

	pub fn New () -> Self
	{
		let mut newGDT = Self([GDTEntry::NewNormal (GDTEntryType::NullEntry); Self::ENTRY_COUNT]);

		newGDT[0] = GDTEntry::NewNormal (GDTEntryType::KernelCode);
		newGDT[1] = GDTEntry::NewNormal (GDTEntryType::KernelData);
		newGDT[2] = GDTEntry::NewNormal (GDTEntryType::UserCode);
		newGDT[3] = GDTEntry::NewNormal (GDTEntryType::UserData);

		newGDT
	}

	pub fn Load (&self)
	{
		unsafe
		{
			let GDTR: u128 = ((&self.0 as *const GDTEntry as u128)<<16) + (Self::ENTRY_COUNT*mem::size_of::<GDTEntry> () - 1) as u128;

			asm! (
					"lgdt [rax]",
					"mov ax, 0x18",
					"mov ds, ax",
					"mov es, ax",
					"mov fs, ax",
					"mov gs, ax",
					"mov ss, ax",
					"push 0x08",
					"call 0f",
					"0:",
					"add qword ptr [rsp], 7",
					".byte 0x48",
					"retf",
					in ("rax") &GDTR,
				);
		}
	}
}

impl Index<usize> for GDT
{
	type Output = GDTEntry;

	fn index (&self, index: usize) -> &Self::Output
	{
		assert! (index <= Self::MAX_ENTRY_INDEX);
		&self.0[index]
	}
}

impl IndexMut<usize> for GDT
{
	fn index_mut (&mut self, index: usize) -> &mut Self::Output
	{
		assert! (index <= Self::MAX_ENTRY_INDEX);
		&mut self.0[index]
	}
}

#[derive(Debug, Clone, Copy)]
struct IDTEntry(u128);

impl IDTEntry
{
	const PRESENT: u128 = 1<<(32+15);
	const USER_CALLABLE: u128 = 0b11<<(32+13);
	const CODE_SEGMENT_SELECTOR_MASK: u128 = 0xffff_0000;
	const IST_MASK: u128 = 0x00ff_0000_0000;
	const TYPE_MASK: u128 = 0x0f00_0000_0000;
	const ADDRESS_MASK: u128 = 0xffff_ffff_ffff_0000_0000_ffff;

	pub fn New (entryType: IDTEntryType) -> Self
	{
		match entryType
		{
			IDTEntryType::InterruptGate => (),
			_ => panic! ("IDTEntry::new called with entryType != IDTEntryType::InterruptGate"),
		}

		let mut newIDTEntry = Self(0);
		newIDTEntry.SetType (IDTEntryType::InterruptGate);
		newIDTEntry
	}

	pub fn IsPresent (&self) -> bool { self.0 & Self::PRESENT == Self::PRESENT }
	pub fn IsUserCallable (&self) -> bool { self.0 & Self::USER_CALLABLE == Self::USER_CALLABLE }
	pub fn SetAsPresent (&mut self) { self.0 |= Self::PRESENT; }
	pub fn SetAsNotPresent (&mut self) { self.0 &= !Self::PRESENT; }
	pub fn SetUserCallable (&mut self) { self.0 |= Self::USER_CALLABLE; }
	pub fn SetUserNotCallable (&mut self) { self.0 &= !Self::USER_CALLABLE; }

	/// Don't care about code segment selector since x86_64 interrupt ignore code segment in IDT entry
	pub fn SetAddress (&mut self, address: KernelAddress)
	{
		// clear old address
		self.0 &= !Self::ADDRESS_MASK;

		// address[0..15]
		self.0 |= address.ToUsize () as u128 & 0xffffu128;

		// address[16..31]
		self.0 |= (address.ToUsize () as u128 &0xffff_0000u128)<<32;

		// address[32..63]
		self.0 |= (address.ToUsize () as u128 & 0xffffffff_00000000u128)<<32;

		// clear old code segment selector
		self.0 &= !Self::CODE_SEGMENT_SELECTOR_MASK;

		// set new code segment selector
		// hard-coded to 0x08: kernel code segment
		self.0 |= (0x08<<16) as u128;
	}

	pub fn SetIST (&mut self, ist: usize)
	{
		assert! (ist < 8);

		// clear old ist
		self.0 &= !Self::IST_MASK;

		// set new ist
		self.0 |= (ist<<32) as u128;
	}

	pub fn SetType (&mut self, entryType: IDTEntryType)
	{
		// clear old type
		self.0 &= !Self::TYPE_MASK;

		// set new type
		self.0 |= match entryType
		{
			IDTEntryType::InterruptGate => 0xeu128<<40,
			IDTEntryType::TrapGate => 0xfu128<<40,
			IDTEntryType::CallGate => 0xcu128<<40,
		};
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum InterruptVectorState
{
	Used(KernelAddress),
	Reserved,
	Free,
}


#[derive(Debug)]
struct IDT
{
	table: [IDTEntry; Self::ENTRY_NUM],
}

impl IDT
{
	const ENTRY_NUM: usize = 256;
	const MAX_ENTRY_INDEX: usize = Self::ENTRY_NUM - 1;

	pub fn New () -> Self
	{
		Self
		{
			table: [IDTEntry::New (IDTEntryType::InterruptGate); Self::ENTRY_NUM],
		}
	}

	pub fn Load (&self)
	{
		unsafe
		{
			let IDTR: u128 = ((&self.table as *const IDTEntry as u128)<<16) + (Self::ENTRY_NUM*mem::size_of::<IDTEntry> () - 1) as u128;

			asm! (
					"lidt [rax]",
					in ("rax") &IDTR,
				);
		}
	}

	pub (in crate::Arch::Amd64) fn InsertInterruptHandler (&mut self, index: usize, handlerAddress: KernelAddress, useDoubleFaultStack: bool, isUserCallable: bool)
	{
		self.table[index].SetAsNotPresent ();
		self.table[index].SetAddress (handlerAddress);
		self.table[index].SetIST (if useDoubleFaultStack { 1 } else { 0 });
		if isUserCallable { self.table[index].SetUserCallable (); }
		else { self.table[index].SetUserNotCallable (); }
		self.table[index].SetAsPresent ();
	}
}


#[repr(C, packed)]
#[derive(Debug, Clone, Copy)]
pub (in crate::Arch::Amd64) struct TSS
{
	pub reserved1: u32,
	pub rsp0: u64,
	pub rsp1: u64,
	pub rsp2: u64,
	pub reserved2: u64,
	pub ist1: u64,
	pub ist2: u64,
	pub ist3: u64,
	pub ist4: u64,
	pub ist5: u64,
	pub ist6: u64,
	pub ist7: u64,
	pub reserved3: u64,
	pub reserved4: u16,
	pub ioMapBaseAddress: u16,
}

impl TSS
{
	pub const fn New () -> Self
	{
		Self
		{
			reserved1: 0,
			rsp0: 0,
			rsp1: 0,
			rsp2: 0,
			reserved2: 0,
			ist1: 0,
			ist2: 0,
			ist3: 0,
			ist4: 0,
			ist5: 0,
			ist6: 0,
			ist7: 0,
			reserved3: 0,
			reserved4: 0,
			ioMapBaseAddress: 104,
		}
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct CPUID(u128);

impl CPUID
{
	pub fn New (id: u128) -> Self { Self(id) }
	pub fn ToU128 (&self) -> u128 { self.0 }
}


impl fmt::Display for CPUID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self.0)
	}
}


#[derive(Debug, Clone, Copy)]
pub struct CPUString
{
	value: [char; Self::MAX_LEN],
	len: usize
}

impl CPUString
{
	const MAX_LEN: usize = 64;

	pub fn New (value: [char; Self::MAX_LEN], len: usize) -> Self
	{
		assert! (len <= Self::MAX_LEN);

		Self
		{
			value,
			len
		}
	}
}


impl fmt::Display for CPUString
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		for i in 0..self.len
		{
			write! (f, "{}", self.value[i]).unwrap ();
		}

		write! (f, "")
	}
}


#[derive(Debug)]
struct CPUMutableData1
{
	isEnabled: bool,
	gdt: GDT,
	idt: IDT,
	tss: TSS,
	interruptVectorStates: [InterruptVectorState; IDT::ENTRY_NUM],
	currentProcess: Option<Arc<Process>>,
	currentThread: Option<Arc<Thread>>,
}

impl CPUMutableData1
{
	pub fn New () -> Self
	{
		Self
		{
			isEnabled: false,
			gdt: GDT::New (),
			idt: IDT::New (),
			tss: TSS::New (),
			interruptVectorStates: [InterruptVectorState::Free; IDT::ENTRY_NUM],
			currentProcess: None,
			currentThread: None,
		}
	}
}


/// This struct represent a cpu in the system
/// The cpu struct cannot be moved after creation
/// due to GDT hold a pointer to tss
#[derive(Debug)]
pub struct CPU
{
	id: CPUID,
	apicID: APICID,
	canEnable: bool,
	cpuString: CPUString,
	doubleFaultStack: [u8; Self::DOUBLE_FAULT_STACK_SIZE],

	cpuMutableData1: Mutex<CPUMutableData1>,
}

impl CPU
{
	pub const MAX_INTERRUPT_COUNT: usize = 256;
	pub const MIN_INTERRUPT_NUMBER: usize = 0;
	pub const MAX_INTERRUPT_NUMBER: usize = Self::MAX_INTERRUPT_COUNT - 1;

	const DOUBLE_FAULT_STACK_SIZE: usize = 16*1024;

	pub (in crate::Arch::Amd64) fn New (id: CPUID, apicID: APICID, canEnable: bool) -> Self
	{
		let mut value = ['a'; 64];

		fn GetChar (value: u32, position: usize) -> char
		{
			((value >> (8*position)) & 0xff) as u8 as char
		}

		for i in 0..3usize
		{
			let (eax, ebx, ecx, edx) = CPU::RunCPUID (0x80000002 + i as u32, 0);

			value[i*16 + 0] = GetChar (eax, 0);
			value[i*16 + 1] = GetChar (eax, 1);
			value[i*16 + 2] = GetChar (eax, 2);
			value[i*16 + 3] = GetChar (eax, 3);
			value[i*16 + 4] = GetChar (ebx, 0);
			value[i*16 + 5] = GetChar (ebx, 1);
			value[i*16 + 6] = GetChar (ebx, 2);
			value[i*16 + 7] = GetChar (ebx, 3);
			value[i*16 + 8] = GetChar (ecx, 0);
			value[i*16 + 9] = GetChar (ecx, 1);
			value[i*16 + 10] = GetChar (ecx, 2);
			value[i*16 + 11] = GetChar (ecx, 3);
			value[i*16 + 12] = GetChar (edx, 0);
			value[i*16 + 13] = GetChar (edx, 1);
			value[i*16 + 14] = GetChar (edx, 2);
			value[i*16 + 15] = GetChar (edx, 3);
		}

		let result = Self
		{
			id,
			apicID,
			canEnable,
			cpuString: CPUString::New (value, 48),
			doubleFaultStack: [0u8; Self::DOUBLE_FAULT_STACK_SIZE],

			cpuMutableData1: Mutex::New (CPUMutableData1::New ()),
		};

		let tssAddress = KernelAddress::New (&result.cpuMutableData1.Lock ().tss as *const TSS as usize);
		result.cpuMutableData1.Lock ().gdt[GDT::TSS_ENTRY_INDEX] = GDTEntry::NewTSS (GDTEntryType::TSS, tssAddress);

		result.cpuMutableData1.Lock ().tss.ist1 = &result.doubleFaultStack as *const u8 as usize as u64;

		result
	}

	pub fn ID (&self) -> CPUID { self.id }
	pub (in crate::Arch::Amd64) fn ApicID (&self) -> APICID { self.apicID }

	pub fn CanEnable (&self) -> bool { self.canEnable }
	pub const fn GetCPUString (&self) -> CPUString { self.cpuString }
	pub fn IsEnabled (&self) -> bool { self.cpuMutableData1.Lock ().isEnabled }
	pub fn SetEnabled (&self) { self.cpuMutableData1.Lock ().isEnabled = true; }

	pub (in crate::Arch::Amd64) fn LoadGDT (&self) { self.cpuMutableData1.Lock ().gdt.Load (); }

	pub (in crate::Arch::Amd64) fn LoadIDT (&self) { self.cpuMutableData1.Lock ().idt.Load (); }

	pub (in crate::Arch::Amd64) fn SetKernelStackAddress (&self, newKernelStackAddress: KernelAddress)
	{
		self.cpuMutableData1.Lock ().tss.rsp0 = newKernelStackAddress.ToU64 ();
	}

	pub fn GetCurrentProcess (&self) -> Arc<Process>
	{
		Arc::clone (self.cpuMutableData1.Lock ().currentProcess.as_ref ().unwrap ())
	}


	/// Set both current process and current thread to nextThread/nextProcess
	pub fn SetCurrentThread (&self, nextThread: Arc<Thread>)
	{
		let mut cpuMutableData1 = self.cpuMutableData1.Lock ();

		let nextProcess = nextThread.GetOwnerProcess ();

		// swap out the process
		// _currentProcess, _currentThread are for delayed process/thread drop until new porcess and new thread is already set
		let _currentProcess = cpuMutableData1.currentProcess.take ();
		let _currentThread = cpuMutableData1.currentThread.take ();

		cpuMutableData1.currentProcess = Some(nextProcess);
		cpuMutableData1.currentThread = Some(nextThread);

		// force cpuMutableData1 to drop before _currentProcess and _currentThread to release the lock
		mem::drop (cpuMutableData1)
	}

	pub fn GetCurrentThread (&self) -> Arc<Thread>
	{
		Arc::clone (self.cpuMutableData1.Lock ().currentThread.as_ref ().unwrap ())
	}

	pub fn GetCurrentVirtualAddressSpace (&self) -> Arc<VirtualAddressSpace>
	{
		self.GetCurrentProcess ().GetVirtualAddressSpace ()
	}

	pub (in crate::Arch::Amd64) fn GetCurrentApicID () -> APICID
	{
		let (_, _, _, edx) = Self::RunCPUID (0xb, 0);

		APICID::New (edx as usize)
	}

	pub fn GetCurrentCPUID () -> CPUID
	{
		CPUID::New (Self::GetCurrentApicID ().ToUsize ().try_into ().unwrap ())
	}

	pub (in crate::Arch::Amd64) fn GetRSP () -> u64
	{
		let result;

		unsafe
		{
			asm! (
					"mov rax, rsp",
					out ("rax") result,
				);
		}

		result
	}

	pub (in crate::Arch::Amd64) fn ReadMSR (msr: u32) -> u64
	{
		let eax: u32;
		let edx: u32;

		unsafe
		{
			asm! (
					"rdmsr",
					in ("ecx") msr,
					out ("eax") eax,
					out ("edx") edx,
				);
		}

		(edx as u64)<<32 | eax as u64
	}

	pub (in crate::Arch::Amd64) fn WriteMSR (msr: u32, value: u64)
	{
		let eax: u32 = (value&0xffff_ffff) as u32;
		let edx: u32 = ((value&0xffff_ffff_0000_0000)>>32) as u32;

		unsafe
		{
			asm! (
					"wrmsr",
					in ("ecx") msr,
					in ("eax") eax,
					in ("edx") edx,
				);
		}
	}

	pub (in crate::Arch::Amd64) fn ReadCR3 () -> u64
	{
		let result;

		unsafe
		{
			asm! (
					"mov rax, cr3",
					out ("rax") result,
				);
		}

		result
	}

	pub (in crate::Arch::Amd64) fn WriteCR3 (value: u64)
	{
		unsafe
		{
			asm! (
					"mov cr3, rax",
					in ("rax") value,
				);
		}
	}

	pub (in crate::Arch::Amd64) fn ReadCR4 () -> u64
	{
		let result;

		unsafe
		{
			asm! (
					"mov rax, cr4",
					out ("rax") result,
				);
		}

		result
	}

	pub (in crate::Arch::Amd64) fn WriteCR4 (value: u64)
	{
		unsafe
		{
			asm! (
					"mov cr4, rax",
					in ("rax") value,
				);
		}
	}

	pub fn EnableInterrupt () -> CPUInterruptState
	{
		let oldFlags: u64;
		const INTERRUPT_ENABLE: usize = 1<<9;

		unsafe
		{
			asm! (
					"pushfq",
					"pop rax",
					"sti",
					"nop",
					out ("rax") oldFlags,
				);
		}

		CPUInterruptState::New ((oldFlags as usize & INTERRUPT_ENABLE) != 0)
	}

	pub fn DisableInterrupt () -> CPUInterruptState
	{
		let oldFlags: u64;
		const INTERRUPT_ENABLE: usize = 1<<9;

		unsafe
		{
			asm! (
					"pushfq",
					"pop rax",
					"cli",
					"nop",
					out ("rax") oldFlags,
				);
		}

		CPUInterruptState::New ((oldFlags as usize & INTERRUPT_ENABLE) != 0)
	}

	pub fn RestoreInterruptState (oldCPUInterruptState: CPUInterruptState)
	{
		let input = if oldCPUInterruptState.IsInterrupted () { 1 } else { 0 };

		unsafe
		{
			asm! (
					"push rax",
					"push rbx",

					"pushfq",
					"pop rbx",
					"shl rax, 9",
					"or rbx, rax",
					"push rbx",
					"popfq",
					"nop",

					"pop rbx",
					"pop rax",

					in ("rax") input,
				);
		}
	}

	pub (in crate::Arch::Amd64) fn RunCPUID (function: u32, subFunction: u32) -> (u32, u32, u32, u32)
	{
		let eax;
		let ebx;
		let ecx;
		let edx;

		unsafe
		{
			asm! (
					"push rbx",
					"cpuid",
					"mov r8, rbx",
					"pop rbx",
					in ("eax") function,
					in ("ecx") subFunction,
					lateout ("eax") eax,
					lateout ("r8d") ebx,
					lateout ("ecx") ecx,
					lateout ("edx") edx,
				);
		}

		(eax, ebx, ecx, edx)
	}

	pub (in crate::Arch::Amd64) fn Is5LevelPagingSupported () -> bool
	{
		let (_, _, ecx, _) = Self::RunCPUID (7, 0);
		(ecx>>16)&0x1 == 1
	}

	pub (in crate::Arch::Amd64) fn Is1GPageSupported () -> bool
	{
		let (_, _, _, edx) = Self::RunCPUID (0x8000_0001, 0);
		(edx>>26)&0x1 == 1
	}

	pub (in crate::Arch::Amd64) fn GetMaxPagingLevel () -> usize
	{
		if Self::Is5LevelPagingSupported () { 5 } else { 4 }
	}

	pub fn Pause () { unsafe { asm! ("pause"); } }

	pub fn Halt () { unsafe { asm! ("hlt"); } }

	pub (in crate::Arch::Amd64) fn ReadTSC () -> u64
	{
		let eax: u32;
		let edx: u32;

		unsafe
		{
			asm! (
					"rdtsc",
					out ("eax") eax,
					out ("edx") edx,
				);
		}

		((edx as u64)<<32)|eax as u64
	}

	pub fn GetCPUSpeed () -> u64
	{
		let a = Self::ReadTSC ();
		Timer::Sleep (TimeDuration::New (TimeUnit::SECOND, 1));
		let b = Self::ReadTSC ();
		b - a
	}

	pub (in crate::Arch::Amd64) fn GetCoreID () -> usize
	{
		let (_, result, _, _) = Self::RunCPUID (1, 0);

		(result>>24) as usize
	}

	pub (in crate::Arch::Amd64) fn InsertInterruptHandler (&self, vector: InterruptVector, handlerAddress: KernelAddress, useDoubleFaultStack: bool, isUserCallable: bool) -> Result<InterruptVector, InterruptError>
	{
		let mut cpuMutableData1 = self.cpuMutableData1.Lock ();

		let index = vector.ToUsize ();

		// check state
		if let InterruptVectorState::Used(currentHandlerAddress) = cpuMutableData1.interruptVectorStates[index]
		{
			panic! ("Attempt to insert new interrupt handler ({}) to an used vector ({}, {})", handlerAddress, vector, currentHandlerAddress);
		}

		cpuMutableData1.idt.InsertInterruptHandler (index, handlerAddress, useDoubleFaultStack, isUserCallable);

		// change state
		cpuMutableData1.interruptVectorStates[index] = InterruptVectorState::Used(handlerAddress);

		Ok(vector)
	}

	pub (in crate::Arch::Amd64) fn GetFreeInterruptVectors (&self) -> Vec<InterruptVector>
	{
		let cpuMutableData1 = self.cpuMutableData1.Lock ();

		let mut result = Vec::new ();

		for index in Self::MAX_INTERRUPT_NUMBER..=Self::MAX_INTERRUPT_NUMBER
		{
			if let InterruptVectorState::Free = cpuMutableData1.interruptVectorStates[index]
			{
				result.push (InterruptVector::New (index));
			}
		}

		result
	}

	pub fn GetRandomNumberU64 () -> u64
	{
		let mut rax: u64;
		let mut rcx: u64;

		loop
		{
			unsafe
			{
				asm! (
						"xor rax, rax",
						"xor rcx, rcx",
						"rdrand rax",
						"adc rcx, 0",
						out ("rax") rax,
						out ("rcx") rcx,
					);
			}

			if rcx == 1 // data is valid
			{
				break;
			}
		}

		rax
	}

	pub fn GetRandomNumberU128 () -> u128
	{
		(Self::GetRandomNumberU64 () as u128) << 64 | (Self::GetRandomNumberU64 () as u128)
	}


	fn PatchAddress (address: VirtualAddress, baseAddress: PhysicalAddress)
	{
		let offset = *address.ToConst::<u32> ();
		let baseAddress: u32 = baseAddress.ToUsize ().try_into ().unwrap ();
		*address.ToMut::<u32> () = baseAddress + offset;
	}

	pub fn GetAPBootstrapCode (baseAddress: PhysicalAddress, topLevelPageTableAddress: PhysicalAddress, kernelEntryAddress: KernelAddress, kernelStackEndAddress: KernelAddress) -> Vec<u8>
	{
		let mut code = include_bytes! ("./CPU/APBootstrap.bin").to_vec ();

		// align check
		if !baseAddress.IsAlign (4096)
		{
			panic! ("Base Address ({}) is not align ({})", baseAddress, 4096);
		}

		if !topLevelPageTableAddress.IsAlign (4096)
		{
			panic! ("Top Level Page Table Address ({}) is not align ({})", topLevelPageTableAddress, 4096);
		}


		// check is baseAddress is < 1mb
		if baseAddress.ToUsize () >= 1*1024*1024
		{
			panic! ("Base Address ({}) is larger than limit ({})", baseAddress, PhysicalAddress::New (1*1024*1024 - 1));
		}


		// check topLevelPageTableAddress < 4gb
		if topLevelPageTableAddress.ToUsize () >= 4*1024*1024*1024
		{
			panic! ("Top Level Page Table Address ({}) is larger than limit ({})", topLevelPageTableAddress, PhysicalAddress::New (4*1024*1024*1024 - 1));
		}

		// patch code
		let codeAddress = VirtualAddress::New (code.as_mut_ptr () as usize);

		// patch offsets
		// 0x11
		// 0x19
		// 0x1f
		// 0x43
		// 0x5c
		// 0x76
		// 0xbb
		// 0xc8

		Self::PatchAddress (codeAddress + 0x11, baseAddress);
		Self::PatchAddress (codeAddress + 0x19, baseAddress);
		Self::PatchAddress (codeAddress + 0x1f, baseAddress);
		Self::PatchAddress (codeAddress + 0x43, baseAddress);
		Self::PatchAddress (codeAddress + 0x5c, baseAddress);
		Self::PatchAddress (codeAddress + 0x76, baseAddress);
		Self::PatchAddress (codeAddress + 0xbb, baseAddress);
		Self::PatchAddress (codeAddress + 0xc8, baseAddress);


		// patch data
		let BOOT_VIRTUAL_ADDRESS_SPACE_OFFSET = 4096 - 512 - 512;
		let KERNEL_ENTRY_ADDRESS_OFFSET = 4096 - 512 - 512 + 8;
		let KERNEL_STACK_END_ADDRESS_OFFSET = 4096 - 512 - 512 + 8 + 8;

		*(codeAddress + BOOT_VIRTUAL_ADDRESS_SPACE_OFFSET).ToMut::<u64> () = topLevelPageTableAddress.ToU64 ();
		*(codeAddress + KERNEL_ENTRY_ADDRESS_OFFSET).ToMut::<u64> () = kernelEntryAddress.ToU64 ();
		*(codeAddress + KERNEL_STACK_END_ADDRESS_OFFSET).ToMut::<u64> () = kernelStackEndAddress.ToU64 ();


		code
	}

	/// This function must be called once per CPU at boot
	pub fn EnableSecurityFlags ()
	{
		println! ("[CPU {}][+] Enable CPU Security Flags...", Self::GetCurrentCPUID ());

		const SMEP_ENABLE: u64 = 1<<20;
		const SMAP_ENABLE: u64 = 1<<21;

		static calledCPUID: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
		unsafe { assert! ((*calledCPUID.get ()).Insert (Self::GetCurrentCPUID ()).is_ok ()); }

		// enable SMEP
		println! ("[CPU {}][+] Enable SMEP...", Self::GetCurrentCPUID ());
		Self::WriteCR4 (Self::ReadCR4 ()|SMEP_ENABLE);

		// enable SMAP
		println! ("[CPU {}][+] Enable SMAP...", Self::GetCurrentCPUID ());
		Self::WriteCR4 (Self::ReadCR4 ()|SMAP_ENABLE);
	}

	pub (in crate::Arch::Amd64) fn InvalidTLB (pageAddress: VirtualAddress)
	{
		unsafe { asm!("invlpg [rax]", in("rax") pageAddress.ToU64 ()); }
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub (in crate::Arch::Amd64) struct InterruptVector(usize);

impl InterruptVector
{
	pub const fn New (value: usize) -> Self
	{
		assert! (value <= CPU::MAX_INTERRUPT_NUMBER);
		Self(value)
	}

	pub fn PriorityClass (&self) -> usize { (self.0&0xf0)>>4 }
	pub fn PrioritySubClass (&self) -> usize { self.0&0xf }
	pub fn ToUsize (&self) -> usize { self.0 }
}


impl From<InterruptID> for InterruptVector
{
	fn from (interruptID: InterruptID) -> Self
	{
		Self(interruptID.ToUsize ())
	}
}

impl fmt::Display for InterruptVector
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		writeln! (f, "Vector Number: 0x{:02x}", self.0).unwrap ();
		writeln! (f, "Priority Class: {}", self.PriorityClass ()).unwrap ();
		writeln! (f, "Priority Sub Class: {}", self.PrioritySubClass ())
	}
}


// the only type is used is InterruptGate
#[derive(Debug)]
enum IDTEntryType
{
	InterruptGate,
	TrapGate,
	CallGate,
}


pub fn BochsBrk ()
{
	unsafe { asm! ("xchg bx, bx"); }
}
