//! This module use cpu functions that's only available in crate::Arch


use core::fmt;
use core::arch::asm;
use core::cell::SyncUnsafeCell;

use crate::SMP;
use crate::DataStruct::DefaultHashSet;
use crate::Memory::Virtual::{VirtualAddress, KernelAddress};
use crate::Arch::Amd64::CPU::{self, CPUID, InterruptVector, APIC};
use crate::Arch::Amd64::Interrupt::IOAPIC::{InterruptLevel, InterruptDestinationMode, InterruptDeliveryMode};


use crate::Interrupt::*;


pub (in crate::Arch::Amd64) mod PIC;
pub (in crate::Arch::Amd64) mod IOAPIC;


#[repr(C, packed)]
pub (in crate::Arch::Amd64) struct InterruptStackFrame
{
	pub rip: VirtualAddress,
	pub cs: u64,
	pub rflags: u64,
	pub rsp: VirtualAddress,
	pub ss: u64,
}


impl fmt::Display for InterruptStackFrame
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		writeln! (f, "RIP: {}", { self.rip })?;
		writeln! (f, "CS: 0x{:X}", { self.cs })?;
		writeln! (f, "RFLAGS: 0x{:X}", { self.rflags })?;
		writeln! (f, "RSP: {}", { self.rsp })?;
		write! (f, "SS: 0x{:X}", { self.ss })?;

		Ok(())
	}
}


/// This function must be called only once per CPU at boot
pub fn InitInterrupt ()
{
	println! ("[CPU {}][+] Init Interrupt System...", CPU::CPU::GetCurrentCPUID ());

	static calledCPUID: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCPUID.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }


	// insert interrupt handlers
	println! ("[CPU {}][+] Insert Dummy CPU Interrupt Handler from 0->20...", CPU::CPU::GetCurrentCPUID ());
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (0), KernelAddress::New (Dummy0 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (1), KernelAddress::New (Dummy1 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (2), KernelAddress::New (Dummy2 as *const () as usize), true, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (3), KernelAddress::New (Dummy3 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (4), KernelAddress::New (Dummy4 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (5), KernelAddress::New (Dummy5 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (6), KernelAddress::New (Dummy6 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (7), KernelAddress::New (Dummy7 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (8), KernelAddress::New (Dummy8 as *const () as usize), true, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (9), KernelAddress::New (Dummy9 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (10), KernelAddress::New (Dummy10 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (11), KernelAddress::New (Dummy11 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (12), KernelAddress::New (Dummy12 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (13), KernelAddress::New (Dummy13 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (14), KernelAddress::New (Dummy14 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (16), KernelAddress::New (Dummy16 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (17), KernelAddress::New (Dummy17 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (18), KernelAddress::New (Dummy18 as *const () as usize), true, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (19), KernelAddress::New (Dummy19 as *const () as usize), false, false).unwrap ();
	SMP::GetCurrentCPU ().InsertInterruptHandler (InterruptVector::New (20), KernelAddress::New (Dummy20 as *const () as usize), false, false).unwrap ();


	// load IDT to current CPU
	println! ("[CPU {}][+] Load IDT...", CPU::CPU::GetCurrentCPUID ());
	SMP::GetCurrentCPU ().LoadIDT ();


	// load task register
	println! ("[CPU {}][+] Load Task Register...", CPU::CPU::GetCurrentCPUID ());
	unsafe
	{
		asm! (
				"push rax",
				"mov eax, 0x40",
				"ltr ax",
				"pop rax",
			);
	}
}


/// Add an interrupt handler based on priority for current CPU
pub fn AddInterruptHandler (handlerAddress: KernelAddress, interruptAllocationStrategy: InterruptAllocationStrategy, isUserCallable: bool) -> Result<InterruptID, InterruptError>
{
	// get the free interruptVertor
	let interruptVector = match interruptAllocationStrategy
	{
		InterruptAllocationStrategy::Fixed(interruptID) => interruptID.into (),
		InterruptAllocationStrategy::PriorityBased(priority) =>
		{
			let freeInterruptVectors = SMP::GetCurrentCPU ().GetFreeInterruptVectors ();

			if freeInterruptVectors.len () == 0
			{
				return Err(InterruptError::NoInterruptIDAvailable);
			}

			match priority
			{
				InterruptPriority::High => *freeInterruptVectors.iter ().max ().unwrap (),
				InterruptPriority::Low => *freeInterruptVectors.iter ().min ().unwrap (),
			}
		},
		InterruptAllocationStrategy::Any =>
		{
			let freeInterruptVectors = SMP::GetCurrentCPU ().GetFreeInterruptVectors ();

			if freeInterruptVectors.len () == 0
			{
				return Err(InterruptError::NoInterruptIDAvailable);
			}

			*freeInterruptVectors.iter ().min ().unwrap ()
		}
	};

	let interruptVector = SMP::GetCurrentCPU ().InsertInterruptHandler (interruptVector, handlerAddress, false, isUserCallable)?;

	Ok(InterruptID::New (interruptVector.ToUsize ()))
}


/// This function send a ipi to other cpu
pub fn SendIPI (targetCPUID: CPUID, interruptID: InterruptID)
{
	let targetApicID = SMP::GetCPU (targetCPUID).ApicID ();
	let vector = interruptID.into ();

	let destinationShortHand = InterruptDestinationShortHand::NoShortHand;
	let triggerMode = InterruptTriggerMode::Edge;

	// always Assert, except for INIT level de-assert
	let level = InterruptLevel::Assert;

	let destinationMode = InterruptDestinationMode::Physical;
	let deliveryMode = InterruptDeliveryMode::Fixed;

	APIC::SendIPI (targetApicID, destinationShortHand, triggerMode, level, destinationMode, deliveryMode, vector);
}


extern "x86-interrupt" fn Dummy0 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 0 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy1 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 1 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy2 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 2 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy3 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 3 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy4 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 4 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy5 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 5 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy6 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 6 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy7 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 7 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy8 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 8 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy9 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 9 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy10 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 10 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy11 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 11 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy12 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 12 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy13 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 13 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy14 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 14 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy15 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 15 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy16 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 16 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy17 (interruptStackFrame: InterruptStackFrame, errorCode: u64)
{
	println! ("Dummy 17 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	println! ("Error code: 0x{:x}", errorCode);
	loop {}
}

extern "x86-interrupt" fn Dummy18 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 18 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy19 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 19 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}

extern "x86-interrupt" fn Dummy20 (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy 20 called!");
	println! ("Stack frame:\n{}", interruptStackFrame);
	loop {}
}
