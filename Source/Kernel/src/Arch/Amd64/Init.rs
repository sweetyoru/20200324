//! this module will init kernel


use core::cell::SyncUnsafeCell;

use alloc::vec;

use crate::ICC;
use crate::IPC;
use crate::ISC;
use crate::SMP;
use crate::Arch;
use crate::Debug;
use crate::Timer;
use crate::Memory;
use crate::Process;
use crate::BootInfo;
use crate::Interrupt;
use crate::Scheduler;
use crate::Capability;
use crate::SystemCall;
use crate::Lock::OneTimeFlag;
use crate::CPU::{self, CPUID};
use crate::DataStruct::DefaultHashSet;
use crate::Memory::Virtual::VirtualAddress;
use crate::Memory::Physical::{PhysicalAddress, FrameAllocator::{self, FRAME_L1_SIZE}};


pub fn Init (avoidAreaAddress: u64)
{
	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	Debug::InitDebug ();

	// BootInfo is at the start of avoid area
	let bootInfoAddress = PhysicalAddress::New (avoidAreaAddress as usize);
	BootInfo::InitBootInfo (bootInfoAddress);
	println! ("[Boot CPU][+] Init BootInfo completed!");

	ISC::InitISC ();
	println! ("[Boot CPU][+] Init ISC completed!");

	FrameAllocator::InitFrameAllocatorPart1 ();
	println! ("[Boot CPU][+] Init Frame Allocator part 1 completed!");

	Memory::Virtual::InitBootVirtualAddressSpace ();
	println! ("[Boot CPU][+] Init Boot Virtual Address Space completed!");

	Memory::Heap::InitHeap ();
	println! ("[Boot CPU][+] Kernel Heap created!");

	Capability::InitCapability ();
	println! ("[Boot CPU][+] Init Capability System completed!");

	SMP::InitSMPPart1 ();
	println! ("[Boot CPU][+] Init SMP part 1 completed!");

	Process::InitProcess ();
	println! ("[Boot CPU][+] Init Process completed!");

	// get initrd
	println! ("[Boot CPU][+] Getting Initrd...");

	let mut initrd = vec! [0u8; BootInfo::GetBootInfo ().initrdSize];
	println! ("[Boot CPU][+] Initrd Size: 0x{:x}", initrd.len ());

	BootInfo::GetBootInfo ().initrdStartAddress.ReadToBuffer (VirtualAddress::New (initrd[..].as_mut_ptr () as usize), initrd.len ());

	println! ("[Boot CPU][+] Got Initrd!");

	FrameAllocator::InitFrameAllocatorPart2 ();
	println! ("[Boot CPU][+] Init Frame Allocator part 2 completed!");

	Arch::Amd64::ACPI::InitACPI ();
	println! ("[Boot CPU][+] Init ACPI completed!");

	Arch::Amd64::Interrupt::PIC::InitPIC ();
	println! ("[Boot CPU][+] Init PIC completed!");

	Arch::Amd64::Interrupt::IOAPIC::InitIOAPIC ();
	println! ("[Boot CPU][+] Init IOAPIC completed!");

	Scheduler::InitGlobalScheduler ();
	println! ("[Boot CPU][+] Init Global Scheduler completed!");

	Timer::ClockTimer::InitClockTimerGlobalPart ();
	println! ("[Boot CPU][+] Init Clock Timer global part completed!");

	IPC::InitIPC ();
	println! ("[Boot CPU][+] Init Inter Process communication completed!");

	ICC::InitICCGlobalPart ();
	println! ("[Boot CPU][+] Init ICC System global part completed!");

	CPU::BochsBrk ();
	// init cpu specific stuffs
	EachCPU ();


	SMP::InitSMPPart2 ();
	println! ("[Boot CPU][+] Init SMP part 2 completed!");

	println! ("[Boot CPU][+] Boot process completed!");
	println! ("[Boot CPU][+] Hello world!!!!!!!!!!!!!");

	MiscStuffs ();
}


/// This function must be called once per CPU at boot
pub fn EachCPU ()
{
	println! ("[CPU {}][+] Init CPU specific functions...", CPU::CPU::GetCurrentCPUID ());

	static calledCPUID: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCPUID.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	Memory::Virtual::EnablePagingFeatureFlags ();
	println! ("[CPU {}][+] Enable Paging Feature Flags completed!", CPU::CPU::GetCurrentCPUID ());

	// enable cpu security related flags
	CPU::CPU::EnableSecurityFlags ();
	println! ("[CPU {}][+] Enable CPU Security Flags completed!", CPU::CPU::GetCurrentCPUID ());

	Timer::ClockTimer::InitClockTimerLocalPart ();
	println! ("[CPU {}][+] Init Clock Timer local part completed!", CPU::CPU::GetCurrentCPUID ());

	Interrupt::InitInterrupt ();
	println! ("[CPU {}][+] Init Interrupt System completed!", CPU::CPU::GetCurrentCPUID ());

	Arch::Amd64::CPU::APIC::InitAPIC ();
	println! ("[CPU {}][+] Init APIC completed!", CPU::CPU::GetCurrentCPUID ());

	Timer::SchedulerTimer::InitSchedulerTimer ();
	println! ("[CPU {}][+] Init Scheduler Timer completed!", CPU::CPU::GetCurrentCPUID ());

	SystemCall::InitSystemCall ();
	println! ("[CPU {}][+] Init System Call completed!", CPU::CPU::GetCurrentCPUID ());

	ICC::InitICCLocalPart ();
	println! ("[CPU {}][+] Init ICC System local part completed!", CPU::CPU::GetCurrentCPUID ());

	CPU::CPU::EnableInterrupt ();
	println! ("[CPU {}][+] CPU Interrupt Enabled!", CPU::CPU::GetCurrentCPUID ());
}


fn MiscStuffs ()
{
	println! ("[Boot CPU][+] Low RAM: {} bytes", FrameAllocator::TotalLowFrameCount ()*FRAME_L1_SIZE);
	println! ("[Boot CPU][+] Normal RAM: {} bytes", FrameAllocator::TotalNormalFrameCount ()*FRAME_L1_SIZE);
	println! ("[Boot CPU][+] Total RAM: {} bytes", FrameAllocator::TotalFrameCount ()*FRAME_L1_SIZE);

	println! ("[Boot CPU][+] Free Low RAM left: {} bytes", FrameAllocator::FreeLowFrameCount ()*FRAME_L1_SIZE);
	println! ("[Boot CPU][+] Free Normal RAM left: {} bytes", FrameAllocator::FreeNormalFrameCount ()*FRAME_L1_SIZE);
	println! ("[Boot CPU][+] Free RAM left: {} bytes", FrameAllocator::FreeFrameCount ()*FRAME_L1_SIZE);

	let mut totalUsedRam = (FrameAllocator::TotalFrameCount () - FrameAllocator::FreeFrameCount ())*FRAME_L1_SIZE;

	let usedRamInMB = totalUsedRam / (1*1024*1024);

	totalUsedRam = totalUsedRam % (1*1024*1024);

	let usedRamInKB = totalUsedRam / (1*1024);

	totalUsedRam = totalUsedRam % (1*1024);

	let usedRamInBytes = totalUsedRam;

	println! ("[Boot CPU][+] Used RAM: {} MB {} KB {} bytes", usedRamInMB, usedRamInKB, usedRamInBytes);
	println! ("[Boot CPU][+] CPU string: {}", SMP::GetCurrentCPU ().GetCPUString ());
	println! ("[Boot CPU][+] Time to complete boot process: {}", Timer::ClockTimer::TimeFromBoot ());
}
