use core::fmt;
use core::num::Wrapping;
use core::cell::SyncUnsafeCell;

use alloc::string::String;

use crate::BootInfo;
use crate::Lock::OneTimeFlag;
use crate::Memory::Physical::PhysicalAddress;
use crate::Interrupt::{InterruptPinPolarity, InterruptTriggerMode};


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub (in crate::Arch::Amd64) struct APICID(usize);

impl APICID
{
	pub fn New (value: usize) -> Self { Self(value) }
	pub fn ToUsize (&self) -> usize { self.0 }
}


impl fmt::Display for APICID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:08x}", self.0)
	}
}


#[repr(C, packed)]
#[derive(Debug, Clone, Copy)]
pub (in crate::Arch::Amd64) struct RSDP
{
	signature: [u8; 8],
	checksum: u8,
	oemID: [u8; 6],
	revision: u8,
	rsdtAddress: u32,
	length: u32,
	xsdtAddress: u64,
	extendedChecksum: u8,
	reserved: [u8; 3],
}

impl RSDP
{
	const ACPI_VERSION_1: usize = 0;
	const ACPI_VERSION_2: usize = 2;

	pub fn New (pointerAddress: PhysicalAddress) -> Self
	{
		let result = pointerAddress.VolatileRead::<Self> ();
		assert! (result.signature == *b"RSD PTR ");

		// calc checksum 1
		let mut currentChecksum1 = Wrapping (0u8);

		for offset in 0..20
		{
			currentChecksum1 += (pointerAddress + offset.try_into ().unwrap ()).VolatileRead::<u8> ();
		}

		assert! (currentChecksum1.0 == 0);

		// calc checksum2
		let mut currentChecksum2 = Wrapping (0u8);

		for offset in 0..result.length
		{
			currentChecksum2 += (pointerAddress + offset.try_into ().unwrap ()).VolatileRead::<u8> ();
		}

		assert! (currentChecksum2.0 == 0);

		result
	}

	pub fn Version (&self) -> usize
	{
		self.revision.try_into ().unwrap ()
	}

	pub fn RSDTAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New (self.rsdtAddress.try_into ().unwrap ())
	}

	pub fn XSDTAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New (self.xsdtAddress.try_into ().unwrap ())
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub (in crate::Arch::Amd64) enum SdtTableType
{
	BERT,
	BGRT,
	CPEP,
	DSDT,
	ECDT,
	EINJ,
	ERST,
	FADT,
	FPDT,
	GTDT,
	HEST,
	HPET,
	MADT,
	MSCT,
	MPST,
	NFIT,
	PCCT,
	PMTT,
	PSDT,
	RASF,
	RSDT,
	SBST,
	SDEV,
	SLIT,
	SRAT,
	SSDT,
	XSDT,
	Unknown,
}


impl From<u32> for SdtTableType
{
	fn from (value: u32) -> Self
	{
		match value
		{
			x if x == u32::from_le_bytes (*b"BERT") => Self::BERT,
			x if x == u32::from_le_bytes (*b"BGRT") => Self::BGRT,
			x if x == u32::from_le_bytes (*b"CPEP") => Self::CPEP,
			x if x == u32::from_le_bytes (*b"DSDT") => Self::DSDT,
			x if x == u32::from_le_bytes (*b"ECDT") => Self::ECDT,
			x if x == u32::from_le_bytes (*b"EINJ") => Self::EINJ,
			x if x == u32::from_le_bytes (*b"ERST") => Self::ERST,
			x if x == u32::from_le_bytes (*b"FACP") => Self::FADT,
			x if x == u32::from_le_bytes (*b"FPDT") => Self::FPDT,
			x if x == u32::from_le_bytes (*b"GTDT") => Self::GTDT,
			x if x == u32::from_le_bytes (*b"HEST") => Self::HEST,
			x if x == u32::from_le_bytes (*b"HPET") => Self::HPET,
			x if x == u32::from_le_bytes (*b"APIC") => Self::MADT,
			x if x == u32::from_le_bytes (*b"MSCT") => Self::MSCT,
			x if x == u32::from_le_bytes (*b"MPST") => Self::MPST,
			x if x == u32::from_le_bytes (*b"NFIT") => Self::NFIT,
			x if x == u32::from_le_bytes (*b"PCCT") => Self::PCCT,
			x if x == u32::from_le_bytes (*b"PMTT") => Self::PMTT,
			x if x == u32::from_le_bytes (*b"PSDT") => Self::PSDT,
			x if x == u32::from_le_bytes (*b"RASF") => Self::RASF,
			x if x == u32::from_le_bytes (*b"RSDT") => Self::RSDT,
			x if x == u32::from_le_bytes (*b"SDEV") => Self::SDEV,
			x if x == u32::from_le_bytes (*b"SBST") => Self::SBST,
			x if x == u32::from_le_bytes (*b"SLIT") => Self::SLIT,
			x if x == u32::from_le_bytes (*b"SRAT") => Self::SRAT,
			x if x == u32::from_le_bytes (*b"SSDT") => Self::SSDT,
			x if x == u32::from_le_bytes (*b"XSDT") => Self::XSDT,
			_ => Self::Unknown,
		}
	}
}


pub (in crate::Arch::Amd64) trait SdtTable
{
	const HEADER_SIZE: usize = 36;

	const SIGNATURE_OFFSET: usize = 0;
	const LENGTH_OFFSET: usize = 4;
	const REVISION_OFFSET: usize = 8;
	const CHECKSUM_OFFSET: usize = 9;
	const OEM_ID_OFFSET: usize = 10;
	const OEM_TABLE_ID_OFFSET: usize = 16;
	const OEM_REVISON_OFFSET: usize = 24;
	const CREATOR_ID_OFFSET: usize = 28;
	const CREATOR_REVISION_OFFSET: usize = 32;

	// signature: [u8; 4],
	// length: u32,
	// revison: u8,
	// checksum: u8,
	// oemID: [u8; 6],
	// oemTableID: [u8; 8],
	// oemRevison: u32,
	// creatorID: u32,
	// creatorRevision: u32,

	fn HeaderAddress (&self) -> PhysicalAddress;
	fn TableAddress (&self) -> PhysicalAddress { self.HeaderAddress () }
	fn DataAddress (&self) -> PhysicalAddress { self.TableAddress () + self.HeaderSize () }
	fn DataSize (&self) -> usize { self.Length () - self.HeaderSize () }
	fn TableType (&self) -> SdtTableType;

	fn IsChecksumValid (&self) -> bool
	{
		let mut currentChecksum = Wrapping (0u8);

		for offset in 0..self.Length ()
		{
			currentChecksum += (self.TableAddress () + offset).VolatileRead::<u8> ();
		}

		currentChecksum.0 == 0
	}

	fn HeaderSize (&self) -> usize { Self::HEADER_SIZE }

	fn Signature (&self) -> u32
	{
		(self.HeaderAddress () + Self::SIGNATURE_OFFSET).VolatileRead::<u32> ()
	}

	fn Length (&self) -> usize
	{
		(self.HeaderAddress () + Self::LENGTH_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	fn Revision (&self) -> usize
	{
		(self.HeaderAddress () + Self::REVISION_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	fn Checksum (&self) -> usize
	{
		(self.HeaderAddress () + Self::CHECKSUM_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	fn OemRevision (&self) -> usize
	{
		(self.HeaderAddress () + Self::OEM_REVISON_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	fn CreatorID (&self) -> usize
	{
		(self.HeaderAddress () + Self::CREATOR_ID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	fn CreatorRevision (&self) -> usize
	{
		(self.HeaderAddress () + Self::CREATOR_REVISION_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct SdtTablePointer
{
	tableAddress: PhysicalAddress,
}

impl SdtTablePointer
{
	pub fn New (tableAddress: PhysicalAddress) -> Self
	{
		let result = Self { tableAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for SdtTablePointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.tableAddress }
	fn TableType (&self) -> SdtTableType { self.Signature ().into () }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct BERTPointer
{
	bertAddress: PhysicalAddress,
}

impl BERTPointer
{
	pub fn New (bertAddress: PhysicalAddress) -> Self
	{
		let result = Self { bertAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for BERTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.bertAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::BERT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct BGRTPointer
{
	bgrtAddress: PhysicalAddress,
}

impl BGRTPointer
{
	pub fn New (bgrtAddress: PhysicalAddress) -> Self
	{
		let result = Self { bgrtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for BGRTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.bgrtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::BGRT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct CPEPPointer
{
	cpepAddress: PhysicalAddress,
}

impl CPEPPointer
{
	pub fn New (cpepAddress: PhysicalAddress) -> Self
	{
		let result = Self { cpepAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for CPEPPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.cpepAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::CPEP }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct DSDTPointer
{
	dsdtAddress: PhysicalAddress,
}

impl DSDTPointer
{
	pub fn New (dsdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { dsdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for DSDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.dsdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::DSDT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct ECDTPointer
{
	ecdtAddress: PhysicalAddress,
}

impl ECDTPointer
{
	pub fn New (ecdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { ecdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for ECDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.ecdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::ECDT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct EINJPointer
{
	einjAddress: PhysicalAddress,
}

impl EINJPointer
{
	pub fn New (einjAddress: PhysicalAddress) -> Self
	{
		let result = Self { einjAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for EINJPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.einjAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::EINJ }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct ERSTPointer
{
	erstAddress: PhysicalAddress,
}

impl ERSTPointer
{
	pub fn New (erstAddress: PhysicalAddress) -> Self
	{
		let result = Self { erstAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for ERSTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.erstAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::ERST }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct FADTPointer
{
	fadtAddress: PhysicalAddress,
}

impl FADTPointer
{
	pub fn New (fadtAddress: PhysicalAddress) -> Self
	{
		let result = Self { fadtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for FADTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.fadtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::FADT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct GTDTPointer
{
	gtdtAddress: PhysicalAddress,
}

impl GTDTPointer
{
	pub fn New (gtdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { gtdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for GTDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.gtdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::GTDT }
}


#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub (in crate::Arch::Amd64) struct HpetAddress
{
	addressSpaceID: u8,
	regBitWidth: u8,
	regBitOffset: u8,
	reversed: u8,
	address: u64,
}

impl HpetAddress
{
	pub fn BaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New (self.address.try_into ().unwrap ())
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct HPETPointer
{
	hpetAddress: PhysicalAddress,
}

impl HPETPointer
{
	const EVENT_TIMER_BLOCK_ID_OFFSET: usize = 36;
	const HPET_ADDRESS_OFFSET: usize = 40;
	const HPET_NUMBER_OFFSET: usize = 52;
	const MAIN_COUNTER_MINIMUM_CLOCK_TICK_OFFSET: usize = 53;
	const PAGE_PROTECTION_AND_OEM_ATTRIBUTE_OFFSET: usize = 55;

	pub fn New (hpetAddress: PhysicalAddress) -> Self
	{
		let result = Self { hpetAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}

	pub fn EventTimerBlockID (&self) -> u32
	{
		(self.hpetAddress + Self::EVENT_TIMER_BLOCK_ID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn HpetAddress (&self) -> HpetAddress
	{
		(self.hpetAddress + Self::HPET_ADDRESS_OFFSET).VolatileRead::<HpetAddress> ()
	}

	pub fn HpetNumber (&self) -> usize
	{
		(self.hpetAddress + Self::HPET_NUMBER_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn MainCounterMinimumClockTick (&self) -> usize
	{
		(self.hpetAddress + Self::MAIN_COUNTER_MINIMUM_CLOCK_TICK_OFFSET).VolatileRead::<u16> ().try_into ().unwrap ()
	}

	pub fn PageProtectionAndOEMAttribute (&self) -> u8
	{
		(self.hpetAddress + Self::PAGE_PROTECTION_AND_OEM_ATTRIBUTE_OFFSET).VolatileRead::<u8> ()
	}

	pub fn HardwareRevisionID (&self) -> usize
	{
		(self.hpetAddress + Self::EVENT_TIMER_BLOCK_ID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn ComparatorCount (&self) -> usize
	{
		(((self.hpetAddress + Self::EVENT_TIMER_BLOCK_ID_OFFSET + 1).VolatileRead::<u8> () & 0b11111) + 1).try_into ().unwrap ()
	}

	pub fn CounterSize (&self) -> usize
	{
		match (self.hpetAddress + Self::EVENT_TIMER_BLOCK_ID_OFFSET).VolatileRead::<u32> () & (1<<13)
		{
			0 => 32,
			_ => 64,
		}
	}

	pub fn LegacyReplacement (&self) -> usize
	{
		match (self.hpetAddress + Self::EVENT_TIMER_BLOCK_ID_OFFSET).VolatileRead::<u32> () & (1<<15)
		{
			0 => 0,
			_ => 1,
		}
	}
}


impl SdtTable for HPETPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.hpetAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::HPET }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct HESTPointer
{
	hestAddress: PhysicalAddress,
}

impl HESTPointer
{
	pub fn New (hestAddress: PhysicalAddress) -> Self
	{
		let result = Self { hestAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for HESTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.hestAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::HEST }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) enum MADTEntryType
{
	ProcessorLocalAPIC,
	IOAPIC,
	InterruptSourceOverride,
	NMISource,
	LocalAPICNMI,
	LocalAPICAddressOverride,
	IOSAPIC,
	LocalSAPIC,
	PlatformInterruptSource,
	ProcessorLocalX2APIC,
	LocalX2APICNMI,
	GICCPUInterface,
	GICDistributor,
	GICMSIFrame,
	GICRedistributor,
	GICInterruptTranslationService,
	Unknown,
}


impl From<u8> for MADTEntryType
{
	fn from (value: u8) -> Self
	{
		match value
		{
			0x00 => Self::ProcessorLocalAPIC,
			0x01 => Self::IOAPIC,
			0x02 => Self::InterruptSourceOverride,
			0x03 => Self::NMISource,
			0x04 => Self::LocalAPICNMI,
			0x05 => Self::LocalAPICAddressOverride,
			0x06 => Self::IOSAPIC,
			0x07 => Self::LocalSAPIC,
			0x08 => Self::PlatformInterruptSource,
			0x09 => Self::ProcessorLocalX2APIC,
			0x0a => Self::LocalX2APICNMI,
			0x0b => Self::GICCPUInterface,
			0x0c => Self::GICDistributor,
			0x0d => Self::GICMSIFrame,
			0x0e => Self::GICRedistributor,
			0x0f => Self::GICInterruptTranslationService,
			_ => Self::Unknown,
		}
	}
}


pub (in crate::Arch::Amd64) trait MADTEntry
{
	const ENTRY_TYPE_OFFSET: usize = 0;
	const ENTRY_LENGTH_OFFSET: usize = 1;

	fn EntryAddress (&self) -> PhysicalAddress;
	fn EntryType (&self) -> MADTEntryType;

	fn Length (&self) -> usize
	{
		(self.EntryAddress () + Self::ENTRY_LENGTH_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	fn Type (&self) -> u8
	{
		(self.EntryAddress () + Self::ENTRY_TYPE_OFFSET).VolatileRead::<u8> ()
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct MADTEntryPointer
{
	entryAddress: PhysicalAddress,
}

impl MADTEntryPointer
{
	pub fn New (entryAddress: PhysicalAddress) -> Self { Self { entryAddress } }
}


impl MADTEntry for MADTEntryPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.entryAddress }

	fn EntryType (&self) -> MADTEntryType
	{
		self.Type ().into ()
	}
}


#[derive(Debug)]
pub (in crate::Arch::Amd64) struct MADTEntryIter
{
	nextEntryAddress: PhysicalAddress,
	remainingLength: usize,
}

impl MADTEntryIter
{
	pub fn New (nextEntryAddress: PhysicalAddress, remainingLength: usize) -> Self
	{
		Self
		{
			nextEntryAddress,
			remainingLength,
		}
	}
}


impl Iterator for MADTEntryIter
{
	type Item = MADTEntryPointer;

	fn next (&mut self) -> Option<Self::Item>
	{
		if self.remainingLength > 0
		{
			let entry = Self::Item::New (self.nextEntryAddress);
			self.nextEntryAddress += entry.Length ();
			self.remainingLength -= entry.Length ();

			return Some(entry);
		}

		None
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct ProcessorLocalAPICPointer
{
	processorLocalAPICAddress: PhysicalAddress,
}

impl ProcessorLocalAPICPointer
{
	const ACPI_PROCESSOR_ID_OFFSET: usize = 2;
	const APICID_OFFSET: usize = 3;
	const FLAGS_OFFSET: usize = 4;

	const FLAG_ENABLED: u32 = 1<<0;
	const FLAG_ONLINE_CAPBABLE: u32 = 1<<1;

	pub fn New (processorLocalAPICAddress: PhysicalAddress) -> Self
	{
		Self
		{
			processorLocalAPICAddress
		}
	}

	pub fn ACPIProcessorID (&self) -> u8
	{
		(self.processorLocalAPICAddress + Self::ACPI_PROCESSOR_ID_OFFSET).VolatileRead::<u8> ()
	}

	pub fn ApicID (&self) -> APICID
	{
		APICID::New ((self.processorLocalAPICAddress + Self::APICID_OFFSET).VolatileRead::<u8> ().into ())
	}

	pub fn Flags (&self) -> u32
	{
		(self.processorLocalAPICAddress + Self::FLAGS_OFFSET).VolatileRead::<u32> ()
	}

	pub fn Enabled (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ENABLED != 0
	}

	pub fn OnlineCapable (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ONLINE_CAPBABLE != 0
	}

	pub fn CanEnable (&self) -> bool
	{
		self.Enabled () || self.OnlineCapable ()
	}
}


impl MADTEntry for ProcessorLocalAPICPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.processorLocalAPICAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::ProcessorLocalAPIC }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct IOAPICPointer
{
	ioapicAddress: PhysicalAddress,
}

impl IOAPICPointer
{
	const IOAPIC_ID_OFFSET: usize = 2;
	const IOAPIC_ADDRESS_OFFSET: usize = 4;
	const GLOBAL_SYSTEM_INTERRUPT_BASE_OFFSET: usize = 8;

	pub fn New (ioapicAddress: PhysicalAddress) -> Self
	{
		Self
		{
			ioapicAddress
		}
	}

	pub fn IOApicID (&self) -> APICID
	{
		APICID::New ((self.ioapicAddress + Self::IOAPIC_ID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ())
	}

	pub fn IOApicAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.ioapicAddress + Self::IOAPIC_ADDRESS_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ())
	}

	pub fn GlobalSystemInterruptBase (&self) -> usize
	{
		(self.ioapicAddress + Self::GLOBAL_SYSTEM_INTERRUPT_BASE_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}
}


impl MADTEntry for IOAPICPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.ioapicAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::IOAPIC }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct InterruptSourceOverridePointer
{
	interruptSourceOverrideAddress: PhysicalAddress,
}

impl InterruptSourceOverridePointer
{
	const BUS_OFFSET: usize = 2;
	const SOURCE_OFFSET: usize = 3;
	const GLOBAL_SYSTEM_INTERRUPT_OFFSET: usize = 4;
	const FLAGS_OFFSET: usize = 8;

	const FLAG_POLARITY_SHIFT: usize = 0;
	const FLAG_TRIGGER_MODE_SHIFT: usize = 2;

	const FLAG_POLARITY_MASK: u16 = 0b11<<Self::FLAG_POLARITY_SHIFT;
	const FLAG_TRIGGER_MODE_MASK: u16 = 0b11<<Self::FLAG_TRIGGER_MODE_SHIFT;

	pub fn New (interruptSourceOverrideAddress: PhysicalAddress) -> Self
	{
		Self
		{
			interruptSourceOverrideAddress
		}
	}

	pub fn Bus (&self) -> usize
	{
		(self.interruptSourceOverrideAddress + Self::BUS_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn Source (&self) -> usize
	{
		(self.interruptSourceOverrideAddress + Self::SOURCE_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn GlobalSystemInterrupt (&self) -> usize
	{
		(self.interruptSourceOverrideAddress + Self::GLOBAL_SYSTEM_INTERRUPT_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn Flags (&self) -> u16
	{
		(self.interruptSourceOverrideAddress + Self::FLAGS_OFFSET).VolatileRead::<u16> ()
	}

	pub fn Polarity (&self) -> InterruptPinPolarity
	{
		match (self.Flags ()&Self::FLAG_POLARITY_MASK)>>Self::FLAG_POLARITY_SHIFT
		{
			0b00 => InterruptPinPolarity::FollowBus,
			0b01 => InterruptPinPolarity::HighActive,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptPinPolarity::LowActive,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn TriggerMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG_TRIGGER_MODE_MASK)>>Self::FLAG_TRIGGER_MODE_SHIFT
		{
			0b00 => InterruptTriggerMode::FollowBus,
			0b01 => InterruptTriggerMode::Edge,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptTriggerMode::Level,
			value => panic! ("Unknown value ({})", value),
		}
	}
}


impl MADTEntry for InterruptSourceOverridePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.interruptSourceOverrideAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::InterruptSourceOverride }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct NMISourcePointer
{
	nmiSourceAddress: PhysicalAddress,
}

impl NMISourcePointer
{
	const FLAGS_OFFSET: usize = 2;
	const GLOBAL_SYSTEM_INTERRUPT_OFFSET: usize = 4;

	const FLAG_POLARITY_SHIFT: usize = 0;
	const FLAG_TRIGGER_MODE_SHIFT: usize = 2;

	const FLAG_POLARITY_MASK: u16 = 0b11<<Self::FLAG_POLARITY_SHIFT;
	const FLAG_TRIGGER_MODE_MASK: u16 = 0b11<<Self::FLAG_TRIGGER_MODE_SHIFT;

	pub fn New (nmiSourceAddress: PhysicalAddress) -> Self
	{
		Self
		{
			nmiSourceAddress
		}
	}

	pub fn Flags (&self) -> u16
	{
		(self.nmiSourceAddress + Self::FLAGS_OFFSET).VolatileRead::<u16> ()
	}

	pub fn GlobalSystemInterrupt (&self) -> usize
	{
		(self.nmiSourceAddress + Self::GLOBAL_SYSTEM_INTERRUPT_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn Polarity (&self) -> InterruptPinPolarity
	{
		match (self.Flags ()&Self::FLAG_POLARITY_MASK)>>Self::FLAG_POLARITY_SHIFT
		{
			0b00 => InterruptPinPolarity::FollowBus,
			0b01 => InterruptPinPolarity::HighActive,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptPinPolarity::LowActive,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn TriggerMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG_TRIGGER_MODE_MASK)>>Self::FLAG_TRIGGER_MODE_SHIFT
		{
			0b00 => InterruptTriggerMode::FollowBus,
			0b01 => InterruptTriggerMode::Edge,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptTriggerMode::Level,
			value => panic! ("Unknown value ({})", value),
		}
	}
}


impl MADTEntry for NMISourcePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.nmiSourceAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::NMISource }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct LocalAPICNMIPointer
{
	localAPICNMIAddress: PhysicalAddress,
}

impl LocalAPICNMIPointer
{
	const ACPI_PROCESSOR_UID_OFFSET: usize = 2;
	const FLAGS_OFFSET: usize = 3;
	const LOCAL_APIC_LINT_OFFSET: usize = 5;

	const FLAG_POLARITY_SHIFT: usize = 0;
	const FLAG_TRIGGER_MODE_SHIFT: usize = 2;

	const FLAG_POLARITY_MASK: u16 = 0b11<<Self::FLAG_POLARITY_SHIFT;
	const FLAG_TRIGGER_MODE_MASK: u16 = 0b11<<Self::FLAG_TRIGGER_MODE_SHIFT;

	pub fn New (localAPICNMIAddress: PhysicalAddress) -> Self
	{
		Self
		{
			localAPICNMIAddress
		}
	}

	pub fn AcpiProcessorUID (&self) -> usize
	{
		(self.localAPICNMIAddress + Self::ACPI_PROCESSOR_UID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn Flags (&self) -> u16
	{
		(self.localAPICNMIAddress + Self::FLAGS_OFFSET).VolatileRead::<u16> ()
	}

	pub fn LocalApicLint (&self) -> usize
	{
		(self.localAPICNMIAddress + Self::LOCAL_APIC_LINT_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn Polarity (&self) -> InterruptPinPolarity
	{
		match (self.Flags ()&Self::FLAG_POLARITY_MASK)>>Self::FLAG_POLARITY_SHIFT
		{
			0b00 => InterruptPinPolarity::FollowBus,
			0b01 => InterruptPinPolarity::HighActive,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptPinPolarity::LowActive,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn TriggerMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG_TRIGGER_MODE_MASK)>>Self::FLAG_TRIGGER_MODE_SHIFT
		{
			0b00 => InterruptTriggerMode::FollowBus,
			0b01 => InterruptTriggerMode::Edge,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptTriggerMode::Level,
			value => panic! ("Unknown value ({})", value),
		}
	}
}


impl MADTEntry for LocalAPICNMIPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.localAPICNMIAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::LocalAPICNMI }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct LocalAPICAddressOverridePointer
{
	localAPICAddressOverrideAddress: PhysicalAddress,
}

impl LocalAPICAddressOverridePointer
{
	const LOCAL_APIC_ADDRESS_OFFSET: usize = 4;

	pub fn New (localAPICAddressOverrideAddress: PhysicalAddress) -> Self
	{
		Self
		{
			localAPICAddressOverrideAddress
		}
	}

	pub fn LocalApicAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.localAPICAddressOverrideAddress + Self::LOCAL_APIC_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}
}


impl MADTEntry for LocalAPICAddressOverridePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.localAPICAddressOverrideAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::LocalAPICAddressOverride }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct IOSAPICPointer
{
	iosapicAddress: PhysicalAddress,
}

impl IOSAPICPointer
{
	const IOAPIC_ID_OFFSET: usize = 2;
	const GLOBAL_SYSTEM_INTERRUPT_BASE_OFFSET: usize = 4;
	const IOSAPIC_ADDRESS_OFFSET: usize = 8;

	pub fn New (iosapicAddress: PhysicalAddress) -> Self
	{
		Self
		{
			iosapicAddress
		}
	}

	pub fn IOApicID (&self) -> usize
	{
		(self.iosapicAddress + Self::IOAPIC_ID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn GlobalSystemInterruptBase (&self) -> usize
	{
		(self.iosapicAddress + Self::GLOBAL_SYSTEM_INTERRUPT_BASE_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn IOSapicAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.iosapicAddress + Self::IOSAPIC_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}
}


impl MADTEntry for IOSAPICPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.iosapicAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::IOSAPIC }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct LocalSAPICPointer
{
	localSAPICAddress: PhysicalAddress,
}

impl LocalSAPICPointer
{
	const ACPI_PROCESSOR_ID_OFFSET: usize = 2;
	const LOCAL_SAPIC_ID_OFFSET: usize = 3;
	const LOCAL_SAPIC_EID_OFFSET: usize = 4;
	const FLAGS_OFFSET: usize = 8;
	const ACPI_PROCESSOR_UID_VALUE_OFFSET: usize = 12;
	const ACPI_PROCESSOR_UID_STRING_OFFSET: usize = 16;

	const FLAG_ENABLED: u32 = 1<<0;
	const FLAG_ONLINE_CAPBABLE: u32 = 1<<1;

	pub fn New (localSAPICAddress: PhysicalAddress) -> Self
	{
		Self
		{
			localSAPICAddress
		}
	}

	pub fn AcpiProcessorID (&self) -> usize
	{
		(self.localSAPICAddress + Self::ACPI_PROCESSOR_ID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn LocalSapicID (&self) -> usize
	{
		(self.localSAPICAddress + Self::LOCAL_SAPIC_ID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn LocalSapicEID (&self) -> usize
	{
		(self.localSAPICAddress + Self::LOCAL_SAPIC_EID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn Flags (&self) -> u32
	{
		(self.localSAPICAddress + Self::FLAGS_OFFSET).VolatileRead::<u32> ()
	}

	pub fn AcpiProcessorUIDValue (&self) -> usize
	{
		(self.localSAPICAddress + Self::ACPI_PROCESSOR_UID_VALUE_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn AcpiProcessorUIDString (&self) -> String
	{
		let nextCharacterAddress = self.localSAPICAddress + Self::ACPI_PROCESSOR_UID_STRING_OFFSET;
		let mut result = String::new ();

		loop
		{
			let c = nextCharacterAddress.VolatileRead::<u8> ();

			if c == 0
			{
				break;
			}

			result.push (c.into ());
		}

		result
	}

	pub fn Enabled (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ENABLED != 0
	}

	pub fn OnlineCapable (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ONLINE_CAPBABLE != 0
	}
}


impl MADTEntry for LocalSAPICPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.localSAPICAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::LocalSAPIC }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct PlatformInterruptSourcePointer
{
	platformInterruptSourceAddress: PhysicalAddress,
}

impl PlatformInterruptSourcePointer
{
	const FLAGS_OFFSET: usize = 2;
	const INTERRUPT_TYPE_OFFSET: usize = 4;
	const PROCESSOR_ID_OFFSET: usize = 5;
	const PROCESSOR_EID_OFFSET: usize = 6;
	const IOSAPIC_VECTOR_OFFSET: usize = 7;
	const GLOBAL_SYSTEM_INTERRUPT_OFFSET: usize = 8;
	const PLATFORM_INTERRUPT_SOURCE_FLAGS_OFFSET: usize = 12;

	const FLAG1_POLARITY_SHIFT: usize = 0;
	const FLAG1_TRIGGER_MODE_SHIFT: usize = 2;

	const FLAG1_POLARITY_MASK: u16 = 0b11<<Self::FLAG1_POLARITY_SHIFT;
	const FLAG1_TRIGGER_MODE_MASK: u16 = 0b11<<Self::FLAG1_TRIGGER_MODE_SHIFT;

	const FLAG2_CPEI_PROCESSOR_OVERRIDE: u32 = 1<<0;

	pub fn New (platformInterruptSourceAddress: PhysicalAddress) -> Self
	{
		Self
		{
			platformInterruptSourceAddress
		}
	}

	pub fn Flags (&self) -> u16
	{
		(self.platformInterruptSourceAddress + Self::FLAGS_OFFSET).VolatileRead::<u16> ()
	}

	pub fn InterruptType (&self) -> u8
	{
		(self.platformInterruptSourceAddress + Self::INTERRUPT_TYPE_OFFSET).VolatileRead::<u8> ()
	}

	pub fn ProcessorID (&self) -> usize
	{
		(self.platformInterruptSourceAddress + Self::PROCESSOR_ID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn ProcessorEID (&self) -> usize
	{
		(self.platformInterruptSourceAddress + Self::PROCESSOR_EID_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn IOSapicVector (&self) -> usize
	{
		(self.platformInterruptSourceAddress + Self::IOSAPIC_VECTOR_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn GlobalSystemInterrupt (&self) -> usize
	{
		(self.platformInterruptSourceAddress + Self::GLOBAL_SYSTEM_INTERRUPT_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn PlatformInterruptSourceFlags (&self) -> u32
	{
		(self.platformInterruptSourceAddress + Self::PLATFORM_INTERRUPT_SOURCE_FLAGS_OFFSET).VolatileRead::<u32> ()
	}

	pub fn Polarity (&self) -> InterruptPinPolarity
	{
		match (self.Flags ()&Self::FLAG1_POLARITY_MASK)>>Self::FLAG1_POLARITY_SHIFT
		{
			0b00 => InterruptPinPolarity::FollowBus,
			0b01 => InterruptPinPolarity::HighActive,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptPinPolarity::LowActive,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn TriggerMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG1_TRIGGER_MODE_MASK)>>Self::FLAG1_TRIGGER_MODE_SHIFT
		{
			0b00 => InterruptTriggerMode::FollowBus,
			0b01 => InterruptTriggerMode::Edge,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptTriggerMode::Level,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn CpeiProcessorOverride (&self) -> bool
	{
		self.PlatformInterruptSourceFlags ()&Self::FLAG2_CPEI_PROCESSOR_OVERRIDE != 0
	}
}


impl MADTEntry for PlatformInterruptSourcePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.platformInterruptSourceAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::PlatformInterruptSource }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct ProcessorLocalX2APICPointer
{
	processorLocalX2APICAddress: PhysicalAddress,
}

impl ProcessorLocalX2APICPointer
{
	const X2APICID_OFFSET: usize = 4;
	const FLAGS_OFFSET: usize = 8;
	const ACPI_PROCESSOR_UID_OFFSET: usize = 12;

	const FLAG_ENABLED: u32 = 1<<0;
	const FLAG_ONLINE_CAPBABLE: u32 = 1<<1;

	pub fn New (processorLocalX2APICAddress: PhysicalAddress) -> Self
	{
		Self
		{
			processorLocalX2APICAddress
		}
	}

	pub fn X2ApicID (&self) -> APICID
	{
		APICID::New ((self.processorLocalX2APICAddress + Self::X2APICID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ())
	}

	pub fn Flags (&self) -> u32
	{
		(self.processorLocalX2APICAddress + Self::FLAGS_OFFSET).VolatileRead::<u32> ()
	}

	pub fn AcpiProcessorUID (&self) -> usize
	{
		(self.processorLocalX2APICAddress + Self::ACPI_PROCESSOR_UID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn Enabled (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ENABLED != 0
	}

	pub fn OnlineCapable (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ONLINE_CAPBABLE != 0
	}

	pub fn CanEnable (&self) -> bool
	{
		self.Enabled () || self.OnlineCapable ()
	}
}


impl MADTEntry for ProcessorLocalX2APICPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.processorLocalX2APICAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::ProcessorLocalX2APIC }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct LocalX2APICNMIPointer
{
	localX2APICNMIAddress: PhysicalAddress,
}

impl LocalX2APICNMIPointer
{
	const FLAGS_OFFSET: usize = 2;
	const ACPI_PROCESSOR_UID_OFFSET: usize = 4;
	const LOCAL_X2APIC_LINT_OFFSET: usize = 8;

	const FLAG_POLARITY_SHIFT: usize = 0;
	const FLAG_TRIGGER_MODE_SHIFT: usize = 2;

	const FLAG_POLARITY_MASK: u16 = 0b11<<Self::FLAG_POLARITY_SHIFT;
	const FLAG_TRIGGER_MODE_MASK: u16 = 0b11<<Self::FLAG_TRIGGER_MODE_SHIFT;

	pub fn New (localX2APICNMIAddress: PhysicalAddress) -> Self
	{
		Self
		{
			localX2APICNMIAddress
		}
	}

	pub fn Flags (&self) -> u16
	{
		(self.localX2APICNMIAddress + Self::FLAGS_OFFSET).VolatileRead::<u16> ()
	}

	pub fn AcpiProcessorUID (&self) -> usize
	{
		(self.localX2APICNMIAddress + Self::ACPI_PROCESSOR_UID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn LocalX2ApicLint (&self) -> usize
	{
		(self.localX2APICNMIAddress + Self::LOCAL_X2APIC_LINT_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn Polarity (&self) -> InterruptPinPolarity
	{
		match (self.Flags ()&Self::FLAG_POLARITY_MASK)>>Self::FLAG_POLARITY_SHIFT
		{
			0b00 => InterruptPinPolarity::FollowBus,
			0b01 => InterruptPinPolarity::HighActive,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptPinPolarity::LowActive,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn TriggerMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG_TRIGGER_MODE_MASK)>>Self::FLAG_TRIGGER_MODE_SHIFT
		{
			0b00 => InterruptTriggerMode::FollowBus,
			0b01 => InterruptTriggerMode::Edge,
			0b10 => panic! ("Reserved value"),
			0b11 => InterruptTriggerMode::Level,
			value => panic! ("Unknown value ({})", value),
		}
	}
}


impl MADTEntry for LocalX2APICNMIPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.localX2APICNMIAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::LocalX2APICNMI }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct GICCPUInterfacePointer
{
	gicCPUInterfaceAddress: PhysicalAddress,
}

impl GICCPUInterfacePointer
{
	const CPU_INTERFACE_NUMBER_OFFSET: usize = 4;
	const ACPI_PROCESSOR_UID_OFFSET: usize = 8;
	const FLAGS_OFFSET: usize = 12;
	const PARKING_PROTOCOL_VERSION_OFFSET: usize = 16;
	const PERFORMANCE_INTERRUPT_GSIV_OFFSET: usize = 20;
	const PARKED_ADDRESS_OFFSET: usize = 24;
	const PHYSICAL_BASE_ADDRESS_OFFSET: usize = 32;
	const GICV_OFFSET: usize = 40;
	const GICH_OFFSET: usize = 48;
	const VGIC_MAINTENANCE_INTERRUPT_OFFSET: usize = 56;
	const GICR_BASE_ADDRESS_OFFSET: usize = 60;
	const MPIDR_OFFSET: usize = 68;
	const PROCESSOR_POWER_EFFICIENCY_OFFSET: usize = 76;
	const SPE_OVERFLOW_INTERRUPT_OFFSET: usize = 78;

	const FLAG_ENABLED: u32 = 1<<0;

	const FLAG_PERFORMANCE_INTERRUPT_MODE_SHIFT: u32 = 1;
	const FLAG_VGIC_MAINTENANCE_INTERRUPT_MODE_SHIFT: u32 = 2;

	const FLAG_PERFORMANCE_INTERRUPT_MODE_MASK: u32 = 1<<Self::FLAG_PERFORMANCE_INTERRUPT_MODE_SHIFT;
	const FLAG_VGIC_MAINTENANCE_INTERRUPT_MODE_MASK: u32 = 1<<Self::FLAG_VGIC_MAINTENANCE_INTERRUPT_MODE_SHIFT;

	pub fn New (gicCPUInterfaceAddress: PhysicalAddress) -> Self
	{
		Self
		{
			gicCPUInterfaceAddress
		}
	}

	pub fn CPUInterfaceNumber (&self) -> usize
	{
		(self.gicCPUInterfaceAddress + Self::CPU_INTERFACE_NUMBER_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn AcpiProcessorUID (&self) -> usize
	{
		(self.gicCPUInterfaceAddress + Self::ACPI_PROCESSOR_UID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn Flags (&self) -> u32
	{
		(self.gicCPUInterfaceAddress + Self::FLAGS_OFFSET).VolatileRead::<u32> ()
	}

	pub fn ParkingProtocolVersion (&self) -> usize
	{
		(self.gicCPUInterfaceAddress + Self::PARKING_PROTOCOL_VERSION_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn PerformanceInterruptGSIV (&self) -> u32
	{
		(self.gicCPUInterfaceAddress + Self::PERFORMANCE_INTERRUPT_GSIV_OFFSET).VolatileRead::<u32> ()
	}

	pub fn ParkedAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicCPUInterfaceAddress + Self::PARKED_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn PhysicalBaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicCPUInterfaceAddress + Self::PHYSICAL_BASE_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn GICV (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicCPUInterfaceAddress + Self::GICV_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn GICH (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicCPUInterfaceAddress + Self::GICH_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn VGICMaintenanceInterrupt (&self) -> u32
	{
		(self.gicCPUInterfaceAddress + Self::VGIC_MAINTENANCE_INTERRUPT_OFFSET).VolatileRead::<u32> ()
	}

	pub fn GICRBaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicCPUInterfaceAddress + Self::GICR_BASE_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn MPIDR (&self) -> u64
	{
		(self.gicCPUInterfaceAddress + Self::MPIDR_OFFSET).VolatileRead::<u64> ()
	}

	pub fn ProcessorPowerEfficiencyClass (&self) -> usize
	{
		(self.gicCPUInterfaceAddress + Self::PROCESSOR_POWER_EFFICIENCY_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}

	pub fn SPEOverflowInterrupt (&self) -> u16
	{
		(self.gicCPUInterfaceAddress + Self::SPE_OVERFLOW_INTERRUPT_OFFSET).VolatileRead::<u16> ()
	}

	pub fn Enabled (&self) -> bool
	{
		self.Flags ()&Self::FLAG_ENABLED != 0
	}

	pub fn PerformanceInterruptMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG_PERFORMANCE_INTERRUPT_MODE_MASK)>>Self::FLAG_PERFORMANCE_INTERRUPT_MODE_SHIFT
		{
			0b0 => InterruptTriggerMode::Level,
			0b1 => InterruptTriggerMode::Edge,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn VGICMaintenanceInterruptMode (&self) -> InterruptTriggerMode
	{
		match (self.Flags ()&Self::FLAG_VGIC_MAINTENANCE_INTERRUPT_MODE_MASK)>>Self::FLAG_VGIC_MAINTENANCE_INTERRUPT_MODE_SHIFT
		{
			0b0 => InterruptTriggerMode::Level,
			0b1 => InterruptTriggerMode::Edge,
			value => panic! ("Unknown value ({})", value),
		}
	}
}


impl MADTEntry for GICCPUInterfacePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.gicCPUInterfaceAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::GICCPUInterface }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct GICDistributorPointer
{
	gicDistributorAddress: PhysicalAddress,
}

impl GICDistributorPointer
{
	const GIC_ID_OFFSET: usize = 4;
	const PHYSICAL_BASE_ADDRESS_OFFSET: usize = 8;
	const SYSTEM_VECTOR_BASE_OFFSET: usize = 16;
	const GIC_VERSION_OFFSET: usize = 20;

	pub fn New (gicDistributorAddress: PhysicalAddress) -> Self
	{
		Self
		{
			gicDistributorAddress
		}
	}

	pub fn GICID (&self) -> usize
	{
		(self.gicDistributorAddress + Self::GIC_ID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn PhysicalBaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicDistributorAddress + Self::PHYSICAL_BASE_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn SystemVectorBase (&self) -> u32
	{
		(self.gicDistributorAddress + Self::SYSTEM_VECTOR_BASE_OFFSET).VolatileRead::<u32> ()
	}

	pub fn GICVersion (&self) -> usize
	{
		(self.gicDistributorAddress + Self::GIC_VERSION_OFFSET).VolatileRead::<u8> ().try_into ().unwrap ()
	}
}


impl MADTEntry for GICDistributorPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.gicDistributorAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::GICDistributor }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct GICMSIFramePointer
{
	gicMSIFrameAddress: PhysicalAddress,
}

impl GICMSIFramePointer
{
	const GIC_MSI_FRAME_ID_OFFSET: usize = 0;
	const PHYSICAL_BASE_ADDRESS_OFFSET: usize = 0;
	const FLAGS_OFFSET: usize = 0;
	const SPI_COUNT_OFFSET: usize = 0;
	const SPI_BASE_OFFSET: usize = 0;

	const FLAG_SPI_COUNT_BASE_SELECT: u32 = 1<<0;

	pub fn New (gicMSIFrameAddress: PhysicalAddress) -> Self
	{
		Self
		{
			gicMSIFrameAddress
		}
	}

	pub fn GICMSIFrameID (&self) -> usize
	{
		(self.gicMSIFrameAddress + Self::GIC_MSI_FRAME_ID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn PhysicalBaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicMSIFrameAddress + Self::PHYSICAL_BASE_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn Flags (&self) -> u32
	{
		(self.gicMSIFrameAddress + Self::FLAGS_OFFSET).VolatileRead::<u32> ()
	}

	pub fn SPICount (&self) -> usize
	{
		(self.gicMSIFrameAddress + Self::SPI_COUNT_OFFSET).VolatileRead::<u16> ().try_into ().unwrap ()
	}

	pub fn SPIBase (&self) -> u16
	{
		(self.gicMSIFrameAddress + Self::SPI_BASE_OFFSET).VolatileRead::<u16> ()
	}

	pub fn SPICountBaseSelect (&self) -> bool
	{
		self.Flags ()&Self::FLAG_SPI_COUNT_BASE_SELECT != 0
	}
}


impl MADTEntry for GICMSIFramePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.gicMSIFrameAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::GICMSIFrame }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct GICRedistributorPointer
{
	gicRedistributorAddress: PhysicalAddress,
}

impl GICRedistributorPointer
{
	const DISCOVERY_RANGE_BASE_ADDRESS_OFFSET: usize = 4;
	const DISCOVERY_RANGE_LENGTH_OFFSET: usize = 12;

	pub fn New (gicRedistributorAddress: PhysicalAddress) -> Self
	{
		Self
		{
			gicRedistributorAddress
		}
	}

	pub fn DiscoveryRangeBaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicRedistributorAddress + Self::DISCOVERY_RANGE_BASE_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}

	pub fn DiscoveryRangeLength (&self) -> usize
	{
		(self.gicRedistributorAddress + Self::DISCOVERY_RANGE_LENGTH_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}
}


impl MADTEntry for GICRedistributorPointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.gicRedistributorAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::GICRedistributor }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct GICInterruptTranslationServicePointer
{
	gicInterruptTranslationServiceAddress: PhysicalAddress,
}

impl GICInterruptTranslationServicePointer
{
	const GIC_ITS_ID_OFFSET: usize = 4;
	const PHYSICAL_BASE_ADDRESS_OFFSET: usize = 8;

	pub fn New (gicInterruptTranslationServiceAddress: PhysicalAddress) -> Self
	{
		Self
		{
			gicInterruptTranslationServiceAddress
		}
	}

	pub fn GICITSID (&self) -> usize
	{
		(self.gicInterruptTranslationServiceAddress + Self::GIC_ITS_ID_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ()
	}

	pub fn PhysicalBaseAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.gicInterruptTranslationServiceAddress + Self::PHYSICAL_BASE_ADDRESS_OFFSET).VolatileRead::<u64> ().try_into ().unwrap ())
	}
}


impl MADTEntry for GICInterruptTranslationServicePointer
{
	fn EntryAddress (&self) -> PhysicalAddress { self.gicInterruptTranslationServiceAddress }
	fn EntryType (&self) -> MADTEntryType { MADTEntryType::GICInterruptTranslationService }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct MADTPointer
{
	madtAddress: PhysicalAddress,
}

impl MADTPointer
{
	const LOCAL_APIC_ADDRESS_OFFSET: usize = 0x24;
	const FLAGS_OFFSET: usize = 0x28;

	pub fn New (madtAddress: PhysicalAddress) -> Self
	{
		let result = Self { madtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}

	pub fn Entries (&self) -> MADTEntryIter
	{
		MADTEntryIter::New (self.DataAddress () + 4 + 4, self.Length () - self.HeaderSize () - 4 - 4)
	}

	pub fn LocalApicAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New ((self.TableAddress () + Self::LOCAL_APIC_ADDRESS_OFFSET).VolatileRead::<u32> ().try_into ().unwrap ())
	}
}


impl SdtTable for MADTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.madtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::MADT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct MSCTPointer
{
	msctAddress: PhysicalAddress,
}

impl MSCTPointer
{
	pub fn New (msctAddress: PhysicalAddress) -> Self
	{
		let result = Self { msctAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for MSCTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.msctAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::MSCT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct MPSTPointer
{
	mpstAddress: PhysicalAddress,
}

impl MPSTPointer
{
	pub fn New (mpstAddress: PhysicalAddress) -> Self
	{
		let result = Self { mpstAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for MPSTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.mpstAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::MPST }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct NFITPointer
{
	nfitAddress: PhysicalAddress,
}

impl NFITPointer
{
	pub fn New (nfitAddress: PhysicalAddress) -> Self
	{
		let result = Self { nfitAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for NFITPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.nfitAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::NFIT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct PCCTPointer
{
	pcctAddress: PhysicalAddress,
}

impl PCCTPointer
{
	pub fn New (pcctAddress: PhysicalAddress) -> Self
	{
		let result = Self { pcctAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for PCCTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.pcctAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::PCCT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct PMTTPointer
{
	pmttAddress: PhysicalAddress,
}

impl PMTTPointer
{
	pub fn New (pmttAddress: PhysicalAddress) -> Self
	{
		let result = Self { pmttAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for PMTTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.pmttAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::PMTT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct PSDTPointer
{
	psdtAddress: PhysicalAddress,
}

impl PSDTPointer
{
	pub fn New (psdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { psdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for PSDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.psdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::PSDT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct RASFPointer
{
	rasfAddress: PhysicalAddress,
}

impl RASFPointer
{
	pub fn New (rasfAddress: PhysicalAddress) -> Self
	{
		let result = Self { rasfAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for RASFPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.rasfAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::RASF }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct RSDTPointer
{
	rsdtAddress: PhysicalAddress,
}

impl RSDTPointer
{
	const ENTRY_SIZE: usize = 4;

	pub fn New (rsdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { rsdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}

	pub fn TableCount (&self) -> usize
	{
		self.DataSize ()/Self::ENTRY_SIZE
	}

	pub fn GetTable (&self, index: usize) -> SdtTablePointer
	{
		// check length
		let offset = index*Self::ENTRY_SIZE;

		if offset + Self::ENTRY_SIZE > self.DataSize ()
		{
			panic! ("Out of bound ({}) entry read ({})", self.DataSize (), index);
		}

		SdtTablePointer::New (PhysicalAddress::New ((self.DataAddress () + offset).VolatileRead::<u32> ().try_into ().unwrap ()))
	}
}


impl SdtTable for RSDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.rsdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::RSDT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct SBSTPointer
{
	sbstAddress: PhysicalAddress,
}

impl SBSTPointer
{
	pub fn New (sbstAddress: PhysicalAddress) -> Self
	{
		let result = Self { sbstAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for SBSTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.sbstAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::SBST }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct SDEVPointer
{
	sdevAddress: PhysicalAddress,
}

impl SDEVPointer
{
	pub fn New (sdevAddress: PhysicalAddress) -> Self
	{
		let result = Self { sdevAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for SDEVPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.sdevAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::SDEV }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct SLITPointer
{
	slitAddress: PhysicalAddress,
}

impl SLITPointer
{
	pub fn New (slitAddress: PhysicalAddress) -> Self
	{
		let result = Self { slitAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for SLITPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.slitAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::SLIT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct SRATPointer
{
	sratAddress: PhysicalAddress,
}

impl SRATPointer
{
	pub fn New (sratAddress: PhysicalAddress) -> Self
	{
		let result = Self { sratAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for SRATPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.sratAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::SRAT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct SSDTPointer
{
	ssdtAddress: PhysicalAddress,
}

impl SSDTPointer
{
	pub fn New (ssdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { ssdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}
}


impl SdtTable for SSDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.ssdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::SSDT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct XSDTPointer
{
	xsdtAddress: PhysicalAddress,
}

impl XSDTPointer
{
	const ENTRY_SIZE: usize = 8;

	pub fn New (xsdtAddress: PhysicalAddress) -> Self
	{
		let result = Self { xsdtAddress };
		assert! (result.TableType () == result.Signature ().into ());
		assert! (result.Length () >= result.HeaderSize ());
		assert! (result.IsChecksumValid ());

		result
	}

	pub fn TableCount (&self) -> usize
	{
		self.DataSize ()/Self::ENTRY_SIZE
	}

	pub fn GetTable (&self, index: usize) -> SdtTablePointer
	{
		// check length
		let offset = index*Self::ENTRY_SIZE;

		if offset + Self::ENTRY_SIZE > self.DataSize ()
		{
			panic! ("Out of bound ({}) entry read ({})", self.DataSize (), index);
		}

		SdtTablePointer::New (PhysicalAddress::New ((self.DataAddress () + offset).VolatileRead::<u64> ().try_into ().unwrap ()))
	}
}


impl SdtTable for XSDTPointer
{
	fn HeaderAddress (&self) -> PhysicalAddress { self.xsdtAddress }
	fn TableType (&self) -> SdtTableType { SdtTableType::XSDT }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum RootTablePointerType
{
	RSDT(RSDTPointer),
	XSDT(XSDTPointer),
}


#[derive(Debug)]
pub (in crate::Arch::Amd64) struct RootTableIter<'a>
{
	rootTablePointerRef: &'a RootTablePointer,
	nextIndex: usize,
	tableCount: usize,
}

impl<'a> RootTableIter<'a>
{
	pub fn New (rootTablePointerRef: &'a RootTablePointer) -> Self
	{
		Self
		{
			rootTablePointerRef,
			nextIndex: 0,
			tableCount: rootTablePointerRef.TableCount (),
		}
	}
}


impl<'a> Iterator for RootTableIter<'a>
{
	type Item = SdtTablePointer;

	fn next (&mut self) -> Option<Self::Item>
	{
		if self.nextIndex < self.tableCount
		{
			self.nextIndex += 1;
			return Some(self.rootTablePointerRef.GetTable (self.nextIndex - 1));
		}

		None
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub (in crate::Arch::Amd64) struct RootTablePointer
{
	pointer: RootTablePointerType,
}

impl RootTablePointer
{
	pub fn New (rsdp: &RSDP) -> Self
	{
		match rsdp.Version ()
		{
			RSDP::ACPI_VERSION_1 =>
			{
				Self
				{
					pointer: RootTablePointerType::RSDT(RSDTPointer::New (rsdp.RSDTAddress ()))
				}
			},
			RSDP::ACPI_VERSION_2 =>
			{
				Self
				{
					pointer: RootTablePointerType::XSDT(XSDTPointer::New (rsdp.XSDTAddress ()))
				}
			},
			_ => panic! ("Unknown rsdp version ({})", rsdp.Version ()),
		}
	}

	fn TableCount (&self) -> usize
	{
		match self.pointer
		{
			RootTablePointerType::RSDT(rsdt) => rsdt.TableCount (),
			RootTablePointerType::XSDT(xsdt) => xsdt.TableCount (),
		}
	}

	fn GetTable (&self, index: usize) -> SdtTablePointer
	{
		match self.pointer
		{
			RootTablePointerType::RSDT(rsdt) => rsdt.GetTable (index),
			RootTablePointerType::XSDT(xsdt) => xsdt.GetTable (index),
		}
	}

	pub fn Tables (&self) -> RootTableIter
	{
		RootTableIter::New (self)
	}

	pub fn GetSdtTableAddress (&self, SdtTableType: SdtTableType) -> Option<PhysicalAddress>
	{
		for table in self.Tables ()
		{
			if table.TableType () == SdtTableType
			{
				return Some(table.TableAddress ())
			}
		}

		None
	}
}


pub (in crate::Arch::Amd64) fn GetRootTablePointer () -> Option<RootTablePointer>
{
	let rsdp = RSDP::New (BootInfo::GetBootInfo ().rsdpAddress);
	Some(RootTablePointer::New (&rsdp))
}


pub (in crate::Arch::Amd64) fn GetSdtTableAddress (SdtTableType: SdtTableType) -> Option<PhysicalAddress>
{
	GetRootTablePointer ()?.GetSdtTableAddress (SdtTableType)
}


/// This function must be called only once at boot by boot CPU
pub (in crate::Arch::Amd64) fn InitACPI ()
{
	println! ("[Boot CPU][+] Init ACPI...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }
}

