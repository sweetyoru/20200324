use core::fmt;

use crate::Memory::Physical::FrameAllocator::{FRAME_L1_SIZE, FRAME_L2_SIZE, FRAME_L3_SIZE};
use crate::Memory::Virtual::{VirtualAddress, VirtualAddressSpace};


#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct PhysicalAddress(u64);

impl PhysicalAddress
{
	pub const fn New (address: usize) -> Self { Self(address as u64) }

	pub const fn Align (&self, align: usize) -> Self
	{
		Self::New (((self.ToUsize () + align - 1) / align) * align)
	}

	pub const fn AlignDown (&self, align: usize) -> Self
	{
		Self::New ((self.ToUsize () / align) * align)
	}

	pub const fn IsAlign (&self, align: usize) -> bool { (self.ToUsize () % align) == 0 }

	pub const fn IsValidTableAddress (&self) -> bool { self.ToUsize () % FRAME_L1_SIZE == 0 }
	pub const fn IsValidFrameL1Address (&self) -> bool { self.ToUsize () % FRAME_L1_SIZE == 0 }
	pub const fn IsValidFrameL2Address (&self) -> bool { self.ToUsize () % FRAME_L2_SIZE == 0 }
	pub const fn IsValidFrameL3Address (&self) -> bool { self.ToUsize () % FRAME_L3_SIZE == 0 }

	pub const fn ToUsize (&self) -> usize { self.0 as usize }
	pub (in crate::Arch::Amd64) const fn ToU64 (&self) -> u64 { self.0 as u64 }

	pub fn VolatileRead<T> (&self) -> T
	{
		VirtualAddressSpace::PhysicalVolatileRead::<T> (*self)
	}

	pub fn VolatileWrite<T> (&self, value: T)
	{
		VirtualAddressSpace::PhysicalVolatileWrite::<T> (*self, value)
	}

	pub fn VolatileReadToBuffer (&self, buffer: VirtualAddress, len: usize)
	{
		VirtualAddressSpace::PhysicalVolatileReadToBuffer (*self, buffer, len);
	}

	pub fn VolatileWriteFromBuffer (&self, buffer: VirtualAddress, len: usize)
	{
		VirtualAddressSpace::PhysicalVolatileWriteFromBuffer (buffer, *self, len);
	}

	pub fn Read<T> (&self) -> T
	{
		VirtualAddressSpace::PhysicalRead::<T> (*self)
	}

	pub fn Write<T> (&self, value: T)
	{
		VirtualAddressSpace::PhysicalWrite::<T> (*self, value)
	}

	pub fn ReadToBuffer (&self, buffer: VirtualAddress, len: usize)
	{
		VirtualAddressSpace::PhysicalReadToBuffer (*self, buffer, len);
	}

	pub fn WriteFromBuffer (&self, buffer: VirtualAddress, len: usize)
	{
		VirtualAddressSpace::PhysicalWriteFromBuffer (buffer, *self, len);
	}

	pub fn MemorySet (&self, len: usize, pattern: &[u8])
	{
		VirtualAddressSpace::PhysicalMemSet (*self, len, pattern);
	}
}


impl Default for PhysicalAddress
{
	fn default () -> Self { Self(0) }
}

impl From<u8> for PhysicalAddress
{
	fn from (addr: u8) -> Self { Self::New(addr as usize) }
}

impl From<u16> for PhysicalAddress
{
	fn from (addr: u16) -> Self { Self::New(addr as usize) }
}

impl From<u32> for PhysicalAddress
{
	fn from (addr: u32) -> Self { Self::New(addr as usize) }
}

impl From<u64> for PhysicalAddress
{
	fn from (addr: u64) -> Self { Self::New(addr as usize) }
}

impl From<usize> for PhysicalAddress
{
	fn from (addr: usize) -> Self { Self::New(addr) }
}

impl const core::ops::Add<usize> for PhysicalAddress
{
	type Output = Self;

	fn add (self, other: usize) -> Self::Output { Self::New (self.ToUsize () + other) }
}

impl core::ops::AddAssign<usize> for PhysicalAddress
{
	fn add_assign (&mut self, other: usize) { self.0 += TryInto::<u64>::try_into (other).unwrap () }
}

impl core::ops::Sub<usize> for PhysicalAddress
{
	type Output = Self;

	fn sub (self, other: usize) -> Self::Output { Self::New (self.ToUsize () - other) }
}

impl core::ops::SubAssign<usize> for PhysicalAddress
{
	fn sub_assign (&mut self, other: usize) { self.0 -= TryInto::<u64>::try_into (other).unwrap () }
}

impl core::ops::Sub<Self> for PhysicalAddress
{
	type Output = usize;

	fn sub (self, other: Self) -> Self::Output { self.ToUsize () - other.ToUsize () }
}

impl core::cmp::PartialOrd for PhysicalAddress
{
	fn partial_cmp (&self, other: &Self) -> Option<core::cmp::Ordering>
	{
		Some(self.cmp (other))
	}
}

impl core::cmp::Ord for PhysicalAddress
{
	fn cmp (&self, other: &Self) -> core::cmp::Ordering
	{
		if self.0 == other.0
		{
			core::cmp::Ordering::Equal
		}
		else if self.0 > other.0
		{
			core::cmp::Ordering::Greater
		}
		else
		{
			core::cmp::Ordering::Less
		}
	}
}

impl core::iter::Step for PhysicalAddress
{
	fn steps_between (start: &Self, end: &Self) -> Option<usize>
	{
		end.ToUsize ().checked_sub (start.ToUsize ())
	}

	fn forward_checked(start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_add (count).and_then (|x| Some(Self::New (x)))
	}

	fn backward_checked(start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_sub (count).and_then (|x| Some(Self::New (x)))
	}
}

impl fmt::Display for PhysicalAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0p{:016X}", self.0)
	}
}

impl fmt::Debug for PhysicalAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}

impl fmt::Pointer for PhysicalAddress
{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}


/// marker trait
/// used to mark struct that live in physical address space
pub trait InPhysicalSpace {}
