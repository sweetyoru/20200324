use core::arch::asm;
use core::intrinsics;
use core::{fmt, ptr, cmp};
use core::cell::SyncUnsafeCell;
use core::sync::atomic::{AtomicBool, Ordering};

use alloc::sync::Arc;

use crate::CPU;
use crate::SMP;
use crate::Arch;
use crate::BootInfo;
use crate::ICC::{self, ICCData};
use crate::Lock::{OneTimeFlag, RwMutex};
use crate::Memory::Physical::{self, PhysicalAddress, PhysicalLinkedList, FrameAllocator, ItemHeader};

use crate::Memory::Virtual::*;


pub const PAGE_SIZE: usize = 4096;


// those two entries must be the same for the address space switching to work
// only requried until the kernel switch to first process
// if this is updated, need to fix the bootloader code as well
// currently this is KERNEL_RESERVED_ENTRY8_INDEX
const BOOTLOADER_STACK_ENTRY_INDEX:		usize = 494;
const KERNEL_BOOT_STACK_ENTRY_INDEX:	usize = BOOTLOADER_STACK_ENTRY_INDEX;


// Those two must be equal
/// Bootloader stack size (256KB)
const BOOTLOADER_STACK_SIZE: usize = 256*1024;

/// Kernel boot stack size (256KB)
const KERNEL_BOOT_STACK_SIZE: usize = BOOTLOADER_STACK_SIZE;


static mut isFinishedUsingBootloaderVirtualAddressSpace: OneTimeFlag = OneTimeFlag::New ();


/// Boot virtual address space, for tempory use at boot
static bootVirtualAddressSpace: SyncUnsafeCell<Option<VirtualAddressSpace>> = SyncUnsafeCell::new (None);



/// Platform-dependent paging flags
#[derive(Debug, Copy, Clone)]
struct HardwarePageFlags(u64);

impl HardwarePageFlags
{
	const PRESENT:			u64 = 1<<0;
	const WRITABLE:			u64 = 1<<1;
	const USER_ACCESSIBLE:	u64 = 1<<2;
	const WRITE_THROUGH:	u64 = 1<<3;
	const CACHE_DISABLED:	u64 = 1<<4;
	const ACCESSED:			u64 = 1<<5;
	const DIRTY:			u64 = 1<<6;
	const HUGE_PAGE:		u64 = 1<<7;
	const GLOBAL:			u64 = 1<<8;
	const NO_EXECUTE:		u64 = 1<<63;

	/// Create new hardware page flags with those attributes:
	/// 	+ Not present
	/// 	+ Not writable
	/// 	+ User not accessible
	/// 	+ Not write throught
	///		+ Cache enabled
	/// 	+ Not accessed
	/// 	+ Not dirty
	/// 	+ Not huge page
	/// 	+ Not global
	/// 	+ Executable
	pub const fn New () -> Self { Self(0) }
	pub const fn FromU64 (flags: u64) -> Self { Self(flags) }

	pub const fn IsPresent (&self) -> bool { (self.0 & HardwarePageFlags::PRESENT) != 0 }
	pub fn SetPresent (&mut self) { self.0 |= HardwarePageFlags::PRESENT; }
	pub fn SetNotPresent (&mut self) { self.0 &= !HardwarePageFlags::PRESENT; }

	pub const fn IsWritable (&self) -> bool { (self.0 & HardwarePageFlags::WRITABLE) != 0 }
	pub fn SetWritable (&mut self) { self.0 |= HardwarePageFlags::WRITABLE; }
	pub fn SetNotWritable (&mut self) { self.0 &= !HardwarePageFlags::WRITABLE; }

	pub const fn IsUserAccessible (&self) -> bool { (self.0 & HardwarePageFlags::USER_ACCESSIBLE) != 0 }
	pub fn SetUserAccessible (&mut self) { self.0 |= HardwarePageFlags::USER_ACCESSIBLE; }
	pub fn SetUserNotAccessible (&mut self) { self.0 &= !HardwarePageFlags::USER_ACCESSIBLE; }

	pub const fn IsWriteThrought (&self) -> bool { (self.0 & HardwarePageFlags::WRITE_THROUGH) != 0 }
	pub fn SetWriteThrought (&mut self) { self.0 |= HardwarePageFlags::WRITE_THROUGH; }
	pub fn SetNotWriteThrought (&mut self) { self.0 &= !HardwarePageFlags::WRITE_THROUGH; }

	pub const fn IsCacheEnabled (&self) -> bool { (self.0 & HardwarePageFlags::CACHE_DISABLED) == 0 }
	pub fn SetCacheEnabled (&mut self) { self.0 &= !HardwarePageFlags::CACHE_DISABLED; }
	pub fn SetCacheDisabled (&mut self) { self.0 |= HardwarePageFlags::CACHE_DISABLED; }

	pub const fn IsAccessed (&self) -> bool { (self.0 & HardwarePageFlags::ACCESSED) != 0 }
	pub fn SetAccessed (&mut self) { self.0 |= HardwarePageFlags::ACCESSED; }
	pub fn SetNotAccessed (&mut self) { self.0 &= !HardwarePageFlags::ACCESSED; }

	pub const fn IsDirty (&self) -> bool { (self.0 & HardwarePageFlags::DIRTY) != 0 }
	pub fn SetDirty (&mut self) { self.0 |= HardwarePageFlags::DIRTY; }
	pub fn SetNotDirty (&mut self) { self.0 &= !HardwarePageFlags::DIRTY; }

	pub const fn IsHugePage (&self) -> bool { (self.0 & HardwarePageFlags::HUGE_PAGE) != 0 }
	pub fn SetHugePage (&mut self) { self.0 |= HardwarePageFlags::HUGE_PAGE; }
	pub fn SetNotHugePage (&mut self) { self.0 &= !HardwarePageFlags::HUGE_PAGE; }

	pub const fn IsGlobal (&self) -> bool { (self.0 & HardwarePageFlags::GLOBAL) != 0 }
	pub fn SetGlobal (&mut self) { self.0 |= HardwarePageFlags::GLOBAL; }
	pub fn SetNotGlobal (&mut self) { self.0 &= !HardwarePageFlags::GLOBAL; }

	pub const fn IsExecutable (&self) -> bool { (self.0 & HardwarePageFlags::GLOBAL) == 0 }
	pub fn SetExecutable (&mut self) { self.0 &= !HardwarePageFlags::NO_EXECUTE; }
	pub fn SetNotExecutable (&mut self) { self.0 |= HardwarePageFlags::NO_EXECUTE; }

	pub const fn ToU64 (&self) -> u64 { self.0 }
}


#[derive(Copy, Clone, Eq, PartialEq, Hash)]
#[repr(C)]
/// Represent virtual address
pub struct VirtualAddress(u64);

impl VirtualAddress
{
	pub const fn New (address: usize) -> Self { Self(address as u64) }

	pub const fn MinAddress () -> Self { Self(0x0000_0000_0000_0000) }
	pub const fn MaxAddress () -> Self { Self(0xffff_ffff_ffff_ffff) }

	pub const fn Align (&self, align: usize) -> Self
	{
		Self::New (((self.ToUsize () + align - 1) / align) * align)
	}

	pub const fn AlignDown (&self, align: usize) -> Self
	{
		Self::New ((self.ToUsize () / align) * align)
	}

	pub const fn IsAlign (&self, align: usize) -> bool { (self.ToUsize () % align) == 0 }

	pub const fn GetContainPageNumber (&self) -> usize { self.ToUsize () / PAGE_SIZE }
	pub const fn GetFullPageNumberLeft (&self) -> usize { (self.ToUsize () - PAGE_SIZE + 1)/PAGE_SIZE }
	pub const fn GetFullPageNumberRight (&self) -> usize { (self.ToUsize () + PAGE_SIZE - 1)/PAGE_SIZE }

	pub const fn ToUsize (&self) -> usize { self.0 as usize }
	pub (in crate::Arch::Amd64) const fn ToU64 (&self) -> u64 { self.0 as u64 }

	pub const fn IsValidPageAddress (&self) -> bool { self.ToUsize () % PAGE_SIZE == 0 }

	pub fn IsReservedKernelAddress (&self) -> bool
	{
		let topLevelIndex = if Arch::Amd64::CPU::CPU::Is5LevelPagingSupported () { self.GetPageIndex (5) }
		else { self.GetPageIndex (4) };

		VirtualAddressSpace::IsKernelReservedEntry (topLevelIndex)
	}

	pub fn IsInPhysicalMappingRange (&self) -> bool
	{
		let topLevelIndex = if Arch::Amd64::CPU::CPU::Is5LevelPagingSupported () { self.GetPageIndex (5) }
		else { self.GetPageIndex (4) };

		VirtualAddressSpace::IsPhysicalMappingEntry (topLevelIndex)
	}

	pub const fn ToConst<T> (&self) -> &'static T { unsafe { &*(self.0 as *const T) } }
	pub const fn ToMut<T> (&self) -> &'static mut T { unsafe { &mut *(self.0 as *mut T)  } }

	pub const unsafe fn ToConstRaw<T> (&self) -> *const T { self.0 as *const T }
	pub const unsafe fn ToMutRaw<T> (&self) -> *mut T { self.0 as *mut T }

	pub fn VolatileRead<T>(&self) -> T
	{
		unsafe { intrinsics::volatile_load (self.ToConstRaw::<T> ()) }
	}

	pub fn VolatileWrite<T> (&self, value: T)
	{
		unsafe { intrinsics::volatile_store (self.ToMutRaw::<T> (), value); }
	}

	const fn GetPageIndex (&self, level: usize) -> usize
	{
		((self.ToU64 () >> (12 + 9*(level - 1))) & 0b1_1111_1111) as usize
	}

	const fn GetPageL1Offset (&self) -> usize { (self.ToU64 () & 0b1111_1111_1111) as usize }
	const fn GetPageL2Offset (&self) -> usize { (self.ToU64 () & 0b1_1111_1111_1111_1111_1111) as usize }
	const fn GetPageL3Offset (&self) -> usize { (self.ToU64 () & 0b11_1111_1111_1111_1111_1111_1111_1111) as usize }

	/// Copy from src to des, behave as if src buffer is copied to a tmp buffer, then the tmp buffer is copied to des
	pub fn MemCopy (src: VirtualAddress, des: VirtualAddress, len: usize)
	{
		if src > des
		{
			for offset in 0..len
			{
				*(des + offset).ToMut::<u8> () = *(src + offset).ToConst::<u8> ();
			}
		}
		else if src < des
		{
			for offset in (0..len).rev ()
			{
				*(des + offset).ToMut::<u8> () = *(src + offset).ToConst::<u8> ();
			}
		}
	}

	pub fn VolatileMemCopy (src: VirtualAddress, des: VirtualAddress, len: usize)
	{
		let src = unsafe { src.ToConstRaw::<u8> () };
		let des = unsafe { des.ToMutRaw::<u8> () };

		for offset in 0..len
		{
			unsafe
			{
				let tmp = ptr::read_volatile (src.offset (offset.try_into ().unwrap ()));
				ptr::write_volatile (des.offset (offset.try_into ().unwrap ()), tmp);
			}
		}
	}
}


impl Default for VirtualAddress
{
	fn default () -> Self { Self(0) }
}

impl From<u8> for VirtualAddress
{
	fn from (addr: u8) -> Self { Self::New(addr as usize) }
}

impl From<u16> for VirtualAddress
{
	fn from (addr: u16) -> Self { Self::New(addr as usize) }
}

impl From<u32> for VirtualAddress
{
	fn from (addr: u32) -> Self { Self::New(addr as usize) }
}

impl From<u64> for VirtualAddress
{
	fn from (addr: u64) -> Self { Self::New(addr as usize) }
}

impl From<usize> for VirtualAddress
{
	fn from (addr: usize) -> Self { Self::New(addr) }
}

impl From<PageTableIndex> for VirtualAddress
{
	fn from (PageTableIndex: PageTableIndex) -> Self
	{
		todo! ();
	}
}

impl const core::ops::Add<usize> for VirtualAddress
{
	type Output = Self;

	fn add (self, other: usize) -> Self::Output { Self::New (self.ToUsize () + other) }
}

impl core::ops::AddAssign<usize> for VirtualAddress
{
	fn add_assign (&mut self, other: usize) { self.0 += TryInto::<u64>::try_into (other).unwrap () }
}

impl core::ops::Sub<usize> for VirtualAddress
{
	type Output = Self;

	fn sub (self, other: usize) -> Self::Output { Self::New (self.ToUsize () - other) }
}

impl core::ops::SubAssign<usize> for VirtualAddress
{
	fn sub_assign (&mut self, other: usize) { self.0 -= TryInto::<u64>::try_into (other).unwrap () }
}

impl core::ops::Sub<Self> for VirtualAddress
{
	type Output = usize;

	fn sub (self, other: Self) -> Self::Output
	{
		assert! (self.0 >= other.0);
		self.ToUsize () - other.ToUsize ()
	}
}

impl core::cmp::PartialOrd for VirtualAddress
{
	fn partial_cmp (&self, other: &Self) -> Option<core::cmp::Ordering>
	{
		Some(self.cmp (other))
	}
}

impl core::cmp::Ord for VirtualAddress
{
	fn cmp (&self, other: &Self) -> core::cmp::Ordering
	{
		if self.0 == other.0
		{
			core::cmp::Ordering::Equal
		}
		else if self.0 > other.0
		{
			core::cmp::Ordering::Greater
		}
		else
		{
			core::cmp::Ordering::Less
		}
	}
}

impl From<KernelAddress> for VirtualAddress
{
	fn from (item: KernelAddress) -> Self { Self::New (item.ToUsize ()) }
}

impl From<UserAddress> for VirtualAddress
{
	fn from (item: UserAddress) -> Self { Self::New (item.ToUsize ()) }
}

impl core::iter::Step for VirtualAddress
{
	fn steps_between (start: &Self, end: &Self) -> Option<usize>
	{
		end.ToUsize ().checked_sub (start.ToUsize ())
	}

	fn forward_checked (start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_add (count).and_then (|x| Some(Self::New (x)))
	}

	fn backward_checked (start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_sub (count).and_then (|x| Some(Self::New (x)))
	}
}

impl fmt::Display for VirtualAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0v{:016X}", self.0)
	}
}

impl fmt::Debug for VirtualAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}

impl fmt::Pointer for VirtualAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0v{:016X}", self.0)
	}
}


#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct KernelAddress(VirtualAddress);

impl KernelAddress
{
	pub const fn New (address: usize) -> Self { Self(VirtualAddress::New (address)) }

	pub const fn Align (&self, align: usize) -> Self
	{
		Self::New (((self.ToUsize () + align - 1) / align) * align)
	}

	pub const fn AlignDown (&self, align: usize) -> Self
	{
		Self::New ((self.ToUsize () / align) * align)
	}

	pub const fn IsAlign (&self, align: usize) -> bool { (self.ToUsize () % align) == 0 }

	pub const fn ToUsize (&self) -> usize { self.0.ToUsize () }

	pub (in crate::Arch::Amd64) const fn ToU64 (&self) -> u64 { self.0.ToU64 () }

	pub fn IsReservedKernelAddress (&self) -> bool
	{
		let topLevelIndex = if Arch::Amd64::CPU::CPU::Is5LevelPagingSupported () { self.0.GetPageIndex (5) }
		else { self.0.GetPageIndex (4) };

		VirtualAddressSpace::IsKernelReservedEntry (topLevelIndex)
	}

	pub fn IsInPhysicalMappingRange (&self) -> bool
	{
		let topLevelIndex = if Arch::Amd64::CPU::CPU::Is5LevelPagingSupported () { self.0.GetPageIndex (5) }
		else { self.0.GetPageIndex (4) };

		VirtualAddressSpace::IsPhysicalMappingEntry (topLevelIndex)
	}

	pub const fn ToConst<T> (&self) -> &'static T { self.0.ToConst::<T> () }
	pub const fn ToMut<T> (&self) -> &'static mut T { self.0.ToMut::<T> () }

	pub const unsafe fn ToConstRaw<T> (&self) -> *const T { self.0.ToConstRaw::<T> () }
	pub const unsafe fn ToMutRaw<T> (&self) -> *mut T { self.0.ToMutRaw::<T> () }

	pub fn VolatileRead<T>(&self) -> T
	{
		unsafe { intrinsics::volatile_load (self.ToConstRaw::<T> ()) }
	}

	pub fn VolatileWrite<T> (&self, value: T)
	{
		unsafe { intrinsics::volatile_store (self.ToMutRaw::<T> (), value); }
	}

	pub fn VolatileMemCopy (src: KernelAddress, des: KernelAddress, len: usize)
	{
		let src = unsafe { src.ToConstRaw::<u8> () };
		let des = unsafe { des.ToMutRaw::<u8> () };

		for offset in 0..len
		{
			unsafe
			{
				let tmp = ptr::read_volatile (src.offset (offset.try_into ().unwrap ()));
				ptr::write_volatile (des.offset (offset.try_into ().unwrap ()), tmp);
			}
		}
	}
}


impl Default for KernelAddress
{
	fn default () -> Self
	{
		Self(VirtualAddress::default ())
	}
}

impl From<u8> for KernelAddress
{
	fn from (addr: u8) -> Self { Self::New(addr as usize) }
}

impl From<u16> for KernelAddress
{
	fn from (addr: u16) -> Self { Self::New(addr as usize) }
}

impl From<u32> for KernelAddress
{
	fn from (addr: u32) -> Self { Self::New(addr as usize) }
}

impl From<u64> for KernelAddress
{
	fn from (addr: u64) -> Self { Self::New(addr as usize) }
}

impl From<usize> for KernelAddress
{
	fn from (addr: usize) -> Self { Self::New(addr) }
}

impl From<PageTableIndex> for KernelAddress
{
	fn from (PageTableIndex: PageTableIndex) -> Self
	{
		Self(VirtualAddress::from (PageTableIndex))
	}
}

impl const core::ops::Add<usize> for KernelAddress
{
	type Output = Self;

	fn add (self, other: usize) -> Self::Output { Self(self.0 + other) }
}

impl core::ops::AddAssign<usize> for KernelAddress
{
	fn add_assign (&mut self, other: usize) { self.0 += other }
}

impl core::ops::Sub<usize> for KernelAddress
{
	type Output = Self;

	fn sub (self, other: usize) -> Self::Output { Self(self.0 - other) }
}

impl core::ops::SubAssign<usize> for KernelAddress
{
	fn sub_assign (&mut self, other: usize) { self.0 -= other }
}

impl core::ops::Sub<Self> for KernelAddress
{
	type Output = usize;

	fn sub (self, other: Self) -> Self::Output { self.0 - other.0 }
}

impl core::cmp::PartialOrd for KernelAddress
{
	fn partial_cmp (&self, other: &Self) -> Option<core::cmp::Ordering>
	{
		Some(self.cmp (other))
	}
}

impl core::cmp::Ord for KernelAddress
{
	fn cmp (&self, other: &Self) -> core::cmp::Ordering
	{
		self.0.cmp (&other.0)
	}
}

impl From<VirtualAddress> for KernelAddress
{
	fn from (item: VirtualAddress) -> Self { Self(item) }
}

impl core::iter::Step for KernelAddress
{
	fn steps_between (start: &Self, end: &Self) -> Option<usize>
	{
		end.ToUsize ().checked_sub (start.ToUsize ())
	}

	fn forward_checked (start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_add (count).and_then (|x| Some(Self::New (x)))
	}

	fn backward_checked (start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_sub (count).and_then (|x| Some(Self::New (x)))
	}
}

impl fmt::Display for KernelAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0k{:016x}", self.ToU64 ())
	}
}

impl fmt::Debug for KernelAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}

impl fmt::Pointer for KernelAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0k{:016x}", self.ToU64 ())
	}
}


#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct UserAddress(VirtualAddress);

impl UserAddress
{
	pub const fn New (address: usize) -> Self { Self(VirtualAddress::New (address)) }

	pub const fn Align (&self, align: usize) -> Self
	{
		Self::New (((self.ToUsize () + align - 1) / align) * align)
	}

	pub const fn AlignDown (&self, align: usize) -> Self
	{
		Self::New ((self.ToUsize () / align) * align)
	}

	pub const fn IsAlign (&self, align: usize) -> bool { (self.ToUsize () % align) == 0 }

	pub const fn ToUsize (&self) -> usize { self.0.ToUsize () }

	pub (in crate::Arch::Amd64) const fn ToU64 (&self) -> u64 { self.0.ToU64 () }

	pub const fn ToConst<T> (&self) -> &'static T { self.0.ToConst::<T> () }
	pub const fn ToMut<T> (&self) -> &'static mut T { self.0.ToMut::<T> () }

	pub const unsafe fn ToConstRaw<T> (&self) -> *const T { self.0.ToConstRaw::<T> () }
	pub const unsafe fn ToMutRaw<T> (&self) -> *mut T { self.0.ToMutRaw::<T> () }

	pub fn VolatileRead<T>(&self) -> T
	{
		unsafe { intrinsics::volatile_load (self.ToConstRaw::<T> ()) }
	}

	pub fn VolatileWrite<T> (&self, value: T)
	{
		unsafe { intrinsics::volatile_store (self.ToMutRaw::<T> (), value); }
	}

	pub fn VolatileMemCopy (src: UserAddress, des: UserAddress, len: usize)
	{
		let src = unsafe { src.ToConstRaw::<u8> () };
		let des = unsafe { des.ToMutRaw::<u8> () };

		for offset in 0..len
		{
			unsafe
			{
				let tmp = ptr::read_volatile (src.offset (offset.try_into ().unwrap ()));
				ptr::write_volatile (des.offset (offset.try_into ().unwrap ()), tmp);
			}
		}
	}
}


impl Default for UserAddress
{
	fn default () -> Self
	{
		Self(VirtualAddress::default ())
	}
}

impl From<u8> for UserAddress
{
	fn from (addr: u8) -> Self { Self::New(addr as usize) }
}

impl From<u16> for UserAddress
{
	fn from (addr: u16) -> Self { Self::New(addr as usize) }
}

impl From<u32> for UserAddress
{
	fn from (addr: u32) -> Self { Self::New(addr as usize) }
}

impl From<u64> for UserAddress
{
	fn from (addr: u64) -> Self { Self::New(addr as usize) }
}

impl From<usize> for UserAddress
{
	fn from (addr: usize) -> Self { Self::New(addr) }
}

impl From<PageTableIndex> for UserAddress
{
	fn from (PageTableIndex: PageTableIndex) -> Self
	{
		Self(VirtualAddress::from (PageTableIndex))
	}
}

impl const core::ops::Add<usize> for UserAddress
{
	type Output = Self;

	fn add (self, other: usize) -> Self::Output { Self(self.0 + other) }
}

impl core::ops::AddAssign<usize> for UserAddress
{
	fn add_assign (&mut self, other: usize) { self.0 += other }
}

impl core::ops::Sub<usize> for UserAddress
{
	type Output = Self;

	fn sub (self, other: usize) -> Self::Output { Self(self.0 - other) }
}

impl core::ops::SubAssign<usize> for UserAddress
{
	fn sub_assign (&mut self, other: usize) { self.0 -= other }
}

impl core::ops::Sub<Self> for UserAddress
{
	type Output = usize;

	fn sub (self, other: Self) -> Self::Output { self.0 - other.0 }
}

impl core::cmp::PartialOrd for UserAddress
{
	fn partial_cmp (&self, other: &Self) -> Option<core::cmp::Ordering>
	{
		Some(self.cmp (other))
	}
}

impl core::cmp::Ord for UserAddress
{
	fn cmp (&self, other: &Self) -> core::cmp::Ordering
	{
		self.0.cmp (&other.0)
	}
}

impl From<VirtualAddress> for UserAddress
{
	fn from (item: VirtualAddress) -> Self { Self(item) }
}

impl core::iter::Step for UserAddress
{
	fn steps_between (start: &Self, end: &Self) -> Option<usize>
	{
		end.ToUsize ().checked_sub (start.ToUsize ())
	}

	fn forward_checked (start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_add (count).and_then (|x| Some(Self::New (x)))
	}

	fn backward_checked (start: Self, count: usize) -> Option<Self>
	{
		start.ToUsize ().checked_sub (count).and_then (|x| Some(Self::New (x)))
	}
}

impl fmt::Display for UserAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0u{:016x}", self.ToU64 ())
	}
}

impl fmt::Debug for UserAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}

impl fmt::Pointer for UserAddress
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0u{:016x}", self.ToU64 ())
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
struct PageTableIndex
{
	pageTableIndexL1: usize,
	pageTableIndexL2: usize,
	pageTableIndexL3: usize,
	pageTableIndexL4: usize,
	pageTableIndexL5: usize,
}

impl PageTableIndex
{
	pub const fn New (pageTableIndexL5: usize, pageTableIndexL4: usize, pageTableIndexL3: usize, pageTableIndexL2: usize, pageTableIndexL1: usize) -> Self
	{
		Self
		{
			pageTableIndexL1,
			pageTableIndexL2,
			pageTableIndexL3,
			pageTableIndexL4,
			pageTableIndexL5,
		}
	}

	pub const fn PageTableIndexL1 (&self) -> usize { self.pageTableIndexL1 }
	pub const fn PageTableIndexL2 (&self) -> usize { self.pageTableIndexL2 }
	pub const fn PageTableIndexL3 (&self) -> usize { self.pageTableIndexL3 }
	pub const fn PageTableIndexL4 (&self) -> usize { self.pageTableIndexL4 }
	pub const fn PageTableIndexL5 (&self) -> usize { self.pageTableIndexL5 }

	pub const fn CanonicalPageTableIndex (index: usize) -> usize
	{
		if index < 256 { 0 } else { 511 }
	}
}

impl From<VirtualAddress> for PageTableIndex
{
	fn from (address: VirtualAddress) -> Self
	{
		let pageIndexL5 = address.GetPageIndex (5);
		let pageIndexL4 = address.GetPageIndex (4);
		let pageIndexL3 = address.GetPageIndex (3);
		let pageIndexL2 = address.GetPageIndex (2);
		let pageIndexL1 = address.GetPageIndex (1);

		Self::New (pageIndexL5, pageIndexL4, pageIndexL3, pageIndexL2, pageIndexL1)
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct PageTableEntry(u64);

impl PageTableEntry
{
	const FRAME_L1_ADDRESS_MASK: u64 = 0x000FFFFF_FFFFF000;
	const FRAME_L2_ADDRESS_MASK: u64 = 0x000FFFFF_FFE00000;
	const FRAME_L3_ADDRESS_MASK: u64 = 0x000FFFFF_C0000000;
	const TABLE_ADDRESS_MASK: u64 = 0x000FFFFF_FFFFF000;
	const DATA_MASK: u64 = 0b1_11111_11111<<Self::DATA_SHIFT;
	const DATA_SHIFT: u64 = 52;
	const FLAGS_MASK: u64 = 0x80000000_00000FFF;

	pub const fn New () -> Self { Self(0) }
	pub fn Clear (&mut self) { self.0 = 0; }

	pub const fn GetFlags (&self) -> HardwarePageFlags
	{
		HardwarePageFlags::FromU64 (self.0 & Self::FLAGS_MASK)
	}

	pub fn SetFlags (&mut self, flags: &HardwarePageFlags)
	{
		assert! (flags.ToU64 () & Self::FLAGS_MASK == flags.ToU64 ());
		self.0 |= flags.ToU64 ();
	}

	pub fn ClearFlags (&mut self, flags: &HardwarePageFlags)
	{
		assert! (flags.ToU64 () & Self::FLAGS_MASK == flags.ToU64 ());
		self.0 &= !flags.ToU64 ();
	}

	pub const fn HaveFlags (&self, flags: &HardwarePageFlags) -> bool
	{
		assert! (flags.ToU64 () & Self::FLAGS_MASK == flags.ToU64 ());
		self.0&flags.ToU64 () == flags.ToU64 ()
	}

	pub const fn GetData (&self) -> usize
	{
		((self.0&Self::DATA_MASK)>>Self::DATA_SHIFT) as usize
	}

	pub fn SetData (&mut self, data: usize)
	{
		let data: u64 = data as u64;
		assert! (((data<<Self::DATA_SHIFT)&Self::DATA_MASK)>>Self::DATA_SHIFT == data);

		// clear old data
		self.0 &= !Self::DATA_MASK;

		// set new data
		self.0 |= (data<<Self::DATA_SHIFT)&Self::DATA_MASK;
	}

	pub const fn IsPresent (&self) -> bool { self.GetFlags ().IsPresent () }
	pub const fn IsWritable (&self) -> bool { self.GetFlags ().IsWritable () }
	pub const fn IsKernelOnly (&self) -> bool { !self.GetFlags ().IsUserAccessible () }
	pub const fn IsUserAccessible (&self) -> bool { self.GetFlags ().IsUserAccessible () }
	pub const fn IsWriteThrought (&self) -> bool { self.GetFlags ().IsWriteThrought () }
	pub const fn IsCacheEnabled (&self) -> bool { self.GetFlags ().IsCacheEnabled () }
	pub const fn IsAccessed (&self) -> bool { self.GetFlags ().IsAccessed () }
	pub const fn IsDirty (&self) -> bool { self.GetFlags ().IsDirty () }
	pub const fn IsHugePage (&self) -> bool { self.GetFlags ().IsHugePage () }
	pub const fn IsGlobal (&self) -> bool { self.GetFlags ().IsGlobal () }
	pub const fn IsExecutable (&self) -> bool { self.GetFlags ().IsExecutable () }


	pub const fn GetTable (&self) -> Option<PageTable>
	{
		match self.GetTableAddress ()
		{
			Some(tableAddress) => Some(PageTable::New (tableAddress)),
			None => None,
		}
	}

	pub fn SetTable (&mut self, pageTable: PageTable)
	{
		self.SetTableAddress (pageTable.tableAddress);
	}

	pub const fn GetTableAddress (&self) -> Option<PhysicalAddress>
	{
		if !self.IsPresent () { None }
		else { Some(PhysicalAddress::New ((self.0&Self::TABLE_ADDRESS_MASK) as usize)) }
	}

	fn SetTableAddress (&mut self, tableAddress: PhysicalAddress)
	{
		assert! (tableAddress.IsValidTableAddress ());

		// clear old address
		self.0 &= !Self::TABLE_ADDRESS_MASK;

		// set new address
		self.0 |= tableAddress.ToUsize () as u64;
	}

	pub const fn GetFrameL1Address (&self) -> Option<PhysicalAddress>
	{
		if !self.IsPresent () { None }
		else { Some(PhysicalAddress::New ((self.0&Self::FRAME_L1_ADDRESS_MASK) as usize)) }
	}

	pub const fn GetFrameL2Address (&self) -> Option<PhysicalAddress>
	{
		if !self.IsPresent () { None }
		else { Some(PhysicalAddress::New ((self.0&Self::FRAME_L2_ADDRESS_MASK) as usize)) }
	}

	pub const fn GetFrameL3Address (&self) -> Option<PhysicalAddress>
	{
		if !self.IsPresent () { None }
		else { Some(PhysicalAddress::New ((self.0&Self::FRAME_L3_ADDRESS_MASK) as usize)) }
	}

	pub fn SetFrameAddress (&mut self, frameAddress: PhysicalAddress, frameLevel: usize)
	{
		match frameLevel
		{
			1 => assert! (frameAddress.IsValidFrameL1Address ()),
			2 => assert! (frameAddress.IsValidFrameL2Address ()),
			3 => assert! (frameAddress.IsValidFrameL3Address ()),
			_ => panic! ("Unknown frame level({})", frameLevel),
		}

		// clear old address
		match frameLevel
		{
			1 => self.0 &= !Self::FRAME_L1_ADDRESS_MASK,
			2 => self.0 &= !Self::FRAME_L2_ADDRESS_MASK,
			3 => self.0 &= !Self::FRAME_L3_ADDRESS_MASK,
			_ => panic! ("Unknown frame level({})", frameLevel),
		}

		// set new address
		self.0 |= frameAddress.ToUsize () as u64;
	}
}


impl fmt::Display for PageTableEntry
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		// raw value
		write! (f, "{:016X}", self.0).unwrap ();

		// data
		write! (f, " {:03X} ", self.GetData ()).unwrap ();

		// flags
		if self.IsPresent ()
		{
			write! (f, "P").unwrap ();
		}

		if self.IsWritable ()
		{
			write! (f, "W").unwrap ();
		}
		else
		{
			write! (f, "R").unwrap ();
		}

		if self.IsKernelOnly ()
		{
			write! (f, "K").unwrap ();
		}
		else
		{
			write! (f, "U").unwrap ();
		}

		if self.IsWriteThrought ()
		{
			write! (f, "T").unwrap ();
		}

		if self.IsCacheEnabled ()
		{
			write! (f, "C").unwrap ();
		}

		if self.IsAccessed ()
		{
			write! (f, "A").unwrap ();
		}

		if self.IsDirty ()
		{
			write! (f, "D").unwrap ();
		}

		if self.IsHugePage ()
		{
			write! (f, "H").unwrap ();
		}

		if self.IsGlobal ()
		{
			write! (f, "G").unwrap ();
		}
		else
		{
			write! (f, "L").unwrap ();
		}

		if self.IsExecutable ()
		{
			write! (f, "E").unwrap ();
		}
		else
		{
			write! (f, "N").unwrap ();
		}

		if self.IsPresent ()
		{
			write! (f, " {}", self.GetTableAddress ().unwrap ())
		}
		else
		{
			write! (f, "")
		}
	}
}


/// This type is a physical pointer to page table in physical memory
#[derive(Debug, Copy, Clone)]
struct PageTable
{
	tableAddress: PhysicalAddress,
}

impl PageTable
{
	const ENTRY_NUM: usize = 512;
	const MAX_ENTRY_INDEX: usize = Self::ENTRY_NUM - 1;
	const ENTRY_SIZE: usize = 8;


	pub const fn New (tableAddress: PhysicalAddress) -> Self { Self { tableAddress: tableAddress, } }

	pub const fn TableAddress (&self) -> PhysicalAddress { self.tableAddress }


	/// # Description:
	/// Return the entry at specified index.
	///
	/// # Args:
	/// - index: entry index.
	///
	/// # Return value:
	/// The entry at specified index.
	///
	/// # Behavior:
	/// Invoke physical volatile read.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function doesn't check for valid entry.
	///
	fn GetEntry (&self, index: usize) -> PageTableEntry
	{
		assert! (index <= Self::MAX_ENTRY_INDEX);

		(self.tableAddress + index*Self::ENTRY_SIZE).VolatileRead::<PageTableEntry> ()
	}


	/// # Description:
	/// Overwrite the entry at specified index.
	///
	/// # Args:
	/// - index: entry index.
	/// - entry: new entry.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Invoke physical volatile write.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function will overwrite the old entry.
	///
	fn SetEntry (&mut self, index: usize, entry: PageTableEntry)
	{
		assert! (index <= Self::MAX_ENTRY_INDEX);

		(self.tableAddress + index*Self::ENTRY_SIZE).VolatileWrite::<PageTableEntry> (entry);
	}


	/// # Description:
	/// Return entry count of the table.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// Entry count of the table.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	///
	pub fn EntryCount (&self) -> usize
	{
		self.GetEntry (0).GetData ()
	}


	/// # Description:
	/// Set entry count of the table.
	///
	/// # Args:
	/// - entryCount: new entry count.
	///
	/// # Return value:
	/// Old entry count.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	pub fn SetEntryCount (&mut self, entryCount: usize) -> usize
	{
		assert! (entryCount <= Self::ENTRY_NUM);

		let mut newEntry = self.GetEntry (0);
		let oldEntryCount = newEntry.GetData ();
		newEntry.SetData (entryCount);
		self.SetEntry (0, newEntry);
		oldEntryCount
	}


	/// # Description:
	/// Increase entry count of the table.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// Old entry count.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	pub fn IncreaseEntryCount (&mut self) -> usize
	{
		assert! (self.EntryCount () < Self::ENTRY_NUM);

		let mut newEntry = self.GetEntry (0);
		let oldEntryCount = newEntry.GetData ();
		newEntry.SetData (newEntry.GetData () + 1);
		self.SetEntry (0, newEntry);
		oldEntryCount
	}


	/// # Description:
	/// Decrease entry count of the table.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// Old entry count.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	pub fn DecreaseEntryCount (&mut self) -> usize
	{
		assert! (self.EntryCount () > 0);
		assert! (self.EntryCount () <= Self::ENTRY_NUM);

		let mut newEntry = self.GetEntry (0);
		let oldEntryCount = newEntry.GetData ();
		newEntry.SetData (newEntry.GetData () - 1);
		self.SetEntry (0, newEntry);
		oldEntryCount
	}


	/// # Description:
	/// Return the page table at specified index.
	///
	/// # Args:
	/// - index: page table index.
	///
	/// # Return value:
	/// The page table at specified index.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function doesn't check for valid table.
	///
	pub fn GetTable (&self, index: usize) -> Option<PageTable>
	{
		self.GetEntry (index).GetTable ()
	}


	/// # Description:
	/// Return the page table at specified index.
	///
	/// # Args:
	/// - index: page table index.
	/// - hardwarePageFlags: hardware paging flags to use when creating new page table (if needed).
	///
	/// # Return value:
	/// The page table at specified index, if any, or the newly created page table.
	///
	/// # Behavior:
	/// Auto increase entry count if create new page table.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function doesn't check for valid table.
	///
	pub fn GetTableOrCreate (&mut self, index: usize, hardwarePageFlags: &HardwarePageFlags) -> PageTable
	{
		self.GetEntry (index).GetTable ().unwrap_or_else (||
			{
				// get an empty frame
				let newFrameAddress = FrameAllocator::GetEmptyNormalFrame ().expect ("Out of RAM!!!");

				// change address, flags of entry at index
				// and increase entry count
				let mut newEntry = self.GetEntry (index);
				let mut newHardwarePageFlags = *hardwarePageFlags;
				newHardwarePageFlags.SetPresent ();

				newEntry.SetTable (PageTable::New (newFrameAddress));
				newEntry.SetFlags (&newHardwarePageFlags);
				self.SetEntry (index, newEntry);
				self.IncreaseEntryCount ();

				// return new table
				PageTable::New (newFrameAddress)
			})
	}


	/// # Description:
	/// Return the frame address at specified index.
	///
	/// # Args:
	/// - index: frame index.
	/// - frameLevel: the frame level
	///
	/// # Return value:
	/// - Some(frameAddress): the frame address at the specified index, if any.
	/// - None: there is no valid frame at that address.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	///
	pub fn GetFrameAddress (&self, index: usize, frameLevel: usize) -> Option<PhysicalAddress>
	{
		match frameLevel
		{
			1 => self.GetEntry (index).GetFrameL1Address (),
			2 => self.GetEntry (index).GetFrameL2Address (),
			3 => self.GetEntry (index).GetFrameL3Address (),
			_ => panic! ("Unknown frame level({})", frameLevel),
		}
	}


	/// # Description:
	/// Map a frame to an entry at specified index, create new entry if needed.
	///
	/// # Args:
	/// - index: entry index.
	/// - frameAddress: frame address.
	/// - frameLevel: frame level.
	/// - hardwarePageFlags: hardware page flags for the mapping.
	///
	/// # Return value:
	/// Old entry at specified index.
	///
	/// # Behavior:
	/// Auto manage entry count.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	pub fn MapEntry (&mut self, index: usize, frameAddress: PhysicalAddress, frameLevel: usize, hardwarePageFlags: &HardwarePageFlags) -> PageTableEntry
	{
		match frameLevel
		{
			1 => assert! (frameAddress.IsValidFrameL1Address ()),
			2 => assert! (frameAddress.IsValidFrameL2Address ()),
			3 => assert! (frameAddress.IsValidFrameL3Address ()),
			_ => panic! ("Unknown frame level({})", frameLevel),
		}

		let oldEntry = self.GetEntry (index);

		// change address, flags of entry at index
		let mut newEntry = oldEntry;
		newEntry.SetFrameAddress (frameAddress, frameLevel);
		newEntry.SetFlags (hardwarePageFlags);
		self.SetEntry (index, newEntry);

		// change entry count if necessary
		if oldEntry.IsPresent () && (!hardwarePageFlags.IsPresent ()) { self.DecreaseEntryCount (); }
		if !oldEntry.IsPresent () && (hardwarePageFlags.IsPresent ()) { self.IncreaseEntryCount (); }

		oldEntry
	}


	/// # Description:
	/// Free a table at specified index if that table entry count is zero.
	///
	/// # Args:
	/// - index: table index.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// Can only be used on table.
	///
	pub fn FreeEntryIfEmpty (&mut self, index: usize, frameLevel: usize)
	{
		assert! (index <= Self::MAX_ENTRY_INDEX);

		if let Some(table) = self.GetTable (index)
		{
			if table.EntryCount () == 0
			{
				FrameAllocator::FreeFrame (self.MapEntry (index, PhysicalAddress::New (0), frameLevel, &HardwarePageFlags::New ()).GetTableAddress ().unwrap ());
			}
		}
	}


	/// # Description:
	/// Map the last entry of the table to itself.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn SetRecursiveEntry (&mut self, entryIndex: usize)
	{
		let mut newEntry = PageTableEntry::New ();

		let mut newHardwarePageFlags = HardwarePageFlags::New ();
		newHardwarePageFlags.SetNotExecutable ();
		newHardwarePageFlags.SetCacheEnabled ();
		newHardwarePageFlags.SetNotGlobal ();
		newHardwarePageFlags.SetUserNotAccessible ();
		newHardwarePageFlags.SetWritable ();
		newHardwarePageFlags.SetPresent ();

		newEntry.SetTable (PageTable::New (self.TableAddress ()));
		newEntry.SetFlags (&newHardwarePageFlags);

		self.SetEntry (entryIndex, newEntry);
	}


	/// # Description:
	/// Dump the whole page table.
	///
	/// # Args:
	/// - currentPageTableLevel
	/// - targetPageTableLevel
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// - Read the page table and print every entry.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	pub fn DumpTable (&self, currentPageTableLevel: usize, targetPageTableLevel: usize)
	{
		assert! (targetPageTableLevel > 0);
		assert! (currentPageTableLevel >= targetPageTableLevel);

		println! ("------------------------------------");
		println! ("Table address: {}, level: {}", self.TableAddress (), currentPageTableLevel);

		// dump current table
		for index in 0..=Self::MAX_ENTRY_INDEX
		{
			let entry = self.GetEntry (index);
			println! ("Entry {}: {}", index, entry);
		}


		// dump all lower table if present
		if currentPageTableLevel >= targetPageTableLevel && currentPageTableLevel > 1
		{
			for index in 0..=Self::MAX_ENTRY_INDEX
			{
				if currentPageTableLevel == CPU::CPU::GetMaxPagingLevel () && index == Self::MAX_ENTRY_INDEX
				{
					continue;
				}

				let entry = self.GetEntry (index);

				if entry.IsPresent () && !entry.IsHugePage ()
				{
					entry.GetTable ().unwrap ().DumpTable (currentPageTableLevel - 1, targetPageTableLevel);
				}
			}
		}
	}
}

impl Physical::InPhysicalSpace for PageTable {}


#[derive(Clone, Copy, Eq, PartialEq)]
pub enum RegionStatus
{
	PrivateMapped(PageFlags),
	Shared,
	Reserved,
	Unmapped,
}


impl fmt::Display for RegionStatus
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		match self
		{
			Self::PrivateMapped(flags) => write! (f, "PrivateMapped({})", flags),
			Self::Shared => write! (f, "Shared"),
			Self::Reserved => write! (f, "Reserved"),
			Self::Unmapped => write! (f, "Unmapped"),
		}
	}
}

impl fmt::Debug for RegionStatus
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		match self
		{
			Self::PrivateMapped(flags) => write! (f, "PrivateMapped({:?})", flags),
			Self::Shared => write! (f, "Shared"),
			Self::Reserved => write! (f, "Reserved"),
			Self::Unmapped => write! (f, "Unmapped"),
		}
	}
}


#[derive(Clone, Copy)]
struct VirtualAddressSpaceRegion
{
	startAddress: VirtualAddress,
	endAddress: VirtualAddress,
	size: usize,
	regionStatus: RegionStatus,
}

impl VirtualAddressSpaceRegion
{
	pub const fn New (startAddress: VirtualAddress, size: usize, regionStatus: RegionStatus) -> Self
	{
		Self
		{
			startAddress,
			endAddress: startAddress + size - 1,
			size,
			regionStatus,
		}
	}

	pub const fn StartAddress (&self) -> VirtualAddress { self.startAddress }

	/// Set new region start address
	/// Auto adjust size
	pub fn SetStartAddress (&mut self, newStartAddress: VirtualAddress)
	{
		self.startAddress = newStartAddress;
		self.size = self.EndAddress () - self.StartAddress () + 1;
	}

	pub const fn EndAddress (&self) -> VirtualAddress { self.endAddress }

	/// Set new region end address
	/// Auto adjust size
	pub fn SetEndAddress (&mut self, newEndAddress: VirtualAddress)
	{
		self.endAddress = newEndAddress;
		self.size = self.EndAddress () - self.StartAddress () + 1;
	}

	pub const fn Size (&self) -> usize { self.size }

	/// Set new region size
	/// auto adjust end address based on start address and enw size
	pub fn SetSize (&mut self, newSize: usize)
	{
		self.size = newSize;
		self.endAddress = self.StartAddress () + self.Size () - 1;
	}

	pub const fn RegionStatus (&self) -> RegionStatus { self.regionStatus }

	/// Set new region status
	pub fn SetRegionStatus (&mut self, newRegionStatus: RegionStatus)
	{
		self.regionStatus = newRegionStatus;
	}
}


impl fmt::Display for VirtualAddressSpaceRegion
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "Start address: {}, size: 0x{:x}, end address: {}, status: {:?}", self.StartAddress (), self.Size (), self.EndAddress (), self.RegionStatus ())
	}
}

impl fmt::Debug for VirtualAddressSpaceRegion
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}

/// This struct is used to tracking of used address space
#[derive(Debug)]
pub struct VirtualAddressSpaceManager
{
	regions: PhysicalLinkedList<VirtualAddressSpaceRegion>,
}

impl VirtualAddressSpaceManager
{
	pub const fn New () -> Self
	{
		Self
		{
			regions: PhysicalLinkedList::New (),
		}
	}


	/// # Description:
	/// Return the region status.
	///
	/// # Args:
	/// - startAddress: region starting address.
	///
	/// # Return value:
	/// - (regionSize, regionStatus): the size and status of the region.
	///
	/// # Behavior:
	/// Return the region status, the region starts at specified address.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn GetRegionStatus (&self, startAddress: VirtualAddress) -> (usize, RegionStatus)
	{
		// iter through all regions
		let mut nextItemHeader = self.regions.FirstItemHeader ();

		// find the correct region
		while let Some(currentItemHeader) = nextItemHeader
		{
			let currentRegion = currentItemHeader.GetData ();

			if currentRegion.StartAddress () <= startAddress && currentRegion.EndAddress () >= startAddress
			{
				// inside a region
				let size = currentRegion.EndAddress () - startAddress + 1;
				let status = currentRegion.RegionStatus ();

				return (size, status);
			}
			else if currentRegion.StartAddress () > startAddress
			{
				// before a region
				let size = currentRegion.StartAddress () - startAddress + 1;
				let status = RegionStatus::Unmapped;

				return (size, status);
			}
			else { }

			nextItemHeader = currentItemHeader.GetNextItemHeader ();
		}

		// below all regions
		let size = VirtualAddress::MaxAddress () - startAddress + 1;
		let status = RegionStatus::Unmapped;

		return (size, status);
	}


	/// # Description:
	/// Update region status.
	///
	/// # Args:
	/// - startAddress: region starting address.
	/// - size: region size.
	/// - regionStatus: new region status.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// This function will overwrite old status.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn UpdateRegion (&mut self, startAddress: VirtualAddress, size: usize, regionStatus: RegionStatus)
	{
		if size == 0 { return; }

		let endAddress = startAddress + size - 1;
		let mut newRegion = VirtualAddressSpaceRegion::New (startAddress, size, regionStatus);

		self.RemoveRegion (startAddress, size);

		// iter through all regions
		let mut nextItemHeader = self.regions.FirstItemHeader ();

		// find the correct location to insert
		while let Some(currentItemHeader) = nextItemHeader
		{
			let currentRegion = currentItemHeader.GetData ();

			if currentRegion.StartAddress () > endAddress
			{
				break;
			}

			nextItemHeader = currentItemHeader.GetNextItemHeader ();
		}

		let mut currentItemHeader = self.regions.InsertItemBeforeItemHeader (newRegion, nextItemHeader);

		// check for merge with prev region
		if let Some(mut prevItemHeader) = currentItemHeader.GetPrevItemHeader ()
		{
			let mut prevRegion = prevItemHeader.GetData ();

			if prevRegion.EndAddress () + 1 == newRegion.StartAddress () && prevRegion.RegionStatus () == newRegion.RegionStatus ()
			{
				// merge
				prevRegion.SetSize (prevRegion.Size () + newRegion.Size ());
				prevItemHeader.SetData (prevRegion);

				// remove current region
				self.regions.RemoveItemHeader (currentItemHeader);

				// change current item header to prev item header
				currentItemHeader = prevItemHeader;
			}
		}

		// check for merge with next region
		if let Some(nextItemHeader) = currentItemHeader.GetNextItemHeader ()
		{
			let nextRegion = nextItemHeader.GetData ();

			if newRegion.EndAddress () + 1 == nextRegion.StartAddress () && nextRegion.RegionStatus () == newRegion.RegionStatus ()
			{
				// merge
				newRegion.SetSize (newRegion.Size () + nextRegion.Size ());
				currentItemHeader.SetData (newRegion);

				// remove next region
				self.regions.RemoveItemHeader (nextItemHeader);
			}
		}
	}


	/// # Description:
	/// Remove a region.
	///
	/// # Args:
	/// - startAddress: region starting address.
	/// - size: region size.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// This function will overwrite old status.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn RemoveRegion (&mut self, startAddress: VirtualAddress, size: usize)
	{
		if size == 0 { return; }

		let endAddress = startAddress + size - 1;

		// iter through all regions
		let mut nextItemHeader = self.regions.FirstItemHeader ();

		while let Some(mut currentItemHeader) = nextItemHeader
		{
			nextItemHeader = currentItemHeader.GetNextItemHeader ();

			let mut currentRegion = currentItemHeader.GetData ();

			if currentRegion.StartAddress () >= startAddress && currentRegion.EndAddress () <= endAddress
			{
				// current region is fully inside or equal
				self.regions.RemoveItemHeader (currentItemHeader);
			}
			else if startAddress > currentRegion.StartAddress () && endAddress < currentRegion.EndAddress ()
			{
				// current region is fully outside
				// break current region into 2
				let firstRegion = VirtualAddressSpaceRegion::New (currentRegion.StartAddress (), startAddress - currentRegion.StartAddress (), currentRegion.RegionStatus ());
				let secondRegion = VirtualAddressSpaceRegion::New (endAddress + 1, currentRegion.EndAddress () - endAddress, currentRegion.RegionStatus ());

				// insert them
				self.regions.InsertItemAfterItemHeader (secondRegion, Some(currentItemHeader));
				self.regions.InsertItemAfterItemHeader (firstRegion, Some(currentItemHeader));
				self.regions.RemoveItemHeader (currentItemHeader);
			}
			else if startAddress > currentRegion.StartAddress () && startAddress <= currentRegion.EndAddress ()
			{
				// current region lower part is overlap
				// shrink the end of current region
				currentRegion.SetSize (startAddress - currentRegion.StartAddress ());
				currentItemHeader.SetData (currentRegion);
			}
			else if endAddress < currentRegion.EndAddress () && endAddress >= currentRegion.StartAddress ()
			{
				// current region higher part is overlap
				// shrink the start of current region
				currentRegion.SetStartAddress (endAddress + 1);
				currentRegion.SetSize (currentRegion.EndAddress () - endAddress);
				currentItemHeader.SetData (currentRegion);
			}
			else { } // no overlap
		}
	}


	/// # Description:
	/// Reserve a free region with specified size. The reserved region is always contained inside the specified range.
	///
	/// # Args:
	/// - size: region size.
	/// - startAddress: specified range starting address.
	/// - endAddress: specified range ending address.
	///
	/// # Return value:
	/// - Some(regionAddress): starting address of the found region.
	/// - None: no region free with specified size is found within specified range.
	///
	/// # Behavior:
	/// The returned region is marked as Reserved.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn ReserveFreeRegion (&mut self, size: usize, startAddress: VirtualAddress, endAddress: VirtualAddress) -> Option<VirtualAddress>
	{
		// early exit
		if endAddress - startAddress + 1 < size
		{
			return None;
		}

		// iter through all regions
		let mut prevRegion: Option<ItemHeader<VirtualAddressSpaceRegion>> = None;
		let mut nextRegion = self.regions.FirstItemHeader ();
		let mut lastInuseAddress = VirtualAddress::MinAddress ();

		while !matches! (nextRegion, None)
		{
			lastInuseAddress = prevRegion.unwrap ().GetData ().EndAddress ();

			// calc free region start address
			let freeRegionStartAddress = if matches! (prevRegion, None)
			{
				VirtualAddress::MinAddress ()
			}
			else
			{
				prevRegion.unwrap ().GetData ().EndAddress () + 1
			};

			// calc free region end address
			let freeRegionEndAddress = nextRegion.unwrap ().GetData ().StartAddress () - 1;

			// try to get requested inside this free region
			let resultStartAddress = cmp::max (freeRegionStartAddress, startAddress);
			let resultEndAddress = resultStartAddress + size - 1;

			if resultEndAddress <= freeRegionEndAddress && resultEndAddress <= endAddress
			{
				// change region status to reserved
				self.UpdateRegion (resultStartAddress, size, RegionStatus::Reserved);

				return Some(resultStartAddress);
			}

			prevRegion = nextRegion;
			nextRegion = nextRegion.unwrap ().GetNextItemHeader ();
		}

		let resultStartAddress = if matches! (prevRegion, None)
		{
			startAddress
		}
		else
		{
			lastInuseAddress + 1
		};

		// try to get requested inside this free region
		let resultStartAddress = resultStartAddress;
		let resultEndAddress = resultStartAddress + size - 1;

		if resultEndAddress <= endAddress
		{
			// change region status to reserved
			self.UpdateRegion (resultStartAddress, size, RegionStatus::Reserved);

			return Some(resultStartAddress);
		}

		unreachable! ("Unreachable!");
	}

	fn PrintAllRegions (&self)
	{
		println! ("Total len: {}", self.regions.Len ());

		for (index, region) in self.regions.Iter ().enumerate ()
		{
			println! ("Region {}: {}", index, region);
		}
	}
}


#[derive(Debug, Clone, Copy)]
enum PhysicalMappingState
{
	UnMapped,
	Mapped(PhysicalAddress),
}


/// Convert platform-independent PageFlags to platform-dependent HardwarePageFlags
fn PageFlagsToHardwarePageFlags (pageFlags: PageFlags) -> HardwarePageFlags
{
	let mut result = HardwarePageFlags::New ();

	if pageFlags.IsPresent () { result.SetPresent (); } else { result.SetNotPresent (); }
	if pageFlags.IsWritable () { result.SetWritable (); } else { result.SetNotWritable (); }
	if pageFlags.IsUserAccessible () { result.SetUserAccessible (); } else { result.SetUserNotAccessible (); }
	if pageFlags.IsCacheEnabled () { result.SetCacheEnabled (); } else { result.SetCacheDisabled (); }
	if pageFlags.IsGlobal () { result.SetGlobal (); } else { result.SetNotGlobal (); }
	if pageFlags.IsExecutable () { result.SetExecutable (); } else { result.SetNotExecutable (); }

	result
}


#[derive(Debug, Clone)]
/// Contain data for multicore tlb shootdown interrupt
pub struct TLBShootdownData
{
	topLevelPageTableAddress: PhysicalAddress,
	startAddress: VirtualAddress,
	size: usize,
	forceTLBShootdown: bool,
	isMapChangingDone: Arc<AtomicBool>,
}

impl TLBShootdownData
{
	pub fn New (topLevelPageTableAddress: PhysicalAddress, startAddress: VirtualAddress, size: usize, forceTLBShootdown: bool, isMapChangingDone: Arc<AtomicBool>) -> Self
	{
		Self
		{
			topLevelPageTableAddress,
			startAddress,
			size,
			forceTLBShootdown,
			isMapChangingDone,
		}
	}
}


#[derive(Debug)]
struct VirtualAddressSpaceInner
{
	topLevelPageTable: PageTable,
	virtualAddressSpaceManager: VirtualAddressSpaceManager,
	physicalMappings: [PhysicalMappingState; VirtualAddressSpace::KERNEL_PHYSICAL_MAPPING_ENTRY_COUNT],
}


#[derive(Debug)]
pub struct VirtualAddressSpace
{
	pagingLevel: usize,
	innerData: RwMutex<VirtualAddressSpaceInner>,
}

impl VirtualAddressSpace
{
	const L1_PAGE_SIZE: usize = PAGE_SIZE;
	const L2_PAGE_SIZE: usize = Self::L1_PAGE_SIZE * 512;
	const L3_PAGE_SIZE: usize = Self::L2_PAGE_SIZE * 512;
	const L4_PAGE_SIZE: usize = Self::L3_PAGE_SIZE * 512;
	const L5_PAGE_SIZE: usize = Self::L4_PAGE_SIZE * 512;

	const KERNEL_NORMAL_ENTRY_FIRST_INDEX:		usize = 256;
	const KERNEL_NORMAL_ENTRY_LAST_INDEX:		usize = 486;

	const KERNEL_RESERVED_ENTRY1_INDEX:			usize = 487;
	const KERNEL_RESERVED_ENTRY2_INDEX:			usize = 488;
	const KERNEL_RESERVED_ENTRY3_INDEX:			usize = 489;
	const KERNEL_RESERVED_ENTRY4_INDEX:			usize = 490;
	const KERNEL_RESERVED_ENTRY5_INDEX:			usize = 491;
	const KERNEL_RESERVED_ENTRY6_INDEX:			usize = 492;
	const KERNEL_RESERVED_ENTRY7_INDEX:			usize = 493;
	const KERNEL_RESERVED_ENTRY8_INDEX:			usize = 494;
	const KERNEL_RESERVED_ENTRY_COUNT:			usize = 8;
	const KERNEL_RESERVED_ENTRY_INDEXES: [usize; Self::KERNEL_RESERVED_ENTRY_COUNT] = 
		[
			Self::KERNEL_RESERVED_ENTRY1_INDEX,
			Self::KERNEL_RESERVED_ENTRY2_INDEX,
			Self::KERNEL_RESERVED_ENTRY3_INDEX,
			Self::KERNEL_RESERVED_ENTRY4_INDEX,
			Self::KERNEL_RESERVED_ENTRY5_INDEX,
			Self::KERNEL_RESERVED_ENTRY6_INDEX,
			Self::KERNEL_RESERVED_ENTRY7_INDEX,
			Self::KERNEL_RESERVED_ENTRY8_INDEX
		];

	const KERNEL_PRIVATE_ENTRY1_INDEX:			usize = 495;
	const KERNEL_PRIVATE_ENTRY2_INDEX:			usize = 496;
	const KERNEL_PRIVATE_ENTRY3_INDEX:			usize = 497;
	const KERNEL_PRIVATE_ENTRY4_INDEX:			usize = 498;
	const KERNEL_PRIVATE_ENTRY5_INDEX:			usize = 499;
	const KERNEL_PRIVATE_ENTRY6_INDEX:			usize = 500;
	const KERNEL_PRIVATE_ENTRY7_INDEX:			usize = 501;
	const KERNEL_PRIVATE_ENTRY8_INDEX:			usize = 502;
	const KERNEL_PRIVATE_ENTRY_COUNT:			usize = 8;
	const KERNEL_PRIVATE_ENTRY_INDEXES: [usize; Self::KERNEL_PRIVATE_ENTRY_COUNT] = 
		[
			Self::KERNEL_PRIVATE_ENTRY1_INDEX,
			Self::KERNEL_PRIVATE_ENTRY2_INDEX,
			Self::KERNEL_PRIVATE_ENTRY3_INDEX,
			Self::KERNEL_PRIVATE_ENTRY4_INDEX,
			Self::KERNEL_PRIVATE_ENTRY5_INDEX,
			Self::KERNEL_PRIVATE_ENTRY6_INDEX,
			Self::KERNEL_PRIVATE_ENTRY7_INDEX,
			Self::KERNEL_PRIVATE_ENTRY8_INDEX
		];

	const KERNEL_PHYSICAL_MAPPING_ENTRY1_INDEX:	usize = 503;
	const KERNEL_PHYSICAL_MAPPING_ENTRY2_INDEX:	usize = 504;
	const KERNEL_PHYSICAL_MAPPING_ENTRY3_INDEX:	usize = 505;
	const KERNEL_PHYSICAL_MAPPING_ENTRY4_INDEX:	usize = 506;
	const KERNEL_PHYSICAL_MAPPING_ENTRY5_INDEX:	usize = 507;
	const KERNEL_PHYSICAL_MAPPING_ENTRY6_INDEX:	usize = 508;
	const KERNEL_PHYSICAL_MAPPING_ENTRY7_INDEX:	usize = 509;
	const KERNEL_PHYSICAL_MAPPING_ENTRY8_INDEX:	usize = 510;
	const KERNEL_PHYSICAL_MAPPING_ENTRY_COUNT:	usize = 8;
	const KERNEL_PHYSICAL_MAPPING_ENTRY_INDEXES: [usize; Self::KERNEL_PHYSICAL_MAPPING_ENTRY_COUNT] = 
		[
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY1_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY2_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY3_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY4_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY5_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY6_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY7_INDEX,
			Self::KERNEL_PHYSICAL_MAPPING_ENTRY8_INDEX
		];

	const RECURSIVE_MAPPING_ENTRY_INDEX:		usize = 511;

	// entry 503
	const BOOTLOADER_PHYSICAL_MAPPING_START_L4: KernelAddress = KernelAddress::New (0xffff_fb80_0000_0000);
	const BOOTLOADER_PHYSICAL_MAPPING_SIZE_L4: usize = 8*512*512*512*4096;
	const BOOTLOADER_MAX_PHYSICAL_ADDRESS_L4: PhysicalAddress = PhysicalAddress::New (Self::BOOTLOADER_PHYSICAL_MAPPING_SIZE_L4 - 1);

	// entry 503
	const BOOTLOADER_PHYSICAL_MAPPING_START_L5: KernelAddress = KernelAddress::New (0xfff7_0000_0000_0000);
	const BOOTLOADER_PHYSICAL_MAPPING_SIZE_L5: usize = 8*512*512*512*512*4096;
	const BOOTLOADER_MAX_PHYSICAL_ADDRESS_L5: PhysicalAddress = PhysicalAddress::New (Self::BOOTLOADER_PHYSICAL_MAPPING_SIZE_L5 - 1);


	/// # Description:
	/// Check if the entry index is kernel reserved entry index.
	///
	/// # Args:
	/// - entryIndex: the entry index to check.
	///
	/// # Return value:
	/// - True: the entry index is kernel reserved entry index.
	/// - False: the entry index is not kernel reserved entry index.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function is only intended for use in top level pagaing table.
	/// The result is only correct for top level paging table.
	///
	pub const fn IsKernelReservedEntry (entryIndex: usize) -> bool
	{
		let mut currentIndex = 0;

		while currentIndex < Self::KERNEL_RESERVED_ENTRY_INDEXES.len ()
		{
			if entryIndex == Self::KERNEL_RESERVED_ENTRY_INDEXES[currentIndex]
			{
				return true;
			}

			currentIndex += 1;
		}

		false
	}


	/// # Description:
	/// Check if the entry index is physical mapping entry index.
	///
	/// # Args:
	/// - entryIndex: the entry index to check.
	///
	/// # Return value:
	/// - True: the entry index is physical mapping entry index.
	/// - False: the entry index is not physical mapping entry index.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function is only intended for use in top level pagaing table.
	/// The result is only correct for top level paging table.
	///
	pub const fn IsPhysicalMappingEntry (entryIndex: usize) -> bool
	{
		let mut currentIndex = 0;

		while currentIndex < Self::KERNEL_PHYSICAL_MAPPING_ENTRY_INDEXES.len ()
		{
			if entryIndex == Self::KERNEL_PHYSICAL_MAPPING_ENTRY_INDEXES[currentIndex]
			{
				return true;
			}

			currentIndex += 1;
		}

		false
	}


	/// # Description:
	/// Check if the entry index is recursive mapping entry index.
	///
	/// # Args:
	/// - entryIndex: the entry index to check.
	///
	/// # Return value:
	/// - True: the entry index is recursive mapping entry index.
	/// - False: the entry index is not recursive mapping entry index.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function is only intended for use in top level pagaing table.
	/// The result is only correct for top level paging table.
	///
	pub const fn IsRecursiveMappingEntry (entryIndex: usize) -> bool
	{
		entryIndex == Self::RECURSIVE_MAPPING_ENTRY_INDEX
	}


	/// # Description:
	/// Create a new virtual address space.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// Newly created virtual address space.
	///
	/// # Behavior:
	/// Alloc paging struct as needed. Clone shared kernel virtual address space.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn New () -> Self
	{
		let newFrameAddress = FrameAllocator::GetEmptyNormalFrame ().expect ("Out of RAM!!!");

		let result = Self
			{
				pagingLevel: CPU::CPU::GetMaxPagingLevel (),
				innerData: RwMutex::New (VirtualAddressSpaceInner
				{
					topLevelPageTable: PageTable::New (newFrameAddress),
					virtualAddressSpaceManager: VirtualAddressSpaceManager::New (),
					physicalMappings: [PhysicalMappingState::UnMapped; Self::KERNEL_PHYSICAL_MAPPING_ENTRY_COUNT],
				}),
			};

		// recursive mapping
		result.innerData.WriterLock ().topLevelPageTable.SetRecursiveEntry (Self::RECURSIVE_MAPPING_ENTRY_INDEX);

		result
	}


	/// # Description:
	/// Return top level paging table address.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// Top level paging table address.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub (in crate::Arch::Amd64) fn TopLevelPageTableAddress (&self) -> PhysicalAddress
	{
		PhysicalAddress::New (self.innerData.ReaderLock ().topLevelPageTable.tableAddress.ToUsize ())
	}


	/// # Description:
	/// Return max paging level.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// Max level paging level.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub (in crate::Arch::Amd64) const fn PagingLevel (&self) -> usize { self.pagingLevel }

	const fn GetEntryAddress (pageTableIndex: PageTableIndex, offset: usize) -> VirtualAddress
	{
		todo! ();
	}


	/// # Description:
	/// Check if a virtual address is mapped.
	///
	/// # Args:
	/// - address: the address need to check.
	///
	/// # Return value:
	/// - True if `address` is mapped.
	/// - False if `address` is not mapped.
	///
	/// # Behavior:
	/// Call `VirtualToPhysical` for actual work.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// None.
	///
	pub fn IsVirtualAddressMapped (&self, address: VirtualAddress) -> bool
	{
		self.VirtualToPhysical (address) != None
	}


	/// # Description:
	/// Check if a virtual address is mapped.
	///
	/// # Args:
	/// - address: the address need to check.
	///
	/// # Return value:
	/// - True: `address` is mapped.
	/// - False: `address` is not mapped.
	///
	/// # Behavior:
	/// Call `VirtualToPhysicalInnerNoLock` for actual work.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function should only be called by internal code.
	///
	fn IsVirtualAddressMappedInnerNoLock (&self, address: VirtualAddress, topLevelPageTable: PageTable) -> bool
	{
		self.VirtualToPhysicalInnerNoLock (address, topLevelPageTable) != None
	}


	/// # Description:
	/// Translate a virtual address to physical address.
	///
	/// # Args:
	/// - address: the address need to translate.
	///
	/// # Return value:
	/// - Some(physicalAddress): `address` is mapped.
	/// - None: `address` is not mapped.
	///
	/// # Behavior:
	/// Call `VirtualToPhysicalInnerNoLock` for actual work.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	pub fn VirtualToPhysical (&self, address: VirtualAddress) -> Option<PhysicalAddress>
	{
		let innerData = self.innerData.ReaderLock ();
		self.VirtualToPhysicalInnerNoLock (address, innerData.topLevelPageTable)
	}


	/// # Description:
	/// Translate a virtual address to physical address using fastest path.
	///
	/// # Args:
	/// - address: the address need to translate, must not be in physical mapping range.
	/// - topLevelPageTable: top level page table of current virtual address space.
	///
	/// # Return value:
	/// - Some(physicalAddress): `address` is mapped.
	/// - None: `address` not mapped.
	///
	/// # Behavior:
	/// Will panic if `address` is in physical mapping range.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	/// Can translate address from any virtual address space.
	/// Will choose the fast or slow path depend on whether table address is equal to current table address in cpu
	///
	fn VirtualToPhysicalInnerNoLock (&self, address: VirtualAddress, topLevelPageTable: PageTable) -> Option<PhysicalAddress>
	{
		// if this address space is the same as currect address space, use the fast path
		if topLevelPageTable.TableAddress () == PhysicalAddress::from (CPU::CPU::ReadCR3 ())

		{
			self.VirtualToPhysicalFastInnerNoLock (address, topLevelPageTable)
		}
		else
		{
			self.VirtualToPhysicalSlowInnerNoLock (address, topLevelPageTable)
		}
	}


	/// # Description:
	/// Translate a virtual address to physical address using fast path (recursive mapping).
	///
	/// # Args:
	/// - address: the address need to translate, must not be in physical mapping range.
	/// - topLevelPageTable: top level page table of current virtual address space.
	///
	/// # Return value:
	/// - Some(physicalAddress): `address` is mapped.
	/// - None: `address` not mapped.
	///
	/// # Behavior:
	/// Will panic if `address` is in physical mapping range.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// Can only translate address from current virtual address space.
	///
	fn VirtualToPhysicalFastInnerNoLock (&self, address: VirtualAddress, topLevelPageTable: PageTable) -> Option<PhysicalAddress>
	{
		let recursiveIndex = if self.PagingLevel () == 5
		{
			[Self::RECURSIVE_MAPPING_ENTRY_INDEX; 5]
		}
		else
		{
			[PageTableIndex::CanonicalPageTableIndex (Self::RECURSIVE_MAPPING_ENTRY_INDEX), Self::RECURSIVE_MAPPING_ENTRY_INDEX, Self::RECURSIVE_MAPPING_ENTRY_INDEX, Self::RECURSIVE_MAPPING_ENTRY_INDEX, Self::RECURSIVE_MAPPING_ENTRY_INDEX]
		};

		// extract all indexes
		let pageTableIndex = PageTableIndex::from (address);

		let pageTableIndexL5 = if self.PagingLevel () == 5
		{
			pageTableIndex.PageTableIndexL5 ()
		}
		else
		{
			Self::RECURSIVE_MAPPING_ENTRY_INDEX
		};

		let pageTableIndexL4 = pageTableIndex.PageTableIndexL4 ();
		let pageTableIndexL3 = pageTableIndex.PageTableIndexL3 ();
		let pageTableIndexL2 = pageTableIndex.PageTableIndexL2 ();
		let pageTableIndexL1 = pageTableIndex.PageTableIndexL1 ();


		// check for l4 table existence
		// by checking if l5 entry is present
		// only check if 5 level paging
		// since otherwise, the table always exist
		if self.PagingLevel () == 5
		{
			let pageTableL5EntryIndex = PageTableIndex::New (recursiveIndex[0], recursiveIndex[1], recursiveIndex[2], recursiveIndex[3], recursiveIndex[4]);
			let pageTableL5EntryAddress = Self::GetEntryAddress (pageTableL5EntryIndex, pageTableIndexL5);
			let pageTableL5Entry = *pageTableL5EntryAddress.ToConst::<PageTableEntry> ();

			if !pageTableL5Entry.IsPresent ()
			{
				return None;
			}
		}

		// check for l3 table existence
		// by checking if l4 entry is present
		let pageTableL4EntryIndex = PageTableIndex::New (recursiveIndex[0], recursiveIndex[1], recursiveIndex[2], recursiveIndex[3], pageTableIndexL5);
		let pageTableL4EntryAddress = Self::GetEntryAddress (pageTableL4EntryIndex, pageTableIndexL4);
		let pageTableL4Entry = *pageTableL4EntryAddress.ToConst::<PageTableEntry> ();

		if !pageTableL4Entry.IsPresent ()
		{
			return None;
		}

		// check for l2 table existence
		// by checking if l3 entry is present
		let pageTableL3EntryIndex = PageTableIndex::New (recursiveIndex[0], recursiveIndex[1], recursiveIndex[2], pageTableIndexL5, pageTableIndexL4);
		let pageTableL3EntryAddress = Self::GetEntryAddress (pageTableL3EntryIndex, pageTableIndexL3);
		let pageTableL3Entry = *pageTableL3EntryAddress.ToConst::<PageTableEntry> ();

		if !pageTableL3Entry.IsPresent ()
		{
			return None;
		}

		// check if this is huge page
		if pageTableL3Entry.IsHugePage ()
		{
			// get the frame address
			let frameAddress = pageTableL3Entry.GetFrameL3Address ().unwrap ();

			// get the offset
			let offset = address.GetPageL3Offset ();

			// return
			return Some(frameAddress + offset);
		}

		// check for l1 table existence
		// by checking if l2 entry is present
		let pageTableL2EntryIndex = PageTableIndex::New (recursiveIndex[0], recursiveIndex[1], pageTableIndexL5, pageTableIndexL4, pageTableIndexL3);
		let pageTableL2EntryAddress = Self::GetEntryAddress (pageTableL2EntryIndex, pageTableIndexL2);
		let pageTableL2Entry = *pageTableL2EntryAddress.ToConst::<PageTableEntry> ();

		if !pageTableL2Entry.IsPresent ()
		{
			return None;
		}

		// check if this is huge page
		if pageTableL2Entry.IsHugePage ()
		{
			// get the frame address
			let frameAddress = pageTableL2Entry.GetFrameL2Address ().unwrap ();

			// get the offset
			let offset = address.GetPageL2Offset ();

			// return
			return Some(frameAddress + offset);
		}

		// check for the mapping present
		// by checking if l1 entry is present
		let pageTableL1EntryIndex = PageTableIndex::New (recursiveIndex[0], pageTableIndexL5, pageTableIndexL4, pageTableIndexL3, pageTableIndexL2);
		let pageTableL1EntryAddress = Self::GetEntryAddress (pageTableL1EntryIndex, pageTableIndexL1);
		let pageTableL1Entry = *pageTableL1EntryAddress.ToConst::<PageTableEntry> ();

		if !pageTableL1Entry.IsPresent ()
		{
			return None;
		}

		// get the frame address
		let frameAddress = pageTableL1Entry.GetFrameL1Address ().unwrap ();

		// get the offset
		let offset = address.GetPageL1Offset ();

		// return
		return Some(frameAddress + offset);
	}


	/// # Description:
	/// Translate a virtual address to physical address using slow path (travel page table).
	///
	/// # Args:
	/// - address: the address need to translate.
	/// - topLevelPageTable: top level page table of current virtual address space.
	///
	/// # Return value:
	/// - Some(physicalAddress): `address` is mapped.
	/// - None: `address` not mapped.
	///
	/// # Behavior:
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// Use slow path to translate the address.
	/// Can translate address from any virtual address space.
	///
	fn VirtualToPhysicalSlowInnerNoLock (&self, address: VirtualAddress, topLevelPageTable: PageTable) -> Option<PhysicalAddress>
	{
		let maxLevel = CPU::CPU::GetMaxPagingLevel ();
		let mut currentPageTable = topLevelPageTable;

		for currentPageTableLevel in (2..=maxLevel).rev ()
		{
			//get the current entry
			let currentPageTableEntry = currentPageTable.GetEntry (address.GetPageIndex (currentPageTableLevel));

			// if the entry is not present, return none
			if !currentPageTableEntry.IsPresent ()
			{
				return None;
			}

			// return if the entry is huge page
			if currentPageTableEntry.IsHugePage ()
			{
				match currentPageTableLevel
				{
					3 =>
					{
						let frameAddress = currentPageTableEntry.GetFrameL3Address ().unwrap ();
						let offset = address.GetPageL3Offset ();

						return Some(frameAddress + offset);
					},
					2 =>
					{
						let frameAddress = currentPageTableEntry.GetFrameL2Address ().unwrap ();
						let offset = address.GetPageL2Offset ();

						return Some(frameAddress + offset);
					},
					_ =>
					{
						panic! ("Unknown frame level({})", currentPageTableLevel);
					}
				}
			}

			// continue
			currentPageTable = currentPageTableEntry.GetTable ().unwrap ();
		}

		let currentPageTableEntry = currentPageTable.GetEntry (address.GetPageIndex (1));
		let frameAddress = currentPageTableEntry.GetFrameL1Address ()?;
		let offset = address.GetPageL1Offset ();

		return Some(frameAddress + offset);
	}


	/// # Description:
	/// Map frameAddress to pageAddress, alloc paging struct if necessary.
	///
	/// # Args:
	/// - frameAddress: the physical address to be mapped, need to be a valid frame address.
	/// - pageAddress: the virtual address to be mapped, need to be a valid page address and not in physical mapping range.
	/// - regionStatus: the status of the new mapping, need to be PrivateMapped or SharedMapped.
	/// - allowOverwrite: whether the functin is allowed to overwrite old mapping.
	///
	/// # Return value:
	/// Return old region status at that page address.
	///
	/// # Behavior:
	///
	/// Behavior with old status:
	/// - If `allowOverwrite == true`:
	/// 	- Private mapped: overwrite.
	/// 	- Reserved: overwrite.
	/// 	- Unmapped: overwrite.
	/// - If `allowOverwrite == false`:
	/// 	- Private mapped: panic.
	/// 	- Reserved: overwrite.
	/// 	- Unmapped: overwrite.
	///
	/// If alloc new paging struct entry:
	/// - Turn on those bits in all paging struct entry except the last one:
	/// 	+ USER_ACCESSIBLE
	/// 	+ WRITABLE
	/// 	+ PRESENT
	/// - Turn off those bits in all new paging struct entry except the last one:
	/// 	+ NO_EXECUTE
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function do not support huge page.
	///
	pub fn Map (&self, frameAddress: PhysicalAddress, pageAddress: VirtualAddress, regionStatus: RegionStatus, allowOverwrite: bool) -> RegionStatus
	{
		/*
		assert! (!pageAddress.IsInPhysicalMappingRange ());
		assert! (pageAddress.IsValidPageAddress ());
		assert! (frameAddress.IsValidFrameAddress ());

		let pageFlags = match regionStatus
		{
			RegionStatus::PrivateMapped(flags) => flags,
			_ => panic! ("regionStatus({:?}) isn't PrivateMapped", regionStatus),
		};

		let mut innerData = self.innerData.WriterLock ();
		let (_, oldRegionStatus) = innerData.virtualAddressSpaceManager.GetRegionStatus (pageAddress);

		// start tlb shootdown
		let isMapChangingDone = self.StartTLBShootdown (innerData.topLevelPageTable.TableAddress (), pageAddress, PAGE_SIZE);


		let hardwarePageFlags = PageFlagsToHardwarePageFlags (pageFlags);
		let mut middleHardwarePageFlags = HardwarePageFlags::New ();
		middleHardwarePageFlags.SetPresent ();
		middleHardwarePageFlags.SetWritable ();
		middleHardwarePageFlags.SetUserAccessible ();
		middleHardwarePageFlags.SetExecutable ();


		// loop to get page table level 1
		let mut currentTable = innerData.topLevelPageTable;

		for currentPageTableLevel in (2..=self.pagingLevel).rev ()
		{
			currentTable = currentTable.GetTableOrCreate (pageAddress.GetPageIndex (currentPageTableLevel), &middleHardwarePageFlags);
		}


		// map the frame address to page table 1
		currentTable.MapEntry (pageAddress.GetPageIndex (1), frameAddress, &hardwarePageFlags).GetFrameAddress ();

		// update self virtual address space manager
		innerData.virtualAddressSpaceManager.UpdateRegion (pageAddress, PAGE_SIZE, RegionStatus::PrivateMapped(pageFlags));

		// update shared kernel address space
		if pageAddress.IsSharedKernelAddress ()
		{
			sharedKernelAddressSpace.UpdateEntry (pageAddress.GetPageIndex (self.pagingLevel), innerData.topLevelPageTable.GetEntry (pageAddress.GetPageIndex (self.pagingLevel)));

			// update shared kernel virtual address space manager
			sharedKernelAddressSpace.CheckUpdateRegion (pageAddress, PAGE_SIZE, RegionStatus::PrivateMapped(pageFlags));
		}

		if !allowOverwrite
		{
			if let RegionStatus::PrivateMapped(oldFrameAddress) = oldRegionStatus
			{
				panic! ("Old mapping ({:?} -> {}) exist when attempt to map {} -> {} with allowOverwrite({})", oldFrameAddress, pageAddress, frameAddress, pageAddress, allowOverwrite);
			}
		}

		// invalid tlb
		self.InvalidTLB (pageAddress);

		// end tlb shootdown
		self.EndTLBShootdown (isMapChangingDone);

		oldRegionStatus
		*/
		todo! ();
	}


	/// # Description:
	/// Unmap an address range.
	///
	/// # Args:
	/// - startAddress: starting address of the will be ummaped region, must be a valid page address and not be in physical mapping range.
	/// - size: region size.
	/// - forceMapped: if true, will force the specified region to be mapped.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// Call `UnmapInnerNoLock` for actual unmap.
	/// If `forceMapped` is **true** and the specified region is reserved or unmapped, the function will panic.
	/// If `forceMapped` is **false** and there are unmapped regions, those will be skipped.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	pub fn Unmap (&self, startAddress: VirtualAddress, size: usize, forceMapped: bool) -> Result<(), VisualAddressSpaceError>
	{
		/*
		// basic checks
		assert! (startAddress.IsValidPageAddress ());
		assert! (!startAddress.IsInPhysicalMappingRange ());

		let endAddress = startAddress + size - 1;

		let mut innerData = self.innerData.WriterLock ();

		// for every region
		let mut nextRegionStartAddress = startAddress;
		while nextRegionStartAddress <= endAddress
		{
			let mut currentRegionStartAddress = nextRegionStartAddress;
			let (mut currentRegionSize, currentRegionStatus) = innerData.virtualAddressSpaceManager.GetRegionStatus (currentRegionStartAddress);
			currentRegionSize = cmp::min (currentRegionSize, endAddress - currentRegionStartAddress + 1);
			nextRegionStartAddress += currentRegionSize;

			// check for forceMapped
			if forceMapped && !matches! (currentRegionStatus, RegionStatus::PrivateMapped(_))
			{
				panic! ("The specified region is not fully mapped({}, {})", startAddress, size);
			}

			// if current region is unmapped, skip to next region
			if matches! (currentRegionStatus, RegionStatus::Unmapped)
			{
				continue;
			}

			// update virtual address space manager
			innerData.virtualAddressSpaceManager.RemoveRegion (currentRegionStartAddress, currentRegionSize);
			sharedKernelAddressSpace.CheckRemoveRegion (currentRegionStartAddress, currentRegionSize);

			// unmap the current region
			while currentRegionSize > 0
			{
				match currentRegionSize
				{
					s if s >= Self::L5_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L5_PAGE_SIZE) && self.pagingLevel >= 5 =>
					{
						self.UnmapInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L5_PAGE_SIZE, 5)?;
						currentRegionStartAddress += Self::L5_PAGE_SIZE;
						currentRegionSize -= Self::L5_PAGE_SIZE;
					},
					s if s >= Self::L4_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L4_PAGE_SIZE) && self.pagingLevel >= 4 =>
					{
						self.UnmapInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L4_PAGE_SIZE, 4)?;
						currentRegionStartAddress += Self::L4_PAGE_SIZE;
						currentRegionSize -= Self::L4_PAGE_SIZE;
					},
					s if s >= Self::L3_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L3_PAGE_SIZE) && self.pagingLevel >= 3 =>
					{
						self.UnmapInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L3_PAGE_SIZE, 3)?;
						currentRegionStartAddress += Self::L3_PAGE_SIZE;
						currentRegionSize -= Self::L3_PAGE_SIZE;
					},
					s if s >= Self::L2_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L2_PAGE_SIZE) && self.pagingLevel >= 2 =>
					{
						self.UnmapInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L2_PAGE_SIZE, 2)?;
						currentRegionStartAddress += Self::L2_PAGE_SIZE;
						currentRegionSize -= Self::L2_PAGE_SIZE;
					},
					s if s >= Self::L1_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L1_PAGE_SIZE) && self.pagingLevel >= 1 =>
					{
						self.UnmapInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L1_PAGE_SIZE, 1)?;
						currentRegionStartAddress += Self::L1_PAGE_SIZE;
						currentRegionSize -= Self::L1_PAGE_SIZE;
					},
					_ => unreachable! ("Impossible mapnew({}) size!", currentRegionSize),
				}
			}
		}

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Unmap an address range.
	///
	/// # Args:
	/// - topLevelPageTable: top level page table.
	/// - startAddress: starting address of the will be ummaped region, must be a valid page address, must not be in physical mapping range and aligned with `size`.
	/// - size: region size.
	/// - targetPageTableLevel: target page level, used to support batch ummapping.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// Will trigger tlb shootdown. Call `RecursiveFreeEntry` for freeing entry.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	fn UnmapInnerNoLock (&self, topLevelPageTable: PageTable, startAddress: VirtualAddress, size: usize, targetPageTableLevel: usize) -> Result<(), VisualAddressSpaceError>
	{
		/*
		assert! (self.pagingLevel >= targetPageTableLevel);
		assert! (startAddress.IsValidPageAddress ());
		assert! (!startAddress.IsInPhysicalMappingRange ());
		assert! (startAddress.IsAlign (size));

		// start tlb shootdown
		let isTLBShootdownDone = self.StartTLBShootdown (topLevelPageTable.TableAddress (), startAddress, size);

		// free the address range
		Self::RecursiveFreeEntry (startAddress, topLevelPageTable, targetPageTableLevel, self.pagingLevel);

		// update shared kernel address space
		if startAddress.IsSharedKernelAddress ()
		{
			sharedKernelAddressSpace.UpdateEntry (startAddress.GetPageIndex (self.pagingLevel), topLevelPageTable.GetEntry (startAddress.GetPageIndex (self.pagingLevel)));
		}

		// invalid tlb
		for address in (startAddress..startAddress + size).step_by (PAGE_SIZE)
		{
			self.InvalidTLB (address);
		}

		// end tlb shootdown
		self.EndTLBShootdown (isTLBShootdownDone);

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Recursive travel down and free page table entry.
	///
	/// # Args:
	/// - startAddress: starting address of the will be ummaped region, must be a valid page address, must not be in physical mapping range and aligned with `size`.
	/// - table: current page table.
	/// - targetPageTableLevel: target page level, used to support batch ummaping.
	/// - currentPageTableLevel: current page level, used to support batch ummaping.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Free the frame if meet, call `RecursiveFreeTableTree` to free the table tree otherwise.
	/// Then unwind to free all intermediate tables if they are empty.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	fn RecursiveFreeEntry (startAddress: VirtualAddress, mut table: PageTable, targetPageTableLevel: usize, currentPageTableLevel: usize)
	{
		/*
		if currentPageTableLevel == 1
		{
			// free the frame
			FrameAllocator::FreeFrame (table.MapEntry (startAddress.GetPageIndex (currentPageTableLevel), PhysicalAddress::New (0), &HardwarePageFlags::New ()).GetFrameAddress ().unwrap ());
		}
		else if currentPageTableLevel == targetPageTableLevel
		{
			// get next table
			let nextTable = table.GetTable (startAddress.GetPageIndex (currentPageTableLevel)).unwrap ();

			// free the table tree
			Self::RecursiveFreeTableTree (nextTable, 1, currentPageTableLevel - 1);

			// free the entry
			table.FreeEntryIfEmpty (startAddress.GetPageIndex (currentPageTableLevel));
		}
		else
		{
			// continue to travel down
			Self::RecursiveFreeEntry (startAddress, table.GetTable (startAddress.GetPageIndex (currentPageTableLevel)).unwrap (), targetPageTableLevel, currentPageTableLevel - 1);

			// free the entry
			table.FreeEntryIfEmpty (startAddress.GetPageIndex (currentPageTableLevel));
		}
		*/
	}


	/// # Description:
	/// Free the whole table tree.
	///
	/// # Args:
	/// - table: the page table to free.
	/// - lastPageTableLevel: the last page table level.
	/// - currentPageTableLevel: page level to start freeing at.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Does not free the table itself, it must be freed by caller.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	fn RecursiveFreeTableTree (mut table: PageTable, lastPageTableLevel: usize, currentPageTableLevel: usize)
	{
		/*
		assert! (currentPageTableLevel >= lastPageTableLevel);

		if currentPageTableLevel == lastPageTableLevel
		{
			// free all frames
			for index in 0..=PageTable::MAX_ENTRY_INDEX
			{
				// free the frame
				FrameAllocator::FreeFrame (table.MapEntry (index, PhysicalAddress::New (0), &HardwarePageFlags::New ()).GetFrameAddress ().unwrap ());
			}
		}
		else
		{
			for index in 0..=PageTable::MAX_ENTRY_INDEX
			{
				// get next table
				let nextTable = table.GetTable (index).unwrap ();

				// call down
				Self::RecursiveFreeTableTree (nextTable, lastPageTableLevel, currentPageTableLevel - 1);

				// free the entry
				table.FreeEntryIfEmpty (index);
			}
		}
		*/
		todo! ();
	}


	/// # Description:
	/// Map new an address range using newly allocated frames.
	///
	/// # Args:
	/// - startAddress: starting address of the new region, must be a valid page address and not in physical mapping range.
	/// - size: requested region size.
	/// - pageFlags: new region page flags.
	/// - needZeroNewFrame: whether newly allocated frames need to be zero'ed.
	/// - forceUnmapped: if true, will force the specified region to be unmapped or reserved.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// The new region will be PrivateMapped. All page table will be created as needed.
	/// If `needZeroNewFrame` is **true**, all the newly mapped frame will be zero'ed.
	/// If `forceUnmapped` is **true** and there is mapped region inside the requested region, the function will panic.
	/// If `forceUnmapped` is **false** and there are mapped regions, those will be skipped without changing anything.
	/// Call `MapNewInnerNoLock` for actual work.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	pub fn MapNew (&self, startAddress: VirtualAddress, size: usize, pageFlags: PageFlags, needZeroNewFrame: bool, forceUnmapped: bool) -> Result<(), VisualAddressSpaceError>
	{
		/*
		// basic checks
		assert! (startAddress.IsValidPageAddress ());
		assert! (!startAddress.IsInPhysicalMappingRange ());

		let endAddress = startAddress + size - 1;

		let mut innerData = self.innerData.WriterLock ();

		// for every region
		let mut nextRegionStartAddress = startAddress;
		while nextRegionStartAddress <= endAddress
		{
			let mut currentRegionStartAddress = nextRegionStartAddress;
			let (mut currentRegionSize, currentRegionStatus) = innerData.virtualAddressSpaceManager.GetRegionStatus (currentRegionStartAddress);
			currentRegionSize = cmp::min (currentRegionSize, endAddress - currentRegionStartAddress + 1);
			nextRegionStartAddress += currentRegionSize;

			// check for forceUnmapped
			if forceUnmapped && !matches! (currentRegionStatus, RegionStatus::Unmapped) && !matches! (currentRegionStatus, RegionStatus::Reserved)
			{
				panic! ("The specified region({}, {}) is not fully unmaped/reserved.", startAddress, size);
			}

			// if current region is mapped, skip to next region
			if matches! (currentRegionStatus, RegionStatus::PrivateMapped(_))
			{
				continue;
			}

			// update virtual address space manager
			innerData.virtualAddressSpaceManager.UpdateRegion (currentRegionStartAddress, currentRegionSize, RegionStatus::PrivateMapped(pageFlags));
			sharedKernelAddressSpace.CheckUpdateRegion (currentRegionStartAddress, currentRegionSize, RegionStatus::PrivateMapped(pageFlags));

			// map new the current region
			while currentRegionSize > 0
			{
				match currentRegionSize
				{
					s if s >= Self::L5_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L5_PAGE_SIZE) && self.pagingLevel >= 5 =>
					{
						self.MapNewInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L5_PAGE_SIZE, pageFlags, needZeroNewFrame, 5)?;
						currentRegionStartAddress += Self::L5_PAGE_SIZE;
						currentRegionSize -= Self::L5_PAGE_SIZE;
					},
					s if s >= Self::L4_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L4_PAGE_SIZE) && self.pagingLevel >= 4 =>
					{
						self.MapNewInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L4_PAGE_SIZE, pageFlags, needZeroNewFrame, 4)?;
						currentRegionStartAddress += Self::L4_PAGE_SIZE;
						currentRegionSize -= Self::L4_PAGE_SIZE;
					},
					s if s >= Self::L3_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L3_PAGE_SIZE) && self.pagingLevel >= 3 =>
					{
						self.MapNewInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L3_PAGE_SIZE, pageFlags, needZeroNewFrame, 3)?;
						currentRegionStartAddress += Self::L3_PAGE_SIZE;
						currentRegionSize -= Self::L3_PAGE_SIZE;
					},
					s if s >= Self::L2_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L2_PAGE_SIZE) && self.pagingLevel >= 2 =>
					{
						self.MapNewInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L2_PAGE_SIZE, pageFlags, needZeroNewFrame, 2)?;
						currentRegionStartAddress += Self::L2_PAGE_SIZE;
						currentRegionSize -= Self::L2_PAGE_SIZE;
					},
					s if s >= Self::L1_PAGE_SIZE && currentRegionStartAddress.IsAlign (Self::L1_PAGE_SIZE) && self.pagingLevel >= 1 =>
					{
						self.MapNewInnerNoLock (innerData.topLevelPageTable, currentRegionStartAddress, Self::L1_PAGE_SIZE, pageFlags, needZeroNewFrame, 1)?;
						currentRegionStartAddress += Self::L1_PAGE_SIZE;
						currentRegionSize -= Self::L1_PAGE_SIZE;
					},
					_ => unreachable! ("Impossible mapnew size ({}).", currentRegionSize),
				}
			}
		}

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Map new an address range using newly allocated frames.
	///
	/// # Args:
	/// - topLevelPageTable: top level page table.
	/// - startAddress: region start address, must be page aligned and size aligned.
	/// - size: requested region size.
	/// - pageFlags: new region page flags.
	/// - needZeroNewFrame: whether newly mapped frame will be zero'ed.
	/// - targetPageTableLevel: target page table level, used to support batch mapping.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// Will trigger tlb shootdown. Call `RecursiveMapNew` for freeing entry.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	fn MapNewInnerNoLock (&self, topLevelPageTable: PageTable, startAddress: VirtualAddress, size: usize, pageFlags: PageFlags, needZeroNewFrame: bool, targetPageTableLevel: usize) -> Result<(), VisualAddressSpaceError>
	{
		/*
		assert! (self.pagingLevel >= targetPageTableLevel);
		assert! (startAddress.IsValidPageAddress ());
		assert! (!startAddress.IsInPhysicalMappingRange ());
		assert! (startAddress.IsAlign (size));

		// start tlb shootdown
		let isTLBShootdownDone = self.StartTLBShootdown (topLevelPageTable.TableAddress (), startAddress, size);

		// build hardware page flags
		let hardwarePageFlags = PageFlagsToHardwarePageFlags (pageFlags);
		let mut middleHardwarePageFlags = HardwarePageFlags::New ();
		middleHardwarePageFlags.SetPresent ();
		middleHardwarePageFlags.SetWritable ();
		middleHardwarePageFlags.SetUserAccessible ();
		middleHardwarePageFlags.SetExecutable ();

		// loop to get the correct table
		let mut currentTable = topLevelPageTable;
		let mut currentPageTableLevel = self.pagingLevel;

		while currentPageTableLevel > targetPageTableLevel
		{
			currentTable = currentTable.GetTableOrCreate (startAddress.GetPageIndex (currentPageTableLevel), &middleHardwarePageFlags);
			currentPageTableLevel -= 1;
		}

		// map new
		if currentPageTableLevel == 1
		{
			// get the frame address
			let frameAddress = if needZeroNewFrame { FrameAllocator::GetEmptyNormalFrame () }
				else { FrameAllocator::GetNormalFrame () }.expect ("Out of RAM!!!");

			// map the frame
			currentTable.MapEntry (startAddress.GetPageIndex (currentPageTableLevel), frameAddress, &hardwarePageFlags).GetFrameAddress ();
		}
		else
		{
			// map new for that table tree
			let nextTable = currentTable.GetTableOrCreate (startAddress.GetPageIndex (currentPageTableLevel), &middleHardwarePageFlags);
			Self::RecursiveMapNew (nextTable, hardwarePageFlags, needZeroNewFrame, currentPageTableLevel - 1)?;
		}

		// update shared kernel address space
		if startAddress.IsSharedKernelAddress ()
		{
			sharedKernelAddressSpace.UpdateEntry (startAddress.GetPageIndex (self.pagingLevel), topLevelPageTable.GetEntry (startAddress.GetPageIndex (self.pagingLevel)));
		}

		// invalid tlb
		for address in (startAddress..startAddress + size).step_by (PAGE_SIZE)
		{
			self.InvalidTLB (address);
		}

		// end tlb shootdown
		self.EndTLBShootdown (isTLBShootdownDone);

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Recursive map new an address range using newly allocated frames.
	///
	/// # Args:
	/// - table: currentpage table.
	/// - hardwarePageFlags: hardware page flags of the newly mapped region.
	/// - needZeroNewFrame: whether newly mapped frame will be zero'ed.
	/// - currentPageTableLevel: current page table level, used to support batch mapping.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// Alloc new frame and paging struct as needed.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	fn RecursiveMapNew (mut table: PageTable, hardwarePageFlags: HardwarePageFlags, needZeroNewFrame: bool, currentPageTableLevel: usize) -> Result<(), VisualAddressSpaceError>
	{
		/*
		if currentPageTableLevel == 1
		{
			// change page flags for all frames
			for index in  0..=PageTable::MAX_ENTRY_INDEX
			{
				// get the frame address
				let frameAddress = if needZeroNewFrame { FrameAllocator::GetEmptyNormalFrame () }
					else { FrameAllocator::GetNormalFrame () }.expect ("Out of RAM!!!");

				// map the frame
				table.MapEntry (index, frameAddress, &hardwarePageFlags).GetFrameAddress ();
			}
		}
		else
		{
			for index in 0..=PageTable::MAX_ENTRY_INDEX
			{
				// get next table
				let nextTable = table.GetTable (index).unwrap ();

				// call down
				Self::RecursiveMapNew (nextTable, hardwarePageFlags, needZeroNewFrame, currentPageTableLevel - 1)?;
			}
		}

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Change flags of a region. The region need to be mapped.
	///
	/// # Args:
	/// - startAddress: start address of the region, must be a valid page address and not in physical mapping range.
	/// - size: requested region size.
	/// - pageFlags: new page flags for the requested region.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// This function will panic if the region is not fully mapped.
	/// Call `ChangeFlagsInnerNoLock` for actual work.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	pub fn ChangeFlags (&self, mut startAddress: VirtualAddress, mut size: usize, pageFlags: PageFlags) -> Result<(), VisualAddressSpaceError>
	{
		/*
		assert! (startAddress.IsValidPageAddress ());
		assert! (!startAddress.IsInPhysicalMappingRange ());

		let mut innerData = self.innerData.WriterLock ();
		let topLevelPageTable = innerData.topLevelPageTable;

		// check that the region is atually mapped
		let (regionSize, regionStatus) = innerData.virtualAddressSpaceManager.GetRegionStatus (startAddress);

		if regionSize < size || !matches! (regionStatus, RegionStatus::PrivateMapped(_))
		{
			panic! ("The requested region({}, {}) is not fully mapped.", startAddress, size);
		}

		// update virtual address space manager
		innerData.virtualAddressSpaceManager.UpdateRegion (startAddress, size, RegionStatus::PrivateMapped(pageFlags));
		sharedKernelAddressSpace.CheckUpdateRegion (startAddress, size, RegionStatus::PrivateMapped(pageFlags));

		while size > 0
		{
			match size
			{
				s if s >= Self::L5_PAGE_SIZE && startAddress.IsAlign (Self::L5_PAGE_SIZE) && self.pagingLevel >= 5 =>
				{
					self.ChangeFlagsInnerNoLock (topLevelPageTable, startAddress, Self::L5_PAGE_SIZE, pageFlags, 5)?;
					startAddress += Self::L5_PAGE_SIZE;
					size -= Self::L5_PAGE_SIZE;
				},
				s if s >= Self::L4_PAGE_SIZE && startAddress.IsAlign (Self::L4_PAGE_SIZE) && self.pagingLevel >= 4 =>
				{
					self.ChangeFlagsInnerNoLock (topLevelPageTable, startAddress, Self::L4_PAGE_SIZE, pageFlags, 4)?;
					startAddress += Self::L4_PAGE_SIZE;
					size -= Self::L4_PAGE_SIZE;
				},
				s if s >= Self::L3_PAGE_SIZE && startAddress.IsAlign (Self::L3_PAGE_SIZE) && self.pagingLevel >= 3 =>
				{
					self.ChangeFlagsInnerNoLock (topLevelPageTable, startAddress, Self::L3_PAGE_SIZE, pageFlags, 3)?;
					startAddress += Self::L3_PAGE_SIZE;
					size -= Self::L3_PAGE_SIZE;
				},
				s if s >= Self::L2_PAGE_SIZE && startAddress.IsAlign (Self::L2_PAGE_SIZE) && self.pagingLevel >= 2 =>
				{
					self.ChangeFlagsInnerNoLock (topLevelPageTable, startAddress, Self::L2_PAGE_SIZE, pageFlags, 2)?;
					startAddress += Self::L2_PAGE_SIZE;
					size -= Self::L2_PAGE_SIZE;
				},
				s if s >= Self::L1_PAGE_SIZE && startAddress.IsAlign (Self::L1_PAGE_SIZE) && self.pagingLevel >= 1 =>
				{
					self.ChangeFlagsInnerNoLock (topLevelPageTable, startAddress, Self::L1_PAGE_SIZE, pageFlags, 1)?;
					startAddress += Self::L1_PAGE_SIZE;
					size -= Self::L1_PAGE_SIZE;
				},
				_ => panic! ("Impossible mapnew size ({}).", size),
			}
		}

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Change flags of a region. The region need to be mapped.
	///
	/// # Args:
	/// - topLevelPageTable: top level page table.
	/// - startAddress: start address of the region, must be a valid page address and not in physical mapping range.
	/// - size: requested region size.
	/// - pageFlags: new page flags for the requested region.
	/// - targetPageTableLevel: target page table level, used to support batch change flags.
	///
	/// # Return value:
	/// - Ok(()): there is no error.
	/// - Err(visualAddressSpaceError): there is error.
	///
	/// # Behavior:
	/// Will start tlb shootdown.
	/// Call `ChangeFlagsInnerNoLock` for actual work.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	fn ChangeFlagsInnerNoLock (&self, topLevelPageTable: PageTable, startAddress: VirtualAddress, size: usize, pageFlags: PageFlags, targetPageTableLevel: usize) -> Result<(), VisualAddressSpaceError>
	{
		/*
		assert! (self.pagingLevel >= targetPageTableLevel);
		assert! (startAddress.IsValidPageAddress ());
		assert! (!startAddress.IsInPhysicalMappingRange ());
		assert! (startAddress.IsAlign (size));

		// start tlb shootdown
		let isTLBShootdownDone = self.StartTLBShootdown (topLevelPageTable.TableAddress (), startAddress, size);

		// build hardware page flags
		let hardwarePageFlags = PageFlagsToHardwarePageFlags (pageFlags);

		// loop to get the correct table
		let mut currentTable = topLevelPageTable;
		let mut currentPageTableLevel = self.pagingLevel;

		while currentPageTableLevel > targetPageTableLevel
		{
			currentTable = currentTable.GetTable (startAddress.GetPageIndex (currentPageTableLevel)).unwrap ();
			currentPageTableLevel -= 1;
		}

		// change page flags
		if currentPageTableLevel == 1
		{
			// get the frame address
			let frameAddress = currentTable.GetFrameAddress (startAddress.GetPageIndex (currentPageTableLevel)).unwrap ();

			// change the flags
			currentTable.MapEntry (startAddress.GetPageIndex (currentPageTableLevel), frameAddress, &hardwarePageFlags);
		}
		else
		{
			// change page flags for the whole table tree
			let nextTable = currentTable.GetTable (startAddress.GetPageIndex (currentPageTableLevel)).unwrap ();
			Self::RecursiveChangeFlagsTable (nextTable, hardwarePageFlags, currentPageTableLevel - 1);
		}

		// update shared kernel address space
		if startAddress.IsSharedKernelAddress ()
		{
			sharedKernelAddressSpace.UpdateEntry (startAddress.GetPageIndex (self.pagingLevel), topLevelPageTable.GetEntry (startAddress.GetPageIndex (self.pagingLevel)));
		}

		// invalid tlb
		for address in (startAddress..startAddress + size).step_by (PAGE_SIZE)
		{
			self.InvalidTLB (address);
		}

		// end tlb shootdown
		self.EndTLBShootdown (isTLBShootdownDone);

		Ok(())
		*/
		todo! ();
	}


	/// # Description:
	/// Change flags of a page table tree.
	///
	/// # Args:
	/// - table: the current page table.
	/// - hardwarePageFlags: new hardware page flags.
	/// - currentPageTableLevel: current page table level, used to support batch change flags.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Recursive tralvel down to the correct table and change page flags of all frame entry.
	/// The function will panic if there is missing entry.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	fn RecursiveChangeFlagsTable (mut table: PageTable, hardwarePageFlags: HardwarePageFlags, currentPageTableLevel: usize)
	{
		/*
		if currentPageTableLevel == 1
		{
			// change page flags for all frames
			for index in  0..=PageTable::MAX_ENTRY_INDEX
			{
				// get the frame address
				let frameAddress = table.GetFrameAddress (index).unwrap ();

				// change the flags
				table.MapEntry (index, frameAddress, &hardwarePageFlags);
			}
		}
		else
		{
			for index in 0..=PageTable::MAX_ENTRY_INDEX
			{
				// get next table
				let nextTable = table.GetTable (index).unwrap ();

				// call down
				Self::RecursiveChangeFlagsTable (nextTable, hardwarePageFlags, currentPageTableLevel - 1);
			}
		}
		*/
		todo! ();
	}


	/// # Description:
	/// Reserve a free region in shared kernel space.
	///
	/// # Args:
	/// - size: requested region size.
	///
	/// # Return value:
	/// - Some(regionAddress): starting address of the found region.
	/// - None: no free region with specified size is found.
	///
	/// # Behavior:
	/// The region is marked as reserved.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	pub fn ReserveFreeSharedKernelRegion (&self, size: usize) -> Option<KernelAddress>
	{
		/*
		let mut innerData = self.innerData.WriterLock ();
		let newRegionStartAddress = innerData.virtualAddressSpaceManager.ReserveFreeRegion (size, KernelAddress::MinSharedAddress ().into (), KernelAddress::MaxSharedAddress ().into ())?;

		sharedKernelAddressSpace.CheckUpdateRegion (newRegionStartAddress, size, RegionStatus::Reserved);

		Some (newRegionStartAddress.into ())
		*/
		todo! ();
	}


	/// # Description:
	/// Reserve a free region in user space.
	///
	/// # Args:
	/// - size: requested region size.
	///
	/// # Return value:
	/// - Some(address): starting address of the found region.
	/// - None: no free region with specified size is found.
	///
	/// # Behavior:
	/// The region is marked as reserved.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	/// This function does not support huge page.
	///
	pub fn ReserveFreeUserRegion (&self, size: usize) -> Option<UserAddress>
	{
		/*
		let mut innerData = self.innerData.WriterLock ();
		Some(innerData.virtualAddressSpaceManager.ReserveFreeRegion (size, UserAddress::MinAddress ().into (), UserAddress::MaxAddress ().into ())?.into ())
		*/
		todo! ();
	}

	/// # Description:
	///
	/// # Args:
	///
	/// # Return value:
	///
	/// # Behavior:
	///
	/// # Thread safe:
	///
	/// # Notes:
	///
	pub fn IsPhysicalAddressMapped (&self, physicalAddress: PhysicalAddress) -> bool
	{
		todo! ();
	}

	/// # Description:
	/// Load the virtual address space to the calling cpu.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Overwrite CR3 register.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	///
	pub fn Load (&self)
	{
		CPU::CPU::WriteCR3 (self.innerData.ReaderLock ().topLevelPageTable.tableAddress.ToU64 ());
	}


	/// # Description:
	/// Invalid a TLB entry at the specified address.
	///
	/// # Args:
	/// - pageAddress: the address to invalid TLB.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Directly invalid TLB entry at the specified address.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	///
	fn InvalidTLB (&self, pageAddress: VirtualAddress)
	{
		CPU::CPU::InvalidTLB (pageAddress);
	}


	/// # Description:
	/// Start the TLB shootdown process for multicore system with the specified region.
	///
	/// # Args:
	/// - topLevelPageTableAddress: top level page table address.
	/// - startAddress: start address of the specified region, must be page aligned and not in physical mapping range.
	/// - size: size of the specified region.
	///
	/// # Return value:
	/// - Some(flagPointer): the shared pointer to the atomic flag for signaling.
	/// - None: nothing to do.
	///
	/// # Behavior:
	/// Will send an inter processor interrupt for all cpus in the system with the specified region start address and size.
	/// Do nothing if only 1 cpu is started.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	fn StartTLBShootdown (&self, topLevelPageTableAddress: PhysicalAddress, startAddress: VirtualAddress, size: usize) -> Option<Arc<AtomicBool>>
	{
		/*
		// only do this if more than 1 cpu is started
		// also have a nice effect of bypassing boot virtual address space
		if SMP::GetInitializedCPUCount () > 1
		{
			// make the data
			let isMapChangingDone = Arc::new (AtomicBool::new (false));

			// Will force a tlb shootdown regardless address space if this is shared kernel address space
			let iccData = ICCData::TLBShootdown (TLBShootdownData::New (topLevelPageTableAddress, startAddress, size, startAddress.IsSharedKernelAddress (), Arc::clone (&isMapChangingDone)));

			// boardcast the message
			ICC::BoardcastICCMessage (Self::TLBShootdownReceiver, &iccData);

			Some(isMapChangingDone)
		}
		else
		{
			None
		}
		*/
		todo! ();
	}


	/// # Description:
	/// End TLB process.
	///
	/// # Args:
	/// - isMapChangingDone: an atomic flag received when starting TLB shootdown process.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// This function will panic if the passed pointer is None.
	/// Do nothing if only 1 cpu is started.
	///
	/// # Thread safe:
	/// Yes.
	///
	/// # Notes:
	///
	fn EndTLBShootdown (&self, isMapChangingDone: Option<Arc<AtomicBool>>)
	{
		// only do this if more than 1 cpu is started
		// also have a nice effect of bypassing boot virtual address space
		if SMP::GetInitializedCPUCount () > 1
		{
			isMapChangingDone.unwrap ().store (true, Ordering::SeqCst);
		}
	}


	/// # Description:
	/// Start the TLB shootdown process for receiver cpu.
	///
	/// # Args:
	/// - iccData: the data received from inter processor interrupt, must be of ICCData::TLBShootdown type.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// Do nothing if the starting virtuall address space is different from the current loaded one.
	/// This function will panic if called with incorrect data type.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// The interrupt must be disabled when this function is called.
	///
	fn TLBShootdownReceiver (iccData: ICCData)
	{
		/*
		// check the data type to ensure we get the correct data type
		if let ICCData::TLBShootdown(tlbShootdownData) = iccData
		{
			// continue if current virtual address space is different than the one being shootdown
			// and this isn't a forced tlb shootdown
			if !tlbShootdownData.forceTLBShootdown && tlbShootdownData.topLevelPageTableAddress != SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().TopLevelPageTableAddress ()
			{
				return;
			}

			// spin until the init cpu finished with changing the mapping
			while !tlbShootdownData.isMapChangingDone.load (Ordering::SeqCst)
			{
				CPU::CPU::Pause ();
			}

			// clear tlb for those addresses
			for offset in (0..tlbShootdownData.size).step_by (PAGE_SIZE)
			{
				CPU::CPU::InvalidTLB (tlbShootdownData.startAddress + offset);
			}
		}
		else
		{
			panic! ("Wrong ICCData type: {:?}", iccData);
		}
		*/
		todo! ();
	}


	pub fn PhysicalVolatileRead<T> (src: PhysicalAddress) -> T
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (src <= Self::GetBootloaderMaxPhysicalAddress ());
			(Self::GetBootloaderPhysicalMappingStart () + src.ToUsize ()).VolatileRead ()
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalVolatileWrite<T> (des: PhysicalAddress, value: T)
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (des <= Self::GetBootloaderMaxPhysicalAddress ());
			(Self::GetBootloaderPhysicalMappingStart () + des.ToUsize ()).VolatileWrite (value);
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalVolatileReadToBuffer (src: PhysicalAddress, buffer: VirtualAddress, len: usize)
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (src <= Self::GetBootloaderMaxPhysicalAddress ());
			assert! (src + len - 1 <= Self::GetBootloaderMaxPhysicalAddress ());

			let src = VirtualAddress::New ((Self::GetBootloaderPhysicalMappingStart () + src.ToUsize ()).ToUsize ());
			let des = buffer;

			VirtualAddress::VolatileMemCopy (src, des, len);
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalVolatileWriteFromBuffer (buffer: VirtualAddress, des: PhysicalAddress, len: usize)
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (des <= Self::GetBootloaderMaxPhysicalAddress ());
			assert! (des + len - 1 <= Self::GetBootloaderMaxPhysicalAddress ());

			let src = buffer;
			let des = VirtualAddress::New ((Self::GetBootloaderPhysicalMappingStart () + des.ToUsize ()).ToUsize ());

			VirtualAddress::VolatileMemCopy (src, des, len);
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalRead<T> (src: PhysicalAddress) -> T
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (src <= Self::GetBootloaderMaxPhysicalAddress ());

			unsafe
			{
				(Self::GetBootloaderPhysicalMappingStart () + src.ToUsize ()).ToConstRaw::<T> ().read_unaligned ()
			}
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalWrite<T> (des: PhysicalAddress, value: T)
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (des <= Self::GetBootloaderMaxPhysicalAddress ());

			unsafe
			{
				(Self::GetBootloaderPhysicalMappingStart () + des.ToUsize ()).ToMutRaw::<T> ().write_unaligned (value)
			}
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalReadToBuffer (src: PhysicalAddress, buffer: VirtualAddress, len: usize)
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (src <= Self::GetBootloaderMaxPhysicalAddress ());
			assert! (src + len - 1 <= Self::GetBootloaderMaxPhysicalAddress ());

			let src = VirtualAddress::New ((Self::GetBootloaderPhysicalMappingStart () + src.ToUsize ()).ToUsize ());
			let des = buffer;

			VirtualAddress::MemCopy (src, des, len);
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalWriteFromBuffer (buffer: VirtualAddress, des: PhysicalAddress, len: usize)
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (des <= Self::GetBootloaderMaxPhysicalAddress ());
			assert! (des + len - 1 <= Self::GetBootloaderMaxPhysicalAddress ());

			let src = buffer;
			let des = VirtualAddress::New ((Self::GetBootloaderPhysicalMappingStart () + des.ToUsize ()).ToUsize ());

			VirtualAddress::MemCopy (src, des, len);
		}
		else
		{
			todo! ();
		}
	}

	pub fn PhysicalMemSet (des: PhysicalAddress, len: usize, pattern: &[u8])
	{
		if Self::IsUsingBootloaderVirtualAddresSpace ()
		{
			assert! (des <= Self::GetBootloaderMaxPhysicalAddress ());
			assert! (des + len - 1 <= Self::GetBootloaderMaxPhysicalAddress ());
			assert! (pattern.len () > 0);

			let desSlice = unsafe { core::slice::from_raw_parts_mut::<u8> ((Self::GetBootloaderPhysicalMappingStart ().ToUsize () + des.ToUsize ()) as *mut u8, len) };

			for index in 0..desSlice.len ()
			{
				desSlice[index] = pattern[index % pattern.len ()];
			}
		}
		else
		{
			todo! ();
		}
	}

	fn GetBootloaderMaxPhysicalAddress () -> PhysicalAddress
	{
		if CPU::CPU::Is5LevelPagingSupported () { Self::BOOTLOADER_MAX_PHYSICAL_ADDRESS_L5 }
		else { Self::BOOTLOADER_MAX_PHYSICAL_ADDRESS_L4 }
	}

	fn GetBootloaderPhysicalMappingStart () -> KernelAddress
	{
		if CPU::CPU::Is5LevelPagingSupported () { Self::BOOTLOADER_PHYSICAL_MAPPING_START_L5 }
		else { Self::BOOTLOADER_PHYSICAL_MAPPING_START_L4 }
	}

	fn GetBootloaderPhysicalMappingSize () -> usize
	{
		if CPU::CPU::Is5LevelPagingSupported () { Self::BOOTLOADER_PHYSICAL_MAPPING_SIZE_L5 }
		else { Self::BOOTLOADER_PHYSICAL_MAPPING_SIZE_L4 }
	}

	pub fn IsUsingBootloaderVirtualAddresSpace () -> bool
	{
		unsafe { !isFinishedUsingBootloaderVirtualAddressSpace.IsSet () }
	}


	/// # Description:
	/// Set isFinishedUsingBootloaderVirtualAddressSpace flag.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// - Set isFinishedUsingBootloaderVirtualAddressSpace flag.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	///
	fn FinishUsingBootloaderVirtualAddresSpace ()
	{
		unsafe { isFinishedUsingBootloaderVirtualAddressSpace.Set () }
	}
}


/// Implement clean up code for virtual address space
impl Drop for VirtualAddressSpace
{
	/// # Description:
	/// Free all used resource of this virtual address space.
	///
	/// # Args:
	/// None.
	///
	/// # Return value:
	/// None.
	///
	/// # Behavior:
	/// - Shared kernel region: do nothing.
	/// - Private kernel region: recursive free every frame and page table.
	/// - Reserved kernel region: do nothing.
	/// - Physical mapping region: do nothing.
	/// - Recursive mapping region: do nothing.
	/// - Other regions: recursive free every frame and page table.
	///
	/// # Thread safe:
	/// No.
	///
	/// # Notes:
	/// This function does not support shared mapping.
	///
	fn drop (&mut self)
	{
		/*
		/// # Description:
		/// Free a whole page table.
		///
		/// # Args:
		/// - pageTable: the page table to free.
		/// - lastPageTableLevel: the page table level at that the traveling will stop.
		/// - freeAllFrame: whether to free all the frames at last level.
		/// - currentLevel: the current page table level.
		///
		/// # Return value:
		/// None.
		///
		/// # Behavior:
		/// Recursive free every frame and page table in this virtual address, except shared kernel address space.
		///
		/// # Thread safe:
		/// No.
		///
		/// # Notes:
		///
		fn RecursiveFreePageTable (pageTable: PageTable, lastPageTableLevel: usize, freeAllFrame: bool, currentLevel: usize)
		{
			assert! (currentLevel >= lastPageTableLevel);

			if currentLevel == lastPageTableLevel && freeAllFrame
			{
				for entryIndex in 0..=PageTable::MAX_ENTRY_INDEX
				{
					if let Some(frameAddress) = pageTable.GetEntry (entryIndex).GetFrameAddress ()
					{
						FrameAllocator::FreeFrame (frameAddress);
					}
				}
			}
			else
			{
				for entryIndex in 0..=PageTable::MAX_ENTRY_INDEX
				{
					if let Some(table) = pageTable.GetEntry (entryIndex).GetTable ()
					{
						RecursiveFreePageTable (table, lastPageTableLevel, freeAllFrame, currentLevel - 1);
					}
				}
			}

			// free this page table
			FrameAllocator::FreeFrame (pageTable.TableAddress ());
		}

		let innerData = self.innerData.WriterLock ();

		// make sure the current in use address space won't be dropped
		assert! (innerData.topLevelPageTable.TableAddress () != PhysicalAddress::New (CPU::CPU::ReadCR3 ().try_into ().unwrap ()));


		for entryIndex in 0..=PageTable::MAX_ENTRY_INDEX
		{
			match entryIndex
			{
				// do nothing if this is kernel shared entry, kernel reserved entry, recursive mapping entry
				_ if Self::IsKernelSharedEntry (entryIndex) => continue,
				_ if Self::IsKernelReservedEntry (entryIndex) => continue,
				_ if Self::IsRecursiveMappingEntry (entryIndex) => continue,
				_ if Self::IsKernelPrivateEntry (entryIndex) =>
				{
					if let Some(nextTable) = innerData.topLevelPageTable.GetTable (entryIndex)
					{
						RecursiveFreePageTable (nextTable, 1, true, self.pagingLevel - 1);
					}
				},
				_ if Self::IsPhysicalMappingEntry (entryIndex) =>
				{
					if let Some(nextTable) = innerData.topLevelPageTable.GetTable (entryIndex)
					{
						RecursiveFreePageTable (nextTable, 3, false, self.pagingLevel - 1);
					}
				},
				_ =>
				{
					if let Some(nextTable) = innerData.topLevelPageTable.GetTable (entryIndex)
					{
						RecursiveFreePageTable (nextTable, 1, true, self.pagingLevel - 1);
					}
				},
			}
		}

		// free top table
		FrameAllocator::FreeFrame (innerData.topLevelPageTable.TableAddress ());
		*/
		todo! ();
	}
}


/// This function must be called once per CPU at boot
pub fn EnablePagingFeatureFlags ()
{
	println! ("[CPU {}][+] Enable Paging Feature Flags...", CPU::CPU::GetCurrentCPUID ());

	unsafe
	{
		asm! (
				// save registers
				"push rax",
				"push rbx",
				"push rcx",
				"push rdx",

				// PGE bit
				"mov rax, cr4",
				"mov rbx, 1",
				"shl rbx, 7",
				"or rax, rbx",
				"mov cr4, rax",

				// NXE bit
				"mov ecx, 0xC0000080",
				"rdmsr",
				"mov ebx, 1",
				"shl ebx, 11",
				"or eax, ebx",
				"wrmsr",

				// WP bit
				"mov rax, cr0",
				"mov rbx, 1",
				"shl rbx, 16",
				"or rax, rbx",
				"mov cr0, rax",

				// restore registers
				"pop rdx",
				"pop rcx",
				"pop rbx",
				"pop rax",
			);
	}
}


/// This function must be called only once at boot
/// Create the address space to use at boot time, with empty allocated pages
/// The kernel stack is allocated at the end of the last KERNEL_RESERVED_ENTRY, which is 494
/// The stack need to be mapped to the same address because it contains many reference to other variables
fn CreateBootVirtualAddressSpace ()
{
	println! ("[Boot CPU][+] Creating Boot Virtual Address Space...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	let myBootVirtualAddressSpace = unsafe
	{
		*bootVirtualAddressSpace.get () = Some(VirtualAddressSpace::New ());
		(*bootVirtualAddressSpace.get ()).as_mut ().unwrap ()
	};


	// map kernel
	println! ("[Boot CPU][+] Mapping Kernel...");

	let mut kernelCodeAndDataPageFlags = PageFlags::New ();
	kernelCodeAndDataPageFlags.SetPresent ();
	kernelCodeAndDataPageFlags.SetWritable ();
	kernelCodeAndDataPageFlags.SetGlobal ();
	kernelCodeAndDataPageFlags.SetUserNotAccessible ();
	kernelCodeAndDataPageFlags.SetExecutable ();

	let kernelSpaceSize = ((GetKernelSpaceEndAddress () - GetKernelSpaceStartAddress () + 1 + PAGE_SIZE - 1)/PAGE_SIZE) * PAGE_SIZE;

	myBootVirtualAddressSpace.MapNew (GetKernelSpaceStartAddress ().into (), kernelSpaceSize, kernelCodeAndDataPageFlags, true, false).unwrap ();


	// map stack
	println! ("[Boot CPU][+] Mapping Stack...");

	let stackPageTableIndex = if CPU::CPU::Is5LevelPagingSupported ()
	{
		PageTableIndex::New (BOOTLOADER_STACK_ENTRY_INDEX + 1, 0, 0, 0, 0)
	}
	else
	{
		PageTableIndex::New (VirtualAddressSpace::RECURSIVE_MAPPING_ENTRY_INDEX, BOOTLOADER_STACK_ENTRY_INDEX + 1, 0, 0, 0)
	};

	let bootloaderStackStartAddress = VirtualAddress::from (stackPageTableIndex) - BOOTLOADER_STACK_SIZE;
	let bootloaderStackEndAddress = bootloaderStackStartAddress + BOOTLOADER_STACK_SIZE - 1;

	let mut kernelStackPageFlags = PageFlags::New ();
	kernelStackPageFlags.SetPresent ();
	kernelStackPageFlags.SetWritable ();
	kernelStackPageFlags.SetNotGlobal ();
	kernelStackPageFlags.SetUserNotAccessible ();
	kernelStackPageFlags.SetNotExecutable ();

	let kernelStackStartAddress = bootloaderStackStartAddress;
	let kernelStackEndAddress = bootloaderStackEndAddress;
	let kernelStackSize = kernelStackEndAddress - kernelStackStartAddress + 1;

	myBootVirtualAddressSpace.MapNew (kernelStackStartAddress, kernelStackSize, kernelStackPageFlags, false, false).unwrap ();


	// physical mapping
	println! ("[Boot CPU][+] Mapping Physical Space...");

	fn RecursiveMapPhysical (mut table: PageTable, currentLevel: usize, mut regionAddress: PhysicalAddress)
	{
		if currentLevel == CPU::CPU::GetMaxPagingLevel ()
		{
			let mut kernelPhysicalHardwarePageFlags3 = HardwarePageFlags::New ();
			kernelPhysicalHardwarePageFlags3.SetPresent ();
			kernelPhysicalHardwarePageFlags3.SetWritable ();
			kernelPhysicalHardwarePageFlags3.SetGlobal ();
			kernelPhysicalHardwarePageFlags3.SetUserNotAccessible ();
			kernelPhysicalHardwarePageFlags3.SetNotExecutable ();

			for entryIndex in 0..=PageTable::MAX_ENTRY_INDEX
			{
				if VirtualAddressSpace::IsPhysicalMappingEntry (entryIndex)
				{
					RecursiveMapPhysical (table.GetTableOrCreate (entryIndex, &kernelPhysicalHardwarePageFlags3), currentLevel - 1, regionAddress);
					regionAddress += 4096 << 9*(currentLevel - 1);
				}
			}
		}
		else if currentLevel == 3 // can use huge page now, begin to map using huge page
		{
			let mut kernelPhysicalHardwarePageFlags2 = HardwarePageFlags::New ();
			kernelPhysicalHardwarePageFlags2.SetPresent ();
			kernelPhysicalHardwarePageFlags2.SetWritable ();
			kernelPhysicalHardwarePageFlags2.SetGlobal ();
			kernelPhysicalHardwarePageFlags2.SetHugePage ();
			kernelPhysicalHardwarePageFlags2.SetUserNotAccessible ();
			kernelPhysicalHardwarePageFlags2.SetNotExecutable ();

			for index in 0..=PageTable::MAX_ENTRY_INDEX
			{
				table.MapEntry (index, regionAddress, 1, &kernelPhysicalHardwarePageFlags2);
				regionAddress += 4096 << 9*(currentLevel - 1);
			}
		}
		else // still middle table, too big, continue to go down
		{
			let mut kernelPhysicalHardwarePageFlags3 = HardwarePageFlags::New ();
			kernelPhysicalHardwarePageFlags3.SetPresent ();
			kernelPhysicalHardwarePageFlags3.SetWritable ();
			kernelPhysicalHardwarePageFlags3.SetGlobal ();
			kernelPhysicalHardwarePageFlags3.SetUserNotAccessible ();
			kernelPhysicalHardwarePageFlags3.SetNotExecutable ();

			for index in 0..=PageTable::MAX_ENTRY_INDEX
			{
				RecursiveMapPhysical (table.GetTableOrCreate (index, &kernelPhysicalHardwarePageFlags3), currentLevel - 1, regionAddress);
				regionAddress += 4096 << 9*(currentLevel - 1);
			}
		}
	}

	RecursiveMapPhysical (myBootVirtualAddressSpace.innerData.WriterLock ().topLevelPageTable, myBootVirtualAddressSpace.pagingLevel, PhysicalAddress::New (0));
}


/// This function must be called only once at boot
/// Copy data from bootloader address space to boot address space and switch to it
/// Including kernel, and stack
fn SwitchToBootVirtualAddressSpace ()
{
	println! ("[Boot CPU][+] Switching to Boot Virtual Address Space...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	// enable various paging flags
	// those bit is better coupled together
	println! ("[Boot CPU][+] Enable Paging Feature Flags...");
	EnablePagingFeatureFlags ();

	// this run in bootloader address space
	// with entry from boot address space mapped in tmp entries for copying data
	// tmp entries are KERNEL_PRIVATE_ENTRY_INDEXES[0] and KERNEL_PRIVATE_ENTRY_INDEXES[1]
	// src is the address from bootloader
	// des is the address from bootloader for temporary mapping

	// get bootloader kernel start virtual address
	let srcKernelSpaceStartAddress = GetKernelSpaceStartAddress ();

	// get kernel size
	let kernelSize = BootInfo::GetBootInfo ().kernelSize;

	// get des kernel start virtual address
	let desKernelPageTableIndex = if CPU::CPU::Is5LevelPagingSupported ()
	{
		PageTableIndex::New (VirtualAddressSpace::KERNEL_PRIVATE_ENTRY_INDEXES[0], 0, 0, 0, 0)
	}
	else
	{
		PageTableIndex::New (VirtualAddressSpace::RECURSIVE_MAPPING_ENTRY_INDEX, VirtualAddressSpace::KERNEL_PRIVATE_ENTRY_INDEXES[0], 0, 0, 0)
	};

	let desKernelSpaceStartAddress = VirtualAddress::from (desKernelPageTableIndex);


	// get src stack start address
	let srcStackStartPageTableIndex = if CPU::CPU::Is5LevelPagingSupported ()
	{
		PageTableIndex::New (BOOTLOADER_STACK_ENTRY_INDEX + 1, 0, 0, 0, 0)
	}
	else
	{
		PageTableIndex::New (VirtualAddressSpace::RECURSIVE_MAPPING_ENTRY_INDEX, BOOTLOADER_STACK_ENTRY_INDEX + 1, 0, 0, 0)
	};

	let srcKernelStackStartAddress = VirtualAddress::from (srcStackStartPageTableIndex) - BOOTLOADER_STACK_SIZE;

	// get stack size
	let kernelStackSize = KERNEL_BOOT_STACK_SIZE;

	// get des stack start address
	let desStackStartPageTableIndex = if CPU::CPU::Is5LevelPagingSupported ()
	{
		PageTableIndex::New (VirtualAddressSpace::KERNEL_PRIVATE_ENTRY_INDEXES[1] + 1, 0, 0, 0, 0)
	}
	else
	{
		PageTableIndex::New (VirtualAddressSpace::RECURSIVE_MAPPING_ENTRY_INDEX, VirtualAddressSpace::KERNEL_PRIVATE_ENTRY_INDEXES[1] + 1, 0, 0, 0)
	};

	let desKernelStackStartAddress = VirtualAddress::from (desStackStartPageTableIndex) - kernelStackSize;


	// get boot kernel table address
	let bootKernelSpaceTableAddress = unsafe
		{
			(*bootVirtualAddressSpace.get ()).as_mut ().unwrap ().innerData.ReaderLock ().topLevelPageTable.GetFrameAddress (VirtualAddressSpace::KERNEL_NORMAL_ENTRY_FIRST_INDEX, 1).unwrap ()
		};
	assert! (bootKernelSpaceTableAddress.IsValidFrameL1Address ());

	// get boot stack table address
	let bootKernelStackTableAddress = unsafe
		{
			(*bootVirtualAddressSpace.get ()).as_mut ().unwrap ().innerData.ReaderLock ().topLevelPageTable.GetFrameAddress (KERNEL_BOOT_STACK_ENTRY_INDEX, 1).unwrap ()
		};
	assert! (bootKernelStackTableAddress.IsValidFrameL1Address ());

	// get boot paging struct address
	let bootPagingStructAddress = unsafe { (*bootVirtualAddressSpace.get ()).as_mut ().unwrap ().innerData.ReaderLock ().topLevelPageTable.tableAddress };

	// from here is assembly
	println! ("[Boot CPU][+] Copying Kernel Space...");
	unsafe
	{
		asm! (
				// save registers
				"push rax",
				"push rbx",
				"push rcx",
				"push rsi",
				"push rdi",

				// map boot kernel start entry to bootloader kernel private entry 0
				"mov rax, cr3",
				"mov rbx, r14",
				"or rbx, 3",
				"mov qword ptr [rax + {privateEntry0}*8], rbx",
				"mov cr3, rax",

				// copy kernel space
				"mov rsi, r8",
				"mov rdi, r9",
				"mov rcx, r10",
				"rep movsb",

				// map boot kernel stack entry to bootloader kernel private entry 1
				"mov rax, cr3",
				"mov rbx, r15",
				"or rbx, 3",
				"mov qword ptr [rax + {privateEntry1}*8], rbx",
				"mov cr3, rax",

				// copy kernel stack
				"mov rsi, r11",
				"mov rdi, r12",
				"mov rcx, r13",
				"rep movsb",

				// load boot virtual address space to current cpu
				"xor rax, rax",
				"mov cr3, rdx",

				// restore registers
				"pop rdi",
				"pop rsi",
				"pop rcx",
				"pop rbx",
				"pop rax",

				privateEntry0 = const { VirtualAddressSpace::KERNEL_PRIVATE_ENTRY_INDEXES[0] },
				privateEntry1 = const { VirtualAddressSpace::KERNEL_PRIVATE_ENTRY_INDEXES[1] },

				in ("rdx") bootPagingStructAddress.ToU64 (),
				in ("r8") srcKernelSpaceStartAddress.ToU64 (),
				in ("r9") desKernelSpaceStartAddress.ToU64 (),
				in ("r10") kernelSize,
				in ("r11") srcKernelStackStartAddress.ToU64 (),
				in ("r12") desKernelStackStartAddress.ToU64 (),
				in ("r13") kernelStackSize,
				in ("r14") bootKernelSpaceTableAddress.ToU64 (),
				in ("r15") bootKernelStackTableAddress.ToU64 (),
			);
	}

	VirtualAddressSpace::FinishUsingBootloaderVirtualAddresSpace ();
}


pub fn InitBootVirtualAddressSpace ()
{
	println! ("[Boot CPU][+] Init Boot Virtual Address Space...");

	CreateBootVirtualAddressSpace ();
	println! ("[Boot CPU][+] Create Boot Virtual Address Space completed!");

	SwitchToBootVirtualAddressSpace ();
	println! ("[Boot CPU][+] Switch To Boot Virtual Address Space completed!");
}


pub fn GetKernelSpaceStartAddress () -> KernelAddress
{
	let kernelPageTableIndex = if CPU::CPU::Is5LevelPagingSupported ()
	{
		PageTableIndex::New (VirtualAddressSpace::KERNEL_NORMAL_ENTRY_FIRST_INDEX, 0, 0, 0, 0)
	}
	else
	{
		PageTableIndex::New (VirtualAddressSpace::RECURSIVE_MAPPING_ENTRY_INDEX, VirtualAddressSpace::KERNEL_NORMAL_ENTRY_FIRST_INDEX, 0, 0, 0)
	};

	KernelAddress::from (kernelPageTableIndex)
}


pub fn GetKernelSpaceEndAddress () -> KernelAddress
{
	let bootInfo = BootInfo::GetBootInfo ();
	GetKernelSpaceStartAddress () + bootInfo.kernelSize - 1
}


/// Let this be to initialize the heap
pub fn GetBootVirtualAddressSpaceRef () -> &'static mut VirtualAddressSpace
{
	unsafe { (*bootVirtualAddressSpace.get ()).as_mut ().unwrap () }
}


/// + Move boot virtual address space out
/// + Called in InitProcess to create boot process
/// + Boot virtual address space moved to boot process after calling this
pub fn GetBootVirtualAddressSpace () -> VirtualAddressSpace
{
	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	unsafe
	{
		(*bootVirtualAddressSpace.get ()).take ().unwrap ()
	}
}


#[derive(Debug)]
pub enum VisualAddressSpaceError
{
	VirtualAddressNotMapped(VirtualAddress),
	EntryMapped,
}