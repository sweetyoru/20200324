#![macro_use]

use core::fmt::{self, Write};
use core::cell::SyncUnsafeCell;

use crate::Arch::Amd64::PortIO;
use crate::Lock::{OneTimeFlag, Mutex};


/// all reg is byte wide
const COM1_BASE: u16 = 0x3F8;
const COM2_BASE: u16 = 0x2F8;
const COM3_BASE: u16 = 0x3E8;
const COM4_BASE: u16 = 0x2E8;

const DATA_REGISTER: u16 = 0;
const INTERRUPT_ENABLE_REGISTER: u16 = 1;
const INTERRUPT_IDENTIFICATION_REGISTER: u16 = 2;
const LINE_CONTROL_REGISTER: u16 = 3;
const MODEM_CONTROL_REGISTER: u16 = 4;
const LINE_STATUS_REGISTER: u16 = 5;
const MODEM_STATUS_REGISTER: u16 = 6;
const SCRATCH_REGISTER: u16 = 7;

// only accessible when DLAB set to 1
//
// To set the divisor to the controller:
//  + Set the most significant bit of the Line Control Register. This is the DLAB bit, and allows access to the divisor registers.
//  + Send the least significant byte of the divisor value to [PORT + 0].
//  + Send the most significant byte of the divisor value to [PORT + 1].
//  + Clear the most significant bit of the Line Control Register.
const LOWER_DIVISOR_REGISTER: u16 = 0;
const UPPER_DIVISOR_REGISTER: u16 = 1;

// number of bit in 1 character
const CHARACTER_LENGHT_SHIFT: u8 = 0;
const CHARACTER_LENGHT_5: u8 = 0b00<<CHARACTER_LENGHT_SHIFT;
const CHARACTER_LENGHT_6: u8 = 0b01<<CHARACTER_LENGHT_SHIFT;
const CHARACTER_LENGHT_7: u8 = 0b10<<CHARACTER_LENGHT_SHIFT;
const CHARACTER_LENGHT_8: u8 = 0b11<<CHARACTER_LENGHT_SHIFT;

// number of stop bit
const STOP_BIT_SHIFT: u8 = 2;
const STOP_BIT_1: u8 = 0<<STOP_BIT_SHIFT;
const STOP_BIT_2: u8 = 1<<STOP_BIT_SHIFT;

// number of parity bit
const PARITY_BIT_SHIFT: u8 = 3;
const PARITY_NONE: u8 = 0b000<<PARITY_BIT_SHIFT;
const PARITY_ODD: u8 = 0b001<<PARITY_BIT_SHIFT;
const PARITY_EVEN: u8 = 0b011<<PARITY_BIT_SHIFT;
const PARITY_MARK: u8 = 0b101<<PARITY_BIT_SHIFT;
const PARITY_SPACE: u8 = 0b111<<PARITY_BIT_SHIFT;

// interrupt enable bits, set the bit to enable correct interrupt
const DATA_AVAILABLE_BIT: u8 = 1<<0;
const TRANMISTER_EMPTY_BIT: u8 = 1<<1;
const BREAK_ERROR_BIT: u8 = 1<<2;
const STATUS_CHANGE_BIT: u8 = 1<<3;

// line status bits, use for polling
const DATA_READY: u8 = 1<<0;
const OVERRUN_ERROR: u8 = 1<<1;
const PARITY_ERROR: u8 = 1<<2;
const FRAMING_ERROR: u8 = 1<<3;
const BREAK_INDICATOR: u8 = 1<<4;
const TRANSMISTTER_BUFFER_EMPTY: u8 = 1<<5;
const TRANSMISTTER_EMPTY: u8 = 1<<6;
const IMPENDING_ERROR: u8 = 1<<7;


struct SerialWriter {}


impl fmt::Write for SerialWriter
{
	fn write_str (&mut self, s: &str) -> fmt::Result
	{
		SerialWrite (s);
		Ok(())
	}
}


/// only use for panic function
/// locking is useless when panic anyway
struct SerialWriterNoLock {}


impl fmt::Write for SerialWriterNoLock
{
	fn write_str (&mut self, s: &str) -> fmt::Result
	{
		SerialWrite (s);
		Ok(())
	}
}


static mySerialWriter: Mutex<SerialWriter> = Mutex::New (SerialWriter {});
static mySerialWriterNoLock: SyncUnsafeCell<SerialWriterNoLock> = SyncUnsafeCell::new (SerialWriterNoLock {});


/// Init serial line COM1 at 115200 8 bits, no parity, 1 stop bit
/// Follow from example code here: <https://wiki.osdev.org/Serial_ports>
/// init call guard work quite differently to this function
pub fn InitSerial ()
{
	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	// disable all serial interrupts
	PortIO::OutPortB (COM1_BASE + INTERRUPT_ENABLE_REGISTER, 0x00);

	// set DLAB bit
	PortIO::OutPortB (COM1_BASE + LINE_CONTROL_REGISTER, 0x80);

	// set baud rate to 115200
	PortIO::OutPortB (COM1_BASE + LOWER_DIVISOR_REGISTER, 0x01);
	PortIO::OutPortB (COM1_BASE + UPPER_DIVISOR_REGISTER, 0x00);

	// set line to 8bits, no parity, 1 stop bit
	// and clear DLAB bit
	PortIO::OutPortB (COM1_BASE + LINE_CONTROL_REGISTER, CHARACTER_LENGHT_8|PARITY_NONE|STOP_BIT_1);

	// enable FIFO
	PortIO::OutPortB (COM1_BASE + INTERRUPT_IDENTIFICATION_REGISTER, 0xC7);

	// IRQs enabled, RTS/DSR set
	PortIO::OutPortB (COM1_BASE + MODEM_CONTROL_REGISTER, 0x0B);
}


/// Check if serial port is ready to accept more data
fn IsTransmistterEmpty () -> bool
{
	PortIO::InPortB (COM1_BASE + LINE_STATUS_REGISTER)&TRANSMISTTER_BUFFER_EMPTY != 0
}


fn SerialWrite (data: &str)
{
	for c in data.bytes ()
	{
		// comment out due to testing error
		while !IsTransmistterEmpty () {}
		PortIO::OutPortB (COM1_BASE + DATA_REGISTER, c);
	}
}


#[macro_export]
macro_rules! print
{
	($($arg:tt)*) => ($crate::Debug::_print(format_args!($($arg)*)));
}


#[macro_export]
macro_rules! println
{
	() => (print!("\n"));
	($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}


// this function is hidden for document
// others module can call this but it is not encouraged
// this function only intended to use by print! macro
#[doc(hidden)]
pub fn _print (args: fmt::Arguments)
{
	mySerialWriter.Lock ().write_fmt (args).unwrap ();
}


// this function is hidden for document
// others module can call this but not encourage
// this function only intended to use by Lock module
#[doc(hidden)]
pub fn _printNoLock (args: fmt::Arguments)
{
	let tmpWriter = unsafe { &mut *mySerialWriterNoLock.get () };
	tmpWriter.write_fmt (args).unwrap ();
}
