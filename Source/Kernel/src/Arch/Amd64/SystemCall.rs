use core::cell::SyncUnsafeCell;

use crate::Interrupt;
use crate::DataStruct::DefaultHashSet;
use crate::Memory::Virtual::KernelAddress;
use crate::Interrupt::InterruptAllocationStrategy;
use crate::Arch::Amd64::{CPU::{self, CPUID}, Interrupt::InterruptStackFrame};

pub use crate::SystemCall::*;


/// This function must be called once at boot by each CPU
pub fn InsertSystemCallInterruptHandler ()
{
	println! ("[CPU {}][+] Insert System Call interrupt handler...", CPU::CPU::GetCurrentCPUID ());

	static calledCpu: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCpu.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	let handlerAddress = KernelAddress::New (SystemCallInterruptHandler as *const () as usize);
	let interruptAllocationStrategy = InterruptAllocationStrategy::Fixed (systemCallInterruptID);
	Interrupt::AddInterruptHandler (handlerAddress, interruptAllocationStrategy, true).unwrap ();
}


extern "x86-interrupt" fn SystemCallInterruptHandler (_interruptStackFrame: InterruptStackFrame)
{
	unimplemented! ();
}


fn GetInput ()
{
	unimplemented! ();
}


fn SendOutput ()
{
	unimplemented! ();
}
