use core::cell::SyncUnsafeCell;

use crate::DataStruct::DefaultHashSet;
use crate::Memory::Virtual::KernelAddress;
use crate::Arch::Amd64::CPU::{self, CPUID, APIC};
use crate::Arch::Amd64::Interrupt::InterruptStackFrame;
use crate::Interrupt::{self, InterruptAllocationStrategy};

use crate::ICC;


/// This function must be called once at boot by each CPU
pub fn InsertICCInterruptHandler (interruptAllocationStrategy: InterruptAllocationStrategy)
{
	println! ("[CPU {}][+] Insert ICC System interrupt handler...", CPU::CPU::GetCurrentCPUID ());

	static calledCpu: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCpu.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	let handlerAddress = KernelAddress::New (ICCInterruptHandler as *const () as usize);
	Interrupt::AddInterruptHandler (handlerAddress, interruptAllocationStrategy, false).unwrap ();
}


/// The entry for ICC interrupt
extern "x86-interrupt" fn ICCInterruptHandler (_interruptStackFrame: InterruptStackFrame)
{
	ICC::ReceiveICCMessage ();
	APIC::SendEOI ();
}
