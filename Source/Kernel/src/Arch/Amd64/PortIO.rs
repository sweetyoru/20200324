use core::arch::asm;


pub fn InPortB (port: u16) -> u8
{
	let data: u8;

	unsafe
	{
		asm! (
				"in al, dx",
				in ("dx") port,
				out ("al") data,
			);
	}

	data
}

pub fn InPortW (port: u16) -> u16
{
	let data: u16;

	unsafe
	{
		asm! (
				"in ax, dx",
				in ("dx") port,
				out ("ax") data,
			);
	}

	data
}

pub fn InPortD (port: u16) -> u32
{
	let data: u32;

	unsafe
	{
		asm! (
				"in eax, dx",
				in ("dx") port,
				out ("eax") data,
			);
	}

	data
}

pub fn OutPortB (port: u16, data: u8)
{
	unsafe
	{
		asm! (
				"out dx, al",
				in ("al") data,
				in ("dx") port,
			);
	}
}

pub fn OutPortW (port: u16, data: u16)
{
	unsafe
	{
		asm! (
				"out dx, ax",
				in ("ax") data,
				in ("dx") port,
			);
	}
}

pub fn OutPortD (port: u16, data: u32)
{
	unsafe
	{
		asm! (
				"out dx, eax",
				in ("eax") data,
				in ("dx") port,
			);
	}
}
