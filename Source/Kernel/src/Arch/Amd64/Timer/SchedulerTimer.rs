//! This module provide scheduler timer for use in scheduler


use core::cell::SyncUnsafeCell;

use crate::Scheduler;
use crate::Timer::TimerMode;
use crate::DataStruct::DefaultHashSet;
use crate::Memory::Virtual::KernelAddress;
use crate::Arch::Amd64::Interrupt::InterruptStackFrame;
use crate::Arch::Amd64::CPU::{self, CPUID, APIC::{self, X2ApicMSR}};
use crate::Interrupt::{self, InterruptID, InterruptAllocationStrategy};


const schedulerTimerInterruptID: InterruptID = InterruptID::New (76);
const schedulerTimerInterruptAllocationStrategy: InterruptAllocationStrategy = InterruptAllocationStrategy::Fixed(schedulerTimerInterruptID);

static SchedulerTimerSpeed: SyncUnsafeCell<usize> = SyncUnsafeCell::new (0);


/// Init Local Apic timer to OneShot mode, divide value == 1
/// This function must be called only once per CPU at boot
pub fn InitSchedulerTimer ()
{
	const TSC_DISABLE_BIT: u64 = 1<<2;

	println! ("[CPU {}][+] Init Scheduler Timer...", CPU::CPU::GetCurrentCPUID ());

	static calledCPUID: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCPUID.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	// set vector + unmask timer interrupt
	APIC::WriteReg (X2ApicMSR::LvtTimer, schedulerTimerInterruptID.ToUsize () as u64);

	// set divide value to 1
	APIC::WriteReg (X2ApicMSR::DivideConfiguration, (APIC::ReadReg (X2ApicMSR::DivideConfiguration)&!0b1111)|0b1011);

	// set mode to one shot
	SetMode (TimerMode::OneShot);

	// insert interrupt handler
	let handlerAddress = KernelAddress::New (ScheduleTimerInterruptHandler as *const () as usize);
	Interrupt::AddInterruptHandler (handlerAddress, schedulerTimerInterruptAllocationStrategy, false).unwrap ();
}


extern "x86-interrupt" fn ScheduleTimerInterruptHandler (_interruptStackFrame: InterruptStackFrame)
{
	SetInitCount (0);
	APIC::SendEOI ();
	Scheduler::GlobalScheduleTimerInterruptCallback ();
}

fn SetInitCount (count: usize) { APIC::WriteReg (X2ApicMSR::InitialCount, count as u64); }

fn SetMode (mode: TimerMode)
{
	const TIMER_MODE_SHIFT: usize = 17;
	const TIMER_MODE_MASK: u64 = 0b11<<TIMER_MODE_SHIFT;

	let mode = match mode
	{
		TimerMode::OneShot => 0b00,
		TimerMode::Period => 0b01,
	};

	// calc new timer mode
	let newLvtTimerValue = (mode<<TIMER_MODE_SHIFT)|(APIC::ReadReg (X2ApicMSR::LvtTimer)&!TIMER_MODE_MASK);

	// set new timer mode
	APIC::WriteReg (X2ApicMSR::LvtTimer, newLvtTimerValue);
}

pub fn EnableSchedulerTimer () { SetInitCount (1); }
pub fn DisableSchedulerTimer () { SetInitCount (0); }
pub fn SetNextInterrupt (count: usize) { SetInitCount (count); }
