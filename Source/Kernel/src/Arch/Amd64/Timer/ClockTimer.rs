//! This module is used to keep track of time
//! Export functions:
//!		+ Init
//!		+ Time From Boot
//!		+ Sleep
//! Use HPET


use core::cell::SyncUnsafeCell;

use crate::Lock::OneTimeFlag;
use crate::Timer::TimeDuration;
use crate::DataStruct::DefaultHashSet;
use crate::Arch::Amd64::Timer::HPET;
use crate::Memory::Virtual::KernelAddress;
use crate::Arch::Amd64::CPU::{self, CPUID};
use crate::Interrupt::{self, InterruptID, InterruptAllocationStrategy};


const clockTimerInterruptID: InterruptID = InterruptID::New (77);
const clockTimerInterruptAllocationStrategy: InterruptAllocationStrategy = InterruptAllocationStrategy::Fixed(clockTimerInterruptID);


/// This function must be called only once at boot by boot CPU
pub fn InitClockTimerGlobalPart ()
{
	println! ("[Boot CPU][+] Init Clock Timer global part...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	HPET::InitHPET (clockTimerInterruptID);
}


/// This function must be called once per CPU at boot
pub fn InitClockTimerLocalPart ()
{
	println! ("[CPU {}][+] Init Clock Timer local part...", CPU::CPU::GetCurrentCPUID ());

	static calledCPUID: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCPUID.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	// insert HPET interrupt handler
	let handlerAddress = KernelAddress::New (HPET::HpetInterruptHandler as *const () as usize);
	Interrupt::AddInterruptHandler (handlerAddress, clockTimerInterruptAllocationStrategy, false).unwrap ();
}


pub fn TimeFromBoot () -> TimeDuration
{
	HPET::TimeFromBoot ()
}
