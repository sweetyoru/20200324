//! This module contain code for PIT


use core::cell::SyncUnsafeCell;

use crate::Lock::OneTimeFlag;
use crate::Arch::Amd64::PortIO;
use crate::Arch::Amd64::CPU::APIC;
use crate::Memory::Virtual::KernelAddress;
use crate::Timer::{TimeUnit, TimeDuration};
use crate::Arch::Amd64::CPU::{InterruptVector};
use crate::Arch::Amd64::Interrupt::{IOAPIC, InterruptStackFrame};
use crate::Interrupt::{self, InterruptID, InterruptAllocationStrategy, InterruptTriggerMode, InterruptPinPolarity};


const PIT_IRQ_NUMBER: usize = 0;
// this is the most accurate frequency divide value
const PIT_FREQUENCY_DIVIDER_VALUE: usize = 1050;
// 880.000111746
const MICROSECOND_PER_PIT_TICK: usize = 880;

const PIT_CHANEL0_DATA_PORT: u16 = 0x40;
const PIT_CHANEL1_DATA_PORT: u16 = 0x41;
const PIT_CHANEL2_DATA_PORT: u16 = 0x42;
const PIT_MODE_COMMAND_PORT: u16 = 0x43;



static pitTicksFromBoot: SyncUnsafeCell<usize> = SyncUnsafeCell::new (0);


/// This function must be called only once at boot by boot CPU
pub (in crate::Arch::Amd64) fn InitPIT (vector: InterruptVector)
{
	println! ("[Boot CPU][+] Init PIT...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	println! ("[Boot CPU][+] Insert PIT Interrupt Handler...");

	IOAPIC::SetIRQInterruptNumber (PIT_IRQ_NUMBER, InterruptTriggerMode::Edge, InterruptPinPolarity::HighActive, vector);

	// insert interrupt handler
	let interruptAllocationStrategy = InterruptAllocationStrategy::Fixed(InterruptID::New (vector.ToUsize ()));
	let handlerAddress = KernelAddress::New (PitInterruptHandler as *const () as usize);
	Interrupt::AddInterruptHandler (handlerAddress, interruptAllocationStrategy, false).unwrap ();


	// set pit frequency divider value
	// channel 0, lo/hi access mode
	PortIO::OutPortB (PIT_MODE_COMMAND_PORT, 0b00_11_011_0);
	PortIO::OutPortB (PIT_CHANEL0_DATA_PORT, (PIT_FREQUENCY_DIVIDER_VALUE&0xff) as u8);
	PortIO::OutPortB (PIT_CHANEL0_DATA_PORT, ((PIT_FREQUENCY_DIVIDER_VALUE&0xff00)>>8) as u8);

	println! ("[Boot CPU][+] Enable PIT...");
	IOAPIC::EnableIRQ (PIT_IRQ_NUMBER);
}

pub (in crate::Arch::Amd64) fn TimeFromBoot () -> TimeDuration
{
	unsafe { TimeDuration::New (TimeUnit::MICROSECOND, (*pitTicksFromBoot.get ())*MICROSECOND_PER_PIT_TICK) }
}

extern "x86-interrupt" fn PitInterruptHandler (_interruptStackFrame: InterruptStackFrame)
{
	unsafe { *pitTicksFromBoot.get () += 1; }
	APIC::SendEOI ();
}
