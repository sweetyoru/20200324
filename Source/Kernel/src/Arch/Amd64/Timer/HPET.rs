//! This module contain code for HPETimer


use core::cell::SyncUnsafeCell;

use alloc::vec::Vec;

use crate::Interrupt::InterruptID;
use crate::Arch::Amd64::CPU::APIC;
use crate::Lock::{OneTimeFlag, RwMutex};
use crate::Timer::{TimeUnit, TimeDuration};
use crate::Memory::Physical::PhysicalAddress;
use crate::Arch::Amd64::ACPI::{self, SdtTableType, HPETPointer};
use crate::Arch::Amd64::Interrupt::{IOAPIC, InterruptStackFrame};
use crate::Interrupt::{InterruptTriggerMode, InterruptPinPolarity};


const HPET_TIMER0_LEGACY_IRQ_NUMBER: usize = 0;
const INTERRUPT_PER_SECOND: usize = 100;


#[derive(Debug)]
struct HPETComparator
{
	baseAddress: PhysicalAddress,
	counterIs64bit: bool,
	havePeriodicMode: bool,
}

impl HPETComparator
{
	const CONFIGURATION_AND_CAPABILITY: usize = 0;
	const COMPARATOR_VALUE: usize = 0x08;
	const FSB_INTERRUPT_ROUTE: usize = 0x10;


	pub fn New (baseAddress: PhysicalAddress) -> Self
	{
		let configurationAndCapability = (baseAddress + Self::CONFIGURATION_AND_CAPABILITY).VolatileRead::<u64> ();

		Self
		{
			baseAddress,
			counterIs64bit: configurationAndCapability&(1<<5) != 0,
			havePeriodicMode: configurationAndCapability&(1<<4) != 0,
		}
	}


	fn ReadReg (&self, offset: usize) -> u64
	{
		(self.baseAddress + offset).VolatileRead::<u64> ()
	}


	fn WriteReg (&mut self, offset: usize, value: u64)
	{
		(self.baseAddress + offset).VolatileWrite::<u64> (value)
	}


	pub fn Force64bitMode (&mut self)
	{
		assert! (self.counterIs64bit, "Force 64bit mode on unsupport comparator!");

		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY)&!(1<<8));
	}


	pub fn MapToIRQX (&mut self, irqNumber: u64)
	{
		assert! (irqNumber < 32, "IRQ number too big!");

		let configurationAndCapability = self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY);

		let bitmap = (configurationAndCapability&0xffff_ffff_0000_0000)>>32;

		assert! ((bitmap&(1<<irqNumber)) != 0, "Unsupported irq mapping!");

		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, configurationAndCapability|(irqNumber<<9));
	}


	pub fn EnablePeriodicMode (&mut self)
	{
		assert! (self.havePeriodicMode, "Trying to enable periodic mode when comparator don't support it!");

		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY)|(1<<3));
	}


	pub fn DisablePeriodicMode (&mut self)
	{
		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY)&!(1<<3));
	}


	pub fn EnableInterrupt (&mut self)
	{
		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY)|(1<<2));
	}


	pub fn DisableInterrupt (&mut self)
	{
		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY)&!(1<<2));
	}


	pub fn SetAccumulator (&mut self, value: u64)
	{
		self.WriteReg (Self::CONFIGURATION_AND_CAPABILITY, self.ReadReg (Self::CONFIGURATION_AND_CAPABILITY)|(1<<6));

		self.WriteReg (Self::COMPARATOR_VALUE, value);
	}
}


#[derive(Debug)]
struct HPETTimer
{
	baseAddress: PhysicalAddress,
	mainCounterTickPeriod: TimeDuration,
	tickPerSecond: usize,
	vendorID: u16,
	haveLegacyReplacementMapping: bool,
	counterIs64bit: bool,
	timerCount: u8,
	hpetComparators: Vec<HPETComparator>,
	revisionID: u8,

	timerInterruptFromBoot: usize,
}

impl HPETTimer
{
	const GENERAL_CAPABILITIES_AND_ID: usize = 0x00;
	const GENERAL_CONFIGURATION: usize = 0x10;
	const GENERAL_INTERRUPT_STATUS: usize = 0x20;
	const MAIN_COUNTER_VALUE: usize = 0xf0;

	const ENABLE_CNF: u64 = 1<<0;
	const LEG_RT_CNF: u64 = 1<<1;


	pub fn New (hpetTable: &HPETPointer) -> Self
	{
		let baseAddress = hpetTable.HpetAddress ().BaseAddress ();

		let generalCapabilitiesAndID = (baseAddress + Self::GENERAL_CAPABILITIES_AND_ID).VolatileRead::<u64> ();

		let mut hpetComparators = Vec::new ();

		for i in 0..hpetTable.ComparatorCount ()
		{
			hpetComparators.push (HPETComparator::New (baseAddress + 0x100 + 0x20*i));
		}

		Self
		{
			baseAddress,
			mainCounterTickPeriod: TimeDuration::New (TimeUnit::FEMTOSECOND, ((generalCapabilitiesAndID&0xffff_ffff_0000_0000)>>32) as usize),
			tickPerSecond: 1_000_000_000_000_000 / (((generalCapabilitiesAndID&0xffff_ffff_0000_0000)>>32) as usize),
			vendorID: ((generalCapabilitiesAndID&0x0000_0000_ffff_0000)>>16) as u16,
			haveLegacyReplacementMapping: (generalCapabilitiesAndID&(1<<15)) != 0,
			counterIs64bit: (generalCapabilitiesAndID&(1<<13)) != 0,
			timerCount: ((generalCapabilitiesAndID&0x0000_0000_0000_0f00)>>8) as u8 + 1,
			hpetComparators,
			revisionID: (generalCapabilitiesAndID&0x0000_0000_0000_00ff) as u8,

			timerInterruptFromBoot: 0,
		}
	}

	fn ReadReg (&self, offset: usize) -> u64
	{
		(self.baseAddress + offset).VolatileRead::<u64> ()
	}

	fn WriteReg (&mut self, offset: usize, value: u64)
	{
		(self.baseAddress + offset).VolatileWrite::<u64> (value)
	}

	pub fn EnableLegacyReplacementMapping (&mut self)
	{
		assert! (self.haveLegacyReplacementMapping);

		self.WriteReg (Self::GENERAL_CONFIGURATION, self.ReadReg (Self::GENERAL_CONFIGURATION)|Self::LEG_RT_CNF);
	}

	pub fn DisableLegacyReplacementMapping (&mut self)
	{
		assert! (self.haveLegacyReplacementMapping);
		self.WriteReg (Self::GENERAL_CONFIGURATION, self.ReadReg (Self::GENERAL_CONFIGURATION)&!Self::LEG_RT_CNF);
	}

	pub fn EnableMainCounter (&mut self)
	{
		self.WriteReg (Self::GENERAL_CONFIGURATION, self.ReadReg (Self::GENERAL_CONFIGURATION)|Self::ENABLE_CNF);
	}

	pub fn DisableMainCounter (&mut self)
	{
		self.WriteReg (Self::GENERAL_CONFIGURATION, self.ReadReg (Self::GENERAL_CONFIGURATION)&!Self::ENABLE_CNF);
	}

	pub fn GetGeneralInterruptStatus (&self) -> u64
	{
		self.ReadReg (Self::GENERAL_INTERRUPT_STATUS)
	}

	pub fn SetGeneralInterruptStatus (&mut self, value: u64)
	{
		self.WriteReg (Self::GENERAL_INTERRUPT_STATUS, value);
	}

	pub fn GetMainCounterValue (&self) -> u64
	{
		self.ReadReg (Self::MAIN_COUNTER_VALUE)
	}

	pub fn SetMainCounterValue (&mut self, value: u64)
	{
		self.WriteReg (Self::MAIN_COUNTER_VALUE, value);
	}
}


static globalHpetTimer: SyncUnsafeCell<Option<RwMutex<HPETTimer>>> = SyncUnsafeCell::new (None);


/// This function must be called only once by boot CPU at boot time
pub (in crate::Arch::Amd64) fn InitHPET (interruptID: InterruptID)
{
	println! ("[Boot CPU][+] Init HPET...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	let hpetPointer = HPETPointer::New (ACPI::GetSdtTableAddress (SdtTableType::HPET).unwrap ());

	let mut globalHpetTimerRef = unsafe
		{
			*globalHpetTimer.get () = Some(RwMutex::New (HPETTimer::New (&hpetPointer)));
			(*globalHpetTimer.get ()).as_ref ().unwrap ()
		}.WriterLock ();


	// check for legacy replacement mapping
	assert! (globalHpetTimerRef.haveLegacyReplacementMapping, "HPET don't have legacy replacement mapping!\n");

	// check for 64bit counter
	assert! (globalHpetTimerRef.counterIs64bit, "HPET don't have 64 bit counter!\n");

	// check that there at least 1 comparator
	assert! (globalHpetTimerRef.hpetComparators.len () > 0, "HPET don't have any comparator!");

	// stop the main counter
	globalHpetTimerRef.DisableMainCounter ();

	// clear the main counter
	globalHpetTimerRef.SetMainCounterValue (0);

	// force comparator 0 to operate in 64 bit mode
	globalHpetTimerRef.hpetComparators[0].Force64bitMode ();

	// enable periodic on comparator 0
	globalHpetTimerRef.hpetComparators[0].EnablePeriodicMode ();

	// enable interrupt on comparator 0
	globalHpetTimerRef.hpetComparators[0].EnableInterrupt ();

	// write new value to comparator 0
	let tickPerInterrupt = globalHpetTimerRef.tickPerSecond / INTERRUPT_PER_SECOND;
	globalHpetTimerRef.hpetComparators[0].SetAccumulator (tickPerInterrupt as u64);

	// set HPET interrupt number
	IOAPIC::SetIRQInterruptNumber (HPET_TIMER0_LEGACY_IRQ_NUMBER, InterruptTriggerMode::Edge, InterruptPinPolarity::HighActive, interruptID.into ());

	// enable legacy mapping
	// this will map timer 0 to IRQ0 and timer 1 to IRQ1
	globalHpetTimerRef.EnableLegacyReplacementMapping ();

	// enable irq 0 in IOAPIC
	IOAPIC::EnableIRQ (HPET_TIMER0_LEGACY_IRQ_NUMBER);

	// enable the main counter so hpet start counting
	globalHpetTimerRef.EnableMainCounter ();
}


pub (in crate::Arch::Amd64) fn TimeFromBoot () -> TimeDuration
{
	let globalHpetTimerRef = unsafe { (*globalHpetTimer.get ()).as_ref ().unwrap () }.ReaderLock ();

	let mainCounterTickPeriod = globalHpetTimerRef.mainCounterTickPeriod;
	let tickPerInterrupt = globalHpetTimerRef.tickPerSecond / INTERRUPT_PER_SECOND;
	let timerInterruptFromBoot = globalHpetTimerRef.timerInterruptFromBoot;


	mainCounterTickPeriod*tickPerInterrupt*timerInterruptFromBoot
}


pub (in crate::Arch::Amd64::Timer) extern "x86-interrupt" fn HpetInterruptHandler (_interruptStackFrame: InterruptStackFrame)
{
	let mut globalHpetTimerRef = unsafe { (*globalHpetTimer.get ()).as_ref ().unwrap () }.WriterLock ();
	globalHpetTimerRef.timerInterruptFromBoot += 1;

	APIC::SendEOI ();
}
