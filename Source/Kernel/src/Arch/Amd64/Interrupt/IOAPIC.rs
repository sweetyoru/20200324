use core::fmt;

use alloc::vec::Vec;
use core::cell::SyncUnsafeCell;

use crate::Lock::OneTimeFlag;
use crate::DataStruct::DefaultHashMap;
use crate::Memory::Physical::PhysicalAddress;
use crate::Arch::Amd64::CPU::InterruptVector;
use crate::Interrupt::{InterruptTriggerMode, InterruptPinPolarity, InterruptDeliveryStatus};
use crate::Arch::Amd64::ACPI::{self, SdtTableType, MADTPointer, MADTEntry, MADTEntryType, APICID, IOAPICPointer, InterruptSourceOverridePointer};



#[derive(Debug)]
enum IoApicReg
{
	IoApicID			= 0x00,
	IoAcpiVersion		= 0x01,
	IoApicArbitration	= 0x02,
}


#[derive(Debug, Clone, Copy)]
struct RedirectionEntry(u64);

impl RedirectionEntry
{
	const DESTINATION_SHIFT: usize = 56;
	const DESTINATION_MASK: u64 = 0b1111_1111<<Self::DESTINATION_SHIFT;
	const INTERRUPT_MASK: u64 = 1<<16;
	const TRIGGER_MODE_SHIFT: u64 = 15;
	const TRIGGER_MODE_MASK: u64 = 1<<Self::TRIGGER_MODE_SHIFT;
	const REMOTE_IRR_MASK: u64 = 1<<14;
	const PIN_POLARITY_SHIFT: u64 = 13;
	const PIN_POLARITY_MASK: u64 = 1<<13;
	const DELIVERY_STATUS_SHIFT: u64 = 12;
	const DELIVERY_STATUS_MASK: u64 = 1<<Self::DELIVERY_STATUS_SHIFT;
	const DESTINATION_MODE_SHIFT: u64 = 11;
	const DESTINATION_MODE_MASK: u64 = 1<<Self::DESTINATION_MODE_SHIFT;
	const DELIVERY_MODE_SHIFT: u64 = 8;
	const DELIVERY_MODE_MASK: u64 = 0b111<<Self::DELIVERY_MODE_SHIFT;
	const VECTOR_MASK: u64 = 0b1111_1111;

	pub fn New (value: u64) -> Self { Self(value) }
	pub fn ToUsize (&self) -> usize { self.0 as usize }

	pub fn Destination (&self) -> APICID
	{
		APICID::New (((self.0&Self::DESTINATION_MASK)>>Self::DESTINATION_SHIFT) as usize)
	}

	pub fn SetDestination (&mut self, des: APICID)
	{
		// clear old destination
		self.0 &= !Self::DESTINATION_MASK;

		// set new destination
		self.0 |= (des.ToUsize () << Self::DESTINATION_SHIFT) as u64;
	}

	pub fn IsMasked (&self) -> bool
	{
		self.0&Self::INTERRUPT_MASK == Self::INTERRUPT_MASK
	}

	pub fn SetMasked (&mut self)
	{
		self.0 |= Self::INTERRUPT_MASK;
	}

	pub fn SetNotMasked (&mut self)
	{
		self.0 &= !Self::INTERRUPT_MASK;
	}

	pub fn TriggerMode (&self) -> InterruptTriggerMode
	{
		match (self.0 & Self::TRIGGER_MODE_MASK)>>Self::TRIGGER_MODE_SHIFT
		{
			0 => InterruptTriggerMode::Edge,
			1 => InterruptTriggerMode::Level,
			value => panic! ("Unknown value ({})", value),
		}
	}

	pub fn SetTriggerMode (&mut self, triggerMode: InterruptTriggerMode)
	{
		// clear old trigger mode
		self.0 &= !Self::TRIGGER_MODE_MASK;

		// set new trigger mode
		self.0 |= (triggerMode as u64)<<Self::TRIGGER_MODE_SHIFT;
	}

	pub fn RemoteIRR (&self) -> usize
	{
		if self.0&Self::REMOTE_IRR_MASK == Self::REMOTE_IRR_MASK { 1 } else { 0 }
	}

	pub fn PinPolarity (&self) -> InterruptPinPolarity
	{
		match (self.0 & Self::PIN_POLARITY_MASK)>>Self::PIN_POLARITY_SHIFT
		{
			0 => InterruptPinPolarity::HighActive,
			1 => InterruptPinPolarity::LowActive,
			_ => panic! ("Unknown value!"),
		}
	}

	pub fn SetPinPolarity (&mut self, pinPolarity: InterruptPinPolarity)
	{
		// clear old pin polarity
		self.0 &= !Self::PIN_POLARITY_MASK;

		// set new pin polarity
		self.0 |= (pinPolarity as u64)<<Self::PIN_POLARITY_SHIFT;
	}

	pub fn DeliveryStatus (&self) -> InterruptDeliveryStatus
	{
		InterruptDeliveryStatus::New (((self.0 & Self::DELIVERY_STATUS_MASK)>>Self::DELIVERY_STATUS_SHIFT) as usize)
	}

	pub fn DestinationMode (&self) -> InterruptDestinationMode
	{
		InterruptDestinationMode::New (((self.0 & Self::DESTINATION_MODE_MASK)>>Self::DESTINATION_MODE_SHIFT) as usize)
	}

	pub fn SetDestinationMode (&mut self, destinationMode: InterruptDestinationMode)
	{
		// clear old destination mode
		self.0 &= !Self::DESTINATION_MODE_MASK;

		// set new destination mode
		self.0 |= (destinationMode as u64)<<Self::DESTINATION_MODE_SHIFT;
	}

	pub fn DeliveryMode (&self) -> InterruptDeliveryMode
	{
		InterruptDeliveryMode::New (((self.0 & Self::DELIVERY_MODE_MASK)>>Self::DELIVERY_MODE_SHIFT) as usize)
	}

	pub fn SetDeliveryMode (&mut self, deliveryMode: InterruptDeliveryMode)
	{
		// clear old delivery mode
		self.0 &= !Self::DELIVERY_MODE_MASK;

		// set new delivery mode
		self.0 |= (deliveryMode as u64)<<Self::DELIVERY_MODE_SHIFT;
	}

	pub fn Vector (&self) -> InterruptVector
	{
		InterruptVector::New ((self.0 & Self::VECTOR_MASK) as usize)
	}

	pub fn SetVector (&mut self, vector: InterruptVector)
	{
		// clear old vector
		self.0 &= !Self::VECTOR_MASK;

		// set new vector
		self.0 |= vector.ToUsize () as u64;
	}
}


impl fmt::Display for RedirectionEntry
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		writeln! (f, "Destination: {}", self.Destination ()).unwrap ();
		writeln! (f, "Is Masked: {}", self.IsMasked ()).unwrap ();
		writeln! (f, "Trigger Mode: {:?}", self.TriggerMode ()).unwrap ();
		writeln! (f, "Remote IRR: {}", self.RemoteIRR ()).unwrap ();
		writeln! (f, "Pin Polarity: {:?}", self.PinPolarity ()).unwrap ();
		writeln! (f, "Delivery Status: {:?}", self.DeliveryStatus ()).unwrap ();
		writeln! (f, "Destination Mode: {:?}", self.DestinationMode ()).unwrap ();
		writeln! (f, "Delivery Mode: {:?}", self.DeliveryMode ()).unwrap ();
		writeln! (f, "{}", self.Vector ()).unwrap ();

		Ok(())
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub (in crate::Arch::Amd64) enum InterruptDestinationMode
{
	Physical	= 0,
	Logical		= 1,
}

impl InterruptDestinationMode
{
	pub fn New (value: usize) -> Self
	{
		match value
		{
			0 => Self::Physical,
			1 => Self::Logical,
			_ => panic! ("Unknown value!"),
		}
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub (in crate::Arch::Amd64) enum InterruptLevel
{
	DeAssert	= 0,
	Assert		= 1,
}

impl InterruptLevel
{
	pub fn New (value: usize) -> Self
	{
		match value
		{
			0 => Self::DeAssert,
			1 => Self::Assert,
			_ => panic! ("Unknown value!"),
		}
	}
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub (in crate::Arch::Amd64) enum InterruptDeliveryMode
{
	Fixed			= 0b000,
	LowestPriority	= 0b001,
	SMI				= 0b010,
	NMI				= 0b100,
	INIT			= 0b101,
	ExtINIT			= 0b111,
	SIPI			= 0b110,
	Reserved		= 0b011,
}

impl InterruptDeliveryMode
{
	pub fn New (value: usize) -> Self
	{
		match value
		{
			0b000 => Self::Fixed,
			0b001 => Self::LowestPriority,
			0b010 => Self::SMI,
			0b011 => Self::Reserved,
			0b100 => Self::NMI,
			0b101 => Self::INIT,
			0b110 => Self::SIPI,
			0b111 => Self::ExtINIT,
			_ => panic! ("Unknown value!"),
		}
	}
}


#[derive(Debug, Clone, Copy)]
struct IOAPIC
{
	apicID: APICID,
	baseAddress: PhysicalAddress,
	globalSystemInterruptBase: usize,
}

impl IOAPIC
{
	const IO_SELECT_REG_OFFSET: usize = 0x00;
	const IO_WINDOW_REG_OFFSET: usize = 0x10;

	const REDIRECTION_TABLE_OFFSET: usize = 0x10;
	const MAX_REDIRECTION_ENTRY_COUNT: usize = 24;
	const MAX_REDIRECTION_ENTRY_SHIFT: usize = 16;
	const MAX_REDIRECTION_ENTRY_MASK: u32 = 0b1111_1111<<Self::MAX_REDIRECTION_ENTRY_SHIFT;

	pub fn New (apicID: APICID, baseAddress: PhysicalAddress, globalSystemInterruptBase: usize) -> Self
	{
		Self
		{
			apicID,
			baseAddress,
			globalSystemInterruptBase,
		}
	}

	pub fn ReadReg (&self, reg: IoApicReg) -> u32
	{
		// select
		(self.baseAddress + Self::IO_SELECT_REG_OFFSET).VolatileWrite::<u32> (reg as u32);

		// read
		(self.baseAddress + Self::IO_WINDOW_REG_OFFSET).VolatileRead::<u32> ()
	}

	pub fn WriteReg (&mut self, reg: IoApicReg, value: u32)
	{
		// select
		(self.baseAddress + Self::IO_SELECT_REG_OFFSET).VolatileWrite::<u32> (reg as u32);

		// write
		(self.baseAddress + Self::IO_WINDOW_REG_OFFSET).VolatileWrite::<u32> (value)
	}

	pub fn RedirectionEntryCount (&self) -> usize
	{
		let result = ((self.ReadReg (IoApicReg::IoAcpiVersion)&Self::MAX_REDIRECTION_ENTRY_MASK)>>Self::MAX_REDIRECTION_ENTRY_SHIFT) + 1;
		assert! (result as usize <= Self::MAX_REDIRECTION_ENTRY_COUNT);
		result as usize
	}

	pub fn MinGlobalSystemInterruptNumber (&self) -> usize
	{
		self.globalSystemInterruptBase
	}

	pub fn MaxGlobalSystemInterruptNumber (&self) -> usize
	{
		self.globalSystemInterruptBase + self.RedirectionEntryCount () - 1
	}

	pub fn ReadRedirectionEntry (&self, index: usize) -> RedirectionEntry
	{
		assert! (index < self.RedirectionEntryCount ());

		// select lower part
		(self.baseAddress + Self::IO_SELECT_REG_OFFSET).VolatileWrite::<u32> ((Self::REDIRECTION_TABLE_OFFSET + index*2) as u32);

		// read lower part
		let lowerPart = (self.baseAddress + Self::IO_WINDOW_REG_OFFSET).VolatileRead::<u32> () as u64;

		// select higher part
		(self.baseAddress + Self::IO_SELECT_REG_OFFSET).VolatileWrite::<u32> ((Self::REDIRECTION_TABLE_OFFSET + index*2 + 1) as u32);

		// read higher part
		let higherPart = (self.baseAddress + Self::IO_WINDOW_REG_OFFSET).VolatileRead::<u32> () as u64;

		RedirectionEntry::New ((higherPart<<32) | lowerPart)
	}

	pub fn WriteRedirectionEntry (&mut self, index: usize, value: RedirectionEntry)
	{
		assert! (index < self.RedirectionEntryCount ());

		let lowerPart = (value.ToUsize () & 0xffff_ffff) as u32;
		let higherPart = ((value.ToUsize () & 0xffff_ffff_0000_0000)>>32) as u32;

		// select higher part
		(self.baseAddress + Self::IO_SELECT_REG_OFFSET).VolatileWrite::<u32> ((Self::REDIRECTION_TABLE_OFFSET + index*2 + 1) as u32);

		// write higher part
		(self.baseAddress + Self::IO_WINDOW_REG_OFFSET).VolatileWrite::<u32> (higherPart);

		// select lower part
		(self.baseAddress + Self::IO_SELECT_REG_OFFSET).VolatileWrite::<u32> ((Self::REDIRECTION_TABLE_OFFSET + index*2) as u32);

		// write lower part
		(self.baseAddress + Self::IO_WINDOW_REG_OFFSET).VolatileWrite::<u32> (lowerPart);
	}
}

impl fmt::Display for IOAPIC
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		writeln! (f, "IOAPIC ID: {}", self.apicID).unwrap ();
		writeln! (f, "Base Address: {}", self.baseAddress).unwrap ();
		writeln! (f, "Global System Interrupt Base: 0x{:08x}", self.globalSystemInterruptBase).unwrap ();

		writeln! (f, "Redirection Entry Count: {}\n", self.RedirectionEntryCount ()).unwrap ();

		for index in 0..self.RedirectionEntryCount ()
		{
			writeln! (f, "Redirection Entry {}:\n{}", index, self.ReadRedirectionEntry (index)).unwrap ();
		}

		Ok(())
	}
}


/// Created once by boot CPU at boot time
/// Never change after that
static ioAPICs: SyncUnsafeCell<Vec::<IOAPIC>> = SyncUnsafeCell::new (Vec::new ());

/// Map from IRQ number to global system interrupt number
/// Created once by boot CPU at boot time
/// Never change after that
static irqToGlobalSystemInterruptMap: SyncUnsafeCell<DefaultHashMap<usize, usize>> = SyncUnsafeCell::new (DefaultHashMap::New ());


/// This function must be called only once at boot by boot CPU
pub (in crate::Arch::Amd64) fn InitIOAPIC ()
{
	println! ("[Boot CPU][+] Init IOAPIC...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	println! ("[Boot CPU][+] Getting IOAPIC list...");
	let myIoAPICs = unsafe { &mut *ioAPICs.get () };
	let myIRQToGlobalsystemInterruptMap = unsafe
		{
			&mut *irqToGlobalSystemInterruptMap.get ()
		};


	// search for IOAPIC entry
	let mut globalSystemInterruptCount = 0;


	// point to lowest empty global system interrupt slot that larger than any others system interrupt
	let mut globalSystemInterruptNumberBound = 0;

	let madtPointer = MADTPointer::New (ACPI::GetSdtTableAddress (SdtTableType::MADT).unwrap ());

	for entryPointer in madtPointer.Entries ()
	{
		if entryPointer.EntryType () == MADTEntryType::IOAPIC
		{
			let ioApicPointer = IOAPICPointer::New (entryPointer.EntryAddress ());
			let ioApic = IOAPIC::New (ioApicPointer.IOApicID (), ioApicPointer.IOApicAddress (), ioApicPointer.GlobalSystemInterruptBase ());

			// update irq count
			globalSystemInterruptCount += ioApic.RedirectionEntryCount ();

			// update max irq number
			if globalSystemInterruptNumberBound < ioApic.MaxGlobalSystemInterruptNumber () + 1
			{
				globalSystemInterruptNumberBound = ioApic.MaxGlobalSystemInterruptNumber () + 1;
			}

			// update irqMap
			for irq in ioApic.MinGlobalSystemInterruptNumber ()..=ioApic.MaxGlobalSystemInterruptNumber ()
			{
				// only insert if no entry in list
				if myIRQToGlobalsystemInterruptMap.GetRef (&irq) == None
				{
					myIRQToGlobalsystemInterruptMap.Insert (irq, irq);
				}
			}

			// push them to io apic list
			myIoAPICs.push (ioApic);
		}
		else if entryPointer.EntryType () == MADTEntryType::InterruptSourceOverride
		{
			let interruptSourceOverridePointer = InterruptSourceOverridePointer::New (entryPointer.EntryAddress ());
			myIRQToGlobalsystemInterruptMap.Insert (interruptSourceOverridePointer.Source (), interruptSourceOverridePointer.GlobalSystemInterrupt ());
		}
	}

	if globalSystemInterruptCount != globalSystemInterruptNumberBound
	{
		panic! ("There is hole in IO APIC IRQs!");
	}

	println! ("[Boot CPU][+] IOAPIC Count: {}", myIoAPICs.len ());
	for ioApic in myIoAPICs
	{
		println! ("{}\n", ioApic);
	}

	// print irq map
	for (irq, globalSystemInterrupt) in myIRQToGlobalsystemInterruptMap
	{
		println! ("\tIRQ {} map to Global System Interrupt {}", irq, globalSystemInterrupt);
	}

	println! ("");
}

/// Add IRQ handler to interrupt vector
/// Destination is hard-coded to APICID::new (0xffff_ffff)
/// Destinetion mode is hard-coded to Logical
/// Delivery mode is hard-coded to Lowest Priority to balance IRQ
/// IRQ is masked by default
pub (in crate::Arch::Amd64) fn SetIRQInterruptNumber (irqNumber: usize, triggerMode: InterruptTriggerMode, pinPolarity: InterruptPinPolarity,
						 vector: InterruptVector)
{
	// made redirection entry
	let mut redirectionEntry = RedirectionEntry::New (0);

	redirectionEntry.SetDestination (APICID::New (0xffff_ffff));
	redirectionEntry.SetTriggerMode (triggerMode);
	redirectionEntry.SetPinPolarity (pinPolarity);
	redirectionEntry.SetDestinationMode (InterruptDestinationMode::Logical);
	redirectionEntry.SetDeliveryMode (InterruptDeliveryMode::LowestPriority);
	redirectionEntry.SetVector (vector);
	redirectionEntry.SetMasked ();

	// get global system interupt number
	let globalSystemInterruptNumber = unsafe
		{
			*(*irqToGlobalSystemInterruptMap.get ()).GetRef (&irqNumber).unwrap ()
		};

	// find correct io apic and redirection entry index to insert
	unsafe
	{
		for ioAPIC in (*ioAPICs.get ()).iter_mut ()
		{
			if globalSystemInterruptNumber >= ioAPIC.MinGlobalSystemInterruptNumber ()
				&& globalSystemInterruptNumber <= ioAPIC.MaxGlobalSystemInterruptNumber ()
			{
				// found
				let redirectionEntryIndex = globalSystemInterruptNumber - ioAPIC.MinGlobalSystemInterruptNumber ();

				ioAPIC.WriteRedirectionEntry (redirectionEntryIndex, redirectionEntry);
			}
		}
	}
}


pub (in crate::Arch::Amd64) fn EnableIRQ (irqNumber: usize)
{
	// get global system interupt number
	let globalSystemInterruptNumber = unsafe
		{
			*(*irqToGlobalSystemInterruptMap.get ()).GetRef (&irqNumber).unwrap ()
		};

	// find correct io apic and redirection entry index to change
	unsafe
	{
		for ioAPIC in (*ioAPICs.get ()).iter_mut ()
		{
			if globalSystemInterruptNumber >= ioAPIC.MinGlobalSystemInterruptNumber ()
				&& globalSystemInterruptNumber <= ioAPIC.MaxGlobalSystemInterruptNumber ()
			{
				// found
				let redirectionEntryIndex = globalSystemInterruptNumber - ioAPIC.MinGlobalSystemInterruptNumber ();

				let mut redirectionEntry = ioAPIC.ReadRedirectionEntry (redirectionEntryIndex);
				redirectionEntry.SetNotMasked ();
				ioAPIC.WriteRedirectionEntry (redirectionEntryIndex, redirectionEntry);
			}
		}
	}
}


pub (in crate::Arch::Amd64) fn DisableIRQ (irqNumber: usize)
{
	// get global system interupt number
	let globalSystemInterruptNumber = unsafe
		{
			*(*irqToGlobalSystemInterruptMap.get ()).GetRef (&irqNumber).unwrap ()
		};

	// find correct io apic and redirection entry index to insert
	unsafe
	{
		for ioAPIC in (*ioAPICs.get ()).iter_mut ()
		{
			if globalSystemInterruptNumber >= ioAPIC.MinGlobalSystemInterruptNumber ()
				&& globalSystemInterruptNumber <= ioAPIC.MaxGlobalSystemInterruptNumber ()
			{
				// found
				let redirectionEntryIndex = globalSystemInterruptNumber - ioAPIC.MinGlobalSystemInterruptNumber ();

				let mut redirectionEntry = ioAPIC.ReadRedirectionEntry (redirectionEntryIndex);
				redirectionEntry.SetMasked ();
				ioAPIC.WriteRedirectionEntry (redirectionEntryIndex, redirectionEntry);
			}
		}
	}
}
