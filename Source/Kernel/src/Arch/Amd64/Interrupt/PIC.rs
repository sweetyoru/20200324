use core::cell::SyncUnsafeCell;

use crate::Arch::Amd64::CPU;
use crate::Lock::OneTimeFlag;
use crate::Arch::Amd64::PortIO;
use crate::Memory::Virtual::KernelAddress;
use crate::Arch::Amd64::Interrupt::InterruptStackFrame;
use crate::Interrupt::{self, InterruptID, InterruptAllocationStrategy};


const PIC_NEW_BASE: usize = 0x20;
const IRQ_COUNT: usize = 16;

const MASTER_BASE: u16 = 0x20;
const MASTER_COMMAND: u16 = MASTER_BASE + 0;
const MASTER_DATA: u16 = MASTER_BASE + 1;

const SLAVE_BASE: u16 = 0xA0;
const SLAVE_COMMAND: u16 = SLAVE_BASE + 0;
const SLAVE_DATA: u16 = SLAVE_BASE + 1;

pub (in crate::Arch::Amd64) struct PicIrqs();

impl PicIrqs
{
	pub const PIT:			u16 = 1<<0;
	pub const KEYBOARD:		u16 = 1<<1;
	pub const CASCADED:		u16 = 1<<2;
	pub const SERIAL2:		u16 = 1<<3;
	pub const SERIAL1:		u16 = 1<<4;
	pub const SOUND:		u16 = 1<<5;
	pub const FLOPPYDISK:	u16 = 1<<6;
	pub const SOUND2:		u16 = 1<<7;
	pub const RTC:			u16 = 1<<8;
	pub const ACPI:			u16 = 1<<9;
	pub const OPEN1:		u16 = 1<<10;
	pub const OPEN2:		u16 = 1<<11;
	pub const MOUSE:		u16 = 1<<12;
	pub const COCPU:		u16 = 1<<13;
	pub const ATA1:			u16 = 1<<14;
	pub const ATA2:			u16 = 1<<15;

	pub const ALL:			u16 = 0xffff;
}


/// This function must be called only once at boot by boot CPU
pub (in crate::Arch::Amd64) fn InitPIC ()
{
	println! ("[Boot CPU][+] Init PIC...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	println! ("[Boot CPU][+] Re-map PIC to 0x{:x}...", PIC_NEW_BASE);
	ReMap (PIC_NEW_BASE);

	println! ("[Boot CPU][+] Disable all IRQs...");
	DisableIRQs (PicIrqs::ALL);

	println! ("[Boot CPU][+] Insert dummy PIC IRQs handler...");
	InsertIRQHandler (0x00, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x01, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x02, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x03, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x04, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x05, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x06, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x07, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x08, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x09, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x0A, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x0B, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x0C, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x0D, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x0E, KernelAddress::New (DummyPICHandler as *const () as usize));
	InsertIRQHandler (0x0F, KernelAddress::New (DummyPICHandler as *const () as usize));
}

fn ReMap (newBase: usize)
{
	assert! (newBase <= CPU::CPU::MAX_INTERRUPT_NUMBER - (IRQ_COUNT - 1));

	PortIO::OutPortB (0x20, 0x10|0x01);				// begin init master PIC chip
	PortIO::OutPortB (0xA0, 0x10|0x01);				// begin init slave PIC chip
	PortIO::OutPortB (0x21, newBase as u8);			// map master PIC IRQs to newBase
	PortIO::OutPortB (0xA1, newBase as u8 + 8);		// map slave PIC IRQs to newBase + 8
	PortIO::OutPortB (0x21, 4);						// tell master PIC that there is a slave PIC at IRQ2
	PortIO::OutPortB (0xA1, 2);						// tell slave PIC its cascade identity
	PortIO::OutPortB (0x21, 1);						//
	PortIO::OutPortB (0xA1, 1);						//
}

extern "x86-interrupt" fn DummyPICHandler (interruptStackFrame: InterruptStackFrame)
{
	println! ("Dummy PIC Handler called!");
	println! ("Stack Frame:\n{}", interruptStackFrame);
}

pub fn InsertIRQHandler (irqNumber: usize, handlerAddress: KernelAddress)
{
	assert! (irqNumber < IRQ_COUNT);
	let interruptAllocationStrategy = InterruptAllocationStrategy::Fixed(InterruptID::New (PIC_NEW_BASE + irqNumber));
	Interrupt::AddInterruptHandler (handlerAddress, interruptAllocationStrategy, false).unwrap ();
}

pub fn EnableIRQs (irqs: u16)
{
	let oldMasterMask = PortIO::InPortB (MASTER_DATA);
	let oldSlaveMask = PortIO::InPortB (SLAVE_DATA);

	let newMasterMask = oldMasterMask & (!irqs&0xff) as u8;
	let newSlaveMask = oldSlaveMask & ((!irqs&0xff00)>>8) as u8;

	PortIO::OutPortB (MASTER_DATA, newMasterMask);
	PortIO::OutPortB (SLAVE_DATA, newSlaveMask);
}

pub fn DisableIRQs (irqs: u16)
{
	let oldMasterMask = PortIO::InPortB (MASTER_DATA);
	let oldSlaveMask = PortIO::InPortB (SLAVE_DATA);

	let newMasterMask = oldMasterMask | (irqs&0xff) as u8;
	let newSlaveMask = oldSlaveMask | ((irqs&0xff00)>>8) as u8;

	PortIO::OutPortB (MASTER_DATA, newMasterMask);
	PortIO::OutPortB (SLAVE_DATA, newSlaveMask);
}
