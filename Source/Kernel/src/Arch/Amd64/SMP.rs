use alloc::vec::Vec;
use alloc::sync::{Arc, Weak};
use core::cell::SyncUnsafeCell;

use crate::ISC::SystemID;
use crate::Lock::OneTimeFlag;
use crate::DataStruct::DefaultHashMap;
use crate::Memory::Physical::FrameAllocator;
use crate::Timer::{self, TimeDuration, TimeUnit};
use crate::Process::{self, ProcessID, ProcessPrivateID};
use crate::Arch::Amd64::CPU::{self, CPUID, APIC, InterruptVector};
use crate::Thread::{self, ThreadID, ThreadPrivateID, ThreadState};
use crate::Memory::Virtual::{VirtualAddress, KernelAddress, PageFlags, RegionStatus};
use crate::Arch::Amd64::ACPI::{self, SdtTableType, MADTPointer, MADTEntry, MADTEntryType, APICID, ProcessorLocalAPICPointer, ProcessorLocalX2APICPointer};


/// used to lookup cpu id from apic id
/// created once at boot time
/// never modified afterware
static apicIDToCpuID: SyncUnsafeCell<DefaultHashMap<APICID, CPUID>> = SyncUnsafeCell::new (DefaultHashMap::New ());


/// used to lookup cpu from cpuid
/// created once at boot time
/// never modified afterware
static cpuIDToCPU: SyncUnsafeCell<DefaultHashMap<CPUID, Weak<CPU::CPU>>> = SyncUnsafeCell::new (DefaultHashMap::New ());


/// CPU list
/// created once at boot time
/// never modified afterware
static cpus: SyncUnsafeCell<Vec<Arc<CPU::CPU>>> = SyncUnsafeCell::new (Vec::new ());


/// initialized cpu
static initializedCPUCount: SyncUnsafeCell<usize> = SyncUnsafeCell::new (1);


/// This will parse acpi table and create cpu struct for every CPU detected
/// This function must be called only once at boot
pub fn InitSMPPart1 ()
{
	println! ("[Boot CPU][+] Init SMP part 1...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }


	// build cpu list
	let madtPointer = MADTPointer::New (ACPI::GetSdtTableAddress (SdtTableType::MADT).unwrap ());


	// print each entry and insert cpu apic id to global lookup table
	let mut cpuCount = 0;

	for entryPointer in madtPointer.Entries ()
	{
		if entryPointer.EntryType () == MADTEntryType::ProcessorLocalAPIC
		{
			// can use random id but just use simple number for pretty printing
			let id = CPUID::New (cpuCount);
			let processorLocalAPICPointer = ProcessorLocalAPICPointer::New (entryPointer.EntryAddress ());

			unsafe
			{
				let cpu = Arc::new (CPU::CPU::New (id, processorLocalAPICPointer.ApicID (), processorLocalAPICPointer.CanEnable ()));

				if processorLocalAPICPointer.ApicID () == CPU::CPU::GetCurrentApicID ()
				{
					cpu.SetEnabled ();
					cpu.LoadGDT ();
					println! ("[Boot CPU][+] Boot cpu id is {}", id);
				}

				(*apicIDToCpuID.get ()).Insert (processorLocalAPICPointer.ApicID (), id);
				(*cpuIDToCPU.get ()).Insert (id, Arc::downgrade (&cpu));
				(*cpus.get ()).push (cpu);
			}

			cpuCount += 1;
		}
		else if entryPointer.EntryType () == MADTEntryType::ProcessorLocalX2APIC
		{
			let id = CPUID::New (cpuCount);
			let processorLocalX2APICPointer = ProcessorLocalX2APICPointer::New (entryPointer.EntryAddress ());

			unsafe
			{
				let cpu = Arc::new (CPU::CPU::New (id, processorLocalX2APICPointer.X2ApicID (), processorLocalX2APICPointer.CanEnable ()));

				if processorLocalX2APICPointer.X2ApicID () == CPU::CPU::GetCurrentApicID ()
				{
					cpu.SetEnabled ();
					cpu.LoadGDT ();
					println! ("[Boot CPU][+] Boot cpu id is {}", id);
				}

				(*cpuIDToCPU.get ()).Insert (id, Arc::downgrade (&cpu));
				(*apicIDToCpuID.get ()).Insert (processorLocalX2APICPointer.X2ApicID (), id);
				(*cpus.get ()).push (cpu);
			}

			cpuCount += 1;
		}
	}

	println! ("[Boot CPU][+] CPU Count: {}", GetCPUCount ());
}


/// This will start all available CPUs
/// This function must be called only once at boot
pub fn InitSMPPart2 ()
{
	println! ("[Boot CPU][+] Init SMP part 2...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	unsafe
	{
		// start up all available CPUs
		for cpu in (*cpus.get ()).iter_mut ()
		{
			if cpu.CanEnable () && !cpu.IsEnabled ()
			{
				println! ("[CPU {}][+] Starting CPU {}...", CPU::CPU::GetCurrentCPUID (), cpu.ID ());

				let bootProcess = GetCurrentCPU ().GetCurrentProcess ();


				// create 1 more boot thread
				let newThreadID = ThreadID::New (SystemID::New (0), ThreadPrivateID::New (cpu.ID ().ToU128 ()));

				let newThread = Arc::new (Thread::Thread::New (Arc::downgrade (&bootProcess), KernelAddress::New (0), Some(newThreadID)));


				// set new thread state to running
				newThread.SetState (ThreadState::Running);
				bootProcess.AddThread (Arc::clone (&newThread));


				// get the frame
				let frameAddress = FrameAllocator::GetEmptyLowFrame ().expect ("Out of memory!!! {}");


				// identity map the frame
				let pageAddress = VirtualAddress::New (frameAddress.ToUsize ());
				let mut pageFlags = PageFlags::New ();
				pageFlags.SetPresent ();
				pageFlags.SetWritable ();
				pageFlags.SetExecutable ();
				pageFlags.SetGlobal ();

				bootProcess.GetVirtualAddressSpace ().Map (frameAddress, pageAddress, RegionStatus::PrivateMapped(pageFlags), false);


				// get the code
				let bootTopLevelPageTableAddress = bootProcess.GetVirtualAddressSpace ().TopLevelPageTableAddress ();
				let kernelEntryAddress = KernelAddress::New (APEntry as *const () as usize);
				let kernelStackEndAddress = newThread.GetKernelStackEndAddress ();

				let code = CPU::CPU::GetAPBootstrapCode (frameAddress, bootTopLevelPageTableAddress, kernelEntryAddress, kernelStackEndAddress);


				// copy the code to the frame
				VirtualAddress::MemCopy (VirtualAddress::New (code.as_ptr () as usize), pageAddress, code.len ());


				// send init
				APIC::SendINIT (cpu.ApicID ());


				// sleep 10ms
				Timer::Sleep (TimeDuration::New (TimeUnit::MILISECOND, 10));


				// send sipi
				let vector = InterruptVector::New (frameAddress.ToUsize () >> 12);
				APIC::SendSIPI (cpu.ApicID (), vector);
			}
		}
	}


	// wait for all CPUs to come up
	let waitTime = TimeDuration::New (TimeUnit::SECOND, 1);
	println! ("[Boot CPU][+] Waiting {} for all CPUs to start up...", waitTime);
	Timer::Sleep (waitTime);


	// then check to see how many successfully started
	unsafe
	{
		for cpu in (*cpus.get ()).iter_mut ()
		{
			if cpu.IsEnabled () && cpu.ID () != CPU::CPU::GetCurrentCPUID ()
			{
				*initializedCPUCount.get () += 1;
			}
		}

		println! ("[Boot CPU][+] Successfully started {}/{} CPU(s)", *initializedCPUCount.get (), GetCPUCount ());
	}
}


fn APEntry ()
{
	// this cpu already run with boot virtual address space as part of ap core trampoline code
	println! ("[CPU {}][+] CPU {} started", CPU::CPU::GetCurrentCPUID (), CPU::CPU::GetCurrentCPUID ());


	// set enabled flag
	GetCurrentCPU ().SetEnabled ();
	GetCurrentCPU ().LoadGDT ();


	// get boot process and boot thread
	let bootProcessID = ProcessID::New (SystemID::New (0), ProcessPrivateID::New (0));
	let bootThreadID = ThreadID::New (SystemID::New (0), ThreadPrivateID::New (CPU::CPU::GetCurrentCPUID ().ToU128 ()));


	let bootProcess = Process::GetProcess (bootProcessID).unwrap ();
	let bootThread = bootProcess.GetThread (&bootThreadID).unwrap ();


	// set current thread to boot thread
	GetCurrentCPU ().SetCurrentThread (Arc::clone (&bootThread));


	// test
	let currentProcessID = GetCurrentCPU ().GetCurrentProcess ().ProcessID ();
	let currentThreadID = GetCurrentCPU ().GetCurrentThread ().ThreadID ();

	println!("[CPU {}][+] Current ProcessID: {}", CPU::CPU::GetCurrentCPUID (), currentProcessID);
	println!("[CPU {}][+] Current ThreadID: {}", CPU::CPU::GetCurrentCPUID (), currentThreadID);


	// start to initialize
	crate::Arch::Amd64::Init::EachCPU ();

	loop {}
}


/// Get cpu detected through acpi table
pub fn GetCPUCount () -> usize
{
	unsafe { (*cpuIDToCPU.get ()).Size () }
}


/// Get initialized cpu
pub fn GetInitializedCPUCount () -> usize
{
	unsafe { *initializedCPUCount.get () }
}


pub fn GetCPUList () -> Vec<Arc<CPU::CPU>>
{
	unsafe { (*cpus.get ()).clone () }
}


pub fn GetCurrentCPU () -> Arc<CPU::CPU>
{
	unsafe { Weak::upgrade ((*cpuIDToCPU.get ()).GetRef (&CPU::CPU::GetCurrentCPUID ()).unwrap ()).unwrap () }
}


pub fn GetCPU (cpuid: CPUID) -> Arc<CPU::CPU>
{
	unsafe { Weak::upgrade ((*cpuIDToCPU.get ()).GetRef (&cpuid).unwrap ()).unwrap () }
}
