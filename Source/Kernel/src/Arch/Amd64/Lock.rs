use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut};
use core::sync::atomic::{Ordering, AtomicU64};

use crate::CPU::{CPU, CPUInterruptState};


/// this struct is used to mark something that can be call/used only once
/// once set, the flag can't be clear and will panic if attemp to set the second time
#[derive(Debug, Clone, Copy)]
pub struct OneTimeFlag(bool);

impl OneTimeFlag
{
	pub const fn New () -> Self { Self(false) }

	pub fn Set (&mut self)
	{
		assert! (!self.0, "Attemp to set already set one time flag!");
		self.0 = true;
	}

	pub fn IsSet (&self) -> bool { self.0 }
}


#[derive(Debug, Clone, Copy)]
struct Ticket
{
	ticketValue: u64,
	isUsed: OneTimeFlag,
	oldCPUInterruptState: CPUInterruptState,
}

impl Ticket
{
	pub fn New (ticketValue: u64, oldCPUInterruptState: CPUInterruptState) -> Self
	{
		Self
		{
			ticketValue,
			isUsed: OneTimeFlag::New (),
			oldCPUInterruptState,
		}
	}

	pub fn CanUse (&self, ticketValue: u64) -> bool
	{
		self.ticketValue == ticketValue
	}


	/// Consume the ticket, also restore old interrupt state stored in this ticket
	pub fn Use (&mut self)
	{
		if self.isUsed.IsSet ()
		{
			panic! ("Trying to use an used ticket ({:?})!!!", self);
		}

		self.isUsed.Set ();
		CPU::RestoreInterruptState (self.oldCPUInterruptState);
	}
}


/// Only have theory problem when there are more than 2^64 threads hold the ticket, but that is impossiable in practice
/// Will disable interrupt until unlock
#[derive(Debug)]
struct TicketLock
{
	currentTicket: AtomicU64,
	nextTicket: AtomicU64,
}

impl TicketLock
{
	pub const fn New () -> Self
	{
		Self
		{
			currentTicket: AtomicU64::new (0),
			nextTicket: AtomicU64::new (0),
		}
	}

	pub fn Lock (&self) -> Ticket
	{
		// disable CPU interrupt
		let thisCpuOldCPUInterruptState = CPU::DisableInterrupt ();

		// get ticket
		let ticket = Ticket::New (self.nextTicket.fetch_add (1, Ordering::SeqCst), thisCpuOldCPUInterruptState);

		loop
		{
			if ticket.CanUse (self.currentTicket.load (Ordering::SeqCst))
			{
				// success
				break;
			}
			else
			{
				// failed, save some CPU cycles
				CPU::Pause ();
			}
		}

		return ticket;
	}

	pub fn Unlock (&self, mut ticket: Ticket)
	{
		if !ticket.CanUse (self.currentTicket.load (Ordering::SeqCst))
		{
			panic! ("Trying to unlock TicketLock while not holding the lock!");
		}

		self.currentTicket.fetch_add (1, Ordering::SeqCst);

		ticket.Use ();
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum TicketLockError
{
	LOCKED,
}


#[derive(Debug, Clone, Copy)]
struct ReaderTicket
{
	ticketValue: u64,
	groupTicketValue: u64,
	isUsed: OneTimeFlag,
	oldCPUInterruptState: CPUInterruptState,
}

impl ReaderTicket
{
	pub fn New (ticketValue: u64, groupTicketValue: u64, oldCPUInterruptState: CPUInterruptState) -> Self
	{
		Self
		{
			ticketValue,
			groupTicketValue,
			isUsed: OneTimeFlag::New (),
			oldCPUInterruptState,
		}
	}

	pub fn CanUse (&self, groupTicketValue: u64) -> bool
	{
		self.groupTicketValue == groupTicketValue
	}


	/// Consume the ticket, also restore old interrupt state stored in this ticket
	pub fn Use (&mut self)
	{
		if self.isUsed.IsSet ()
		{
			panic! ("Trying to use an used reader ticket ({:?})!!!", self);
		}

		self.isUsed.Set ();
		CPU::RestoreInterruptState (self.oldCPUInterruptState);
	}
}


#[derive(Debug, Clone, Copy)]
struct WriterTicket
{
	ticketValue: u64,
	groupTicketValue: u64,
	isUsed: OneTimeFlag,
	oldCPUInterruptState: CPUInterruptState,
}

impl WriterTicket
{
	pub fn New (ticketValue: u64, groupTicketValue: u64, oldCPUInterruptState: CPUInterruptState) -> Self
	{
		Self
		{
			ticketValue,
			groupTicketValue,
			isUsed: OneTimeFlag::New (),
			oldCPUInterruptState,
		}
	}

	pub fn CanUse (&self, ticketValue: u64) -> bool
	{
		self.ticketValue == ticketValue
	}


	/// Consume the ticket, also restore old interrupt state stored in this ticket
	pub fn Use (&mut self)
	{
		if self.isUsed.IsSet ()
		{
			panic! ("Trying to use an used ReaderTicket ({:?})!!!", self);
		}

		self.isUsed.Set ();
		CPU::RestoreInterruptState (self.oldCPUInterruptState);
	}
}


#[derive(Debug)]
struct RwTicketLockMutableData1
{
	currentTicket: u64,
	nextTicket: u64,
	currentGroupTicket: u64,
	nextGroupTicket: u64,

	// number of reader currently holding this lock
	readerCount: usize,

	// number of writer currently holding this lock
	// should always <= 1
	writerCount: usize,
}

impl RwTicketLockMutableData1
{
	pub const fn New () -> Self
	{
		Self
		{
			currentTicket: 0,
			nextTicket: 0,
			currentGroupTicket: 0,
			nextGroupTicket: 0,

			readerCount: 0,
			writerCount: 0,
		}
	}
}


/// A reader-writer ticket lock, allow multi concurrent reader or one writer
/// Only have theory problem when there are more than 2^64 threads hold the ticket, but that is impossiable in practice
/// Will disable interrupt until unlock
#[derive(Debug)]
struct RwTicketLock
{
	// use to ensure mutural of internal data
	internalTicketLock: TicketLock,
	rwTicketLockMutableData1: UnsafeCell<RwTicketLockMutableData1>,
}

impl RwTicketLock
{
	pub const fn New () -> Self
	{
		Self
		{
			internalTicketLock: TicketLock::New (),
			rwTicketLockMutableData1: UnsafeCell::new (RwTicketLockMutableData1::New ())
		}
	}

	pub fn ReaderLock (&self) -> ReaderTicket
	{
		// disable CPU interrupt
		let thisCpuOldCPUInterruptState = CPU::DisableInterrupt ();

		let internalTicketLockTicket = self.internalTicketLock.Lock ();

		let rwTicketLockMutableData1 = unsafe { &mut *self.rwTicketLockMutableData1.get () };

		// get ticket
		let readerTicket = ReaderTicket::New (rwTicketLockMutableData1.nextTicket, rwTicketLockMutableData1.nextGroupTicket, thisCpuOldCPUInterruptState);

		rwTicketLockMutableData1.nextTicket += 1;

		self.internalTicketLock.Unlock (internalTicketLockTicket);

		// now try to grab the lock
		loop
		{
			if readerTicket.CanUse (rwTicketLockMutableData1.currentGroupTicket)
			{
				// success
				break;
			}
			else
			{
				// failed, save some CPU cycles
				CPU::Pause ();
			}
		}

		let internalTicketLockTicket = self.internalTicketLock.Lock ();

		assert! (rwTicketLockMutableData1.writerCount == 0, "Got ReaderLock while there is writer!");

		rwTicketLockMutableData1.readerCount += 1;

		self.internalTicketLock.Unlock (internalTicketLockTicket);

		return readerTicket;
	}

	pub fn ReaderUnlock (&self, mut readerTicket: ReaderTicket)
	{
		let rwTicketLockMutableData1 = unsafe { &mut *self.rwTicketLockMutableData1.get () };

		if !readerTicket.CanUse (rwTicketLockMutableData1.currentGroupTicket)
		{
			panic! ("Trying to unlock RwTicketLock but ReaderTicket can't be used!");
		}

		let internalTicketLockTicket = self.internalTicketLock.Lock ();

		assert! (rwTicketLockMutableData1.readerCount > 0, "Reader attempt to unlock RwTicketLock while there is no reader!");
		assert! (rwTicketLockMutableData1.writerCount == 0, "Reader attempt to unlock RwTicketLock while there is writer!");

		rwTicketLockMutableData1.readerCount -= 1;
		rwTicketLockMutableData1.currentTicket += 1;

		self.internalTicketLock.Unlock (internalTicketLockTicket);

		readerTicket.Use ();
	}

	pub fn WriterLock (&self) -> WriterTicket
	{
		// disable CPU interrupt
		let thisCpuOldCPUInterruptState = CPU::DisableInterrupt ();

		let internalTicketLockTicket = self.internalTicketLock.Lock ();

		let rwTicketLockMutableData1 = unsafe { &mut *self.rwTicketLockMutableData1.get () };

		// get ticket
		rwTicketLockMutableData1.nextGroupTicket += 1;

		let writerTicket = WriterTicket::New (rwTicketLockMutableData1.nextTicket, rwTicketLockMutableData1.nextGroupTicket, thisCpuOldCPUInterruptState);

		rwTicketLockMutableData1.nextGroupTicket += 1;

		rwTicketLockMutableData1.nextTicket += 1;

		self.internalTicketLock.Unlock (internalTicketLockTicket);

		// now try to grab the lock
		loop
		{
			if writerTicket.CanUse (rwTicketLockMutableData1.currentTicket)
			{
				// success
				break;
			}
			else
			{
				// failed, save some CPU cycles
				CPU::Pause ();
			}
		}

		let internalTicketLockTicket = self.internalTicketLock.Lock ();

		assert! (rwTicketLockMutableData1.readerCount == 0, "Got WriterLock while there is reader!");
		assert! (rwTicketLockMutableData1.writerCount == 0, "Got WriterLock while there is writer!");

		rwTicketLockMutableData1.writerCount += 1;

		self.internalTicketLock.Unlock (internalTicketLockTicket);

		return writerTicket;
	}

	pub fn WriterUnlock (&self, mut writerTicket: WriterTicket)
	{
		let rwTicketLockMutableData1 = unsafe { &mut *self.rwTicketLockMutableData1.get () };

		if !writerTicket.CanUse (rwTicketLockMutableData1.currentTicket)
		{
			panic! ("Trying to unlock RwTicketLock but WriterTicket can't be used!");
		}

		let internalTicketLockTicket = self.internalTicketLock.Lock ();

		assert! (rwTicketLockMutableData1.writerCount > 0, "Writer attempt to unlock RwTicketLock while there is no writer!");
		assert! (rwTicketLockMutableData1.writerCount == 1, "Multiple writers are holding RwTicketLock!");
		assert! (rwTicketLockMutableData1.readerCount == 0, "Writer attempt to unlock RwTicketLock while there is reader!");

		rwTicketLockMutableData1.writerCount -= 1;
		rwTicketLockMutableData1.currentTicket += 1;
		rwTicketLockMutableData1.currentGroupTicket += 2;

		self.internalTicketLock.Unlock (internalTicketLockTicket);

		writerTicket.Use ();
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum RwTicketLockError
{
	LOCKED,
}


#[derive(Debug)]
pub struct MutexGuard<'a, T>
{
	ticketLockRef: &'a TicketLock,
	ticket: Ticket,
	dataRef: &'a mut T,
}

impl<'a, T> MutexGuard<'a, T>
{
	fn New (ticketLockRef: &'a TicketLock, ticket: Ticket, dataRef: &'a mut T) -> Self
	{
		Self
		{
			ticketLockRef,
			ticket,
			dataRef,
		}
	}
}


impl<'a, T> Deref for MutexGuard<'a, T>
{
	type Target = T;

	fn deref (&self) -> &Self::Target
	{
		self.dataRef
	}
}

impl<'a, T> DerefMut for MutexGuard<'a, T>
{
	fn deref_mut (&mut self) -> &mut Self::Target
	{
		self.dataRef
	}
}

impl<'a, T> Drop for MutexGuard<'a, T>
{
	fn drop (&mut self)
	{
		self.ticketLockRef.Unlock (self.ticket);
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum MutexError
{
	LOCKED,
}


/// Work like rust's std mutex
/// Hold the data inside it
/// Use Mutex internally
#[derive(Debug)]
pub struct Mutex<T>
{
	ticketLock: TicketLock,
	data: UnsafeCell<T>,
}

impl<T> Mutex<T>
{
	pub const fn New (data: T) -> Self
	{
		Self
		{
			ticketLock: TicketLock::New (),
			data: UnsafeCell::new (data),
		}
	}

	pub fn Lock (&self) -> MutexGuard<T>
	{
		unsafe
		{
			let ticket = self.ticketLock.Lock ();
			MutexGuard::New (&self.ticketLock, ticket, &mut *self.data.get ())
		}
	}
}


unsafe impl<T> Sync for Mutex<T> {}
unsafe impl<T> Send for Mutex<T> {}


#[derive(Debug)]
pub struct RwMutexReaderGuard<'a, T>
{
	rwTicketLockRef: &'a RwTicketLock,
	readerTicket: ReaderTicket,
	dataRef: &'a mut T,
}

impl<'a, T> RwMutexReaderGuard<'a, T>
{
	fn New (rwTicketLockRef: &'a RwTicketLock, readerTicket: ReaderTicket, dataRef: &'a mut T) -> Self
	{
		Self
		{
			rwTicketLockRef,
			readerTicket,
			dataRef,
		}
	}
}


impl<'a, T> Deref for RwMutexReaderGuard<'a, T>
{
	type Target = T;

	fn deref (&self) -> &Self::Target
	{
		self.dataRef
	}
}

impl<'a, T> Drop for RwMutexReaderGuard<'a, T>
{
	fn drop (&mut self)
	{
		self.rwTicketLockRef.ReaderUnlock (self.readerTicket);
	}
}


#[derive(Debug)]
pub struct RwMutexWriterGuard<'a, T>
{
	rwTicketLockRef: &'a RwTicketLock,
	writerTicket: WriterTicket,
	dataRef: &'a mut T,
}

impl<'a, T> RwMutexWriterGuard<'a, T>
{
	fn New (rwTicketLockRef: &'a RwTicketLock, writerTicket: WriterTicket, dataRef: &'a mut T) -> Self
	{
		Self
		{
			rwTicketLockRef,
			writerTicket,
			dataRef,
		}
	}
}


impl<'a, T> Deref for RwMutexWriterGuard<'a, T>
{
	type Target = T;

	fn deref (&self) -> &Self::Target
	{
		self.dataRef
	}
}

impl<'a, T> DerefMut for RwMutexWriterGuard<'a, T>
{
	fn deref_mut (&mut self) -> &mut Self::Target
	{
		self.dataRef
	}
}

impl<'a, T> Drop for RwMutexWriterGuard<'a, T>
{
	fn drop (&mut self)
	{
		self.rwTicketLockRef.WriterUnlock (self.writerTicket);
	}
}


#[derive(Debug)]
pub struct RwMutex<T>
{
	rwTicketLock: RwTicketLock,
	data: UnsafeCell<T>,
}

impl<T> RwMutex<T>
{
	pub const fn New (data: T) -> Self
	{
		Self
		{
			rwTicketLock: RwTicketLock::New (),
			data: UnsafeCell::new (data),
		}
	}

	pub fn ReaderLock (&self) -> RwMutexReaderGuard<T>
	{
		unsafe
		{
			let readerTicket = self.rwTicketLock.ReaderLock ();
			RwMutexReaderGuard::New (&self.rwTicketLock, readerTicket, &mut *self.data.get ())
		}
	}

	pub fn WriterLock (&self) -> RwMutexWriterGuard<T>
	{
		unsafe
		{
			let writerTicket = self.rwTicketLock.WriterLock ();
			RwMutexWriterGuard::New (&self.rwTicketLock, writerTicket, &mut *self.data.get ())
		}
	}
}


unsafe impl<T> Sync for RwMutex<T> {}
unsafe impl<T> Send for RwMutex<T> {}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum RwMutexError
{
	LOCKED,
}

