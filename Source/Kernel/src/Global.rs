use core::cell::SyncUnsafeCell;

use crate::Lock::OneTimeFlag;


static mut isBootingFinished: OneTimeFlag = OneTimeFlag::New ();


pub fn IsBooting () -> bool
{
	unsafe { !isBootingFinished.IsSet () }
}

pub fn FinishBooting ()
{
	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	unsafe { isBootingFinished.Set (); }
}
