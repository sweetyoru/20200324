//! This module handler Inter-process Communication


use core::fmt;
use core::cell::SyncUnsafeCell;

use alloc::vec::Vec;
use alloc::sync::{Arc, Weak};

use crate::println;
use crate::ISC::SystemID;
use crate::Process::ProcessID;
use crate::DataStruct::DefaultHashMap;
use crate::Thread::{Thread, ThreadID};
use crate::Memory::Virtual::UserAddress;
use crate::Lock::{OneTimeFlag, Mutex, RwMutex};


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct MailboxMessagePrivateID(u128);

impl MailboxMessagePrivateID
{
	pub const fn New (mailboxMessagePrivateID: u128) -> Self
	{
		Self(mailboxMessagePrivateID)
	}
}


impl fmt::Display for MailboxMessagePrivateID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:032x}", self.0)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct MailboxMessageID
{
	systemID: SystemID,
	privateID: MailboxMessagePrivateID,
}

impl MailboxMessageID
{
	pub const fn New (systemID: SystemID, privateID: MailboxMessagePrivateID) -> Self
	{
		Self
		{
			systemID,
			privateID,
		}
	}
}


impl fmt::Display for MailboxMessageID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "({}, {})", self.systemID, self.privateID)
	}
}


#[derive(Debug)]
pub struct MailboxMessage
{
	messageID: MailboxMessageID,
	senderProcessID: ProcessID,
	senderThreadID: ThreadID,
	targetMailboxID: MailboxID,
	data: Vec<u8>,
}

impl MailboxMessage
{
	const MAX_DATA_SIZE: usize = 1*1024*1024*1024;

	pub fn New (messageID: MailboxMessageID, senderProcessID: ProcessID, senderThreadID: ThreadID,targetMailboxID: MailboxID, data: Vec<u8>) -> Self
	{
		assert! (data.len () <= Self::MAX_DATA_SIZE, "Message data size too large!");

		Self
		{
			messageID,
			senderProcessID,
			senderThreadID,
			targetMailboxID,
			data,
		}
	}

	pub fn Clone (&self) -> Self
	{
		Self
		{
			data: self.data.clone (),
			..*self
		}
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct MailboxPrivateID(u128);

impl MailboxPrivateID
{
	pub const fn New (mailboxPrivateID: u128) -> Self
	{
		Self(mailboxPrivateID)
	}
}


impl fmt::Display for MailboxPrivateID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:032x}", self.0)
	}
}


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct MailboxID
{
	systemID: SystemID,
	privateID: MailboxPrivateID,
}

impl MailboxID
{
	pub const fn New (systemID: SystemID, privateID: MailboxPrivateID) -> Self
	{
		Self { systemID, privateID, }
	}
}


impl fmt::Display for MailboxID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "({}, {})", self.systemID, self.privateID)
	}
}


#[derive(Debug)]
struct AsyncHandler
{
	targetThread: Weak<Thread>,
	handlerStartAddress: UserAddress,
}

impl AsyncHandler
{
	pub fn New (targetThread: Arc<Thread>, handlerStartAddress: UserAddress) -> Self
	{
		Self
		{
			targetThread: Arc::downgrade (&targetThread),
			handlerStartAddress,
		}
	}
}


#[derive(Debug)]
struct MailboxMutableData1
{
	messages: Vec<MailboxMessage>,
	registedAsyncHandler: Vec<AsyncHandler>,
	registedListener: Vec<Weak<Mailbox>>,
}

impl MailboxMutableData1
{
	pub fn New () -> Self
	{
		Self
		{
			messages: Vec::new (),
			registedAsyncHandler: Vec::new (),
			registedListener: Vec::new (),
		}
	}
}


#[derive(Debug)]
struct Mailbox
{
	mailboxID: MailboxID,
	mailboxMutableData1: Mutex<MailboxMutableData1>,
}

impl Mailbox
{
	pub fn New (mailboxID: MailboxID) -> Self
	{
		Self
		{
			mailboxID,
			mailboxMutableData1: Mutex::New (MailboxMutableData1::New ()),
		}
	}

	pub fn PutMessage (&self, messages: MailboxMessage)
	{
		let mut mailboxMutableData1 = self.mailboxMutableData1.Lock ();

		// send singal to all registed async handler
		mailboxMutableData1.registedAsyncHandler.retain (|hander|
			{
				if let Some(targetThread) = Weak::upgrade (&hander.targetThread)
				{
					targetThread.CreateAsyncState (hander.handlerStartAddress);
					true
				}
				else
				{
					false
				}
			});

		// send message to all registed listeners
		mailboxMutableData1.registedListener.retain (|listener|
			{
				if let Some(mailbox) = Weak::upgrade (listener)
				{
					mailbox.PutMessage (messages.Clone ());
					true
				}
				else
				{
					false
				}
			});

		mailboxMutableData1.messages.push (messages);
	}
}


static mailboxs: RwMutex<DefaultHashMap<MailboxID, Arc<Mutex<Mailbox>>>> = RwMutex::New (DefaultHashMap::New ());


/// This function must be called only once by boot CPU at boot time
pub fn InitIPC ()
{
	println! ("[Boot CPU][+] Init Inter Process Communication System...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }
}


fn GetMailbox (mailboxID: MailboxID) -> Option<Arc<Mutex<Mailbox>>>
{
	mailboxs.ReaderLock ().GetRef (&mailboxID).and_then (|x| Some(Arc::clone (x)))
}


pub fn SendMessageToMailbox (mailboxID: MailboxID, messages: MailboxMessage) -> Result<(), ()>
{
	if let Some(mailbox) = GetMailbox (mailboxID)
	{
		mailbox.Lock ().PutMessage (messages);
		return Ok(());
	}
	else
	{
		return Err(());
	}
}