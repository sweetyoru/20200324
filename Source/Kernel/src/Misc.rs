use crate::{print, println};
use crate::Memory::Virtual::VirtualAddress;
use crate::Memory::Physical::PhysicalAddress;


pub fn DumpMemory (startAddress: VirtualAddress, len: usize)
{
	let blockCount = (len + 15)/16;

	for i in 0..blockCount
	{
		print! ("{}: ", startAddress + i*16);

		for j in 0..16
		{
			print! ("{:02X} ", (startAddress + i*16 + j).VolatileRead::<u8> ());
		}

		println! ("");
	}
}


pub fn DumpMemoryPhysical (startAddress: PhysicalAddress, len: usize)
{
	let blockCount = (len + 15)/16;

	for i in 0..blockCount
	{
		print! ("{}: ", startAddress + i*16);

		for j in 0..16
		{
			print! ("{:02X} ", (startAddress + i*16 + j).VolatileRead::<u8> ());
		}

		println! ("");
	}
}

