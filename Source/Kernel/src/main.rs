#![no_std]									// don't link the Rust standard library
#![cfg_attr(not(doc), no_main)]				// disable all Rust-level entry points
#![allow(non_snake_case)]					// turn off warning about snake_case name
#![allow(dead_code)]						// turn off dead code warning
#![allow(non_upper_case_globals)]			// turn off global uppercase
#![allow(non_camel_case_types)]				// turn off type casing
#![feature(step_trait)]						// enable step trait for implement range
#![feature(alloc_error_handler)]			// enble alloc error handler
#![feature(panic_info_message)]				// enable panic message
#![feature(abi_x86_interrupt)]				// enable x86-interrupt calling convention
#![feature(naked_functions)]				// allow the use of naked function
#![feature(core_intrinsics)]				// allow to use core intrinsics
#![feature(sync_unsafe_cell)]				// allow to use sync unsafe cell to get rid of static mut
#![feature(inline_const)]
#![feature(const_trait_impl)]
#![feature(const_mut_refs)]
#![feature(asm_const)]
#![feature(generic_const_exprs)]
#![feature(linked_list_remove)]
#![feature(effects)]


extern crate alloc;


// Debug module must be first for all others module to use println!
mod Debug;

mod CPU;

// this module contain architecture dependent code
mod Arch;

mod Init;

mod BootInfo;
mod Crypto;
mod DataStruct;
mod Lock;
mod Panic;
mod Memory;
mod Process;
mod Thread;
mod SMP;
mod Interrupt;
mod Timer;
mod Scheduler;
mod ICC;
mod IPC;
mod ISC;
mod Capability;
mod SystemCall;
mod Misc;
mod Global;


static testData0: usize = 0;
static testData1: usize = 1;
static testData6666: usize = 6666;
static testDataMax: usize = 0xffff_ffff_ffff_ffff;


#[no_mangle]
pub extern "C" fn _start (avoidAreaAddress: u64) -> !
{
	// test data to ensure bootloader load everything correctly
	unsafe
	{
		assert_eq! (core::ptr::read_volatile (&testData0), 0);
		assert_eq! (core::ptr::read_volatile (&testData1), 1);
		assert_eq! (core::ptr::read_volatile (&testData6666), 6666);
		assert_eq! (core::ptr::read_volatile (&testDataMax), 0xffff_ffff_ffff_ffff);
	}

	Init::Init (avoidAreaAddress);

	loop {}
}
