//! Define various general functions, structs and enums

pub mod ClockTimer;
pub mod SchedulerTimer;


use core::fmt;
use core::ops;

use crate::CPU::CPU;

#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::Timer::*;


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum TimerMode
{
	OneShot,
	Period,
}


pub enum TimeUnit
{
	SECOND,
	MILISECOND,
	MICROSECOND,
	NANOSECOND,
	PICOSECOND,
	FEMTOSECOND,
}


/// each tick is 1 femtosecond
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub struct TimeDuration(u128);

impl TimeDuration
{
	const TICK_PER_SECOND: u128 = 1_000_000_000_000_000;
	const TICK_PER_MILISECOND: u128 = 1_000_000_000_000;
	const TICK_PER_MICROSECOND: u128 = 1_000_000_000;
	const TICK_PER_NANOSECOND: u128 = 1_000_000;
	const TICK_PER_PICOSECOND: u128 = 1_000;
	const TICK_PER_FEMTOSECOND: u128 = 1;

	pub fn New (timeUnit: TimeUnit, value: usize) -> Self
	{
		let tickMultiplier = match timeUnit
		{
			TimeUnit::SECOND => Self::TICK_PER_SECOND,
			TimeUnit::MILISECOND => Self::TICK_PER_MILISECOND,
			TimeUnit::MICROSECOND => Self::TICK_PER_MICROSECOND,
			TimeUnit::NANOSECOND => Self::TICK_PER_NANOSECOND,
			TimeUnit::PICOSECOND => Self::TICK_PER_PICOSECOND,
			TimeUnit::FEMTOSECOND => Self::TICK_PER_FEMTOSECOND,
		};

		Self((value as u128).checked_mul (tickMultiplier).unwrap ())
	}
}

impl ops::Add for TimeDuration
{
	type Output = Self;

	fn add (self, other: Self) -> Self
	{
		Self(self.0.checked_add (other.0).unwrap ())
	}
}

impl ops::Sub for TimeDuration
{
	type Output = Self;

	fn sub (self, other: Self) -> Self
	{
		Self(self.0.checked_sub (other.0).unwrap ())
	}
}

impl ops::Mul<usize> for TimeDuration
{
	type Output = Self;

	fn mul (self, other: usize) -> Self
	{
		Self(self.0.checked_mul (other as u128).unwrap ())
	}
}

impl ops::Div<usize> for TimeDuration
{
	type Output = Self;

	fn div (self, other: usize) -> Self
	{
		Self(self.0.checked_div (other as u128).unwrap ())
	}
}


impl fmt::Display for TimeDuration
{
	fn fmt (&self, f : &mut fmt::Formatter<'_>) -> fmt::Result
	{
		let mut v = self.0;

		let second = v / Self::TICK_PER_SECOND;
		v %= Self::TICK_PER_SECOND;

		let milisecond = v / Self::TICK_PER_MILISECOND;
		v %= Self::TICK_PER_MILISECOND;

		let microsecond = v / Self::TICK_PER_MICROSECOND;
		v %= Self::TICK_PER_MICROSECOND;

		let nanosecond = v / Self::TICK_PER_NANOSECOND;

		write! (f, "{}.{:03}{:03}{:03} second(s)", second, milisecond, microsecond, nanosecond)
	}
}


pub fn Sleep (duration: TimeDuration)
{
	let targetTime = ClockTimer::TimeFromBoot () + duration;
	while ClockTimer::TimeFromBoot () < targetTime { CPU::Pause (); }
}
