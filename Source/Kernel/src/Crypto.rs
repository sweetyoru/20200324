use core::marker::PhantomData;


const fnvOffset32: u32 = 0x811c9dc5;
const fnvOffset64: u64 = 0xcbf29ce484222325;
const fnvOffset128: u128 = 0x6c62272e07bb014262b821756295c58d;

const fnvPrime32: u32 = 0x01000193;
const fnvPrime64: u64 = 0x00000100000001B3;
const fnvPrime128: u128 = 0x0000000001000000000000000000013B;


pub trait Hasher: Clone
{
	const HASH_SIZE: usize;

	fn New () -> Self;
	fn Write (&mut self, bytes: &[u8]);
	fn GetHash (&self) -> [u8; Self::HASH_SIZE];
}


#[derive (Clone, Copy, Debug)]
pub struct FNV1A32_Hasher
{
	currentHash: u32,
}

impl FNV1A32_Hasher
{
	pub const fn New () -> Self { Self { currentHash: fnvOffset32 } }

	pub fn Write (&mut self, bytes: &[u8])
	{
		for &b in bytes
		{
			self.currentHash ^= b as u32;
			self.currentHash *= fnvPrime32;
		}
	}

	pub fn GetHash (&self) -> u32 { self.currentHash }
}

impl Hasher for FNV1A32_Hasher
{
	const HASH_SIZE: usize = 4;

	fn New () -> Self { Self::New () }

	fn Write (&mut self, bytes: &[u8]) { self.Write (bytes) }

	fn GetHash (&self) -> [u8; Self::HASH_SIZE]
	{
		unimplemented! ();
	}
}


#[derive (Clone, Copy, Debug)]
pub struct FNV1A64_Hasher
{
	currentHash: u64,
}

impl FNV1A64_Hasher
{
	pub const fn New () -> Self { Self { currentHash: fnvOffset64 } }

	pub fn Write (&mut self, bytes: &[u8])
	{
		for &b in bytes
		{
			self.currentHash ^= b as u64;
			self.currentHash *= fnvPrime64;
		}
	}

	pub fn GetHash (&self) -> u64 { self.currentHash }
}

impl Hasher for FNV1A64_Hasher
{
	const HASH_SIZE: usize = 8;

	fn New () -> Self { Self::New () }

	fn Write (&mut self, bytes: &[u8]) { self.Write (bytes) }

	fn GetHash (&self) -> [u8; Self::HASH_SIZE]
	{
		unimplemented! ();
	}
}

impl core::hash::Hasher for FNV1A64_Hasher
{
	fn write (&mut self, bytes: &[u8])
	{
		self.Write (bytes)
	}

	fn finish (&self) -> u64
	{
		unimplemented! ();
	}
}


#[derive (Clone, Copy, Debug)]
pub struct FNV1A128_Hasher
{
	currentHash: u128,
}

impl FNV1A128_Hasher
{
	pub const fn New () -> Self { Self { currentHash: fnvOffset128 } }

	pub fn Write (&mut self, bytes: &[u8])
	{
		for &b in bytes
		{
			self.currentHash ^= b as u128;
			self.currentHash *= fnvPrime128;
		}
	}

	pub fn GetHash (&self) -> u128 { self.currentHash }
}

impl Hasher for FNV1A128_Hasher
{
	const HASH_SIZE: usize = 16;

	fn New () -> Self { Self::New () }

	fn Write (&mut self, bytes: &[u8]) { self.Write (bytes) }

	fn GetHash (&self) -> [u8; Self::HASH_SIZE]
	{
		unimplemented! ();
	}
}


#[derive (Clone, Copy, Debug)]
pub struct HasherBuilder<H>
where H: Hasher
{
	phantomData: PhantomData<H>,
}

impl<H> HasherBuilder<H>
where H: Hasher
{
	pub const fn New () -> Self
	{
		Self
		{
			phantomData: PhantomData,
		}
	}

	pub fn NewHasher (&self) -> H { H::New () }
}
