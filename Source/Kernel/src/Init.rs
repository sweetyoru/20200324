//! This module will init kernel


use core::cell::SyncUnsafeCell;

use crate::Lock::OneTimeFlag;


pub fn Init (avoidAreaAddress: u64)
{
	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	#[cfg(architecture = "Amd64")]
	crate::Arch::Amd64::Init::Init (avoidAreaAddress);
}
