#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::Serial::*;


pub fn InitDebug ()
{
	#[cfg(architecture = "Amd64")]
	InitSerial ();
}