use core::cell::SyncUnsafeCell;

use crate::println;
use crate::Lock::{OneTimeFlag, Mutex};
use crate::BootInfo::{self, MMEntryType};
use crate::Memory::Physical::PhysicalAddress;


pub const FRAME_L1_SIZE: usize = 4096;
pub const FRAME_L2_SIZE: usize = 4096*512;
pub const FRAME_L3_SIZE: usize = 4096*512*512;
const LOW_FRAME_ADDRESS_LIMIT: PhysicalAddress = PhysicalAddress::New (1*1024*1024);


#[derive(Debug, Copy, Clone)]
struct SegmentHeader
{
	pub nextSegmentAddress: Option<PhysicalAddress>,
	pub numFrameInSegment: usize,
}

impl SegmentHeader
{
	pub fn New () -> Self
	{
		Self
		{
			nextSegmentAddress: None,
			numFrameInSegment: 0,
		}
	}
}


#[derive(Debug)]
struct FramePool
{
	totalFrameCount: usize,
	topSegmentAddress: Option<PhysicalAddress>,
	freeFrameCount: usize,
	usedFrameCount: usize,
}

impl FramePool
{
	pub fn TotalFrameCount (&self) -> usize { self.totalFrameCount }
	pub fn FreeFrameCount (&self) -> usize { self.freeFrameCount }
	pub fn UsedFrameCount (&self) -> usize { self.usedFrameCount }

	pub fn GetFrame (&mut self) -> Option<PhysicalAddress>
	{
		let result = self.topSegmentAddress;

		if self.freeFrameCount > 0
		{
			self.freeFrameCount -= 1;
			self.usedFrameCount += 1;
		}

		if self.freeFrameCount == 0
		{
			self.topSegmentAddress = None;
		}
		else if self.freeFrameCount > 0
		{
			let mut newSegmentHeader = self.topSegmentAddress.unwrap ().Read::<SegmentHeader> ();

			if newSegmentHeader.numFrameInSegment == 1
			{
				self.topSegmentAddress = newSegmentHeader.nextSegmentAddress;
			}
			else if newSegmentHeader.numFrameInSegment > 1
			{
				newSegmentHeader.numFrameInSegment -= 1;
				self.topSegmentAddress = Some(self.topSegmentAddress.unwrap () + FRAME_L1_SIZE);
				self.topSegmentAddress.unwrap ().Write::<SegmentHeader> (newSegmentHeader);
			}
			else { panic! ("numPageInSegment == 0"); }
		}

		if let Some(frameAddress) = result { assert! (frameAddress.IsValidFrameL1Address ()); };

		result
	}

	pub fn GetEmptyFrame (&mut self) -> Option<PhysicalAddress>
	{
		self.GetFrame ().and_then (|newFrameAddress| { newFrameAddress.MemorySet (FRAME_L1_SIZE, &[0]); Some(newFrameAddress) })
	}

	pub fn FreeFrame (&mut self, frameAddress: PhysicalAddress)
	{
		assert! (frameAddress.IsValidFrameL1Address ());

		self.freeFrameCount += 1;
		self.usedFrameCount -= 1;


		// create new segment header
		let mut newSegmentHeader = SegmentHeader::New ();
		newSegmentHeader.numFrameInSegment = 1;

		if let Some (oldTopSegmentAddress) = self.topSegmentAddress
		{
			// point new segment to old segment
			newSegmentHeader.nextSegmentAddress = Some(oldTopSegmentAddress);
		}


		// write new segment header to new frame
		frameAddress.Write::<SegmentHeader> (newSegmentHeader);


		// point pool to new segment
		self.topSegmentAddress = Some(frameAddress);
	}

	pub fn AddSegmentToPool (&mut self, segmentStartAddress: PhysicalAddress, numFrameInSegment: usize)
	{
		assert! (segmentStartAddress.IsValidFrameL1Address ());
		assert! (numFrameInSegment > 0);


		self.totalFrameCount += numFrameInSegment;
		self.freeFrameCount += numFrameInSegment;

		// create new segment header
		let mut newSegmentHeader = SegmentHeader::New ();
		newSegmentHeader.numFrameInSegment = numFrameInSegment;

		if let Some(oldTopSegmentAddress) = self.topSegmentAddress
		{
			// point new segment to old segment
			newSegmentHeader.nextSegmentAddress = Some(oldTopSegmentAddress);
		}

		// write new segment header to new segment
		segmentStartAddress.Write::<SegmentHeader> (newSegmentHeader);


		// point pool to new segment
		self.topSegmentAddress = Some(segmentStartAddress);
	}
}


static normalFramePool: Mutex<FramePool> = Mutex::New (FramePool
	{
		totalFrameCount: 0,
		topSegmentAddress: None,
		freeFrameCount: 0,
		usedFrameCount: 0,
	});


static lowFramePool: Mutex<FramePool> = Mutex::New (FramePool
	{
		totalFrameCount: 0,
		topSegmentAddress: None,
		freeFrameCount: 0,
		usedFrameCount: 0,
	});


/// This function must be called only once at boot by boot CPU
pub fn InitFrameAllocatorPart1 ()
{
	println! ("[Boot CPU][+] Init Frame Allocator part 1...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	let bootInfo = BootInfo::GetBootInfo ();

	let avoidAreaStartFrameAddress = bootInfo.avoidAreaStartAddress.AlignDown (FRAME_L1_SIZE);
	let avoidAreaEndFrameAddress = bootInfo.avoidAreaEndAddress.AlignDown (FRAME_L1_SIZE);

	// add all free memory segments to free pool, exclude avoid area
	for memoryMapEntry in bootInfo.memoryMap.iter ()
	{
		if memoryMapEntry.Type () == MMEntryType::Free
		{
			let startAddress = memoryMapEntry.Address ();
			let endAddress = memoryMapEntry.Address () + memoryMapEntry.Size () - 1;

			// get the start frame and end frame address
			// only count frame that is fully usable
			let startFrameAddress = startAddress.Align (FRAME_L1_SIZE);
			let endFrameAddress = (endAddress + 1 - FRAME_L1_SIZE).AlignDown (FRAME_L1_SIZE);

			// check for overlap
			if endFrameAddress < avoidAreaStartFrameAddress || startFrameAddress > avoidAreaEndFrameAddress
			{
				// outside, not overlap
				AddSegmentToPool (startFrameAddress, (endFrameAddress - startFrameAddress)/FRAME_L1_SIZE + 1);
			}
			else // overlap
			{
				// the first part is overlapped
				if startFrameAddress < avoidAreaStartFrameAddress
				{
					AddSegmentToPool (startFrameAddress, (avoidAreaStartFrameAddress - startFrameAddress)/FRAME_L1_SIZE + 1);
				}

				// the second part is overlapped
				if endFrameAddress > avoidAreaEndFrameAddress
				{
					AddSegmentToPool (avoidAreaEndFrameAddress, (endFrameAddress - avoidAreaEndFrameAddress)/FRAME_L1_SIZE + 1);
				}
			}
		}
	}
}


/// This function is only called at boot
fn AddSegmentToPool (startFrameAddress: PhysicalAddress, numFrameInSegment: usize)
{
	let endFrameAddress = startFrameAddress + (numFrameInSegment - 1)*FRAME_L1_SIZE;

	assert! (startFrameAddress <= endFrameAddress);

	// if this segment below 1mb
	if endFrameAddress < LOW_FRAME_ADDRESS_LIMIT
	{
		lowFramePool.Lock ().AddSegmentToPool (startFrameAddress, numFrameInSegment);
	}
	else if startFrameAddress >= LOW_FRAME_ADDRESS_LIMIT // if this segment above 1mb
	{
		normalFramePool.Lock ().AddSegmentToPool (startFrameAddress, numFrameInSegment);
	}
	else // if this segment cross 1mb
	{
		// add first segment
		lowFramePool.Lock ().AddSegmentToPool (startFrameAddress, (LOW_FRAME_ADDRESS_LIMIT - startFrameAddress)/FRAME_L1_SIZE);

		// add second segment
		normalFramePool.Lock ().AddSegmentToPool (LOW_FRAME_ADDRESS_LIMIT, (endFrameAddress - LOW_FRAME_ADDRESS_LIMIT)/FRAME_L1_SIZE + 1);
	}
}


/// This function must be called only once at boot by boot CPU
pub fn InitFrameAllocatorPart2 ()
{
	println! ("[Boot CPU][+] Init Frame Allocator part 2...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	let bootInfo = BootInfo::GetBootInfo ();

	let avoidAreaStartFrameAddress = bootInfo.avoidAreaStartAddress.AlignDown (FRAME_L1_SIZE);
	let avoidAreaEndFrameAddress = bootInfo.avoidAreaEndAddress.AlignDown (FRAME_L1_SIZE);

	AddSegmentToPool (avoidAreaStartFrameAddress, (avoidAreaEndFrameAddress - avoidAreaStartFrameAddress)/FRAME_L1_SIZE + 1);
}


pub fn TotalLowFrameCount () -> usize { lowFramePool.Lock ().TotalFrameCount () }
pub fn TotalNormalFrameCount () -> usize { normalFramePool.Lock ().TotalFrameCount () }
pub fn TotalFrameCount () -> usize { lowFramePool.Lock ().TotalFrameCount () + normalFramePool.Lock ().TotalFrameCount () }


pub fn FreeLowFrameCount () -> usize { lowFramePool.Lock ().FreeFrameCount () }
pub fn FreeNormalFrameCount () -> usize { normalFramePool.Lock ().FreeFrameCount () }
pub fn FreeFrameCount () -> usize { lowFramePool.Lock ().FreeFrameCount () + normalFramePool.Lock ().FreeFrameCount () }


pub fn UsedLowFrameCount () -> usize { lowFramePool.Lock ().UsedFrameCount () }
pub fn UsedNormalFrameCount () -> usize { normalFramePool.Lock ().UsedFrameCount () }
pub fn UsedFrameCount () -> usize { lowFramePool.Lock ().UsedFrameCount () + normalFramePool.Lock ().UsedFrameCount () }


/// Return frame in normal pool, fall back to low pool if normal pool is empty
pub fn GetNormalFrame () -> Option<PhysicalAddress>
{
	normalFramePool.Lock ().GetFrame ().or_else (|| lowFramePool.Lock ().GetFrame ())
}


/// Return frame in normal pool, fall back to low pool if normal pool is empty
/// Returned frame is zero'ed
pub fn GetEmptyNormalFrame () -> Option<PhysicalAddress>
{
	normalFramePool.Lock ().GetEmptyFrame ().or_else (|| lowFramePool.Lock ().GetEmptyFrame ())
}


/// Return frame in low pool
pub fn GetLowFrame () -> Option<PhysicalAddress> { lowFramePool.Lock ().GetFrame () }


/// Return frame in low pool
/// Returned frame is zero'ed
pub fn GetEmptyLowFrame () -> Option<PhysicalAddress> { lowFramePool.Lock ().GetEmptyFrame () }


/// Put a free frame to normal pool or low pool depend on frame address
pub fn FreeFrame (frameAddress: PhysicalAddress)
{
	if frameAddress < LOW_FRAME_ADDRESS_LIMIT { lowFramePool.Lock ().FreeFrame (frameAddress) }
	else { normalFramePool.Lock ().FreeFrame (frameAddress) }
}
