use core::mem;
use core::cell::SyncUnsafeCell;
use core::alloc::{GlobalAlloc, Layout};

use crate::SMP;
use crate::println;
use crate::Lock::{OneTimeFlag, Mutex};
use crate::Memory::Virtual::{self, KernelAddress, PAGE_SIZE, PageFlags};


const HEAP_DEBUG: bool = false;


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct KernelHeapChunkPtr(KernelAddress);

impl KernelHeapChunkPtr
{
	pub const fn New (chunkAddress: KernelAddress) -> Self { Self(chunkAddress) }
}


impl core::ops::Deref for KernelHeapChunkPtr
{
	type Target = KernelHeapChunk;
	fn deref (&self) -> &Self::Target { self.0.ToConst::<Self::Target> () }
}

impl core::ops::DerefMut for KernelHeapChunkPtr
{
	fn deref_mut (&mut self) -> &mut Self::Target { self.0.ToMut::<Self::Target> () }
}


#[repr(C, packed)]
struct KernelHeapChunk
{
	size: u64,
	flags: u64,
	prevChunkPtr: KernelHeapChunkPtr,
	nextChunkPtr: KernelHeapChunkPtr,
	prevFreeChunkPtr: KernelHeapChunkPtr,
	nextFreeChunkPtr: KernelHeapChunkPtr,
	freeBinSize: u64,
	reserved: u64,
}

impl KernelHeapChunk
{
	const HEADER_SIZE: usize = mem::size_of::<KernelHeapChunk> () as usize;
	const CHUNK_SIZE_STEP: usize = Self::HEADER_SIZE;

	const IN_USE: u64 = 1<<0;
	const IN_FREE_BIN: u64 = 1<<1;
	const GUARDED: u64 = 1<<2;
	const HAVE_PREV_CHUNK: u64 = 1<<3;
	const HAVE_NEXT_CHUNK: u64 = 1<<4;
	const HAVE_PREV_FREE_CHUNK: u64 = 1<<5;
	const HAVE_NEXT_FREE_CHUNK: u64 = 1<<6;

	/// Create a new chunk with follow attributes:
	/// 1. not in use
	/// 2. not in any free bin
	/// 3. not guarded
	/// 4. size = 0
	/// 5. don't have prev chunk
	/// 6. don't have next chunk
	/// 7. don't have prev free chunk
	/// 8. don't have next free chunk
	pub const fn New () -> Self
	{
		Self
		{
			size: 0,
			flags: 0,
			prevChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			nextChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			prevFreeChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			nextFreeChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			freeBinSize: 0,
			reserved: 0,
		}
	}

	pub fn Address (&self) -> KernelAddress { KernelAddress::New (self as *const Self as usize) }
	pub fn ToPtr (&self) -> KernelHeapChunkPtr { KernelHeapChunkPtr::New (self.Address ()) }

	pub fn DataAddress (&self) -> KernelAddress { self.Address () + Self::HEADER_SIZE }

	pub fn Size (&self) -> usize
	{
		assert! (self.size as usize >= Self::HEADER_SIZE);
		assert! (self.size as usize % Self::CHUNK_SIZE_STEP == 0);
		self.size as usize
	}

	pub fn SetSize (&mut self, size: usize)
	{
		assert! (size >= Self::HEADER_SIZE);
		assert! (size % Self::CHUNK_SIZE_STEP == 0);
		self.size = size as u64
	}

	pub fn DataSize (&self) -> usize
	{
		assert! (self.size as usize >= Self::HEADER_SIZE);
		assert! (self.size as usize % Self::CHUNK_SIZE_STEP == 0);
		(self.size as usize - Self::HEADER_SIZE) as usize
	}

	pub const fn IsInUse (&self) -> bool { (self.flags & Self::IN_USE) != 0 }

	pub fn SetAsInUse (&mut self)
	{
		assert! (!self.IsInUse ());
		self.flags |= Self::IN_USE;
	}

	pub const fn IsFree (&self) -> bool { !self.IsInUse () }

	pub fn SetAsFree (&mut self)
	{
		assert! (self.IsInUse ());
		self.flags &= !Self::IN_USE;
	}

	pub const fn IsInFreeBin (&self) -> bool { (self.flags & Self::IN_FREE_BIN) != 0 }

	pub fn SetInFreeBin (&mut self)
	{
		assert! (!self.IsInFreeBin ());
		self.flags |= Self::IN_FREE_BIN;
	}

	pub fn SetNotInFreeBin (&mut self)
	{
		assert! (self.IsInFreeBin ());
		self.flags &= !Self::IN_FREE_BIN;
	}

	pub const fn IsGuarded (&self) -> bool { (self.flags & Self::GUARDED) != 0 }

	pub fn SetAsGuarded (&mut self)
	{
		assert! (!self.IsGuarded ());
		self.flags |= Self::GUARDED;
	}

	pub fn SetAsNotGuarded (&mut self)
	{
		assert! (self.IsGuarded ());
		self.flags &= !Self::GUARDED;
	}

	pub const fn HavePrevChunk (&self) -> bool { (self.flags & Self::HAVE_PREV_CHUNK) != 0 }

	pub fn SetHavePrevChunk (&mut self)
	{
		assert! (!self.HavePrevChunk ());
		self.flags |= Self::HAVE_PREV_CHUNK;
	}

	pub fn SetNotHavePrevChunk (&mut self)
	{
		assert! (self.HavePrevChunk ());
		self.flags &= !Self::HAVE_PREV_CHUNK;
	}

	pub fn GetPrevChunkPtr (&self) -> KernelHeapChunkPtr
	{
		assert! (self.HavePrevChunk ());
		self.prevChunkPtr
	}

	pub const fn HaveNextChunk (&self) -> bool { (self.flags & Self::HAVE_NEXT_CHUNK) != 0 }

	pub fn SetHaveNextChunk (&mut self)
	{
		assert! (!self.HaveNextChunk ());
		self.flags |= Self::HAVE_NEXT_CHUNK;
	}

	pub fn SetNotHaveNextChunk (&mut self)
	{
		assert! (self.HaveNextChunk ());
		self.flags &= !Self::HAVE_NEXT_CHUNK;
	}

	pub fn GetNextChunkPtr (&self) -> KernelHeapChunkPtr
	{
		assert! (self.HaveNextChunk ());
		self.nextChunkPtr
	}

	pub const fn HavePrevFreeChunk (&self) -> bool { (self.flags & Self::HAVE_PREV_FREE_CHUNK) != 0 }

	pub fn SetHavePrevFreeChunk (&mut self)
	{
		assert! (!self.HavePrevFreeChunk ());
		self.flags |= Self::HAVE_PREV_FREE_CHUNK;
	}

	pub fn SetNotHavePrevFreeChunk (&mut self)
	{
		assert! (self.HavePrevFreeChunk ());
		self.flags &= !Self::HAVE_PREV_FREE_CHUNK;
	}

	pub fn GetPrevFreeChunkPtr (&self) -> KernelHeapChunkPtr
	{
		assert! (self.HavePrevFreeChunk ());
		self.prevFreeChunkPtr
	}

	pub const fn HaveNextFreeChunk (&self) -> bool { (self.flags & Self::HAVE_NEXT_FREE_CHUNK) != 0 }

	pub fn SetHaveNextFreeChunk (&mut self)
	{
		assert! (!self.HaveNextFreeChunk ());
		self.flags |= Self::HAVE_NEXT_FREE_CHUNK;
	}

	pub fn SetNotHaveNextFreeChunk (&mut self)
	{
		assert! (self.HaveNextFreeChunk ());
		self.flags &= !Self::HAVE_NEXT_FREE_CHUNK;
	}

	pub fn GetNextFreeChunkPtr (&self) -> KernelHeapChunkPtr
	{
		assert! (self.HaveNextFreeChunk ());
		self.nextFreeChunkPtr
	}

	/// check if a chunk can be splited in haft, with one of the chunk satisfy chunkSize and dataAlign
	/// 2 chunks is guaranteed to not overlap
	///
	/// current chunk dataSize must >= chunkSize
	/// dataAlign must > 0
	///
	/// input chunk state:
	/// - not in use
	/// - not in bin
	/// - not guarded
	pub fn CanSplit (&self, chunkSize: usize, dataAlign: usize) -> bool
	{
		assert! (self.IsFree ());
		assert! (self.DataSize () >= chunkSize);
		assert! (!self.IsInFreeBin ());
		assert! (!self.IsGuarded ());
		assert! (dataAlign > 0);
		assert! (chunkSize >= KernelHeapChunk::HEADER_SIZE);

		let dataSize = chunkSize - KernelHeapChunk::HEADER_SIZE;

		// split type A
		if self.DataAddress ().ToUsize () % dataAlign == 0 && self.DataSize () - dataSize >= KernelHeapChunk::HEADER_SIZE
		{
			return true
		}

		// split type B
		let left = self.DataAddress ().Align (dataAlign);
		let right = (self.DataAddress () + self.DataSize ()).AlignDown (dataAlign);
		let end = self.DataAddress () + self.DataSize ();

		let mut addr = right;

		while addr >= left
		{
			if addr - self.DataAddress () >= Self::HEADER_SIZE && addr + dataSize <= end
			{
				return true
			}

			addr -= dataAlign;
		}

		return false
	}

	/// split a chunk in haft, with one of the chunk satisfy dataSize and dataAlign
	/// 2 chunks is guaranteed to not overlap
	///
	/// return the second chunk
	///
	/// current chunk dataSize must >= chunkSize
	/// dataAlign must > 0
	///
	/// input chunk state:
	/// - not in use
	/// - not in bin
	/// - not in free list
	/// - not guarded
	///
	/// second chunk state:
	/// - not in use
	/// - not in bin
	/// - not in free list
	/// - not guarded
	pub fn Split (&mut self, chunkSize: usize, dataAlign: usize) -> KernelHeapChunkPtr
	{
		self.TrySplit (chunkSize, dataAlign).unwrap ()
	}

	/// split a chunk in haft, with one of the chunk satisfy dataSize and dataAlign
	/// 2 chunks is guaranteed to not overlap
	///
	/// return the second chunk
	///
	/// current chunk dataSize must >= chunkSize
	/// dataAlign must > 0
	///
	/// input chunk state:
	/// - not in use
	/// - not in bin
	/// - not in free list
	/// - not guarded
	///
	/// second chunk state:
	/// - not in use
	/// - not in bin
	/// - not in free list
	/// - not guarded
	pub fn TrySplit (&mut self, chunkSize: usize, dataAlign: usize) -> Result<KernelHeapChunkPtr, ()>
	{
		assert! (self.IsFree ());
		assert! (!self.IsInFreeBin ());
		assert! (!self.HavePrevFreeChunk ());
		assert! (!self.HaveNextFreeChunk ());
		assert! (!self.IsGuarded ());
		assert! (self.DataSize () >= chunkSize);
		assert! (dataAlign > 0);
		assert! (chunkSize >= KernelHeapChunk::HEADER_SIZE);

		let dataSize = chunkSize - KernelHeapChunk::HEADER_SIZE;

		let mut secondChunkPtr;

		// split type A
		if self.DataAddress ().ToUsize () % dataAlign == 0 && self.DataSize () - dataSize >= KernelHeapChunk::HEADER_SIZE
		{
			secondChunkPtr = KernelHeapChunkPtr::New (self.DataAddress () + dataSize);
		}
		else // split type B
		{
			// find split point
			let left = self.DataAddress ().Align (dataAlign);
			let right = (self.DataAddress () + self.DataSize ()).AlignDown (dataAlign);
			let end = self.DataAddress () + self.DataSize ();

			let mut addr = right;
			let mut found = false;

			while addr >= left
			{
				if addr - self.DataAddress () >= Self::HEADER_SIZE && addr + dataSize <= end
				{
					found = true;
					break;
				}

				addr -= dataAlign;
			}

			if !found { return Err(()) }

			// now split
			secondChunkPtr = KernelHeapChunkPtr::New (addr - Self::HEADER_SIZE);
		}

		// prepare second chunk
		*secondChunkPtr = KernelHeapChunk::New ();

		let tmp = secondChunkPtr.Address () - self.Address ();
		secondChunkPtr.SetSize (self.Size () - tmp);
		secondChunkPtr.prevChunkPtr = self.ToPtr ();
		secondChunkPtr.SetHavePrevChunk ();

		// second chunk next chunk ptr
		if self.HaveNextChunk ()
		{
			assert! (self.GetNextChunkPtr ().GetPrevChunkPtr () == self.ToPtr ());
			secondChunkPtr.nextChunkPtr = self.nextChunkPtr;
			secondChunkPtr.SetHaveNextChunk ();
			self.GetNextChunkPtr ().prevChunkPtr = secondChunkPtr;
		}
		else
		{
			self.SetHaveNextChunk ();
		}

		self.SetSize (self.Size () - secondChunkPtr.Size ());
		self.nextChunkPtr = secondChunkPtr;

		return Ok(secondChunkPtr)
	}

	/// unlink a chunk from any free list
	pub fn UnlinkChunkFromFreeList (&mut self)
	{
		assert! (self.IsFree ());

		if self.HavePrevFreeChunk ()
		{
			assert! (self.GetPrevFreeChunkPtr ().HaveNextFreeChunk ());
			assert! (self.GetPrevFreeChunkPtr ().GetNextFreeChunkPtr () == self.ToPtr ());

			if self.HaveNextFreeChunk ()
			{
				self.GetPrevFreeChunkPtr ().nextFreeChunkPtr = self.nextFreeChunkPtr;
			}
			else
			{
				self.GetPrevFreeChunkPtr ().SetNotHaveNextFreeChunk ();
			}
		}

		if self.HaveNextFreeChunk ()
		{
			assert! (self.GetNextFreeChunkPtr ().HavePrevFreeChunk ());
			assert! (self.GetNextFreeChunkPtr ().GetPrevFreeChunkPtr () == self.ToPtr ());

			if self.HavePrevFreeChunk ()
			{
				self.GetNextFreeChunkPtr ().prevFreeChunkPtr = self.prevFreeChunkPtr;
			}
			else
			{
				self.GetNextFreeChunkPtr ().SetNotHavePrevFreeChunk ();
			}
		}

		if self.HavePrevFreeChunk ()
		{
			self.SetNotHavePrevFreeChunk ();
		}

		if self.HaveNextFreeChunk ()
		{
			self.SetNotHaveNextFreeChunk ();
		}
	}

	/// merge with next chunk
	///
	/// current chunk state:
	/// - is free
	/// - not in bin
	/// - not in free list
	/// - not guarded
	/// - have next chunk
	///
	/// next chunk state:
	/// - is free
	/// - not in bin
	/// - not in free list
	/// - not guarded
	pub fn MergeWithNextChunk (&mut self)
	{
		assert! (self.IsFree ());
		assert! (!self.IsInFreeBin ());
		assert! (!self.HavePrevFreeChunk ());
		assert! (!self.HaveNextFreeChunk ());
		assert! (!self.IsGuarded ());
		assert! (self.HaveNextChunk ());
		assert! (self.GetNextChunkPtr ().GetPrevChunkPtr () == self.ToPtr ());

		assert! (self.GetNextChunkPtr ().IsFree ());
		assert! (!self.GetNextChunkPtr ().IsInFreeBin ());
		assert! (!self.GetNextChunkPtr ().HavePrevFreeChunk ());
		assert! (!self.GetNextChunkPtr ().HaveNextFreeChunk ());
		assert! (!self.GetNextChunkPtr ().IsGuarded ());

		let cSize = self.Size ();
		let nextChunkPtr = self.nextChunkPtr;

		self.SetSize (cSize + nextChunkPtr.Size ());

		if nextChunkPtr.HaveNextChunk ()
		{
			assert! (nextChunkPtr.GetNextChunkPtr ().HavePrevChunk ());
			assert! (nextChunkPtr.GetNextChunkPtr ().GetPrevChunkPtr () == nextChunkPtr.ToPtr ());

			self.nextChunkPtr = nextChunkPtr.nextChunkPtr;
			nextChunkPtr.GetNextChunkPtr ().prevChunkPtr = self.ToPtr ();
		}
		else
		{
			self.SetNotHaveNextChunk ();
		}
	}
}


/// normal freebin like libc malloc
/// chunks in this bin have a same size and inbin bit set
/// and don't be merged
#[derive(Debug)]
struct FreeBin
{
	chunkSize: usize,
	firstFreeChunkPtr: KernelHeapChunkPtr,
	chunkCount: usize,
	maxChunkCount: usize,
	totalSize: usize,
	usableSize: usize,
}

impl FreeBin
{
	pub const fn New (chunkSize: usize, maxChunkCount: usize) -> Self
	{
		Self
		{
			chunkSize,
			firstFreeChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			chunkCount: 0,
			maxChunkCount,
			totalSize: 0,
			usableSize: 0,
		}
	}

	pub const fn CanInsertNewChunk (&self) -> bool
	{
		self.chunkCount < self.maxChunkCount
	}

	/// insert a new chunk to bin
	///
	/// input chunk state:
	/// - not in use
	/// - not in bin
	/// - not in free list
	/// - not guarded
	/// - chunk size must equa bin chunk size
	pub fn InsertChunk (&mut self, mut chunkPtr: KernelHeapChunkPtr)
	{
		assert! (chunkPtr.IsFree ());
		assert! (!chunkPtr.IsInFreeBin ());
		assert! (!chunkPtr.HavePrevFreeChunk ());
		assert! (!chunkPtr.HaveNextFreeChunk ());
		assert! (!chunkPtr.IsGuarded ());
		assert! (chunkPtr.Size () == self.chunkSize);
		assert! (self.CanInsertNewChunk ());

		chunkPtr.SetInFreeBin ();
		chunkPtr.freeBinSize = self.chunkSize as u64;

		if self.chunkCount > 0
		{
			assert! (!self.firstFreeChunkPtr.HavePrevFreeChunk ());

			chunkPtr.nextFreeChunkPtr = self.firstFreeChunkPtr;
			chunkPtr.SetHaveNextFreeChunk ();
			self.firstFreeChunkPtr.prevFreeChunkPtr = chunkPtr;
			self.firstFreeChunkPtr.SetHavePrevFreeChunk ();
		}

		self.firstFreeChunkPtr = chunkPtr;
		self.chunkCount += 1;
		self.totalSize += chunkPtr.Size ();
		self.usableSize += chunkPtr.DataSize ();
	}

	/// get a free chunk from bin
	/// return a chunk if there is a chunk in bin satisfy align requirement, None otherwise
	///
	/// output chunk state:
	/// - not in use
	/// - not in bin
	/// - not in free list
	/// - not guarded
	pub fn GetChunk (&mut self, chunkSize: usize, dataAlign: usize) -> Option<KernelHeapChunkPtr>
	{
		assert! (self.chunkSize >= chunkSize);

		if self.chunkCount == 0
		{
			return None
		}

		// iter through all chunks
		// if there is a chunk satisfy dataAlign, return that chunk
		// else return none
		let mut currentChunkPtr = self.firstFreeChunkPtr;
		loop
		{
			assert! (currentChunkPtr.IsFree ());
			assert! (currentChunkPtr.IsInFreeBin ());
			assert! (!currentChunkPtr.IsGuarded ());
			assert! (currentChunkPtr.freeBinSize as usize == self.chunkSize);

			if currentChunkPtr.DataAddress ().ToUsize () % dataAlign == 0
			{
				// adjust firstFreeChunkPtr
				if currentChunkPtr == self.firstFreeChunkPtr && currentChunkPtr.HaveNextFreeChunk ()
				{
					self.firstFreeChunkPtr = currentChunkPtr.nextFreeChunkPtr;
				}

				// unlink current chunk from free list
				currentChunkPtr.UnlinkChunkFromFreeList ();

				assert! (!currentChunkPtr.HavePrevFreeChunk ());
				assert! (!currentChunkPtr.HaveNextFreeChunk ());

				// fix everything that follow
				self.chunkCount -= 1;
				self.totalSize -= currentChunkPtr.Size ();
				self.usableSize -= currentChunkPtr.DataSize ();

				currentChunkPtr.SetNotInFreeBin ();

				return Some(currentChunkPtr)
			}

			if currentChunkPtr.HaveNextFreeChunk ()
			{
				currentChunkPtr = currentChunkPtr.nextFreeChunkPtr;
			}
			else
			{
				return None
			}
		}
	}
}


#[derive(Debug)]
struct FreeChunkList
{
	firstFreeChunkPtr: KernelHeapChunkPtr,
	chunkCount: usize,
	totalSize: usize,
	usableSize: usize,
}

impl FreeChunkList
{
	pub const fn New () -> Self
	{
		Self
		{
			firstFreeChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			chunkCount: 0,
			totalSize: 0,
			usableSize: 0,
		}
	}

	/// insert a chunk to free list
	/// merge that chunk with prev/next chunk if prev/next chunk is free and also in free list
	///
	/// input chunk state:
	/// - is free
	/// - not in bin
	/// - not in free list
	/// - not guarded
	pub fn InsertChunk (&mut self, mut currentChunkPtr: KernelHeapChunkPtr)
	{
		assert! (currentChunkPtr.IsFree ());
		assert! (!currentChunkPtr.IsInFreeBin ());
		assert! (!currentChunkPtr.HavePrevFreeChunk ());
		assert! (!currentChunkPtr.HaveNextFreeChunk ());
		assert! (!currentChunkPtr.IsGuarded ());

		// check if can merge with prev chunk
		// if can merge, unlink prev chunk from free list then merge with prev chunk
		// also make prev chunk become current chunk
		if currentChunkPtr.HavePrevChunk () && currentChunkPtr.GetPrevChunkPtr ().IsFree () && !currentChunkPtr.GetPrevChunkPtr ().IsInFreeBin ()
		{
			self.chunkCount -= 1;
			self.totalSize -= currentChunkPtr.GetPrevChunkPtr ().Size ();
			self.usableSize -= currentChunkPtr.GetPrevChunkPtr ().DataSize ();

			currentChunkPtr = currentChunkPtr.GetPrevChunkPtr ();

			// adjust firstFreeChunkPtr if needed
			if self.firstFreeChunkPtr == currentChunkPtr && currentChunkPtr.HaveNextFreeChunk ()
			{
				self.firstFreeChunkPtr = currentChunkPtr.nextFreeChunkPtr;
			}

			currentChunkPtr.UnlinkChunkFromFreeList ();
			currentChunkPtr.MergeWithNextChunk ();
		}

		// check if can merge with next chunk
		// if can merge, unlink next chunk from free list then merge with next chunk
		if currentChunkPtr.HaveNextChunk () && currentChunkPtr.GetNextChunkPtr ().IsFree () && !currentChunkPtr.GetNextChunkPtr ().IsInFreeBin ()
		{
			self.chunkCount -= 1;
			self.totalSize -= currentChunkPtr.GetNextChunkPtr ().Size ();
			self.usableSize -= currentChunkPtr.GetNextChunkPtr ().DataSize ();

			// adjust firstFreeChunkPtr if needed
			if self.firstFreeChunkPtr == currentChunkPtr.GetNextChunkPtr () && currentChunkPtr.GetNextChunkPtr ().HaveNextFreeChunk ()
			{
				self.firstFreeChunkPtr = currentChunkPtr.GetNextChunkPtr ().nextFreeChunkPtr;
			}

			currentChunkPtr.GetNextChunkPtr ().UnlinkChunkFromFreeList ();
			currentChunkPtr.MergeWithNextChunk ();
		}

		// now insert current chunk to free list
		assert! (currentChunkPtr.IsFree ());
		assert! (!currentChunkPtr.IsInFreeBin ());
		assert! (!currentChunkPtr.HavePrevFreeChunk ());
		assert! (!currentChunkPtr.HaveNextFreeChunk ());

		if self.chunkCount > 0
		{
			currentChunkPtr.nextFreeChunkPtr = self.firstFreeChunkPtr;
			currentChunkPtr.SetHaveNextFreeChunk ();
			self.firstFreeChunkPtr.prevFreeChunkPtr = currentChunkPtr;
			self.firstFreeChunkPtr.SetHavePrevFreeChunk ();
		}

		self.firstFreeChunkPtr = currentChunkPtr;

		self.chunkCount += 1;
		self.totalSize += currentChunkPtr.Size ();
		self.usableSize += currentChunkPtr.DataSize ();
	}

	/// get a chunk from free list
	/// return a chunk that satisfy chunkSize and dataAlign if no splitting occur
	/// return a pair of chunk with at least 1 chunk satisfy chunkSize and dataAlign if splitting occur
	///
	/// output chunks state:
	/// - is free
	/// - not in bin
	/// - not in free list
	/// - not guarded
	pub fn GetChunk (&mut self, chunkSize: usize, dataAlign: usize) -> (Option<KernelHeapChunkPtr>, Option<KernelHeapChunkPtr>)
	{
		assert! (chunkSize >= KernelHeapChunk::HEADER_SIZE);

		if self.chunkCount == 0
		{
			return (None, None);
		}

		// for all chunks in free list
		let mut currentChunkPtr = self.firstFreeChunkPtr;
		loop
		{
			// if size is ok
			if currentChunkPtr.Size () == chunkSize
			{
				// check if align is ok
				if currentChunkPtr.DataAddress ().ToUsize () % dataAlign == 0
				{
					// align is ok too~
					// adjust firstFreeChunkPtr if needed
					if self.firstFreeChunkPtr == currentChunkPtr && currentChunkPtr.HaveNextFreeChunk ()
					{
						self.firstFreeChunkPtr = currentChunkPtr.nextFreeChunkPtr;
					}

					currentChunkPtr.UnlinkChunkFromFreeList ();
					self.chunkCount -= 1;
					self.totalSize -= currentChunkPtr.Size ();
					self.usableSize -= currentChunkPtr.DataSize ();

					return (Some(currentChunkPtr), None);
				}
			}
			else if currentChunkPtr.Size () > chunkSize && currentChunkPtr.CanSplit (chunkSize, dataAlign)
			{
				// adjust firstFreeChunkPtr if needed
				if self.firstFreeChunkPtr == currentChunkPtr && currentChunkPtr.HaveNextFreeChunk ()
				{
					self.firstFreeChunkPtr = currentChunkPtr.nextFreeChunkPtr;
				}

				// unlink current chunk
				currentChunkPtr.UnlinkChunkFromFreeList ();
				self.chunkCount -= 1;
				self.totalSize -= currentChunkPtr.Size ();
				self.usableSize -= currentChunkPtr.DataSize ();

				let secondChunkPtr = currentChunkPtr.Split (chunkSize, dataAlign);

				return (Some(currentChunkPtr), Some(secondChunkPtr));
			}

			if currentChunkPtr.HaveNextFreeChunk ()
			{
				currentChunkPtr = currentChunkPtr.nextFreeChunkPtr;
			}
			else
			{
				break;
			}
		}

		return (None, None);
	}
}


#[derive(Debug)]
struct KernelHeapAllocator
{
	size: usize,
	firstKernelHeapAddress: KernelAddress,
	lastKernelHeapAddress: KernelAddress,
	firstChunkPtr: KernelHeapChunkPtr,
	lastChunkPtr: KernelHeapChunkPtr,
	freeChunkList: FreeChunkList,
	freeBin: [FreeBin; Self::FREEBIN_COUNT],
}

impl KernelHeapAllocator
{
	const FREEBIN_COUNT: usize = 32;
	const ALIGN_LIMIT: usize = PAGE_SIZE;
	const KERNEL_HEAP_INIT_SIZE: usize = 32*1024*1024;

	pub const fn New () -> Self
	{
		Self
		{
			size: 0,
			firstKernelHeapAddress: KernelAddress::New (0),
			lastKernelHeapAddress: KernelAddress::New (0),
			firstChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			lastChunkPtr: KernelHeapChunkPtr::New (KernelAddress::New (0)),
			freeChunkList: FreeChunkList::New (),
			freeBin:
			[
				FreeBin::New (64*2, 64),
				FreeBin::New (64*3, 64),
				FreeBin::New (64*4, 64),
				FreeBin::New (64*5, 64),
				FreeBin::New (64*6, 64),
				FreeBin::New (64*7, 64),
				FreeBin::New (64*8, 64),
				FreeBin::New (64*9, 64),

				FreeBin::New (64*10, 64),
				FreeBin::New (64*11, 64),
				FreeBin::New (64*12, 64),
				FreeBin::New (64*13, 64),
				FreeBin::New (64*14, 64),
				FreeBin::New (64*15, 64),
				FreeBin::New (64*16, 64),
				FreeBin::New (64*17, 64),

				FreeBin::New (64*18, 64),
				FreeBin::New (64*19, 64),
				FreeBin::New (64*20, 64),
				FreeBin::New (64*21, 64),
				FreeBin::New (64*22, 64),
				FreeBin::New (64*23, 64),
				FreeBin::New (64*24, 64),
				FreeBin::New (64*25, 64),

				FreeBin::New (64*26, 64),
				FreeBin::New (64*27, 64),
				FreeBin::New (64*28, 64),
				FreeBin::New (64*29, 64),
				FreeBin::New (64*30, 64),
				FreeBin::New (64*31, 64),
				FreeBin::New (64*32, 64),
				FreeBin::New (64*33, 64),
			],
		}
	}

	/// expand kernel heap segment by size
	/// size must > 0 and be multiple of PAGE_SIZE
	fn Grow (&mut self, size: usize)
	{
		assert! (size > 0);
		assert! (size % PAGE_SIZE == 0);
		assert! (self.size % PAGE_SIZE == 0);
		assert! (self.firstKernelHeapAddress < self.lastKernelHeapAddress);
		assert! (self.size == self.lastKernelHeapAddress - self.firstKernelHeapAddress + 1);

		let mut heapPageFlags = PageFlags::New ();
		heapPageFlags.SetPresent ();
		heapPageFlags.SetWritable ();
		heapPageFlags.SetGlobal ();
		heapPageFlags.SetUserNotAccessible ();
		heapPageFlags.SetNotExecutable ();

		SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().MapNew ((self.lastKernelHeapAddress + 1).into (), size, heapPageFlags, false, true).unwrap ();
	}

	/// return a chunk with requested dataSize and dataAlign
	/// if failed, it will grow the heap and return None to signal heap grow occur
	/// the next time it's called, the alloc should not fail
	fn InternalAlloc (&mut self, requestedDataSize: usize, requestedDataAlign: usize) -> Option<KernelHeapChunkPtr>
	{
		assert! (requestedDataAlign <= Self::ALIGN_LIMIT);

		let chunkSize;
		let mut dataSize = requestedDataSize;
		let dataAlign;

		// calc size and align
		if dataSize == 0
		{
			dataSize = KernelHeapChunk::CHUNK_SIZE_STEP;
		}

		if requestedDataAlign == 0
		{
			dataAlign = KernelHeapChunk::CHUNK_SIZE_STEP;
		}
		else
		{
			dataAlign = requestedDataAlign;
		}

		dataSize = ((requestedDataSize + KernelHeapChunk::CHUNK_SIZE_STEP - 1) / KernelHeapChunk::CHUNK_SIZE_STEP ) * KernelHeapChunk::CHUNK_SIZE_STEP;

		chunkSize = dataSize + KernelHeapChunk::HEADER_SIZE;

		for index in 0..Self::FREEBIN_COUNT
		{
			if self.freeBin[index].chunkSize >= chunkSize
			{
				if let Some(mut chunkPtr) = self.freeBin[index].GetChunk (chunkSize, dataAlign)
				{
					// sanity checks
					assert! (chunkPtr.IsFree ());
					assert! (!chunkPtr.IsInFreeBin ());
					assert! (!chunkPtr.IsGuarded ());
					assert! (!chunkPtr.HavePrevFreeChunk ());
					assert! (!chunkPtr.HaveNextFreeChunk ());
					assert! (!chunkPtr.HavePrevChunk () || chunkPtr.GetPrevChunkPtr ().GetNextChunkPtr () == chunkPtr.ToPtr ());
					assert! (!chunkPtr.HaveNextChunk () || chunkPtr.GetNextChunkPtr ().GetPrevChunkPtr () == chunkPtr.ToPtr ());

					chunkPtr.SetAsInUse ();
					return Some(chunkPtr);
				}
			}
		}

		// free bin failed, check in free list
		let (firstChunk, secondChunk) = self.freeChunkList.GetChunk (chunkSize, dataAlign);
		if let (Some(mut chunkPtr), None) = (firstChunk, secondChunk)
		{
			// got a chunk with no splitting!
			// sanity checks
			assert! (chunkPtr.IsFree ());
			assert! (!chunkPtr.IsInFreeBin ());
			assert! (!chunkPtr.IsGuarded ());
			assert! (!chunkPtr.HavePrevFreeChunk ());
			assert! (!chunkPtr.HaveNextFreeChunk ());
			assert! (!chunkPtr.HavePrevChunk () || chunkPtr.GetPrevChunkPtr ().GetNextChunkPtr () == chunkPtr.ToPtr ());
			assert! (!chunkPtr.HavePrevChunk () || chunkPtr.GetPrevChunkPtr ().GetNextChunkPtr () == chunkPtr.ToPtr ());

			chunkPtr.SetAsInUse ();

			return Some(chunkPtr);
		}
		else if let (Some(firstChunkPtr), Some(secondChunkPtr)) = (firstChunk, secondChunk)
		{
			// got 2 chunks, splitting occurs!

			// update lastChunkPtr
			if self.lastChunkPtr == firstChunkPtr
			{
				self.lastChunkPtr = secondChunkPtr;
			}

			let mut resultChunkPtr;
			let mut remainderChunkPtr;

			if firstChunkPtr.Size () >= chunkSize && firstChunkPtr.DataAddress ().ToUsize () % dataAlign == 0
			{
				// first chunk satisfy requirement
				resultChunkPtr = firstChunkPtr;
				remainderChunkPtr = secondChunkPtr;
			}
			else if secondChunkPtr.Size () >= chunkSize && secondChunkPtr.DataAddress ().ToUsize () % dataAlign == 0
			{
				// second chunk satisfy requirement
				resultChunkPtr = secondChunkPtr;
				remainderChunkPtr = firstChunkPtr;
			}
			else
			{
				// this can't happen, panic!
				panic! ("Splitting error!");
			}

			resultChunkPtr.SetAsInUse ();
			remainderChunkPtr.SetAsInUse ();
			self.InternalFree (remainderChunkPtr);

			return Some(resultChunkPtr);
		}
		else
		{
			// free list failed, grow the heap
			let mut newChunkPtr = KernelHeapChunkPtr::New (self.lastKernelHeapAddress + 1);

			let expandSize = ((KernelHeapChunk::HEADER_SIZE + requestedDataSize*2 + PAGE_SIZE - 1) / PAGE_SIZE) * PAGE_SIZE;
			self.Grow (expandSize);

			// create new chunk at new mem
			*newChunkPtr = KernelHeapChunk::New ();
			newChunkPtr.SetSize (expandSize);
			newChunkPtr.prevChunkPtr = self.lastChunkPtr;
			newChunkPtr.SetHavePrevChunk ();
			self.lastChunkPtr.nextChunkPtr = newChunkPtr;
			self.lastChunkPtr.SetHaveNextChunk ();

			// free that chunk
			self.InternalFree (newChunkPtr);

			// return None to signal retry again
			return None;
		}
	}

	/// free a chunk
	/// input chunk state:
	/// - in use
	/// - not in free bin
	/// - not in free list
	fn InternalFree (&mut self, mut chunkPtr: KernelHeapChunkPtr)
	{
		assert! (chunkPtr.IsInUse ());
		assert! (!chunkPtr.IsInFreeBin ());
		assert! (!chunkPtr.HavePrevFreeChunk ());
		assert! (!chunkPtr.HaveNextFreeChunk ());

		chunkPtr.SetAsFree ();

		// check if can get into any free bin
		for index in 0..Self::FREEBIN_COUNT
		{
			if self.freeBin[index].chunkSize == chunkPtr.Size () && self.freeBin[index].CanInsertNewChunk ()
			{
				// found a bin for this chunk
				self.freeBin[index].InsertChunk (chunkPtr);
				return;
			}
		}

		// calc new lastChunkPtr
		let mut newLastChunkPtr = self.lastChunkPtr;

		if self.lastChunkPtr == chunkPtr
		{
			if chunkPtr.HavePrevChunk () && chunkPtr.GetPrevChunkPtr ().IsFree () && !chunkPtr.GetPrevChunkPtr ().IsInFreeBin ()
			{
				// current heap state
				//
				// prev chunk (in free list)
				// current chunk (being freed) <--- lastChunkPtr
				//
				// new lastChunkPtr will be prevChunk
				newLastChunkPtr = chunkPtr.prevChunkPtr;
			}
		}
		else if chunkPtr.HaveNextChunk () && chunkPtr.GetNextChunkPtr () == self.lastChunkPtr
		{
			if chunkPtr.GetNextChunkPtr ().IsFree () && !chunkPtr.GetNextChunkPtr ().IsInFreeBin ()
			{
				// current heap state
				//
				// current chunk (being freed)
				// next chunk (in free list)    <--- lastChunkPtr
				//
				// new lastChunkPtr will be currentChunk
				newLastChunkPtr = chunkPtr;
			}
		}

		// no bin found, insert this chunk to free list
		self.freeChunkList.InsertChunk (chunkPtr);

		// adjust lastChunkPtr
		self.lastChunkPtr = newLastChunkPtr;
	}

	pub fn Alloc (&mut self, dataSize: usize, dataAlign: usize) -> KernelAddress
	{
		// try to alloc up to 2 times
		for _ in 0..2
		{
			if let Some(chunkPtr) = self.InternalAlloc (dataSize, dataAlign)
			{
				return chunkPtr.DataAddress ();
			}
		}

		// failed
		panic! ("Out of memory, alloc failed 3 times!!!!");
	}

	pub fn Free (&mut self, dataAddress: KernelAddress)
	{
		self.InternalFree (KernelHeapChunkPtr::New (dataAddress - KernelHeapChunk::HEADER_SIZE));
	}
}


/// A wrapper around KernelHeapAllocator to deal with mutex lock
#[derive(Debug)]
struct GlobalKernelHeapAllocator
{
	allocator: Mutex<KernelHeapAllocator>,
}

impl GlobalKernelHeapAllocator
{
	pub const fn New () -> Self
	{
		Self
		{
			allocator: Mutex::New (KernelHeapAllocator::New ()),
		}
	}

	pub fn Alloc (&self, dataSize: usize, dataAlign: usize) -> KernelAddress
	{
		self.allocator.Lock ().Alloc (dataSize, dataAlign)
	}

	pub fn Free (&self, dataAddress: KernelAddress)
	{
		self.allocator.Lock ().Free (dataAddress);
	}
}


unsafe impl GlobalAlloc for GlobalKernelHeapAllocator
{
	unsafe fn alloc (&self, layout: Layout) -> *mut u8
	{
		self.Alloc (layout.size (), layout.align ()).ToMutRaw::<u8> ()
	}

	unsafe fn dealloc (&self, ptr: *mut u8, layout: Layout)
	{
		assert! (ptr as usize % layout.align () == 0);
		self.Free (KernelAddress::New (ptr as usize));
	}
}


#[alloc_error_handler]
fn MyAllocErrorHandler (_layout: Layout) -> !
{
	panic! ("My Heap Alloc Error Handler called!!!");
}


#[global_allocator]
static globalKernelHeapAllocator: GlobalKernelHeapAllocator = GlobalKernelHeapAllocator::New ();


pub fn Alloc (dataSize: usize, dataAlign: usize) -> KernelAddress
{
	globalKernelHeapAllocator.Alloc (dataSize, dataAlign)
}


pub fn Free (dataAddress: KernelAddress)
{
	globalKernelHeapAllocator.Free (dataAddress);
}


pub fn InitHeap ()
{
	println! ("[Boot CPU][+] Init Kernel Heap...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	println! ("[Boot CPU][+] Kernel Heap Init Size: {}", KernelHeapAllocator::KERNEL_HEAP_INIT_SIZE);


	let mut myKernelHeapAllocator = globalKernelHeapAllocator.allocator.Lock ();


	// init kernel heap state
	myKernelHeapAllocator.size = KernelHeapAllocator::KERNEL_HEAP_INIT_SIZE;
	myKernelHeapAllocator.firstKernelHeapAddress = (Virtual::GetKernelSpaceEndAddress () + 1).Align (PAGE_SIZE);
	myKernelHeapAllocator.lastKernelHeapAddress = myKernelHeapAllocator.firstKernelHeapAddress + myKernelHeapAllocator.size - 1;

	myKernelHeapAllocator.firstChunkPtr = KernelHeapChunkPtr::New (myKernelHeapAllocator.firstKernelHeapAddress);
	myKernelHeapAllocator.lastChunkPtr = myKernelHeapAllocator.firstChunkPtr;

	println! ("[Boot CPU][+] Kernel Heap Begin Address: {}", myKernelHeapAllocator.firstKernelHeapAddress);


	// grow heap to KERNEL_HEAP_INIT_SIZE
	let mut heapPageFlags = PageFlags::New ();
	heapPageFlags.SetPresent ();
	heapPageFlags.SetWritable ();
	heapPageFlags.SetUserNotAccessible ();
	heapPageFlags.SetGlobal ();
	heapPageFlags.SetNotExecutable ();

	Virtual::GetBootVirtualAddressSpaceRef ().MapNew (myKernelHeapAllocator.firstKernelHeapAddress.into (), KernelHeapAllocator::KERNEL_HEAP_INIT_SIZE, heapPageFlags, false, true).unwrap ();


	// create first chunk
	*myKernelHeapAllocator.firstChunkPtr = KernelHeapChunk::New ();
	myKernelHeapAllocator.firstChunkPtr.SetSize (KernelHeapAllocator::KERNEL_HEAP_INIT_SIZE);
	myKernelHeapAllocator.firstChunkPtr.SetAsInUse ();

	// free first chunk
	let firstChunkAddress = myKernelHeapAllocator.firstChunkPtr.DataAddress ();
	myKernelHeapAllocator.Free (firstChunkAddress);
}
