use core::{mem, slice};
use core::sync::atomic::{AtomicU8, AtomicUsize, Ordering};

use alloc::alloc::{Layout, GlobalAlloc};

use crate::SMP;
use crate::Lock::RwMutex;
use crate::DataStruct::DefaultHashMap;
use crate::Arch::Amd64::CPU::CPU;
use crate::Memory::Virtual::{KernelAddress, PageFlags};


#[derive(Debug)]
struct RegionHeaderInnerData
{
	prevRegionHeader: Option<&'static RegionHeader>,
	nextRegionHeader: Option<&'static RegionHeader>,
}

impl RegionHeaderInnerData
{
	pub const fn New () -> Self
	{
		Self
		{
			prevRegionHeader: None,
			nextRegionHeader: None,
		}
	}
}


/// Almost lock-free and thread-safe
/// Only the linked list part isn't lock-free
#[derive(Debug)]
struct RegionHeader
{
	regionAddress: KernelAddress,
	regionSize: usize,
	slotSize: usize,
	totalSlotCount: usize,
	hintArraySlotCount: usize,

	freeSlotCount: AtomicUsize,

	innerData: RwMutex<RegionHeaderInnerData>,
}

impl RegionHeader
{
	const HINT_ARRAY_OFFSET: usize = mem::size_of::<Self> ();


	// This function return a &mut ref, since the region header is made in-place together with region
	pub fn NewAt (regionAddress: KernelAddress, regionSize: usize, slotSize: usize, hintArraySlotCount: usize) -> &'static mut Self
	{
		let newRegionHeader = regionAddress.ToMut::<RegionHeader> ();

		newRegionHeader.regionAddress = regionAddress;
		newRegionHeader.regionSize = regionSize;
		newRegionHeader.slotSize = slotSize;
		newRegionHeader.hintArraySlotCount = hintArraySlotCount;
		newRegionHeader.innerData = RwMutex::New (RegionHeaderInnerData::New ());


		// calc slotCount
		let mut slotCount = 0;

		for tmpSlotCount in (0..regionSize/slotSize).rev ()
		{
			let usedSlotBitmapSize = (tmpSlotCount + 8 - 1)/8;
			let totalSlotSize = slotSize*tmpSlotCount;

			if (regionAddress + newRegionHeader.UsedSlotBitmapOffset () + usedSlotBitmapSize).Align (slotSize) + totalSlotSize - regionAddress + 1 <= regionSize
			{
				slotCount = tmpSlotCount;
				break;
			}
		};

		newRegionHeader.totalSlotCount = slotCount;
		newRegionHeader.freeSlotCount.store (slotCount, Ordering::SeqCst);


		// fill in hint array
		for hint in newRegionHeader.HintArray ()
		{
			hint.store (0, Ordering::SeqCst);
		}


		// fill in used slot bitmap
		for slotBitmap in newRegionHeader.UsedSlotBitmap ()
		{
			slotBitmap.store (0, Ordering::SeqCst);
		}

		newRegionHeader
	}

	pub const fn RegionAddress (&self) -> KernelAddress { self.regionAddress }
	pub const fn RegionSize (&self) -> usize { self.regionSize }
	pub const fn SlotSize (&self) -> usize { self.slotSize }
	pub const fn TotalSlotCount (&self) -> usize { self.totalSlotCount }
	pub const fn HintArraySlotCount (&self) -> usize { self.hintArraySlotCount }
	pub const fn HintArraySize (&self) -> usize { self.HintArraySlotCount ()*mem::size_of::<AtomicUsize> () }
	pub fn FreeSlotCount (&self) -> usize { self.freeSlotCount.load (Ordering::SeqCst) }

	pub fn GetPrevRegionHeader (&self) -> Option<&'static RegionHeader> { self.innerData.ReaderLock ().prevRegionHeader }
	pub fn SetPrevRegionHeader (&self, prevRegionHeader: Option<&'static RegionHeader>)
	{
		self.innerData.WriterLock ().prevRegionHeader = prevRegionHeader;
	}

	pub fn GetNextRegionHeader (&self) -> Option<&'static RegionHeader> { self.innerData.ReaderLock ().nextRegionHeader }
	pub fn SetNextRegionHeader (&self, nextRegionHeader: Option<&'static RegionHeader>)
	{
		self.innerData.WriterLock ().nextRegionHeader = nextRegionHeader;
	}

	const fn UsedSlotBitmapOffset (&self) -> usize { Self::HINT_ARRAY_OFFSET + self.HintArraySize () }
	const fn UsedSlotBitmapSize (&self) -> usize { (self.TotalSlotCount () + 8 - 1)/8 }

	const fn HintArray (&self) -> &[AtomicUsize]
	{
		unsafe { slice::from_raw_parts ((self.RegionAddress () + Self::HINT_ARRAY_OFFSET).ToConstRaw::<AtomicUsize> (), self.HintArraySlotCount ()) }
	}

	const fn UsedSlotBitmap (&self) -> &[AtomicU8]
	{
		unsafe { slice::from_raw_parts ((self.RegionAddress () + self.UsedSlotBitmapOffset ()).ToConstRaw::<AtomicU8> (), self.UsedSlotBitmapSize ()) }
	}

	const fn GetSlotArrayAddress (&self) -> KernelAddress
	{
		(self.RegionAddress () + self.UsedSlotBitmapOffset () + self.UsedSlotBitmapSize ()).Align (self.SlotSize ())
	}

	const fn IsValidSlotAddress (&self, slotAddress: KernelAddress) -> bool
	{
		// check slotAddress is inside expected range
		if slotAddress < self.GetSlotArrayAddress () { return false; }
		if slotAddress >= self.RegionAddress () + self.RegionSize () { return false; }

		// check if slotAddress is aligned
		if !slotAddress.IsAlign (self.SlotSize ()) { return false; }

		true
	}

	const fn GetSlotAddress (&self, slotIndex: usize) -> KernelAddress
	{
		assert! (slotIndex < self.TotalSlotCount ());
		self.GetSlotArrayAddress () + slotIndex*self.SlotSize ()
	}

	const fn GetSlotIndex (&self, slotAddress: KernelAddress) -> usize
	{
		assert! (self.IsValidSlotAddress (slotAddress));
		(slotAddress - self.GetSlotArrayAddress ())/self.SlotSize ()
	}

	fn IncreaseFreeSlotCount (&self)
	{
		let oldFreeSlotCount = self.freeSlotCount.fetch_add (1, Ordering::SeqCst);
		assert! (oldFreeSlotCount < self.TotalSlotCount ());
	}

	fn DecreaseFreeSlotCount (&self)
	{
		let oldFreeSlotCount = self.freeSlotCount.fetch_sub (1, Ordering::SeqCst);
		assert! (oldFreeSlotCount > 0);
	}

	fn AddSlotToHintArray (&self, slotIndex: usize)
	{
		let index = CPU::GetRandomNumberU128 () as usize % self.HintArraySlotCount ();
		self.HintArray ()[index].store (slotIndex, Ordering::SeqCst);
	}

	pub fn AllocSlot (&self) -> Option<KernelAddress>
	{
		let hintArray = self.HintArray ();
		let usedSlotBitmap = self.UsedSlotBitmap ();

		while self.FreeSlotCount() > 0
		{
			// find free slot in hint array
			// choose a random starting point to spread out the allocation
			// in hope that this can grab a free slot quicker than normal
			let hintArrayStartIndex = CPU::GetRandomNumberU128 () as usize % self.HintArraySlotCount ();

			for hintIndex in hintArrayStartIndex..self.HintArraySlotCount ()
			{
				// get the slot index
				let slotIndex = hintArray[hintIndex].load (Ordering::SeqCst);

				// check if ok
				let byteOffset = slotIndex/8;
				let bitOffset = slotIndex&0b111;

				if usedSlotBitmap[byteOffset].fetch_or (1<<bitOffset, Ordering::SeqCst)&(1<<bitOffset) == 0

				{
					self.DecreaseFreeSlotCount ();
					return Some(self.GetSlotAddress (slotIndex));
				}
			}

			for hintIndex in 0..hintArrayStartIndex
			{
				// get the slot index
				let slotIndex = hintArray[hintIndex].load (Ordering::SeqCst);

				// check if ok
				let byteOffset = slotIndex/8;
				let bitOffset = slotIndex&0b111;

				if usedSlotBitmap[byteOffset].fetch_or (1<<bitOffset, Ordering::SeqCst)&(1<<bitOffset) == 0

				{
					self.DecreaseFreeSlotCount ();
					return Some(self.GetSlotAddress (slotIndex));
				}
			}

			// find free slot in used slot bitmap
			// choose a random starting point to spread out the allocation
			// in hope that this can grab a free slot quicker than normal
			let slotArrayStartIndex = CPU::GetRandomNumberU128 () as usize % self.TotalSlotCount ();

			for slotIndex in slotArrayStartIndex..self.TotalSlotCount ()
			{
				let byteOffset = slotIndex/8;
				let bitOffset = slotIndex&0b111;

				if usedSlotBitmap[byteOffset].fetch_or (1<<bitOffset, Ordering::SeqCst)&(1<<bitOffset) == 0
				{
					self.DecreaseFreeSlotCount ();
					return Some(self.GetSlotAddress (slotIndex));
				}
			}

			for slotIndex in 0..slotArrayStartIndex
			{
				let byteOffset = slotIndex/8;
				let bitOffset = slotIndex&0b111;

				if usedSlotBitmap[byteOffset].fetch_or (1<<bitOffset, Ordering::SeqCst)&(1<<bitOffset) == 0
				{
					self.DecreaseFreeSlotCount ();
					return Some(self.GetSlotAddress (slotIndex));
				}
			}
		}

		None
	}

	pub fn DeallocSlot (&self, slotAddress: KernelAddress)
	{
		assert! (self.IsValidSlotAddress (slotAddress));

		let slotIndex = self.GetSlotIndex (slotAddress);
		let usedSlotBitmap = self.UsedSlotBitmap ();

		let byteOffset = slotIndex/8;
		let bitOffset = slotIndex&0b111;

		assert! (usedSlotBitmap[byteOffset].fetch_and (!(1<<bitOffset), Ordering::SeqCst)&(1<<bitOffset) != 0);

		self.AddSlotToHintArray (slotIndex);
		self.IncreaseFreeSlotCount ();
	}
}


#[derive(Debug)]
struct RegionGroupInnerData
{
	regionCount: usize,
	firstRegionHeader: Option<&'static RegionHeader>,
	lastRegionHeader: Option<&'static RegionHeader>,
}

impl RegionGroupInnerData
{
	pub const fn New () -> Self
	{
		Self
		{
			regionCount: 0,
			firstRegionHeader: None,
			lastRegionHeader: None,
		}
	}
}


#[derive(Debug)]
struct RegionGroup
{
	regionSize: usize,
	slotSize: usize,
	innerData: RwMutex<RegionGroupInnerData>,
}

impl RegionGroup
{
	pub const fn New (regionSize: usize, slotSize: usize) -> Self
	{
		Self
		{
			regionSize,
			slotSize,
			innerData: RwMutex::New (RegionGroupInnerData::New ()),
		}
	}

	pub const fn RegionSize (&self) -> usize { self.regionSize }
	pub const fn SlotSize (&self) -> usize { self.slotSize }

	fn AddNewRegion (&self) -> KernelAddress
	{
		// lock the list
		let mut innerData = self.innerData.WriterLock ();

		// build page flags
		let mut heapPageFlags = PageFlags::New ();
		heapPageFlags.SetPresent ();
		heapPageFlags.SetWritable ();
		heapPageFlags.SetGlobal ();
		heapPageFlags.SetUserNotAccessible ();
		heapPageFlags.SetNotExecutable ();

		// reserve a free shared kernel region
		let newRegionAddress = SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().ReserveFreeSharedKernelRegion (self.RegionSize ()).unwrap ();

		// map the reserved free region
		SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().MapNew (newRegionAddress.into (), self.RegionSize (), heapPageFlags, false, true).unwrap ();

		// create new region at that address
		RegionHeader::NewAt (newRegionAddress, self.regionSize, self.slotSize, 16);
		let newRegionHeader =  newRegionAddress.ToConst::<RegionHeader> ();

		// insert the region to the list
		if innerData.firstRegionHeader.is_none () && innerData.lastRegionHeader.is_none ()
		{
			innerData.firstRegionHeader = Some(newRegionHeader);
			innerData.lastRegionHeader = Some(newRegionHeader);
		}
		else if !innerData.firstRegionHeader.is_none () && !innerData.lastRegionHeader.is_none ()
		{
			let lastRegionHeader = innerData.lastRegionHeader.unwrap ();
			lastRegionHeader.SetNextRegionHeader (Some(newRegionHeader));
			newRegionHeader.SetPrevRegionHeader (Some(lastRegionHeader));
			innerData.lastRegionHeader = Some(newRegionHeader);
		}
		else
		{
			panic! ("first region: {:?}, last region: {:?}", innerData.firstRegionHeader, innerData.lastRegionHeader);
		}

		// adjust region count
		innerData.regionCount += 1;

		newRegionAddress
	}

	fn FreeRegion (&self, oldRegionAddress: KernelAddress)
	{
		// checks
		let oldRegionHeader = oldRegionAddress.ToConst::<RegionHeader> ();
		assert! (oldRegionHeader.FreeSlotCount () == oldRegionHeader.TotalSlotCount ());

		// lock the region group mutex
		let mut innerData = self.innerData.WriterLock ();

		// unlink the region
		let prevRegionHeader = oldRegionHeader.GetPrevRegionHeader ();
		let nextRegionHeader = oldRegionHeader.GetNextRegionHeader ();

		if let Some(prevRegionHeader) = prevRegionHeader
		{
			prevRegionHeader.SetNextRegionHeader (nextRegionHeader);
		}

		if let Some(nextRegionHeader) = nextRegionHeader
		{
			nextRegionHeader.SetPrevRegionHeader (prevRegionHeader);
		}

		// adjust first region header
		if innerData.firstRegionHeader.unwrap ().RegionAddress () == oldRegionHeader.RegionAddress ()
		{
			innerData.firstRegionHeader = nextRegionHeader;
		}

		// adjust last region header
		if innerData.lastRegionHeader.unwrap ().RegionAddress () == oldRegionHeader.RegionAddress ()
		{
			innerData.lastRegionHeader = prevRegionHeader;
		}

		// unmap and free the region
		SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().Unmap (oldRegionAddress.into (), self.RegionSize (), true).unwrap ();

		// adjust region count
		innerData.regionCount -= 1;
	}

	pub fn AllocSlot (&self) -> Option<KernelAddress>
	{
		let innerData = self.innerData.ReaderLock ();
		let mut nextRegionHeader = innerData.firstRegionHeader;

		// try to alloc a slot in exsited regions
		while let Some(currentRegionHeader) = nextRegionHeader
		{
			// get a chunk, return
			if let Some(slotAddress) = currentRegionHeader.AllocSlot ()
			{
				return Some(slotAddress);
			}

			nextRegionHeader = currentRegionHeader.GetNextRegionHeader ();
		}

		// drop the readonly mutex lock
		mem::drop (innerData);

		// create new region until alloc success
		loop
		{
			// create new region
			let newRegionAddress = self.AddNewRegion ();

			// grab the lock
			#[allow(unused)]
			let innerData = self.innerData.ReaderLock ();

			if let Some(slotAddress) = newRegionAddress.ToMut::<RegionHeader> ().AllocSlot ()
			{
				return Some(slotAddress);
			}
		}
	}

	pub fn DeallocSlot (&self, slotAddress: KernelAddress)
	{
		// check slot address valid
		let region = slotAddress.AlignDown (self.regionSize).ToMut::<RegionHeader> ();
		assert! (region.IsValidSlotAddress (slotAddress));
		region.DeallocSlot (slotAddress);
	}
}


#[derive(Debug, Clone, Copy)]
struct BigSlot
{
	slotAddress: KernelAddress,
	slotSize: usize,
	align: usize,
}

impl BigSlot
{
	pub fn New (slotAddress: KernelAddress, slotSize: usize, align: usize) -> Self
	{
		Self
		{
			slotAddress,
			slotSize,
			align,
		}
	}

	pub const fn SlotAddress (&self) -> KernelAddress { self.slotAddress }
	pub const fn SlotSize (&self) -> usize { self.slotSize }
	pub const fn SlotAlign (&self) -> usize { self.align }
}


#[derive(Debug)]
struct GlobalKernelHeapSizeMap
{
	slotSize: usize,
	regionSize: usize,
}

impl GlobalKernelHeapSizeMap
{
	pub const fn New (slotSize: usize, regionSize: usize) -> Self
	{
		Self
		{
			slotSize,
			regionSize,
		}
	}

	pub const fn SlotSize (&self) -> usize { self.slotSize }
	pub const fn RegionSize (&self) -> usize { self.regionSize }
}


#[derive(Debug)]
struct GlobalKernelHeapInnerData
{
	bigSlots: DefaultHashMap<KernelAddress, BigSlot>,
}

impl GlobalKernelHeapInnerData
{
	pub const fn New () -> Self
	{
		Self
		{
			bigSlots: DefaultHashMap::New (),
		}
	}
}


#[derive(Debug)]
struct GlobalKernelHeap
{
	regionGroups: [RegionGroup; Self::REGION_GROUP_COUNT],
	innerData: RwMutex<GlobalKernelHeapInnerData>,
}

impl GlobalKernelHeap
{
	const REGION_GROUP_COUNT: usize = 16;
	const SIZE_MAP: [GlobalKernelHeapSizeMap; Self::REGION_GROUP_COUNT] =
	[
		GlobalKernelHeapSizeMap::New (16, 4*1024),
		GlobalKernelHeapSizeMap::New (32, 4*1024),
		GlobalKernelHeapSizeMap::New (64, 16*1024),
		GlobalKernelHeapSizeMap::New (128, 16*1024),
		GlobalKernelHeapSizeMap::New (256, 64*1024),
		GlobalKernelHeapSizeMap::New (512, 64*1024),
		GlobalKernelHeapSizeMap::New (1*1024, 256*1024),
		GlobalKernelHeapSizeMap::New (2*1024, 256*1024),
		GlobalKernelHeapSizeMap::New (4*1024, 1*1024*1024),
		GlobalKernelHeapSizeMap::New (8*1024, 1*1024*1024),
		GlobalKernelHeapSizeMap::New (16*1024, 1*1024*1024),
		GlobalKernelHeapSizeMap::New (32*1024, 1*1024*1024),
		GlobalKernelHeapSizeMap::New (64*1024, 2*1024*1024),
		GlobalKernelHeapSizeMap::New (128*1024, 2*1024*1024),
		GlobalKernelHeapSizeMap::New (256*1024, 4*1024*1024),
		GlobalKernelHeapSizeMap::New (512*1024, 8*1024*1024),
	];

	pub const fn New () -> Self
	{
		// init all regions
		Self
		{
			regionGroups:
			[
				RegionGroup::New (Self::SIZE_MAP[0].RegionSize (), Self::SIZE_MAP[0].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[1].RegionSize (), Self::SIZE_MAP[1].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[2].RegionSize (), Self::SIZE_MAP[2].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[3].RegionSize (), Self::SIZE_MAP[3].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[4].RegionSize (), Self::SIZE_MAP[4].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[5].RegionSize (), Self::SIZE_MAP[5].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[6].RegionSize (), Self::SIZE_MAP[6].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[7].RegionSize (), Self::SIZE_MAP[7].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[8].RegionSize (), Self::SIZE_MAP[8].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[9].RegionSize (), Self::SIZE_MAP[9].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[10].RegionSize (), Self::SIZE_MAP[10].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[11].RegionSize (), Self::SIZE_MAP[11].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[12].RegionSize (), Self::SIZE_MAP[12].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[13].RegionSize (), Self::SIZE_MAP[13].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[14].RegionSize (), Self::SIZE_MAP[14].SlotSize ()),
				RegionGroup::New (Self::SIZE_MAP[15].RegionSize (), Self::SIZE_MAP[15].SlotSize ()),
			],
			innerData: RwMutex::New (GlobalKernelHeapInnerData::New ()),
		}
	}

	fn AllocSmallSlot (&self, size: usize, align: usize) -> Option<KernelAddress>
	{
		for (index, sizeMap) in Self::SIZE_MAP.iter ().enumerate ()
		{
			if sizeMap.SlotSize () >= size
			{
				assert! (self.regionGroups[index].SlotSize () >= size);
				let slotAddress = self.regionGroups[index].AllocSlot ().unwrap ();
				assert! (slotAddress.IsAlign (align));
				return Some(slotAddress);
			}
		}

		None
	}

	fn DeallocSmallSlot (&self, slotAddress: KernelAddress, size: usize) -> Result<(), ()>
	{
		for (index, sizeMap) in Self::SIZE_MAP.iter ().enumerate ()
		{
			if sizeMap.SlotSize () >= size
			{
				assert! (self.regionGroups[index].SlotSize () >= size);
				self.regionGroups[index].DeallocSlot (slotAddress);
				return Ok(());
			}
		}

		Err(())
	}

	fn AllocBigSlot (&self, size: usize, align: usize) -> Option<KernelAddress>
	{
		// lock the list
		let mut innerData = self.innerData.WriterLock ();

		// grab a free region of memory
		let mut heapPageFlags = PageFlags::New ();
		heapPageFlags.SetPresent ();
		heapPageFlags.SetWritable ();
		heapPageFlags.SetGlobal ();
		heapPageFlags.SetUserNotAccessible ();
		heapPageFlags.SetNotExecutable ();

		// reserve a free shared kernel region
		let newBigSlotAddress = SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().ReserveFreeSharedKernelRegion (size).unwrap ();
		assert! (newBigSlotAddress.IsAlign (align));

		// map the reserved free region
		SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().MapNew (newBigSlotAddress.into (), size, heapPageFlags, false, true).unwrap ();

		// add entry to list
		let newBigSlot = BigSlot::New (newBigSlotAddress, size, align);
		innerData.bigSlots.Insert (newBigSlotAddress, newBigSlot);

		// return
		Some(newBigSlotAddress)
	}

	fn DeallocBigSlot (&self, slotAddress: KernelAddress) -> Result<(), ()>
	{
		// lock the list
		let mut innerData = self.innerData.WriterLock ();

		// remove the entry from list
		if let Some(oldBigSlot) = innerData.bigSlots.Remove (&slotAddress)
		{
			// unmap it
			SMP::GetCurrentCPU ().GetCurrentVirtualAddressSpace ().Unmap (oldBigSlot.SlotAddress ().into (), oldBigSlot.SlotSize (), true).unwrap ();
			return Ok(());
		}

		Err(())
	}

	pub fn Alloc (&self, size: usize, align: usize) -> KernelAddress
	{
		// alloc small slot successful
		if let Some(slotAddress) = self.AllocSmallSlot (size, align)
		{
			assert! (slotAddress.IsAlign (align));
			return slotAddress;
		}

		let slotAddress = self.AllocBigSlot (size, align).unwrap ();
		assert! (slotAddress.IsAlign (align));
		slotAddress
	}

	pub fn Dealloc (&self, slotAddress: KernelAddress, size: usize)
	{
		if self.DeallocSmallSlot (slotAddress, size).is_err ()
		{
			// big slot
			self.DeallocBigSlot (slotAddress).unwrap ();
		}
	}
}


unsafe impl GlobalAlloc for GlobalKernelHeap
{
	unsafe fn alloc (&self, layout: Layout) -> *mut u8
	{
		self.Alloc (layout.size (), layout.align ()).ToMutRaw::<u8> ()
	}

	unsafe fn dealloc (&self, ptr: *mut u8, layout: Layout)
	{
		assert! (ptr as usize % layout.align () == 0);
		self.Dealloc (KernelAddress::New (ptr as usize), layout.size ());
	}
}
