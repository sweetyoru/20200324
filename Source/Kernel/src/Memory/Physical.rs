use core::{mem, fmt};
use core::marker::PhantomData;

use crate::Arch::Amd64::CPU::CPU;
use crate::Memory::Physical::FrameAllocator::FRAME_L1_SIZE;

use crate::println;
pub mod FrameAllocator;


#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::Memory::Physical::*;


#[derive(Debug, Clone, Copy)]
pub struct RegionHeader
{
	regionAddress: PhysicalAddress,
}

impl RegionHeader
{
	// prevRegionAddress: Option<PhysicalAddress>,
	// nextRegionAddress: Option<PhysicalAddress>,
	// regionSize: usize
	// slotSize: usize
	// totalSlotCount: usize,
	// usedSlotCount: usize,
	// usedBitmap: [u8],

	const PREV_REGION_ADDRESS_OFFSET: usize = 0;
	const NEXT_REGION_ADDRESS_OFFSET: usize = Self::PREV_REGION_ADDRESS_OFFSET + mem::size_of::<Option<PhysicalAddress>> ();
	const REGION_SIZE_OFFSET: usize = Self::NEXT_REGION_ADDRESS_OFFSET + mem::size_of::<Option<PhysicalAddress>> ();
	const SLOT_SIZE_OFFSET: usize = Self::REGION_SIZE_OFFSET + mem::size_of::<usize> ();
	const TOTAL_SLOT_COUNT_OFFSET: usize = Self::SLOT_SIZE_OFFSET + mem::size_of::<usize> ();
	const USED_SLOT_COUNT_OFFSET: usize = Self::TOTAL_SLOT_COUNT_OFFSET + mem::size_of::<usize> ();
	const USED_SLOT_BITMAP_OFFSET: usize = Self::USED_SLOT_COUNT_OFFSET + mem::size_of::<usize> ();


	pub const fn RegionHeaderSize () -> usize
	{
		// next + prev ptr + region size + slot size + total slot count + used slot count
		mem::size_of::<Option<PhysicalAddress>> () + mem::size_of::<Option<PhysicalAddress>> () + mem::size_of::<usize> () + mem::size_of::<usize> () + mem::size_of::<usize> () + mem::size_of::<usize> ()
	}

	pub const fn New (regionAddress: PhysicalAddress) -> Self
	{
		Self
		{
			regionAddress,
		}
	}

	pub fn NewAt (regionAddress: PhysicalAddress, regionSize: usize, slotSize: usize) -> Self
	{
		// calculate total slot count
		let mut totalSlotCount = 0;

		for tmpTotalSlotCount in (0..regionSize/slotSize).rev ()
		{
			let usedSlotBitmapSize = (tmpTotalSlotCount + 8 - 7)/8;
			let totalSlotSize = slotSize*tmpTotalSlotCount;

			if (regionAddress + Self::USED_SLOT_BITMAP_OFFSET + usedSlotBitmapSize).Align (slotSize) + totalSlotSize - regionAddress + 1 <= regionSize
			{
				totalSlotCount = tmpTotalSlotCount;
				break;
			}
		};

		// init fields
		(regionAddress + Self::PREV_REGION_ADDRESS_OFFSET).Write::<Option<RegionHeader>> (None);
		(regionAddress + Self::NEXT_REGION_ADDRESS_OFFSET).Write::<Option<RegionHeader>> (None);
		(regionAddress + Self::REGION_SIZE_OFFSET).Write::<usize> (regionSize);
		(regionAddress + Self::SLOT_SIZE_OFFSET).Write::<usize> (slotSize);
		(regionAddress + Self::TOTAL_SLOT_COUNT_OFFSET).Write::<usize> (totalSlotCount);
		(regionAddress + Self::USED_SLOT_COUNT_OFFSET).Write::<usize> (0);

		// init used slot bitmap
		let usedSlotBitmapAddress = regionAddress + Self::USED_SLOT_BITMAP_OFFSET;
		let usedSlotBitmapSize = (totalSlotCount + 8 - 1)/8;

		for index in 0..usedSlotBitmapSize
		{
			(usedSlotBitmapAddress + index).Write::<u8> (0);
		}

		Self
		{
			regionAddress,
		}
	}

	pub const fn RegionAddress (&self) -> PhysicalAddress { self.regionAddress }

	pub fn GetPrevRegionHeader (&self) -> Option<RegionHeader>
	{
		(self.RegionAddress () + Self::PREV_REGION_ADDRESS_OFFSET).Read::<Option<PhysicalAddress>> ().map (|address| RegionHeader::New (address))
	}

	pub fn SetPrevRegionHeader (&mut self, prevRegionHeader: Option<RegionHeader>)
	{
		let prevRegionHeaderAddress = prevRegionHeader.map (|regionHeader| regionHeader.RegionAddress ());
		(self.RegionAddress () + Self::PREV_REGION_ADDRESS_OFFSET).Write::<Option<PhysicalAddress>> (prevRegionHeaderAddress);
	}

	pub fn GetNextRegionHeader (&self) -> Option<RegionHeader>
	{
		(self.RegionAddress () + Self::NEXT_REGION_ADDRESS_OFFSET).Read::<Option<PhysicalAddress>> ().map (|address| RegionHeader::New (address))
	}

	pub fn SetNextRegionHeader (&mut self, nextRegionHeader: Option<RegionHeader>)
	{
		let nextRegionHeaderAddress = nextRegionHeader.map (|regionHeader| regionHeader.RegionAddress ());
		(self.RegionAddress () + Self::NEXT_REGION_ADDRESS_OFFSET).Write::<Option<PhysicalAddress>> (nextRegionHeaderAddress);
	}

	pub fn RegionSize (&self) -> usize
	{
		(self.RegionAddress () + Self::REGION_SIZE_OFFSET).Read::<usize> ()
	}

	pub fn SlotSize (&self) -> usize
	{
		(self.RegionAddress () + Self::SLOT_SIZE_OFFSET).Read::<usize> ()
	}

	pub fn TotalSlotCount (&self) -> usize
	{
		(self.RegionAddress () + Self::TOTAL_SLOT_COUNT_OFFSET).Read::<usize> ()
	}

	pub fn GetUsedSlotCount (&self) -> usize
	{
		(self.RegionAddress () + Self::USED_SLOT_COUNT_OFFSET).Read::<usize> ()
	}

	pub fn SetUsedSlotCount (&mut self, usedSlotCount: usize)
	{
		(self.RegionAddress () + Self::USED_SLOT_COUNT_OFFSET).Write::<usize> (usedSlotCount)
	}

	fn IncreaseUsedSlotCount (&mut self)
	{
		self.SetUsedSlotCount (self.GetUsedSlotCount () + 1);
	}

	fn DecreaseUsedSlotCount (&mut self)
	{
		self.SetUsedSlotCount (self.GetUsedSlotCount () - 1);
	}

	fn UsedSlotBitmapAddress (&self) -> PhysicalAddress
	{
		self.RegionAddress () + Self::USED_SLOT_BITMAP_OFFSET
	}

	fn UsedSlotBitmapSize (&self) -> usize
	{
		(self.TotalSlotCount () + 8 - 1)/8
	}

	fn GetSlotArrayAddress (&self) -> PhysicalAddress
	{
		(self.UsedSlotBitmapAddress () + self.UsedSlotBitmapSize ()).Align (self.SlotSize ())
	}

	fn GetSlotIndex (&self, slotAddress: PhysicalAddress) -> usize
	{
		assert! (self.IsValidSlotAddress (slotAddress));
		(slotAddress - self.GetSlotArrayAddress ())/self.SlotSize ()
	}

	fn GetSlotAddress (&self, slotNumber: usize) -> PhysicalAddress
	{
		self.GetSlotArrayAddress () + self.SlotSize ()*slotNumber
	}

	fn IsValidSlotAddress (&self, slotAddress: PhysicalAddress) -> bool
	{
		// check slotAddress is inside expected range
		if slotAddress < self.GetSlotArrayAddress () { return false; }
		if slotAddress >= self.RegionAddress () + self.RegionSize () { return false; }

		// check if slotAddress is aligned
		if !slotAddress.IsAlign (self.SlotSize ()) { return false; }

		true
	}

	/// whether this frame have any used slot
	pub fn IsEmpty (&self) -> bool
	{
		let usedSlotBitmapAddress = self.UsedSlotBitmapAddress ();

		for index in 0..self.UsedSlotBitmapSize ()
		{
			// there is an used slot
			if (usedSlotBitmapAddress + index).Read::<u8> () != 0
			{
				return false;
			}
		}

		true
	}


	pub fn AllocSlot (&mut self) -> Option<PhysicalAddress>
	{
		let usedSlotBitmapAddress = self.UsedSlotBitmapAddress ();

		// find free slot in used slot bitmap
		// choose a random starting point to spread out the allocation
		// in hope that this can grab a free slot quicker than normal
		let slotArrayStartIndex = CPU::GetRandomNumberU128 () as usize % self.TotalSlotCount ();

		for slotIndex in slotArrayStartIndex..self.TotalSlotCount ()
		{
			let byteOffset = slotIndex/8;
			let bitOffset = slotIndex&0b111;

			let mut byteBitmap = (usedSlotBitmapAddress + byteOffset).Read::<u8> ();

			// return if the slot is unused
			if (byteBitmap & (1<<bitOffset)) == 0
			{
				self.IncreaseUsedSlotCount ();

				// set the used bit
				byteBitmap |= 1<<bitOffset;
				(usedSlotBitmapAddress + byteOffset).Write::<u8> (byteBitmap);

				return Some(self.GetSlotAddress (slotIndex));
			}
		}

		for slotIndex in 0..slotArrayStartIndex
		{
			let byteOffset = slotIndex/8;
			let bitOffset = slotIndex&0b111;

			let mut byteBitmap = (usedSlotBitmapAddress + byteOffset).Read::<u8> ();

			// return if the slot is unused
			if (byteBitmap & (1<<bitOffset)) == 0
			{
				self.IncreaseUsedSlotCount ();

				// set the used bit
				byteBitmap |= 1<<bitOffset;
				(usedSlotBitmapAddress + byteOffset).Write::<u8> (byteBitmap);

				return Some(self.GetSlotAddress (slotIndex));
			}
		}

		None
	}

	pub fn DeallocSlot (&mut self, slotAddress: PhysicalAddress)
	{
		assert! (self.IsValidSlotAddress (slotAddress));

		let slotIndex = self.GetSlotIndex (slotAddress);
		let usedSlotBitmapAddress = self.UsedSlotBitmapAddress ();

		let byteOffset = slotIndex/8;
		let bitOffset = slotIndex&0b111;

		let mut byteBitmap = (usedSlotBitmapAddress + byteOffset).Read::<u8> ();

		// ensure the used bit is set
		assert! ((byteBitmap & (1<<bitOffset)) != 0);

		// unset used bit
		byteBitmap &= !(1<<bitOffset);
		(usedSlotBitmapAddress + byteOffset).Write::<u8> (byteBitmap);

		self.DecreaseUsedSlotCount ();
	}
}

impl InPhysicalSpace for RegionHeader {}


#[derive(Debug)]
/// A bucket physical storage struct
/// Allow to alloc and dealloc slots from physical address space
/// No concurency support
pub struct PhysicalSlotStorage
{
	regionSize: usize,
	slotSize: usize,
	firstRegionHeader: Option<RegionHeader>,
	lastRegionHeader: Option<RegionHeader>,
}

impl PhysicalSlotStorage
{
	/// Create a new physical slot storage
	pub const fn New (slotSize: usize) -> Self
	{
		Self
		{
			regionSize: FRAME_L1_SIZE,
			slotSize,
			firstRegionHeader: None,
			lastRegionHeader: None,
		}
	}

	/// Alloc a slot
	/// Alloc new region if there isn't empty slot
	/// Return the physical address of the new slot if success
	pub fn AllocSlot (&mut self) -> Option<PhysicalAddress>
	{
		// iter through all regions
		let mut nextRegionHeader = self.firstRegionHeader;

		while let Some(mut currentRegionHeader) = nextRegionHeader
		{
			if let Some(slotAddress) = currentRegionHeader.AllocSlot ()
			{
				return Some(slotAddress);
			}

			nextRegionHeader = currentRegionHeader.GetNextRegionHeader ();
		}

		// no empty slot, alloc new region
		let mut currentRegionHeader = match self.AllocRegion ()
		{
			Some(regionHeader) => regionHeader,
			None => return None,
		};

		if let Some(slotAddress) = currentRegionHeader.AllocSlot ()
		{
			return Some(slotAddress);
		}

		None
	}

	/// Dealloc a slot
	/// Dealloc region if the region become empty
	pub fn DeallocSlot (&mut self, slotAddress: PhysicalAddress)
	{
		// get the region
		let regionAddress = slotAddress.AlignDown (self.regionSize);
		let mut regionHeader = RegionHeader::New (regionAddress);

		// dealloc the slot
		regionHeader.DeallocSlot (slotAddress);

		// dealloc the region if it is empty
		if regionHeader.IsEmpty ()
		{
			self.DeallocRegion (regionAddress);
		}
	}


	/// Alloc a new region
	fn AllocRegion (&mut self) -> Option<RegionHeader>
	{
		// get a new frame
		let newRegionAddress = match FrameAllocator::GetNormalFrame ()
		{
			Some(regionAddress) => regionAddress,
			None => return None,
		};

		// make the region header
		let mut newRegionHeader = RegionHeader::NewAt (newRegionAddress, self.regionSize, self.slotSize);

		// adjust first region pointer
		if self.firstRegionHeader.is_none ()
		{
			self.firstRegionHeader = Some(newRegionHeader);
		}

		// insert new region to the list
		if let Some(mut lastRegionHeader) = self.lastRegionHeader
		{
			lastRegionHeader.SetNextRegionHeader (Some(newRegionHeader));
			newRegionHeader.SetPrevRegionHeader (Some(lastRegionHeader));
		}

		// adjust last region pointer
		self.lastRegionHeader = Some(newRegionHeader);

		Some(newRegionHeader)
	}

	/// Dealloc a region
	fn DeallocRegion (&mut self, regionAddress: PhysicalAddress)
	{
		// get the region header
		let regionHeader = RegionHeader::New (regionAddress);

		// make sure the region is empty
		assert! (regionHeader.IsEmpty ());

		// unlink the region
		let prevRegionHeader = regionHeader.GetPrevRegionHeader ();
		let nextRegionHeader = regionHeader.GetNextRegionHeader ();

		if let Some(mut prevRegionHeader) = prevRegionHeader
		{
			prevRegionHeader.SetNextRegionHeader (regionHeader.GetNextRegionHeader ());
		}

		if let Some(mut nextRegionHeader) = nextRegionHeader
		{
			nextRegionHeader.SetPrevRegionHeader (regionHeader.GetPrevRegionHeader ());
		}

		// adjust first and last region pointers
		if self.firstRegionHeader.unwrap ().RegionAddress () == regionHeader.RegionAddress ()
		{
			self.firstRegionHeader = regionHeader.GetNextRegionHeader ();
		}

		if self.lastRegionHeader.unwrap ().RegionAddress () == regionHeader.RegionAddress ()
		{
			self.lastRegionHeader = regionHeader.GetPrevRegionHeader ();
		}

		// free the frame
		FrameAllocator::FreeFrame (regionAddress);
	}
}


impl Drop for PhysicalSlotStorage
{
	fn drop (&mut self)
	{
		// iter through all regions and free them
		let mut nextRegionHeader = self.firstRegionHeader;

		while let Some(currentRegionHeader) = nextRegionHeader
		{
			// get next region
			nextRegionHeader = currentRegionHeader.GetNextRegionHeader ();

			// get current region address
			let regionAddress = currentRegionHeader.RegionAddress ();

			// free the frame
			FrameAllocator::FreeFrame (regionAddress);
		}
	}
}


pub struct ItemHeader<T>
{
	_marker: PhantomData<T>,
	itemAddress: PhysicalAddress,
}

impl<T> ItemHeader<T>
{
	// prevItemAddress: Option<PhysicalAddress>,
	// nextItemAddress: Option<PhysicalAddress>,
	// data: T,

	const PREV_ITEM_ADDRESS_OFFSET: usize = 0;
	const NEXT_ITEM_ADDRESS_OFFSET: usize = Self::PREV_ITEM_ADDRESS_OFFSET + mem::size_of::<Option<PhysicalAddress>> ();


	pub const fn ItemHeaderSize () -> usize
	{
		// prev + next
		mem::size_of::<Option<PhysicalAddress>> () + mem::size_of::<Option<PhysicalAddress>> ()
	}

	pub const fn ItemSize () -> usize
	{
		PhysicalAddress::New (Self::ItemHeaderSize ()).Align (mem::size_of::<T> ()).ToUsize () + mem::size_of::<T> ()
	}

	pub const fn New (itemAddress: PhysicalAddress) -> Self
	{
		Self
		{
			_marker: PhantomData,
			itemAddress,
		}
	}

	pub fn NewAt (itemAddress: PhysicalAddress, data: T) -> Self
	{
		// init
		(itemAddress + Self::PREV_ITEM_ADDRESS_OFFSET).Write::<Option<PhysicalAddress>> (None);
		(itemAddress + Self::NEXT_ITEM_ADDRESS_OFFSET).Write::<Option<PhysicalAddress>> (None);
		let dataAddress = (itemAddress + Self::NEXT_ITEM_ADDRESS_OFFSET + mem::size_of::<Option<PhysicalAddress>> ()).Align (mem::size_of::<T> ());

		dataAddress.Write::<T> (data);

		Self
		{
			_marker: PhantomData,
			itemAddress,
		}
	}

	pub const fn ItemAddress (&self) -> PhysicalAddress { self.itemAddress }

	fn GetPrevItemHeaderAddress (&self) -> Option<PhysicalAddress>
	{
		(self.ItemAddress () + Self::PREV_ITEM_ADDRESS_OFFSET).Read::<Option<PhysicalAddress>> ()
	}

	pub fn GetPrevItemHeader (&self) -> Option<ItemHeader<T>>
	{
		self.GetPrevItemHeaderAddress ().map (|address| ItemHeader::New (address))
	}

	pub fn SetPrevItemHeader (&mut self, prevItemHeader: Option<ItemHeader<T>>)
	{
		let prevItemAddress = prevItemHeader.map (|item| item.ItemAddress ());
		(self.ItemAddress () + Self::PREV_ITEM_ADDRESS_OFFSET).Write::<Option<PhysicalAddress>> (prevItemAddress);
	}

	fn GetNextItemHeaderAddress (&self) -> Option<PhysicalAddress>
	{
		(self.ItemAddress () + Self::NEXT_ITEM_ADDRESS_OFFSET).Read::<Option<PhysicalAddress>> ()
	}

	pub fn GetNextItemHeader (&self) -> Option<ItemHeader<T>>
	{
		self.GetNextItemHeaderAddress ().map (|address| ItemHeader::New (address))
	}

	pub fn SetNextItemHeader (&mut self, nextItemHeader: Option<ItemHeader<T>>)
	{
		let nextItemAddress = nextItemHeader.map (|item| item.ItemAddress ());
		(self.ItemAddress () + Self::NEXT_ITEM_ADDRESS_OFFSET).Write::<Option<PhysicalAddress>> (nextItemAddress);
	}

	fn DataAddress (&self) -> PhysicalAddress
	{
		(self.ItemAddress () + Self::NEXT_ITEM_ADDRESS_OFFSET + mem::size_of::<Option<PhysicalAddress>> ()).Align (mem::size_of::<T> ())
	}

	pub fn GetData (&self) -> T
	{
		self.DataAddress ().Read::<T> ()
	}

	pub fn SetData (&mut self, data: T)
	{
		self.DataAddress ().Write::<T> (data)
	}
}

// need to do this manually because of PhantomData
impl<T> Clone for ItemHeader<T>
{
	fn clone (&self) -> Self
	{
		Self
		{
			_marker: PhantomData,
			itemAddress: self.itemAddress,
		}
	}
}

impl<T> Copy for ItemHeader<T> {}

impl<T> fmt::Display for ItemHeader<T>
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "Addr: {}, Prev {:?}, Next: {:?}", self.ItemAddress (), self.GetPrevItemHeaderAddress (), self.GetNextItemHeaderAddress ())
	}
}

impl<T> fmt::Debug for ItemHeader<T>
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "{}", self)
	}
}


impl<T> InPhysicalSpace for ItemHeader<T> {}


#[derive(Debug)]
pub struct PhysicalLinkedList<T>
{
	_marker: PhantomData<T>,
	storage: PhysicalSlotStorage,
	itemCount: usize,
	firstItemHeader: Option<ItemHeader<T>>,
	lastItemHeader: Option<ItemHeader<T>>,
}

impl<T> PhysicalLinkedList<T>
{
	pub const fn New () -> Self
	{
		Self
		{
			_marker: PhantomData,
			storage: PhysicalSlotStorage::New (ItemHeader::<T>::ItemSize ()),
			itemCount: 0,
			firstItemHeader: None,
			lastItemHeader: None,
		}
	}

	pub const fn Len (&self) -> usize { self.itemCount }
	pub const fn ItemCount (&self) -> usize { self.itemCount }

	pub fn PushFront (&mut self, item: T)
	{
		self.InsertItem (0, item);
	}

	pub fn PopFront (&mut self) -> T
	{
		self.RemoveItem (0)
	}

	pub fn PushBack (&mut self, item: T)
	{
		self.InsertItem (self.Len (), item);
	}

	pub fn PopBack (&mut self) -> T
	{
		self.RemoveItem (self.Len () - 1)
	}

	// Index must be between 0 and list length
	pub fn InsertItem (&mut self, index: usize, data: T)
	{
		// find the correct slot to insert
		let mut currentIndex = 0;
		let mut prevItemHeader: Option<ItemHeader<T>> = None;
		let mut nextItemHeader: Option<ItemHeader<T>> = self.firstItemHeader;

		while let Some(currentItemHeader) = nextItemHeader
		{
			if currentIndex == index
			{
				break;
			}

			prevItemHeader = nextItemHeader;
			nextItemHeader = currentItemHeader.GetNextItemHeader ();

			currentIndex += 1;
		}


		// get a new slot
		let itemAddress = self.storage.AllocSlot ().unwrap ();

		// create the Item
		let mut itemHeader = ItemHeader::NewAt (itemAddress, data);
		itemHeader.SetPrevItemHeader (prevItemHeader);
		itemHeader.SetNextItemHeader (nextItemHeader);

		// insert the Item
		if let Some(mut prevItemHeader) = prevItemHeader
		{
			prevItemHeader.SetNextItemHeader (Some(itemHeader));
		}
		else
		{
			self.firstItemHeader = Some(itemHeader);
		}

		if let Some(mut nextItemHeader) = nextItemHeader
		{
			nextItemHeader.SetPrevItemHeader (Some(itemHeader));
		}
		else
		{
			self.lastItemHeader = Some(itemHeader);
		}

		// increase item count
		self.itemCount += 1;
	}

	pub fn RemoveItem (&mut self, index: usize) -> T
	{
		// find that Item address
		let itemAddress = self.GetItemAddress (index);
		let itemHeader = ItemHeader::New (itemAddress);

		// unlink that Item
		let prevItemHeader = itemHeader.GetPrevItemHeader ();
		let nextItemHeader = itemHeader.GetNextItemHeader ();

		if let Some(mut prevItemHeader) = prevItemHeader
		{
			prevItemHeader.SetNextItemHeader (itemHeader.GetNextItemHeader ());
		}

		if let Some(mut nextItemHeader) = nextItemHeader
		{
			nextItemHeader.SetPrevItemHeader (itemHeader.GetPrevItemHeader ());
		}

		// adjust first and last Item pointers
		if self.firstItemHeader.unwrap ().ItemAddress () == itemHeader.ItemAddress ()
		{
			self.firstItemHeader = itemHeader.GetNextItemHeader ();
		}

		if self.lastItemHeader.unwrap ().ItemAddress () == itemHeader.ItemAddress ()
		{
			self.lastItemHeader = itemHeader.GetPrevItemHeader ();
		}

		// copy the data out
		let data = itemHeader.GetData ();

		// free that slot
		self.storage.DeallocSlot (itemAddress);

		// decrease item count
		self.itemCount -= 1;

		// return the data
		data
	}

	// Item index must be valid
	pub fn GetItemAddress (&self, itemIndex: usize) -> PhysicalAddress
	{
		// for loop, iter through the item list
		let mut currentIndex = 0;
		let mut nextItemHeader = self.firstItemHeader;

		while let Some(currentItemHeader) = nextItemHeader
		{
			if currentIndex == itemIndex
			{
				break;
			}

			nextItemHeader = currentItemHeader.GetNextItemHeader ();
			currentIndex += 1;
		}

		nextItemHeader.unwrap ().ItemAddress ()
	}

	// Item index must be valid
	pub fn GetItem (&self, itemIndex: usize) -> T
	{
		assert! (itemIndex < self.ItemCount ());

		let itemAddress = self.GetItemAddress (itemIndex);
		let itemHeader = ItemHeader::New (itemAddress);

		itemHeader.GetData ()
	}

	// Item index must be valid
	pub fn SetItem (&mut self, itemIndex: usize, data: T)
	{
		assert! (itemIndex < self.ItemCount ());

		let itemAddress = self.GetItemAddress (itemIndex);
		let mut itemHeader = ItemHeader::New (itemAddress);

		itemHeader.SetData (data)
	}

	pub const fn FirstItemHeader (&self) -> Option<ItemHeader<T>> { self.firstItemHeader }
	pub const fn LastItemHeader (&self) -> Option<ItemHeader<T>> { self.lastItemHeader }

	// Item index must be valid
	pub fn GetItemHeader (&self, itemHeaderIndex: usize) -> ItemHeader<T>
	{
		assert! (itemHeaderIndex < self.ItemCount ());

		let mut currentItemHeaderIndex = 0;
		let mut nextItemHeader = self.firstItemHeader;

		while let Some(currentItemHeader) = nextItemHeader
		{
			if currentItemHeaderIndex == itemHeaderIndex
			{
				return currentItemHeader;
			}

			nextItemHeader = currentItemHeader.GetNextItemHeader ();
			currentItemHeaderIndex += 1;
		}

		unreachable! ("No item header found at index({})", itemHeaderIndex);
	}

	fn InsertItemHeaderBefore (&mut self, mut itemHeader: ItemHeader<T>, nextItemHeader: Option<ItemHeader<T>>)
	{
		if let Some(mut nextItemHeader) = nextItemHeader
		{
			// insert item header before next
			let prevItemHeader = nextItemHeader.GetPrevItemHeader ();

			itemHeader.SetPrevItemHeader (prevItemHeader);
			itemHeader.SetNextItemHeader (Some(nextItemHeader));

			if let Some(mut prevItemHeader) = prevItemHeader
			{
				prevItemHeader.SetNextItemHeader (Some(itemHeader));
			}

			nextItemHeader.SetPrevItemHeader (Some(itemHeader));

			// adjust first item header
			if self.FirstItemHeader ().unwrap ().ItemAddress () == nextItemHeader.ItemAddress ()
			{
				self.firstItemHeader = Some(itemHeader);
			}
		}
		else
		{
			// insert item header at the end
			itemHeader.SetPrevItemHeader (self.LastItemHeader ());
			itemHeader.SetNextItemHeader (None);

			if let Some(mut lastItemHeader) = self.LastItemHeader ()
			{
				lastItemHeader.SetNextItemHeader (Some(itemHeader));
			}

			// update last header
			self.lastItemHeader = Some(itemHeader);

			// update first header
			if matches! (self.firstItemHeader, None)
			{
				self.firstItemHeader = Some(itemHeader);
			}
		}

		// adjust item count
		self.itemCount += 1;
	}

	fn InsertItemHeaderAfter (&mut self, mut itemHeader: ItemHeader<T>, prevItemHeader: Option<ItemHeader<T>>)
	{
		if let Some(mut prevItemHeader) = prevItemHeader
		{
			// insert item header after prev
			let nextItemHeader = prevItemHeader.GetNextItemHeader ();

			itemHeader.SetPrevItemHeader (Some(prevItemHeader));
			itemHeader.SetNextItemHeader (nextItemHeader);

			if let Some(mut nextItemHeader) = nextItemHeader
			{
				nextItemHeader.SetPrevItemHeader (Some(itemHeader));
			}

			prevItemHeader.SetNextItemHeader (Some(itemHeader));

			// adjust last item header
			if self.LastItemHeader ().unwrap ().ItemAddress () == prevItemHeader.ItemAddress ()
			{
				self.lastItemHeader = Some(itemHeader);
			}
		}
		else
		{
			// insert item header at the start
			itemHeader.SetPrevItemHeader (None);
			itemHeader.SetNextItemHeader (self.FirstItemHeader ());

			if let Some(mut firstItemHeader) = self.firstItemHeader
			{
				firstItemHeader.SetPrevItemHeader (Some(itemHeader));
			}

			// update first header
			self.firstItemHeader = Some(itemHeader);

			// update last header
			if matches! (self.lastItemHeader, None)
			{
				self.lastItemHeader = Some(itemHeader);
			}
		}

		// adjust item count
		self.itemCount += 1;
	}

	pub fn InsertItemBeforeItemHeader (&mut self, item: T, nextItemHeader: Option<ItemHeader<T>>) -> ItemHeader<T>
	{
		// get a new slot
		let itemAddress = self.storage.AllocSlot ().unwrap ();

		// create item header
		let itemHeader = ItemHeader::NewAt (itemAddress, item);

		// insert item header
		self.InsertItemHeaderBefore (itemHeader, nextItemHeader);

		itemHeader
	}

	pub fn InsertItemAfterItemHeader (&mut self, item: T, prevItemHeader: Option<ItemHeader<T>>) -> ItemHeader<T>
	{
		// get a new slot
		let itemAddress = self.storage.AllocSlot ().unwrap ();

		// create item header
		let itemHeader = ItemHeader::NewAt (itemAddress, item);

		// insert item header
		self.InsertItemHeaderAfter (itemHeader, prevItemHeader);

		itemHeader
	}

	// Old item header must belong to this list
	pub fn RemoveItemHeader (&mut self, oldItemHeader: ItemHeader<T>)
	{
		assert! (!self.FirstItemHeader ().is_none ());
		assert! (!self.LastItemHeader ().is_none ());
		assert! (self.ItemCount () > 0);

		let prevItemHeader = oldItemHeader.GetPrevItemHeader ();
		let nextItemHeader = oldItemHeader.GetNextItemHeader ();

		// unlink
		if let Some(mut prevItemHeader) = prevItemHeader
		{
			prevItemHeader.SetNextItemHeader (nextItemHeader);
		}

		if let Some(mut nextItemHeader) = nextItemHeader
		{
			nextItemHeader.SetPrevItemHeader (prevItemHeader);
		}

		// adjust first item header
		if self.firstItemHeader.unwrap ().ItemAddress () == oldItemHeader.ItemAddress ()
		{
			self.firstItemHeader = nextItemHeader;
		}

		// adjust first item header
		if self.lastItemHeader.unwrap ().ItemAddress () == oldItemHeader.ItemAddress ()
		{
			self.lastItemHeader = prevItemHeader;
		}

		// adjust item count
		self.itemCount -= 1;
	}

	pub fn Iter (&self) -> PhysicalLinkedListIter<T>
	{
		PhysicalLinkedListIter::New (self)
	}

	pub fn IterMut (&mut self) -> PhysicalLinkedListIter<T>
	{
		PhysicalLinkedListIter::New (self)
	}

	pub fn IntoIter (self) -> PhysicalLinkedListIter<T>
	{
		PhysicalLinkedListIter::New (&self)
	}

	pub fn PrintList (&self)
	{
		println! ("Len: {}", self.Len ());
		println! ("First header: {:?}", self.firstItemHeader);
		println! ("Last header: {:?}", self.lastItemHeader);

		let mut currentIndex = 0;

		while currentIndex < self.Len ()
		{
			let itemAddress = self.GetItemAddress (currentIndex);
			let itemHeader = ItemHeader::<T>::New (itemAddress);
			println! ("Index {}: {}", currentIndex, itemHeader);

			currentIndex += 1;
		}
	}
}


impl<T: Clone> Clone for PhysicalLinkedList<T>
{
	fn clone (&self) -> Self
	{
		let mut newPhysicalLinkedList = Self::New ();

		for item in self
		{
			newPhysicalLinkedList.PushBack (item.clone ());
		}

		newPhysicalLinkedList
	}
}

impl<T> IntoIterator for &PhysicalLinkedList<T>
{
	type Item = T;
	type IntoIter = PhysicalLinkedListIter<T>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}

impl<T> IntoIterator for &mut PhysicalLinkedList<T>
{
	type Item = T;
	type IntoIter = PhysicalLinkedListIter<T>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}

impl<T> IntoIterator for PhysicalLinkedList<T>
{
	type Item = T;
	type IntoIter = PhysicalLinkedListIter<T>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (&self) }
}


#[derive(Debug)]
pub struct PhysicalLinkedListIter<T>
{
	currentItemHeader: Option<ItemHeader<T>>,
}

impl<T> PhysicalLinkedListIter<T>
{
	pub fn New (physicalLinkedList: &PhysicalLinkedList<T>) -> Self
	{
		Self { currentItemHeader: physicalLinkedList.firstItemHeader }
	}
}


impl<T> Iterator for PhysicalLinkedListIter<T>
{
	type Item = T;

	fn next (&mut self) -> Option<Self::Item>
	{
		match self.currentItemHeader
		{
			Some(currentHeader) =>
			{
				self.currentItemHeader = currentHeader.GetNextItemHeader ();
				Some(currentHeader.GetData ())
			},
			None => None,
		}
	}
}
