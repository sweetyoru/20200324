use core::fmt;

#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::Memory::Virtual::*;


/// Platform-independent paging flags
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct PageFlags(usize);

impl PageFlags
{
	const PRESENT:			usize = 1<<0;
	const WRITABLE:			usize = 1<<1;
	const USER_ACCESSIBLE:	usize = 1<<2;
	const CACHE_DISABLE:	usize = 1<<3;
	const GLOBAL:			usize = 1<<4;
	const EXECUTABLE:		usize = 1<<5;

	/// Create new paging flags with those attributes:
	/// 	+ Not present
	/// 	+ Not writable
	/// 	+ User not accessible
	///		+ Cache enabled
	/// 	+ Not global
	/// 	+ Not executable
	pub const fn New () -> Self { Self(0) }

	pub const fn IsPresent (&self) -> bool { (self.0 & PageFlags::PRESENT) != 0 }
	pub fn SetPresent (&mut self) { self.0 |= PageFlags::PRESENT; }
	pub fn SetNotPresent (&mut self) { self.0 &= !PageFlags::PRESENT; }

	pub const fn IsWritable (&self) -> bool { (self.0 & PageFlags::WRITABLE) != 0 }
	pub fn SetWritable (&mut self) { self.0 |= PageFlags::WRITABLE; }
	pub fn SetNotWritable (&mut self) { self.0 &= !PageFlags::WRITABLE; }

	pub const fn IsUserAccessible (&self) -> bool { (self.0 & PageFlags::USER_ACCESSIBLE) != 0 }
	pub fn SetUserAccessible (&mut self) { self.0 |= PageFlags::USER_ACCESSIBLE; }
	pub fn SetUserNotAccessible (&mut self) { self.0 &= !PageFlags::USER_ACCESSIBLE; }

	pub const fn IsCacheEnabled (&self) -> bool { (self.0 & PageFlags::CACHE_DISABLE) == 0 }
	pub fn SetCacheEnabled (&mut self) { self.0 &= !PageFlags::CACHE_DISABLE; }
	pub fn SetCacheDisabled (&mut self) { self.0 |= PageFlags::CACHE_DISABLE; }

	pub const fn IsGlobal (&self) -> bool { (self.0 & PageFlags::GLOBAL) != 0 }
	pub fn SetGlobal (&mut self) { self.0 |= PageFlags::GLOBAL; }
	pub fn SetNotGlobal (&mut self) { self.0 &= !PageFlags::GLOBAL; }

	pub const fn IsExecutable (&self) -> bool { (self.0 & PageFlags::EXECUTABLE) != 0 }
	pub fn SetExecutable (&mut self) { self.0 |= PageFlags::EXECUTABLE; }
	pub fn SetNotExecutable (&mut self) { self.0 &= !PageFlags::EXECUTABLE; }
}


impl fmt::Display for PageFlags
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		if self.IsExecutable () { write! (f, "E")?; }
		else { write! (f, "_")?; }

		if self.IsGlobal () { write! (f, "G")?; }
		else { write! (f, "L")?; }

		if self.IsCacheEnabled () { write! (f, "C")?; }
		else { write! (f, "_")?; }

		if self.IsUserAccessible () { write! (f, "U")?; }
		else { write! (f, "K")?; }

		if self.IsWritable () { write! (f, "W")?; }
		else { write! (f, "R")?; }

		if self.IsPresent () { write! (f, "P")?; }
		else { write! (f, "_")?; }

		Ok(())
	}
}

