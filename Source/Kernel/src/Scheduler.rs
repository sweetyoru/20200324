use core::mem;
use core::cell::SyncUnsafeCell;

use alloc::sync::{Arc, Weak};
use alloc::collections::vec_deque::VecDeque;

use crate::SMP;
use crate::println;
use crate::Process;
use crate::Timer::SchedulerTimer;
use crate::Lock::{OneTimeFlag, Mutex};
use crate::Thread::{Thread, ThreadState};


#[derive(Debug)]
struct GlobalScheduler
{
	isEnabled: bool,
	threads: VecDeque<Weak<Thread>>,
}

impl GlobalScheduler
{
	pub fn New () -> Self
	{
		Self
		{
			isEnabled: false,
			threads: VecDeque::new (),
		}
	}

	pub fn Enable (&mut self)
	{
		self.isEnabled = true;
	}

	pub fn Disable (&mut self)
	{
		self.isEnabled = false;
	}

	pub fn AddThread (&mut self, thread: Arc<Thread>)
	{
		self.threads.push_back (Arc::downgrade (&thread));
	}

	/// Return next thread to switch to
	/// Return current thread if disabled
	pub fn Schedule (&mut self) -> Arc<Thread>
	{
		if !self.isEnabled
		{
			return SMP::GetCurrentCPU ().GetCurrentThread ();
		}

		// get current running thread
		let currentThread = SMP::GetCurrentCPU ().GetCurrentThread ();

		// change current thread state to ready
		currentThread.SetState (ThreadState::Ready);

		// check if scheduler should drop this thread
		// no need to lock since in the worst case scheduler will just schedule killed thread one more time
		// get current process/thread never fail since there is still Arc of current process/thread in CPU storage
		if !currentThread.GetOwnerProcess ().IsKilled () && !currentThread.IsKilled ()
		{
			// push current thread to ready queue
			self.threads.push_back (Arc::downgrade (&currentThread));
		}

		// get next thread
		// there must be always some threads in there
		loop
		{
			// pop a thread at front of ready queue
			// there must always be some threads there
			let candidateThread = self.threads.pop_front ().unwrap ();

			// get the thread
			// no need to check if thread is killed since in the worst case scheduler will just schedule killed thread one more time
			if let Some (x) = candidateThread.upgrade ()
			{
				break x;
			}

			// get thread failed, the thread is freed somewhere else
		}
	}
}


static globalScheduler: SyncUnsafeCell<Option<Mutex<GlobalScheduler>>> = SyncUnsafeCell::new (None);


pub fn InitGlobalScheduler ()
{
	println! ("[Boot CPU][+] Init Global Scheduler...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }


	let mut globalSchedulerRef =  unsafe
	{
		*globalScheduler.get () = Some(Mutex::New (GlobalScheduler::New ()));
		(*globalScheduler.get ()).as_ref ().unwrap ()
	}.Lock ();


	// get root process
	let rootProcess = Process::GetProcess (Process::GetRootProcessID ()).unwrap ();

	// add all threads of root process to ready queue
	let threadList = rootProcess.GetThreadList ();

	for thread in threadList.into_iter ()
	{
		globalSchedulerRef.AddThread (thread);
	}
}


/// Callback for scheduler timer interrupt
pub fn GlobalScheduleTimerInterruptCallback ()
{
	SchedulerTimer::SetNextInterrupt (10_000_000);

	let mut globalSchedulerRef = unsafe { (*globalScheduler.get ()).as_ref ().unwrap () }.Lock ();

	let nextThread = globalSchedulerRef.Schedule ();

	// drop the scheduler lock
	mem::drop (globalSchedulerRef);

	SwitchToThread (nextThread);
}


/// Switch to next thread
pub fn SwitchToThread (nextThread: Arc<Thread>)
{
	let currentThread = SMP::GetCurrentCPU ().GetCurrentThread ();

	nextThread.SetState (ThreadState::Running);

	// load next thread
	SMP::GetCurrentCPU ().SetCurrentThread (Arc::clone (&nextThread));
	nextThread.Load (currentThread);
}


/// Enable global scheduler
pub fn EnableGlobalScheduler ()
{
	let mut globalSchedulerRef = unsafe { (*globalScheduler.get ()).as_ref ().unwrap () }.Lock ();

	globalSchedulerRef.Enable ();

	SchedulerTimer::EnableSchedulerTimer ();
}


/// Disable global scheduler
pub fn DisableGlobalScheduler ()
{
	let mut globalSchedulerRef = unsafe { (*globalScheduler.get ()).as_ref ().unwrap () }.Lock ();

	globalSchedulerRef.Disable ();
}


/// Add a thread to scheduler
pub fn AddThreadToGlobalScheduler (thread: Arc<Thread>)
{
	let mut globalSchedulerRef = unsafe { (*globalScheduler.get ()).as_ref ().unwrap () }.Lock ();

	globalSchedulerRef.AddThread (thread);
}
