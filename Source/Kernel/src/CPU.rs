#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::CPU::*;


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct CPUInterruptState(bool);

impl CPUInterruptState
{
	pub const fn New (isInterrupted: bool) -> Self { Self(isInterrupted) }
	pub fn IsInterrupted (&self) -> bool { self.0 }
}
