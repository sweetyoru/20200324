//! This module handler Inter Core Communication


use core::cell::SyncUnsafeCell;
use core::sync::atomic::{AtomicBool, Ordering};

use crate::SMP;
use crate::println;
use crate::Lock::{Mutex, OneTimeFlag};
use crate::DataStruct::{DefaultHashSet, DefaultHashMap};
use crate::Arch::Amd64::CPU::{self, CPUID};
use crate::Memory::Virtual::TLBShootdownData;
use crate::Interrupt::{self, InterruptID, InterruptAllocationStrategy};

#[cfg(architecture = "Amd64")]
pub use crate::Arch::Amd64::ICC::*;


const iccInterruptID: InterruptID = InterruptID::New (75);
const iccInterruptAllocationStrategy: InterruptAllocationStrategy = InterruptAllocationStrategy::Fixed(iccInterruptID);


/// This hold the core data for icc
#[derive(Debug, Clone)]
pub enum ICCData
{
	// send tlb shootdown notice, with the physical address
	TLBShootdown(TLBShootdownData),

	Test1(usize),
	Test2(usize),
}


/// This own the data, and info about sender
/// Target cpu will call targetFunction (iccData)
#[derive(Debug)]
struct ICCMessage
{
	senderID: CPUID,
	targetFunction: fn(ICCData),
	iccData: ICCData,
}

impl ICCMessage
{
	pub fn New (senderID: CPUID, targetFunction: fn(ICCData), iccData: ICCData) -> Self
	{
		Self
		{
			senderID,
			targetFunction,
			iccData,
		}
	}
}


/// A target to send to
#[derive(Debug)]
struct ReceiveTarget
{
	cpuID: CPUID,

	// whoever acquire this lock can send icc message to this cpu
	sendLock: Mutex<()>,

	iccMessage: Option<ICCMessage>,

	// ack, sender check this, receiver set this, then sender clear it
	iccMessageReceived: AtomicBool,
}

impl ReceiveTarget
{
	pub fn New (cpuID: CPUID) -> Self
	{
		Self
		{
			cpuID,
			sendLock: Mutex::New (()),
			iccMessage: None,
			iccMessageReceived: AtomicBool::new (false),
		}
	}
}


/// Created once by boot CPU at boot time
/// Never change after that
static receiveTargets: SyncUnsafeCell<DefaultHashMap<CPUID, ReceiveTarget>> = SyncUnsafeCell::new (DefaultHashMap::New ());


/// This function must be called only once at boot by boot CPU
pub fn InitICCGlobalPart ()
{
	println! ("[Boot CPU][+] Init ICC System global part...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	// get cpu list
	let cpus = SMP::GetCPUList ();

	// make a receive target for every cpu in the system
	for cpu in cpus
	{
		unsafe
		{
			(*receiveTargets.get ()).Insert (cpu.ID (), ReceiveTarget::New (cpu.ID ()));
		}
	}
}


/// This function must be called once at boot by each CPU
pub fn InitICCLocalPart ()
{
	println! ("[CPU {}][+] Init ICC System local part...", CPU::CPU::GetCurrentCPUID ());

	static calledCpu: SyncUnsafeCell<DefaultHashSet<CPUID>> = SyncUnsafeCell::new (DefaultHashSet::New ());
	unsafe { assert! ((*calledCpu.get ()).Insert (CPU::CPU::GetCurrentCPUID ()).is_ok ()); }

	InsertICCInterruptHandler (iccInterruptAllocationStrategy);
}


/// This funtion will send an ipi to target cpu, together with data
pub fn SendICCMessage (cpuID: CPUID, targetFunction: fn(ICCData), iccData: ICCData)
{
	// craft the message
	let iccMessage = ICCMessage::New (CPU::CPU::GetCurrentCPUID (), targetFunction, iccData);

	// grab the target
	let target = unsafe { (*receiveTargets.get ()).GetMutRef (&cpuID).unwrap () };

	// grab the send lock
	let _sendLock = target.sendLock.Lock ();

	// check if there is still data inside the target, or iccMessageReceived still not reset
	assert! (target.iccMessage.is_none () && target.iccMessageReceived.load (Ordering::SeqCst) == false);

	// put the message inside the target
	target.iccMessage = Some(iccMessage);

	// send ipi
	Interrupt::SendIPI (target.cpuID, iccInterruptID);

	// wait for ack to be set
	// also unset the flag if set
	while target.iccMessageReceived.compare_exchange (true, false, Ordering::SeqCst, Ordering::SeqCst).is_err ()
	{
		CPU::CPU::Pause ();
	}
}


/// This funtion will boardcast an ipi to all cpus, together with data
/// The data will be cloned
pub fn BoardcastICCMessage (targetFunction: fn(ICCData), iccData: &ICCData)
{
	if SMP::GetInitializedCPUCount () > 1
	{
		for cpu in SMP::GetCPUList ()
		{
			// send the message
			SendICCMessage (cpu.ID (), targetFunction, iccData.clone ());
		}
	}
}


/// This function will be called to handler ICC Message
/// Will grab the iccMessage out and set iccMessageReceived flag
pub fn ReceiveICCMessage ()
{
	let cpuID = CPU::CPU::GetCurrentCPUID ();

	// grab my target
	let myTarget = unsafe { (*receiveTargets.get ()).GetMutRef (&cpuID).unwrap () };

	// grab the data
	let iccMessage = myTarget.iccMessage.take ().unwrap ();

	// ack the message set iccMessageReceived flag
	myTarget.iccMessageReceived.store (true, Ordering::SeqCst);

	// call the passed function
	(iccMessage.targetFunction) (iccMessage.iccData);
}
