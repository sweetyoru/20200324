use core::mem;
use core::hash::Hash;

use alloc::vec::Vec;
use alloc::collections::{LinkedList, linked_list::{Iter, IterMut}};

use crate::Crypto::{Hasher, HasherBuilder, FNV1A64_Hasher};


#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum HashSetError
{
	ValueExist,
	ValueNotExist,
}


#[derive(Debug)]
struct HashSetNode<V: Eq>
{
	valueHash: u64,
	value: V,
}

impl<V: Eq> HashSetNode<V>
{
	pub fn New (valueHash: u64, value: V) -> Self
	{
		Self
		{
			valueHash,
			value,
		}
	}

	pub const fn ValueHash (&self) -> u64 { self.valueHash }

	pub fn GetValueRef (&self) -> &V { &self.value }
	pub fn GetValueMutRef (&mut self) -> &mut V { &mut self.value }
}


#[derive(Clone, Copy, Debug)]
pub struct HashSetConfiguration<H: Hasher + core::hash::Hasher>
{
	hashSalt: u64,
	loadFactor: f64,
	bucketSize: usize,
	firstInsertBucketCapacity: usize,
	minimumBucketCapacity: usize,
	maximumBucketCapacity: Option<usize>,
	bucketCapacityGrowStep: usize,
	hasherBuilder: HasherBuilder<H>,
}

impl<H: Hasher + core::hash::Hasher> HashSetConfiguration<H>
{
	const fn Default () -> Self
	{
		Self
		{
			hashSalt: 0,
			loadFactor: 0.75,
			bucketSize: 16,
			firstInsertBucketCapacity: 32,
			minimumBucketCapacity: 16,
			maximumBucketCapacity: None,
			bucketCapacityGrowStep: 32,
			hasherBuilder: HasherBuilder::<H>::New (),
		}
	}

	pub const fn HashSalt (&self) -> u64 { self.hashSalt }
	pub const fn LoadFactor (&self) -> f64 { self.loadFactor }
	pub const fn BucketSize (&self) -> usize { self.bucketSize }
	pub const fn MinimumBucketCapacity (&self) -> usize { self.minimumBucketCapacity }
	pub const fn MaximumBucketCapacity (&self) -> Option<usize> { self.maximumBucketCapacity }
	pub const fn BucketCapacityGrowStep (&self) -> usize { self.bucketCapacityGrowStep }
}


#[derive(Debug)]
pub struct HashSet<V: Eq + PartialEq + Hash, H: Hasher + core::hash::Hasher>
{
	hashSetConfiguration: HashSetConfiguration<H>,
	buckets: Vec<LinkedList<HashSetNode<V>>>,
	usedBucketCount: usize,
	nodeCount: usize,
}

impl<V: Eq + PartialEq + Hash, H: Hasher + core::hash::Hasher> HashSet<V, H>
{
	pub const fn New () -> Self
	{
		Self
		{
			hashSetConfiguration: HashSetConfiguration::<H>::Default (),
			buckets: Vec::new (),
			usedBucketCount: 0,
			nodeCount: 0,
		}
	}

	pub const fn NewWithConfiguration (hashSetConfiguration: HashSetConfiguration<H>) -> Self
	{
		Self
		{
			hashSetConfiguration,
			buckets: Vec::new (),
			usedBucketCount: 0,
			nodeCount: 0,
		}
	}

	pub const fn Size (&self) -> usize { self.nodeCount }

	const fn HashSalt (&self) -> u64 { self.hashSetConfiguration.HashSalt () }
	fn BucketCapacity (&self) -> usize { self.buckets.len () }
	const fn UsedBucketCount (&self) -> usize { self.usedBucketCount }

	fn NewHasher (&self) -> H { self.hashSetConfiguration.hasherBuilder.NewHasher () }

	fn ValueHash (&self, value: &V) -> u64
	{
		let mut valueHasher = self.NewHasher ();
		self.HashSalt ().hash (&mut valueHasher);
		value.hash (&mut valueHasher);
		valueHasher.finish ()
	}

	fn BucketIndex (&self, valueHash: u64) -> usize { valueHash as usize % self.BucketCapacity () }

	fn InsertNode (&mut self, bucketIndex: usize, newNode: HashSetNode<V>)
	{
		self.buckets[bucketIndex].push_front (newNode);
		self.nodeCount += 1;

		if self.buckets[bucketIndex].len () == 1
		{
			self.usedBucketCount += 1;
		}
	}

	fn RemoveNode (&mut self, bucketIndex: usize, nodeIndex: usize)
	{
		self.buckets[bucketIndex].remove (nodeIndex);
		self.nodeCount -= 1;

		if self.buckets[bucketIndex].len () == 0
		{
			self.usedBucketCount -= 1;
		}
	}

	fn ResizeBucket (&mut self, newBucketCapacity: usize)
	{
		// do nothing if new capacity is still the same
		if newBucketCapacity == self.BucketCapacity ()
		{
			return;
		}

		// create new bucket
		let newBuckets: Vec<LinkedList<HashSetNode<V>>> = Vec::with_capacity (newBucketCapacity);
		self.usedBucketCount = 0;

		// swap the bucket
		let oldBuckets = mem::replace (&mut self.buckets, newBuckets);

		// move all old elements to new bucket
		for oldBucket in oldBuckets
		{
			for oldNode in oldBucket
			{
				// insert old node into new bucket
				let newBucketIndex = oldNode.ValueHash () as usize % newBucketCapacity;
				self.InsertNode (newBucketIndex, oldNode);
			}
		}
	}

	fn EnsureBucketCapacity (&mut self, headroom: usize)
	{
		// check if first insert
		if self.BucketCapacity () == 0
		{
			self.ResizeBucket (self.hashSetConfiguration.firstInsertBucketCapacity);
		}

		// check if the load factor is satisfied
		if self.Size () >= (self.hashSetConfiguration.BucketSize () as f64 * self.BucketCapacity () as f64 * self.hashSetConfiguration.LoadFactor ()) as usize
		{
			// calc new bucket size
			let newBucketCapacity = core::cmp::max (self.BucketCapacity () + self.hashSetConfiguration.BucketCapacityGrowStep (), self.hashSetConfiguration.MaximumBucketCapacity ().unwrap_or (1));

			// resize bucket
			self.ResizeBucket (newBucketCapacity);
		}
	}

	pub fn Contains (&self, value: &V) -> bool
	{
		let valueHash = self.ValueHash (value);
		let bucketIndex = self.BucketIndex (valueHash);

		for node in &self.buckets[bucketIndex]
		{
			if node.ValueHash () == valueHash && node.GetValueRef () == value
			{
				return true;
			}
		}

		false
	}

	pub fn Insert (&mut self, value: V) -> Result<(), HashSetError>
	{
		// grow the bucket if necessary
		self.EnsureBucketCapacity (1);

		let valueHash = self.ValueHash (&value);
		let bucketIndex = self.BucketIndex (valueHash);

		// travel the list to compare all hash
		for node in &self.buckets[bucketIndex]
		{
			if node.ValueHash () == valueHash && *node.GetValueRef () == value
			{
				return Err(HashSetError::ValueExist);
			}
		}

		// insert new node
		let newNode = HashSetNode::New (valueHash, value);
		self.InsertNode (bucketIndex, newNode);

		return Ok(());
	}

	pub fn Remove (&mut self, value: &V) -> Result<(), HashSetError>
	{
		let valueHash = self.ValueHash (&value);
		let bucketIndex = self.BucketIndex (valueHash);

		// travel the list to compare all hash
		for (nodeIndex, node) in self.buckets[bucketIndex].iter ().enumerate ()
		{
			if node.ValueHash () == valueHash && node.GetValueRef () == value
			{
				// remove old node
				self.RemoveNode (bucketIndex, nodeIndex);

				// shirk the bucket if necessary
				self.EnsureBucketCapacity (0);

				return Ok(());
			}
		}

		return Err(HashSetError::ValueNotExist);
	}

	pub fn Iter (&self) -> HashSetIter<V, H> { HashSetIter::<V, H>::New (self) }
	pub fn IterMut (&mut self) -> HashSetIterMut<V, H> { HashSetIterMut::<V, H>::New (self) }
	pub fn IntoIter (self) -> HashSetIntoIter<V, H> { HashSetIntoIter::<V, H>::New (self) }
}


impl<'a, V: Eq + Hash, H: Hasher + core::hash::Hasher> IntoIterator for &'a HashSet<V, H>
{
	type Item = &'a V;
	type IntoIter = HashSetIter<'a, V, H>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}

impl<'a, V: Eq + Hash, H: Hasher + core::hash::Hasher> IntoIterator for &'a mut HashSet<V, H>
{
	type Item = &'a mut V;
	type IntoIter = HashSetIterMut<'a, V, H>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}

impl<V: Eq + Hash, H: Hasher + core::hash::Hasher> IntoIterator for HashSet<V, H>
{
	type Item = V;
	type IntoIter = HashSetIntoIter<V, H>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}


#[derive(Debug)]
pub struct HashSetIter<'a, V: Eq + Hash, H: Hasher + core::hash::Hasher>
{
	bucketIndex: usize,
	iter: Option<Iter<'a, HashSetNode<V>>>,
	hashSet: &'a HashSet<V, H>,
}

impl<'a, V: 'a + Eq + Hash, H: Hasher + core::hash::Hasher> HashSetIter<'a, V, H>
{
	pub fn New (hashSet: &'a HashSet<V, H>) -> Self
	{
		Self
		{
			bucketIndex: 0,
			iter: None,
			hashSet,
		}
	}
}


impl<'a, V: 'a + Eq + Hash, H: Hasher + core::hash::Hasher> Iterator for HashSetIter<'a, V, H>
{
	type Item = &'a V;

	fn next (&mut self) -> Option<Self::Item>
	{
		loop
		{
			if self.bucketIndex < self.hashSet.buckets.len ()
			{
				match &mut self.iter
				{
					Some(iter) =>
					{
						match iter.next ()
						{
							Some(node) => return Some(&node.value),
							None =>
							{
								// move to next bucket
								self.bucketIndex += 1;
								self.iter = None;
							},
						}
					}
					None => self.iter = Some(self.hashSet.buckets[self.bucketIndex].iter ()),
				}
			}
			else { return None; }
		}
	}
}


#[derive(Debug)]
pub struct HashSetIterMut<'a, V: Eq + Hash, H: Hasher + core::hash::Hasher>
{
	bucketIndex: usize,
	iter: Option<IterMut<'a, HashSetNode<V>>>,
	hashSet: &'a mut HashSet<V, H>,
}

impl<'a, V: 'a + Eq + Hash, H: Hasher + core::hash::Hasher> HashSetIterMut<'a, V, H>
{
	pub fn New (hashSet: &'a mut HashSet<V, H>) -> Self
	{
		Self
		{
			bucketIndex: 0,
			iter: None,
			hashSet,
		}
	}
}


impl<'a, V: 'a + Eq + Hash, H: Hasher + core::hash::Hasher> Iterator for HashSetIterMut<'a, V, H>
{
	type Item = &'a mut V;

	fn next (&mut self) -> Option<Self::Item>
	{
		let bucketCount = self.hashSet.buckets.len ();

		loop
		{
			if self.bucketIndex < bucketCount
			{
				match &mut self.iter
				{
					Some(iter) =>
					{
						match iter.next ()
						{
							Some(node) => return Some(&mut node.value),
							None =>
							{
								// move to next bucket
								self.bucketIndex += 1;
								self.iter = None;
							},
						}
					}
					None =>
					{
						let hashSet = unsafe { (self.hashSet as *mut HashSet<V, H>).as_mut ()? };
						self.iter = Some(hashSet.buckets[self.bucketIndex].iter_mut ());
					},
				}
			}
			else { return None; }
		}
	}
}


#[derive(Debug)]
pub struct HashSetIntoIter<V: Eq + Hash, H: Hasher + core::hash::Hasher>
{
	bucketIndex: usize,
	hashSet: HashSet<V, H>,
}

impl<V: Eq + Hash, H: Hasher + core::hash::Hasher> HashSetIntoIter<V, H>
{
	pub fn New (hashSet: HashSet<V, H>) -> Self
	{
		Self
		{
			bucketIndex: 0,
			hashSet,
		}
	}
}


impl<V: Eq + Hash, H: Hasher + core::hash::Hasher> Iterator for HashSetIntoIter<V, H>
{
	type Item = V;

	fn next (&mut self) -> Option<Self::Item>
	{
		loop
		{
			if self.bucketIndex < self.hashSet.buckets.len ()
			{
				match self.hashSet.buckets[self.bucketIndex].pop_front ()
				{
					Some(node) => return Some(node.value),
					None => self.bucketIndex += 1,
				}
			}
			else { return None; }
		}
	}
}


#[derive(Debug)]
pub struct DefaultHashSet<V: Hash + Eq>
{
	hashSetInner: HashSet<V, FNV1A64_Hasher>,
}

impl<V: Hash + Eq> DefaultHashSet<V>
{
	pub const fn New () -> Self
	{
		Self
		{
			hashSetInner: HashSet::New (),
		}
	}

	pub fn Size (&self) -> usize { self.hashSetInner.Size () }
	pub fn Contains (&self, value: &V) -> bool { self.hashSetInner.Contains (value) }
	pub fn Insert (&mut self, value: V) -> Result<(), HashSetError> { self.hashSetInner.Insert (value) }
	pub fn Remove (&mut self, value: &V) -> Result<(), HashSetError> { self.hashSetInner.Remove (value) }

	pub fn Iter (&self) -> HashSetIter<V, FNV1A64_Hasher>
	{
		HashSetIter::<V, FNV1A64_Hasher>::New (&self.hashSetInner)
	}

	pub fn IterMut (&mut self) -> HashSetIterMut<V, FNV1A64_Hasher>
	{
		HashSetIterMut::<V, FNV1A64_Hasher>::New (&mut self.hashSetInner)
	}

	pub fn IntoIter (self) -> HashSetIntoIter<V, FNV1A64_Hasher>
	{
		HashSetIntoIter::<V, FNV1A64_Hasher>::New (self.hashSetInner)
	}
}


impl<'a, V: Eq + Hash> IntoIterator for &'a DefaultHashSet<V>
{
	type Item = &'a V;
	type IntoIter = HashSetIter<'a, V, FNV1A64_Hasher>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (&self.hashSetInner) }
}

impl<'a, V: Eq + Hash> IntoIterator for &'a mut DefaultHashSet<V>
{
	type Item = &'a mut V;
	type IntoIter = HashSetIterMut<'a, V, FNV1A64_Hasher>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (&mut self.hashSetInner) }
}

impl<V: Eq + Hash> IntoIterator for DefaultHashSet<V>
{
	type Item = V;
	type IntoIter = HashSetIntoIter<V, FNV1A64_Hasher>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self.hashSetInner) }
}


#[derive(Debug)]
struct HashMapNode<K, V>
{
	keyHash: u64,
	key: K,
	value: V,
}

impl<K, V> HashMapNode<K, V>
{
	pub fn New (keyHash: u64, key: K, value: V) -> Self
	{
		Self
		{
			keyHash,
			key,
			value,
		}
	}

	pub const fn KeyHash (&self) -> u64 { self.keyHash }

	pub fn GetKeyRef (&self) -> &K { &self.key }
	pub fn GetKeyMutRef (&mut self) -> &mut K { &mut self.key }

	pub fn GetValueRef (&self) -> &V { &self.value }
	pub fn GetValueMutRef (&mut self) -> &mut V { &mut self.value }
}


#[derive(Clone, Copy, Debug)]
pub struct HashMapConfiguration<H: Hasher + core::hash::Hasher>
{
	hashSalt: u64,
	loadFactor: f64,
	bucketSize: usize,
	firstInsertBucketCapacity: usize,
	minimumBucketCapacity: usize,
	maximumBucketCapacity: Option<usize>,
	bucketCapacityGrowStep: usize,
	hasherBuilder: HasherBuilder<H>,
}

impl<H: Hasher + core::hash::Hasher> HashMapConfiguration<H>
{
	const fn Default () -> Self
	{
		Self
		{
			hashSalt: 0,
			loadFactor: 0.75,
			bucketSize: 16,
			firstInsertBucketCapacity: 32,
			minimumBucketCapacity: 16,
			maximumBucketCapacity: None,
			bucketCapacityGrowStep: 32,
			hasherBuilder: HasherBuilder::<H>::New (),
		}
	}

	pub const fn HashSalt (&self) -> u64 { self.hashSalt }
	pub const fn LoadFactor (&self) -> f64 { self.loadFactor }
	pub const fn BucketSize (&self) -> usize { self.bucketSize }
	pub const fn MinimumBucketCapacity (&self) -> usize { self.minimumBucketCapacity }
	pub const fn MaximumBucketCapacity (&self) -> Option<usize> { self.maximumBucketCapacity }
	pub const fn BucketCapacityGrowStep (&self) -> usize { self.bucketCapacityGrowStep }
}


#[derive(Debug)]
pub struct HashMap<K: Hash + Eq, V, H: Hasher + core::hash::Hasher>
{
	hashMapConfiguration: HashMapConfiguration<H>,
	buckets: Vec<LinkedList<HashMapNode<K, V>>>,
	usedBucketCount: usize,
	nodeCount: usize,
}

impl<K: Hash + Eq, V, H: Hasher + core::hash::Hasher> HashMap<K, V, H>
{
	pub const fn New () -> Self
	{
		Self
		{
			hashMapConfiguration: HashMapConfiguration::<H>::Default (),
			buckets: Vec::new (),
			usedBucketCount: 0,
			nodeCount: 0,
		}
	}

	pub const fn NewWithConfiguration (hashMapConfiguration: HashMapConfiguration<H>) -> Self
	{
		Self
		{
			hashMapConfiguration,
			buckets: Vec::new (),
			usedBucketCount: 0,
			nodeCount: 0,
		}
	}

	const fn HashSalt (&self) -> u64 { self.hashMapConfiguration.HashSalt () }
	fn BucketCapacity (&self) -> usize { self.buckets.len () }
	const fn UsedBucketCount (&self) -> usize { self.usedBucketCount }
	pub const fn Size (&self) -> usize { self.nodeCount }

	fn NewHasher (&self) -> H { self.hashMapConfiguration.hasherBuilder.NewHasher () }

	fn KeyHash (&self, key: &K) -> u64
	{
		let mut keyHasher = self.NewHasher ();
		self.HashSalt ().hash (&mut keyHasher);
		key.hash (&mut keyHasher);
		keyHasher.finish ()
	}

	fn BucketIndex (&self, keyHash: u64) -> usize { keyHash as usize % self.BucketCapacity () }

	pub fn GetRef (&self, key: &K) -> Option<&V>
	{
		let keyHash = self.KeyHash (&key);
		let bucketIndex = self.BucketIndex (keyHash);

		for node in &self.buckets[bucketIndex]
		{
			if node.KeyHash () == keyHash && node.GetKeyRef () == key
			{
				return Some(node.GetValueRef ());
			}
		}

		None
	}

	pub fn GetMutRef (&mut self, key: &K) -> Option<&mut V>
	{
		let keyHash = self.KeyHash (&key);
		let bucketIndex = self.BucketIndex (keyHash);

		for node in &mut self.buckets[bucketIndex]
		{
			if node.KeyHash () == keyHash && node.GetKeyRef () == key
			{
				return Some(node.GetValueMutRef ());
			}
		}

		None
	}

	pub fn Contains (&self, key: &K) -> bool
	{
		let keyHash = self.KeyHash (&key);
		let bucketIndex = self.BucketIndex (keyHash);

		for node in &self.buckets[bucketIndex]
		{
			if node.KeyHash () == keyHash && node.GetKeyRef () == key
			{
				return true;
			}
		}

		false
	}

	fn InsertNode (&mut self, bucketIndex: usize, newNode: HashMapNode<K, V>)
	{
		self.buckets[bucketIndex].push_front (newNode);
		self.nodeCount += 1;

		if self.buckets[bucketIndex].len () == 1
		{
			self.usedBucketCount += 1;
		}
	}

	fn RemoveNode (&mut self, bucketIndex: usize, nodeIndex: usize) -> HashMapNode<K, V>
	{
		let oldNode = self.buckets[bucketIndex].remove (nodeIndex);
		self.nodeCount -= 1;

		if self.buckets[bucketIndex].len () == 0
		{
			self.usedBucketCount -= 1;
		}

		oldNode
	}

	fn ResizeBucket (&mut self, newBucketCapacity: usize)
	{
		// create new bucket
		let newBuckets: Vec<LinkedList<HashMapNode<K, V>>> = Vec::with_capacity (newBucketCapacity);
		self.usedBucketCount = 0;

		// swap the bucket
		let oldBuckets = mem::replace (&mut self.buckets, newBuckets);

		// move all old elements to new bucket
		for oldBucket in oldBuckets
		{
			for oldNode in oldBucket
			{
				// insert old node into new bucket
				let newBucketIndex = oldNode.KeyHash () as usize % newBucketCapacity;
				self.InsertNode (newBucketIndex, oldNode);
			}
		}
	}

	fn EnsureBucketCapacity (&mut self, headroom: usize)
	{
		// check if first insert
		if self.BucketCapacity () == 0
		{
			self.ResizeBucket (self.hashMapConfiguration.firstInsertBucketCapacity);
		}

		// check if the load factor is satisfied
		if self.Size () >= (self.hashMapConfiguration.BucketSize () as f64 * self.BucketCapacity () as f64 * self.hashMapConfiguration.LoadFactor ()) as usize
		{
			// calc new bucket size
			let newBucketCapacity = core::cmp::max (self.BucketCapacity () + self.hashMapConfiguration.BucketCapacityGrowStep (), self.hashMapConfiguration.MaximumBucketCapacity ().unwrap_or (1));

			// resize bucket
			self.ResizeBucket (newBucketCapacity);
		}
	}

	pub fn Insert (&mut self, key: K, value: V) -> Option<V>
	{
		// grow the bucket if necessary
		self.EnsureBucketCapacity (1);

		let keyHash = self.KeyHash (&key);
		let bucketIndex = self.BucketIndex (keyHash);

		// travel the list to compare all hash
		for node in &mut self.buckets[bucketIndex]
		{
			if node.KeyHash () == keyHash && *node.GetKeyRef () == key
			{
				return Some(mem::replace (&mut node.value, value));
			}
		}

		// insert new node
		let newNode = HashMapNode::New (keyHash, key, value);
		self.InsertNode (bucketIndex, newNode);

		return None;
	}

	pub fn Remove (&mut self, key: &K) -> Option<V>
	{
		let keyHash = self.KeyHash (&key);
		let bucketIndex = self.BucketIndex (keyHash);

		// travel the list to compare all hash
		for (nodeIndex, node) in self.buckets[bucketIndex].iter ().enumerate ()
		{
			if node.KeyHash () == keyHash && node.GetKeyRef () == key
			{
				// remove old node
				let oldNode = self.RemoveNode (bucketIndex, nodeIndex);

				// shirk the bucket if necessary
				self.EnsureBucketCapacity (0);

				return Some(oldNode.value);
			}
		}

		return None;
	}

	pub fn Iter (&self) -> HashMapIter<K, V, H> { HashMapIter::<K, V, H>::New (self) }
	pub fn IterMut (&mut self) -> HashMapIterMut<K, V, H> { HashMapIterMut::<K, V, H>::New (self) }
	pub fn IntoIter (self) -> HashMapIntoIter<K, V, H> { HashMapIntoIter::<K, V, H>::New (self) }
}


impl<'a, K: Hash + Eq, V, H: Hasher + core::hash::Hasher> IntoIterator for &'a HashMap<K, V, H>
{
	type Item = (&'a K, &'a V);
	type IntoIter = HashMapIter<'a, K, V, H>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}

impl<'a, K: Hash + Eq, V, H: Hasher + core::hash::Hasher> IntoIterator for &'a mut HashMap<K, V, H>
{
	type Item = (&'a mut K, &'a mut V);
	type IntoIter = HashMapIterMut<'a, K, V, H>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}

impl<K: Hash + Eq, V, H: Hasher + core::hash::Hasher> IntoIterator for HashMap<K, V, H>
{
	type Item = (K, V);
	type IntoIter = HashMapIntoIter<K, V, H>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self) }
}


#[derive(Debug)]
pub struct HashMapIter<'a, K: Eq + Hash, V, H: Hasher + core::hash::Hasher>
{
	bucketIndex: usize,
	iter: Option<Iter<'a, HashMapNode<K, V>>>,
	hashMap: &'a HashMap<K, V, H>,
}

impl<'a, K: Eq + Hash, V, H: Hasher + core::hash::Hasher> HashMapIter<'a, K, V, H>
{
	pub fn New (hashMap: &'a HashMap<K, V, H>) -> Self
	{
		Self
		{
			bucketIndex: 0,
			iter: None,
			hashMap,
		}
	}
}


impl<'a, K: 'a + Eq + Hash, V: 'a, H: Hasher + core::hash::Hasher> Iterator for HashMapIter<'a, K, V, H>
{
	type Item = (&'a K, &'a V);

	fn next (&mut self) -> Option<Self::Item>
	{
		loop
		{
			if self.bucketIndex < self.hashMap.buckets.len ()
			{
				match &mut self.iter
				{
					Some(iter) =>
					{
						match iter.next ()
						{
							Some(node) => return Some((&node.key, &node.value)),
							None =>
							{
								// move to next bucket
								self.bucketIndex += 1;
								self.iter = None;
							},
						}
					}
					None => self.iter = Some(self.hashMap.buckets[self.bucketIndex].iter ()),
				}
			}
			else { return None; }
		}
	}
}


#[derive(Debug)]
pub struct HashMapIterMut<'a, K: Eq + Hash, V, H: Hasher + core::hash::Hasher>
{
	bucketIndex: usize,
	iter: Option<IterMut<'a, HashMapNode<K, V>>>,
	hashMap: &'a mut HashMap<K, V, H>,
}

impl<'a, K: Eq + Hash, V, H: Hasher + core::hash::Hasher> HashMapIterMut<'a, K, V, H>
{
	pub fn New (hashMap: &'a mut HashMap<K, V, H>) -> Self
	{
		Self
		{
			bucketIndex: 0,
			iter: None,
			hashMap,
		}
	}
}


impl<'a, K: 'a + Eq + Hash, V: 'a, H: Hasher + core::hash::Hasher> Iterator for HashMapIterMut<'a, K, V, H>
{
	type Item = (&'a mut K, &'a mut V);

	fn next (&mut self) -> Option<Self::Item>
	{
		let bucketCount = self.hashMap.buckets.len ();

		loop
		{
			if self.bucketIndex < bucketCount
			{
				match &mut self.iter
				{
					Some(iter) =>
					{
						match iter.next ()
						{
							Some(node) => return Some((&mut node.key, &mut node.value)),
							None =>
							{
								// move to next bucket
								self.bucketIndex += 1;
								self.iter = None;
							},
						}
					},
					None =>
					{
						let hashMap = unsafe { (self.hashMap as *mut HashMap<K, V, H>).as_mut ()? };
						self.iter = Some(hashMap.buckets[self.bucketIndex].iter_mut ());
					}
				}
			}
			else { return None; }
		}
	}
}


#[derive(Debug)]
pub struct HashMapIntoIter<K: Eq + Hash, V, H: Hasher + core::hash::Hasher>
{
	bucketIndex: usize,
	hashMap: HashMap<K, V, H>,
}

impl<K: Eq + Hash, V, H: Hasher + core::hash::Hasher> HashMapIntoIter<K, V, H>
{
	pub fn New (hashMap: HashMap<K, V, H>) -> Self
	{
		Self
		{
			bucketIndex: 0,
			hashMap,
		}
	}
}


impl<K: Eq + Hash, V, H: Hasher + core::hash::Hasher> Iterator for HashMapIntoIter<K, V, H>
{
	type Item = (K, V);

	fn next (&mut self) -> Option<Self::Item>
	{
		loop
		{
			if self.bucketIndex < self.hashMap.buckets.len ()
			{
				match self.hashMap.buckets[self.bucketIndex].pop_front ()
				{
					Some(node) => return Some((node.key, node.value)),
					None => self.bucketIndex += 1,
				}
			}
			else { return None; }
		}
	}
}


#[derive(Debug)]
pub struct DefaultHashMap<K: Hash + Eq, V>
{
	hashMapInner: HashMap<K, V, FNV1A64_Hasher>,
}

impl<K: Hash + Eq, V> DefaultHashMap<K, V>
{
	pub const fn New () -> Self
	{
		Self
		{
			hashMapInner: HashMap::New (),
		}
	}

	pub fn Size (&self) -> usize { self.hashMapInner.Size () }
	pub fn GetRef (&self, key: &K) -> Option<&V> { self.hashMapInner.GetRef (key) }
	pub fn GetMutRef (&mut self, key: &K) -> Option<&mut V> { self.hashMapInner.GetMutRef (key) }
	pub fn Contains (&self, key: &K) -> bool { self.hashMapInner.Contains (key) }
	pub fn Insert (&mut self, key: K, value: V) -> Option<V> { self.hashMapInner.Insert (key, value) }
	pub fn Remove (&mut self, key: &K) -> Option<V> { self.hashMapInner.Remove (key) }

	pub fn Iter (&self) -> HashMapIter<K, V, FNV1A64_Hasher>
	{
		HashMapIter::<K, V, FNV1A64_Hasher>::New (&self.hashMapInner)
	}

	pub fn IterMut (&mut self) -> HashMapIterMut<K, V, FNV1A64_Hasher>
	{
		HashMapIterMut::<K, V, FNV1A64_Hasher>::New (&mut self.hashMapInner)
	}

	pub fn IntoIter (self) -> HashMapIntoIter<K, V, FNV1A64_Hasher>
	{
		HashMapIntoIter::<K, V, FNV1A64_Hasher>::New (self.hashMapInner)
	}
}


impl<'a, K: Hash + Eq, V> IntoIterator for &'a DefaultHashMap<K, V>
{
	type Item = (&'a K, &'a V);
	type IntoIter = HashMapIter<'a, K, V, FNV1A64_Hasher>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (&self.hashMapInner) }
}

impl<'a, K: Hash + Eq, V> IntoIterator for &'a mut DefaultHashMap<K, V>
{
	type Item = (&'a mut K, &'a mut V);
	type IntoIter = HashMapIterMut<'a, K, V, FNV1A64_Hasher>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (&mut self.hashMapInner) }
}

impl<K: Hash + Eq, V> IntoIterator for DefaultHashMap<K, V>
{
	type Item = (K, V);
	type IntoIter = HashMapIntoIter<K, V, FNV1A64_Hasher>;

	fn into_iter (self) -> Self::IntoIter { Self::IntoIter::New (self.hashMapInner) }
}
