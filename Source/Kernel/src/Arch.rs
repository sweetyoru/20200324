//! This module export architecture dependent code depend on config

#[cfg(architecture = "Amd64")]
pub mod Amd64;

#[cfg(architecture = "Arm")]
pub mod Arm;