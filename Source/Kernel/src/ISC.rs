//! This module handler Inter-system Communication


use core::fmt;
use core::cell::SyncUnsafeCell;

use crate::CPU::CPU;
use crate::println;
use crate::Lock::OneTimeFlag;


#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct SystemID(u128);

impl SystemID
{
	pub const fn New (systemID: u128) -> Self
	{
		Self(systemID)
	}
}

impl fmt::Display for SystemID
{
	fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		write! (f, "0x{:032x}", self.0)
	}
}


static CURRENT_SYSTEM_ID: SyncUnsafeCell<Option<SystemID>> = SyncUnsafeCell::new (None);


pub fn GetCurrentSystemID () -> SystemID
{
	unsafe { (*CURRENT_SYSTEM_ID.get ()).unwrap () }
}


/// This function must be called only once by boot CPU at boot time
pub fn InitISC ()
{
	println! ("[Boot CPU][+] Init ISC...");

	static called: SyncUnsafeCell<OneTimeFlag> = SyncUnsafeCell::new (OneTimeFlag::New ());
	unsafe { (*called.get ()).Set (); }

	let systemID: u128 = CPU::GetRandomNumberU128 ();

	println! ("[Boot CPU][+] SystemID: {:X}", systemID);

	unsafe
	{
		*CURRENT_SYSTEM_ID.get () = Some (SystemID::New (systemID));
	}
}
