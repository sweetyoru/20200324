use core::panic::PanicInfo;

use crate::Debug;


#[panic_handler]
pub fn MyPacnicHandler (panicInfo: &PanicInfo) -> !
{
	if let Some(location) = panicInfo.location ()
	{
		if let Some(message) = panicInfo.message ()
		{
			Debug::_printNoLock (format_args! ("[PANIC] {}@{}: {}\n", location.file (), location.line (), message));
		}
		else
		{
			Debug::_printNoLock (format_args! ("[PANIC] {}@{} but cannot get message\n", location.file (), location.line ()));
		}
	}
	else { Debug::_printNoLock (format_args! ("[PANIC] Panic occurred but can't get location infomation...\n")); }

	Debug::_printNoLock (format_args! ("[PANIC] Enter infinity loop...\n"));

	loop {}
}
