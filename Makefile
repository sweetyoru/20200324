export ROOT:=$(shell pwd)
export OUTPUT_DIR :=$(shell pwd)/Output
export NASM:=$(shell pwd)/Toolchains/nasm


.PHONY: all
all: release


.PHONY: debug
debug: bootloader kernel_debug initrd disk source_doc design_doc


.PHONY: release
release: bootloader kernel_release initrd disk source_doc design_doc


.PHONY: source_doc
source_doc:
	rm -rf '$(OUTPUT_DIR)/Document/SourceDocument'
	'$(MAKE)' -C '$(ROOT)/Source/Kernel' source_doc


.PHONY: bootloader
bootloader:
	'$(MAKE)' -C '$(ROOT)/Source/Bootloader'


.PHONY: kernel_debug
kernel_debug:
	'$(MAKE)' -C '$(ROOT)/Source/Kernel' debug


.PHONY: kernel_release
kernel_release:
	'$(MAKE)' -C '$(ROOT)/Source/Kernel' release


.PHONY: make_initrd_image
make_initrd_image:
	'$(MAKE)' -C '$(ROOT)/Toolchains/make_initrd_image'


.PHONY: initrd
initrd: make_initrd_image
	mkdir -p '$(OUTPUT_DIR)/Images'
	'$(ROOT)/Toolchains/make_initrd_image/make_initrd_image' '$(OUTPUT_DIR)/Bin/kernel.elf' '$(OUTPUT_DIR)/Images/initrd.img'


.PHONY: make_disk_image
make_disk_image:
	'$(MAKE)' -C '$(ROOT)/Toolchains/make_disk_image'


.PHONY: disk
disk: bootloader initrd make_disk_image
	mkdir -p '$(OUTPUT_DIR)/Images'
	'$(ROOT)/Toolchains/make_disk_image/make_disk_image' '$(OUTPUT_DIR)/Bin/bootloader.bin' '$(OUTPUT_DIR)/Images/initrd.img' '$(OUTPUT_DIR)/Images/disk.img' 16777216


.PHONY: bochs
bochs: debug disk
	@rm -rf '$(OUTPUT_DIR)/Images/disk.img.lock' 2>&1 >/dev/null | true
	bochs -q -f '$(ROOT)/Toolchains/bochsrc.txt'


# run debug version
.PHONY: qemu_normal_debug
qemu_normal_debug: debug disk
	qemu-system-x86_64 -drive file='$(OUTPUT_DIR)/Images/disk.img',format=raw -serial stdio -m 512M --enable-kvm -cpu host -smp 8


# run release version
.PHONY: qemu_normal_release
qemu_normal_release: release disk
	qemu-system-x86_64 -drive file='$(OUTPUT_DIR)/Images/disk.img',format=raw -serial stdio -m 512M --enable-kvm -cpu host -smp 8


# debug debug version
.PHONY: qemu_debug_debug
qemu_debug_debug: debug disk
	qemu-system-x86_64 -gdb tcp::1234 -S -drive file='$(OUTPUT_DIR)/Images/disk.img',format=raw -serial stdio -m 512M --enable-kvm -cpu host -smp 8 -D ./qemu_log -d cpu_reset


# debug release version
.PHONY: qemu_debug_release
qemu_debug_release: release disk
	qemu-system-x86_64 -gdb tcp::1234 -S -drive file='$(OUTPUT_DIR)/Images/disk.img',format=raw -serial stdio -m 512M --enable-kvm -cpu host -smp 8 -D ./qemu_log -d cpu_reset


.PHONY: design_doc
design_doc:
	'$(MAKE)' -C '$(ROOT)/Document/KernelDesign'


.PHONY: clean
clean:
	'$(MAKE)' -C '$(ROOT)/Source/Bootloader' clean
	'$(MAKE)' -C '$(ROOT)/Source/Kernel' clean
	'$(MAKE)' -C '$(ROOT)/Toolchains/make_initrd_image' clean
	'$(MAKE)' -C '$(ROOT)/Toolchains/make_disk_image' clean
	rm -rf '$(OUTPUT_DIR)' 2>&1 >/dev/null | true
