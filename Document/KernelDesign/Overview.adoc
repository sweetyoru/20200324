== Overview

This is an operating system with following core design:

- Microkernel.
- Async message passing as THE inter-process communication.
- Async/bulk system call.
- Capability to define what process can do.


=== Core design

Following microkernel design principles, the kernel only expost a litmited set of services to userspace:

- Process management.
- Inter-process communication.
- Memory management.
- Hardware management.
- Capability management.


=== Requirements

==== Amd64

- CPUID >= 0xb && cpuid.0x0b-0x00:ebx != 0
- Long mode
- NX bit
- Global page bit
- Support 1GB page
- Have x2apic, apic
- RDTSC instruction
- Always run lapic
- HPET timer

*Basically any cpu within the last 5 years will do*


==== Arm

Not written yet.


=== Limitations

- Bootloader size 64KB.
- Initrd size is multify of 4KB.
- Initrd is loaded completely below 4GB.
- Initrd size limit is 1GB.
- Loaded kernel size in memory <= 1GB.

- Initrd size limit exist because realmode only have access to 4gb ram, due to bootloader limit, can overcome with better bootloader, this is just temporary limit.


=== Bootloader virtual memory map

```
----+-------------------------+
0   |                         |
    |        User space       |
255 |                         |
----+-------------------------+
256 |                         |
    |          Kernel         |
486 |                         |
----+-------------------------+
487 |                         |
    |           Gap           |
493 |                         |
----+-------------------------+
494 |      Kernel Stack       |
----+-------------------------+
502 |           Gap           |
----+-------------------------+
503 |                         |
    |    Physical Mapping     |
510 |                         |
----+-------------------------+
511 |    Recursive Mapping    |
----+-------------------------+
```


=== Boot process

==== Boot process virtual memory map

```
----+-------------------------+
0   |                         |
    |        User space       |
255 |                         |
----+-------------------------+
256 |                         |
    |                         |
    |   Kernel shared entry   |
    |       Kernel Heap       |
    |                         |
486 |                         |
----+-------------------------+
487 |                         |
    |  Kernel reserved entry  |
493 |                         |
----+-------------------------+
494 |      Kernel Stack       |
----+-------------------------+
495 |                         |
    |   Kernel private entry  |
502 |                         |
----+-------------------------+
503 |                         |
    |    Physical Mapping     |
510 |                         |
----+-------------------------+
511 |    Recursive Mapping    |
----+-------------------------+
```


==== Normal virtual memory map

Only physical mapping use huge page.

```
----+-------------------------+
0   |                         |
    |        User space       |
255 |                         |
----+-------------------------+
256 |                         |
    |                         |
    |   Kernel shared entry   |
    |       Kernel Heap       |
    |                         |
486 |                         |
----+-------------------------+
487 |                         |
    |  Kernel reserved entry  |
494 |                         |
----+-------------------------+
495 |                         |
    |   Kernel private entry  |
502 |                         |
----+-------------------------+
503 |                         |
    |    Physical Mapping     |
510 |                         |
----+-------------------------+
511 |    Recursive Mapping    |
----+-------------------------+
```