= 20200324 operating system kernel design document
Name here, <email here>
v0.4, 2021-04-09
:toc: right
:toclevels: 6
:sectnums:
:sectlinks:


include::Overview.adoc[]

include::Process.adoc[]

include::AddressSpace.adoc[]

include::InterProcessCommunication.adoc[]

include::SystemRequest.adoc[]

include::HardwareDevice.adoc[]

include::Capability.adoc[]
