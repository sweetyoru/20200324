== Process

Process is the basic building block of the system. Each process consist of:

- ProcessID: ProcessID is used to identify a process and is guaranteed to be unique. ProcessID have 2 parts:
	** SystemID (128 bits): describe what system this process belong to.
	** PrivateID (128 bits): which process in that system.

- ParentRef: a reference to this process's parent.
- ProcessType: the type of this process.
- Child list: An array of reference to children of this process.
- VirtualAddressSpace: the virtual address space of this process. Every process is guaranteed to have one and only one virtual address space.
- Thread list: a list of all threads that belong to this process.

Each process can have many childs or none at all, and each process only has 1 parent.


=== Thread

Thread is the basic executable block of system, the unit of schedule.
Each thread consist of:

- ThreadID: used to identify a thread and is guaranteed to be unique.
- Process reference: a referrent to process this thread belong to.

- Thread status:
	** Running: the thread is running and doing its work.
	** Ready: the thread is stopped because there is no available cpu, the thread can run at anytime if there is available cpu.
	** Blocking: the thread is blocking and can't run even if there is available cpu until the blocking reason is resolved.

- Main execution state: the first and main execution state of a thread.
- Aync execution states: a list of thread's async execution states.
- Current load execution state pointer: a pointer point to the execution state in the thread's execution state list that scheduler will load when next schedule event happen.
- Next load execution state pointer: a pointer point to the execution state in the thread's execution state list that will become current load execution state pointer when next schedule event happen.
- Current save execution state pointer: a pointer point to the execution state in the thread's execution state list that scheduler will save when next schedule event happen.
- Next save execution state pointer: a pointer point to the execution state in the thread's execution state list that will become current save execution state pointer when next schedule event happen.

When a thread is created, it begin to execute in main execution state.
Async execution state will be created as the thread receive signal from registed mailbox.
When the scheduler decided to execute another thread, the current execution state of current thread need to be saved and the execution state of next thread need to be loaded.
Each thread will have a list of execution state, and the execution state load/save will happen at the current load/save execution state pointer.
After each execution state load/save, the next load/save execution state pointer will become the current load/save execution state pointer.
Aync execution state will be proritied over main excution state.


==== Thread's execution state

Thread execution state is the place to save current thread's execution state.
Thread execution state consist of:

- Low level cpu execution state.
- Kernel stack.

Thread execution state is parallel execution stream inside thread just like thread is parallel execution stream inside process.
But unlike process, a thread can only have 1 and only 1 execution state running at any point in time.

All thread execution state share same userspace stack, but have different kernel stack.


==== Blocking

When a thread need to wait for an event and can't continue its execution, that thread need to be blocked and wait for that event. The event will be delivered by a message, so waiting for an event in fact is waiting for a message.

When a message arrive at a mailbox, kernel will check whether any thread is waiting for that message, if there is a thread waiting for that message, kernel will change that thread's status from blocking to ready.


=== Signal handling

Unlike other operating system, there is no separate singal handling system. Instead, signal handling is implemented through <<Async message handler, async async handler>>.
