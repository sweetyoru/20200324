#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


#define SECTOR_SIZE 512
#define INITRD_MAX_SIZE (1*1024*1024*1024)


void Panic (char *msg)
{
	puts (msg);
	exit (-1);
}

int main (int argc, char *argv[], char *env[])
{
	if (argc != 5)
		Panic ("Usage: ./make_disk_image [bootloader path] [initrd path] [disk name] [disk size]");

	printf ("[+] Bootloader path: %s\n", argv[1]);
	printf ("[+] Initrd path: %s\n", argv[2]);
	printf ("[+] Disk name: %s\n", argv[3]);
	printf ("[+] Disk size: %lld\n", atoll (argv[4]));

	FILE *in = NULL, *out = NULL;
	uint64_t bootloader_size = 0;
	uint32_t bootloader_size_in_sector = 0;
	uint64_t initrd_size = 0;
	uint32_t initrd_size_in_sector = 0;
	uint64_t disk_size = atoll (argv[4]);

	uint64_t nextPos = 0;
	int tmp = 0;

	// open bootloader file
	if ((in = fopen (argv[1], "rb")) == NULL)
		Panic ("Can't open bootloader file");

	// get bootloader file size
	if (fseek (in, 0, SEEK_END))
		Panic ("Error when seek bootloader file");

	bootloader_size = ftell (in);
	if (bootloader_size <= 512 || bootloader_size > 65536)
		Panic ("Bootloader size invalid");

	bootloader_size_in_sector = (bootloader_size - SECTOR_SIZE + SECTOR_SIZE - 1)/SECTOR_SIZE;

	// seek to start of bootloader file
	if (fseek (in, 0, SEEK_SET))
		Panic ("Error when seek bootloader file");

	// open disk file
	if ((out = fopen (argv[3], "wb")) == NULL)
		Panic ("Can't open disk file");

	// write bootloader to disk file
	while ((tmp = fgetc (in)) != EOF)
		if (fputc (tmp, out) == EOF)
			Panic ("Eror when writing bootloader file to disk file");

	// close bootoader file
	fclose (in);

	// seek to 503th byte of disk file
	if (fseek (out, 502, SEEK_SET))
		Panic ("Error when seek disk file");

	// write bootloader size in sector to disk
	if (fwrite (&bootloader_size_in_sector, 4, 1, out) != 1)
		Panic ("Error when writing bootloader size in sector to disk file");

	// open initrd file
	if ((in = fopen (argv[2], "rb")) == NULL)
		Panic ("Can't open initrd file");

	// get initrd size
	if (fseek (in, 0, SEEK_END))
		Panic ("Error when seek initrd file");

	initrd_size = ftell (in);
	if (initrd_size <= 512 || initrd_size >= INITRD_MAX_SIZE)
		Panic ("Initrd size invalid");

	initrd_size_in_sector = (initrd_size + SECTOR_SIZE - 1)/SECTOR_SIZE;

	// seek to start of initrd file
	if (fseek (in, 0, SEEK_SET))
		Panic ("Error when seek initrd file");

	// seek to 507th byte of disk file
	if (fseek (out, 506, SEEK_SET))
		Panic ("Error when seek disk file");

	// write initrd size in sector to disk
	if (fwrite (&initrd_size_in_sector, 4, 1, out) != 1)
		Panic ("Error when writing initrd size in sector to disk file");

	// seek to end of disk file
	if (fseek (out, 0, SEEK_END))
		Panic ("Error when seek disk file");

	// get next sector position
	nextPos = ((ftell (out) + SECTOR_SIZE - 1)/SECTOR_SIZE)*SECTOR_SIZE;

	// seek to next sector in disk size
	if (fseek (out, nextPos, SEEK_SET))
		Panic ("Error when seek initrd file");

	// write initrd file to disk
	while ((tmp = fgetc (in)) != EOF)
		if (fputc (tmp, out) == EOF)
			Panic ("Error when writing initrd to disk file");

	// close initrd file
	fclose (in);

	// padding disk file to disk size
	int paddingSize = disk_size - ftell (out);
	while (paddingSize--)
		if (fputc ('P', out) == EOF)
			Panic ("Error when writing to disk file");

	// close disk file
	fclose (out);

	return 0;
}
