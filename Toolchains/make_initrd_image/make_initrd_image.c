#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>




void Panic (char *msg)
{
	puts (msg);
	exit (-1);
}

int main (int argc, char *argv[], char *env[])
{
	if (argc != 3)
		Panic ("Usage: make_initrd_image [kernel path] [initrd path]");

	printf ("[+] Kernel path: %s\n", argv[1]);
	printf ("[+] Initrd path: %s\n", argv[2]);

	FILE *in = NULL, *out = NULL;
	uint64_t kernel_size = 0;

	uint64_t entryNum = 1;

	int64_t tmp = 0;
	int nextPos = 0;

	// open kernel file
	if ((in = fopen (argv[1], "rb")) == NULL)
		Panic ("Error when open kernel file");

	// open initrd file
	if ((out = fopen (argv[2], "wb")) == NULL)
		Panic ("Error when open initrd file");

	// get kernel size
	if (fseek (in, 0, SEEK_END))
		Panic ("Error when seek kernel file");

	kernel_size = ftell (in);

	if (kernel_size <= 512)
		Panic ("Kernel size is invalid");

	// seek to start of kernel file
	if (fseek (in, 0, SEEK_SET))
		Panic ("Error when seek kernel file");

	// write entry num to initrd file
	if (fwrite (&entryNum, 8, 1, out) != 1)
		Panic ("Error when writing to initrd file");

	// write kernel start offset to initrd file
	tmp = 4096;
	if (fwrite (&tmp, 8, 1, out) != 1)
		Panic ("Error when writing to initrd file");
	
	// write kernel size to initrd file
	if (fwrite (&kernel_size, 8, 1, out) != 1)
		Panic ("Error when writing to initrd file");

	// get next position initrd file
	nextPos = ((ftell (out) + 4096 - 1)/4096)*4096;

	// seek to next position in initrd file
	if (fseek (out, nextPos, SEEK_SET))
		Panic ("Error when seek initrd file");

	// write kernel to initrd file
	while ((tmp = fgetc (in)) != EOF)
		if (fputc (tmp, out) == EOF)
			Panic ("Error when writing initrd file");

	// close kernel file
	fclose (in);

	// padding initrd to 4KB
	int paddingSize = ((ftell (out) + 4096 - 1)/4096)*4096 - ftell (out);
	while (paddingSize--)
		if (fputc ('I', out) == EOF)
			Panic ("Error when writing initrd");

	// close initrd file
	fclose (out);

	return 0;
}
